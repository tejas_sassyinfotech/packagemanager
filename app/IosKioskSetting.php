<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IosKioskSetting extends Model
{
    protected $table = 'ios_kiosk_settings';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
