<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceRegistration extends Model
{
    protected $table = 'device_registrations';
}
