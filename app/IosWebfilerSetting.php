<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IosWebfilerSetting extends Model
{
    protected $table = 'ios_webfiler_settings';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
