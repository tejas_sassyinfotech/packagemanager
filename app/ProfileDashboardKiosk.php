<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileDashboardKiosk extends Model
{
    protected $table = 'profile_dashboard_kiosks';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
