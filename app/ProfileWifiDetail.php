<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileWifiDetail extends Model
{
    protected $table = 'profile_wifi_details';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
