<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class UserSurvey extends Authenticatable
{
    protected $table = 'user_surveys';

    public function surveyquestion()
    {
        return $this->hasMany('\App\SurveyQuestion', 'survey_id', 'id');
    }

    public function surveyquestionanswer()
    {
        return $this->hasMany('\App\SurveyQuestionAnswer', 'survey_id', 'id');
    }
}
