<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DisableApplication extends Model
{
    protected $table = 'disable_applications';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
