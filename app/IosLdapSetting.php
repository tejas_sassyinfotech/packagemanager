<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IosLdapSetting extends Model
{
    protected $table = 'ios_ldap_settings';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
