<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TabSetting extends Model
{
    protected $table = 'tab_settings';
}
