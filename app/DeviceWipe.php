<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceWipe extends Model
{
    protected $table = 'device_wipes';
}
