<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IosPasswordSetting extends Model
{
    protected $table = 'ios_password_settings';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
