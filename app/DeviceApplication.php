<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceApplication extends Model
{
    protected $table = 'device_applications';
}
