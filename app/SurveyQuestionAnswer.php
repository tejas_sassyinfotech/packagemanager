<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestionAnswer extends Model
{
    protected $table = 'survey_question_answers';

    public function UserSurvey()
    {
        return $this->hasOne('\App\UserSurvey', 'id', 'survey_id');
    }

    public function SurveyQuestion()
    {
        return $this->hasOne('\App\SurveyQuestion', 'id', 'survey_question_id');
    }

    public function SurveyQuestionOption()
    {
        return $this->hasOne('\App\SurveyQuestionOption', 'id', 'survey_question_option_id');
    }
}
