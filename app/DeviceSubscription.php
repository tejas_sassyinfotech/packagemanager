<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceSubscription extends Model
{
    protected $table = 'device_subscriptions';
}
