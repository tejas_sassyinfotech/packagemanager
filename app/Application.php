<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $table = 'applications';

    public function user()
    {
        return $this->hasOne('\App\User', 'id', 'created_by');
    }

    public function profileapplications()
    {
        return $this->hasMany('\App\Application', 'application_id', 'id');
    }
}
