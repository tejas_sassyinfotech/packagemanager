<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestion extends Model
{
    protected $table = 'survey_questions';

    public function usersurvey()
    {
        return $this->hasOne('\App\UserSurvey', 'id', 'survey_id');
    }

    public function surveyquestionoption()
    {
        return $this->hasMany('\App\SurveyQuestionOption', 'survey_question_id', 'id');
    }


}
