<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileActiveSyncSetting extends Model
{
    protected $table = 'profile_active_sync_settings';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
