<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IosProfileEmailSetting extends Model
{
    protected $table = 'ios_profile_email_settings';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
