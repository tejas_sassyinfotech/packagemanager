<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IosWebclipSetting extends Model
{
    protected $table = 'ios_webclip_settings';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
