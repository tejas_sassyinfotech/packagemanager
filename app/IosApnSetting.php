<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IosApnSetting extends Model
{
    protected $table = 'ios_apn_settings';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
