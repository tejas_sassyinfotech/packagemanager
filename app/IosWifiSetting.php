<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IosWifiSetting extends Model
{
    protected $table = 'ios_wifi_settings';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
