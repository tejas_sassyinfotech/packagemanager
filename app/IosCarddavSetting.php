<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IosCarddavSetting extends Model
{
    protected $table = 'ios_carddav_settings';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
