<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\UserProfile;

class ProfileAuthCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $profileData = UserProfile::where('id', $request->profile_id)->first();
        if ($profileData) {
            if($profileData->created_by == Auth::user()->id) {
                return $next($request);
            } else {
                return response()->json(['error' => 'invalid_profile_id'], 401);
            }
        } else {
            return response()->json(['error' => 'invalid_profile_id'], 401);
        }
    }
}
