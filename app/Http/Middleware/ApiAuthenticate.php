<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\ProfileDevice;

class ApiAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $deviceData = ProfileDevice::where('active_access_token', $request->token)->first();
        if ($deviceData) {
            return $next($request);
        } else {
            return response()->json(['error' => 'invalid_token'], 401);
        }
    }
}
