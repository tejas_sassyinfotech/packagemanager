<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\SurveyQuestion;
use App\SurveyQuestionAnswer;
use App\SurveyQuestionOption;
use App\UserSurvey;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\UserProfile;
use App\ProfileDevice;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function survey()
    {
        $Usersurvey = UserSurvey::where('created_by',Auth::getUser()->id)->get();
        $questions = SurveyQuestion::where('survey_id',$Usersurvey[0]['id'])->where('survey_question_status',1)
            ->with('SurveyQuestionOption')->get();
        $surveyId = $Usersurvey[0]['id'];
        return view('survey/surveyuserhome',compact('questions','surveyId'));
    }

    public function savesurvey(Request $request)
    {
        if(Auth::getUser()->role_id == 6){
            if($request['text']){
                foreach($request['text'] as $textkey=>$textval){
                    $id = SurveyQuestionAnswer::insertGetId([
                        'survey_id' => $request['survey_id'],
                        'survey_question_id' => $textkey,
                        'survey_question_answer_desc' => $textval,
                        'created_by' => Auth::getUser()->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                }
            }
            if($request['radio']['text']){
                foreach($request['radio']['text'] as $radiotextkey=>$radiotextvalue){
                    if($request['radio']['option'][$radiotextkey]){
                        $id = DB::table('survey_question_answers')->insertGetId(
                            [
                                'survey_id' => $request['survey_id'],
                                'survey_question_id' => $radiotextkey,
                                'survey_question_option_id' => $request['radio']['option'][$radiotextkey],
                                'survey_question_answer_desc' => $radiotextvalue,
                                'created_by' => Auth::getUser()->id,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s')
                            ]);
                    }
                }
            }
            if($id){
                return redirect('/survey');
            }else{
                return redirect('/survey');
            }
        }
    }

    /*public function usersurveylist()
    {
        $Usersurveys = SurveyQuestionAnswer::join('user_surveys', 'user_surveys.id', '=', 'survey_question_answers.survey_id')
        ->where('user_surveys.created_by',Auth::getUser()->id)
            ->get();
        print_r(array_unique($Usersurveys)); exit;
        foreach($Usersurveys as $Usersurvey){
            print_r($Usersurvey);
        }
         exit;
        return view('survey/usersurveylist',compact('surveys'));
    }*/
}
