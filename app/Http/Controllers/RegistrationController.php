<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ProfileDevice;

class RegistrationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('registration/registrations');
    }

    public function profileregistrationlist()
    {
        try{
            $response = [];
            $statusCode = 200;
            $response = ProfileDevice::join('device_registrations', 'profile_devices.id', '=', 'device_registrations.device_id')
                ->select('device_registrations.*', 'profile_devices.device_serial_number','profile_devices.device_identifier')
                ->where('profile_id', $_POST['profileId'])->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }
}
