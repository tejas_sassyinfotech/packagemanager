<?php

namespace App\Http\Controllers;

use App\User;
use App\UserProfile;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function AdminUserSubUsers(){
        $idArray=array();
        $subUsers = User::where('master_user',Auth::getUser()->id)->get();
        foreach($subUsers as $subUser){
            $idArray[] = $subUser['id'];
        }
        return $idArray;
    }

    public function AdminProfileIds(){
        $idArray=array();
        $profileIds = UserProfile::join('users', 'users.id', '=', 'user_profiles.created_by')
            ->where('profile_status', 1)
            ->where('users.master_user', Auth::getUser()->id)
            ->orWhere('user_profiles.created_by', Auth::getUser()->id)
            ->where('role_id', '>=', Auth::getUser()->role_id)
            ->select('user_profiles.id')
            ->get();
        foreach($profileIds as $profileId){
            $idArray[] = $profileId['id'];
        }
        return $idArray;
    }
}
