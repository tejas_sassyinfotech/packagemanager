<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\CredentialSetting;
use App\IosAdvancedRestrictionsSetting;
use App\IosApnSetting;
use App\IosCarddavSetting;
use App\IosKioskSetting;
use App\IosLdapSetting;
use App\IosPasswordSetting;
use App\IosProfileActiveSyncSetting;
use App\IosProfileEmailSetting;
use App\IosProfileSetting;
use App\IosVpnSetting;
use App\IosWebclipSetting;
use App\IosWebfilerSetting;
use App\IosWifiSetting;
use App\ProfileActiveSyncSetting;
use App\ProfileEmailConfigurationSetting;
use App\TabSetting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\UserProfile;
use App\ProfileDevice;
use App\ProfileWifiDetail;
use App\ProfileDashboardKiosk;
use App\ProfileApplication;
use App\Application;
use App\DisableApplication;
use App\ProfileDashboardKioskSetting;
use Mockery\CountValidator\Exception;

class ConfigurationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('configuration/configurations');
    }

    public function profile($id = null)
    {
        $tabNameArray = '';
        $tabIOSArray = '';
        $tabNameArray = TabSetting::where('profile_type', 1)->where('tab_status',1)->get();
        $tabIOSArray = TabSetting::where('profile_type', 2)->where('tab_status',1)->get();
        return view('configuration/profiles',compact('id','tabNameArray','tabIOSArray'));

    }

    public function profilecreate()
    {
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $userProfileExist = UserProfile::where('profile_name', $_POST['profile_name'])->get();
                if (!$userProfileExist->count()) {
                    $userProfileId = UserProfile::insertGetId([
                        'profile_name' => htmlentities($_POST['profile_name']),
                        'profile_status' => 1,
                        'profile_type' => $_POST['profile_type'],
                        'created_by' => Auth::user()->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                    $profile_id = $this->generateprofileid($userProfileId);
                    UserProfile::where('id', $userProfileId)->update(['profile_id' => $profile_id]);
                    array_push($response, [
                        'id' => $userProfileId,
                        'profile_id' => $profile_id,
                        'profile_type' => $_POST['profile_type']
                    ]);
                } else {
                    $response['profileExist'] = 'Profile name already exist';
                }
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function profileupdate()
    {
        try {
            $response = [];
            $statusCode = 200;
            UserProfile::where('id', $_POST['profile_id'])->update([
                'mdm_display_status' => $_POST['mdm_display_status'],
                'status_bar_status' => $_POST['status_bar_status'],
                'home_key_status' => $_POST['home_key_status'],
                'back_key_status' => $_POST['back_key_status'],
                'recents_key_status' => $_POST['recents_key_status'],
                'power_key_status' => $_POST['power_key_status'],
                'firmware_upgrade_status' => $_POST['firmware_upgrade_status'],
                'factory_reset_status' => $_POST['factory_reset_status'],
                's_voice_status' => $_POST['s_voice_status'],
                'android_settings_status' => $_POST['android_settings_status'],
                'volume_key_status' => $_POST['volume_key_status'],
                'multi_window_status' => $_POST['multi_window_status'],
                'notifications_status' => $_POST['notifications_status'],
                'lock_screen_status' => $_POST['lock_screen_status'],
                'bluetooth_status' => $_POST['bluetooth_status'],
                'wifi_status' => $_POST['wifi_status'],
                'gps_status' => $_POST['gps_status'],
                'developer_mode_menu_status' => $_POST['developer_mode_menu_status'],
                'backup_reset_menu_status' => $_POST['backup_reset_menu_status'],
                'airplane_mode_menu_status' => $_POST['airplane_mode_menu_status'],
                'language_menu_status' => $_POST['language_menu_status'],
                'multi_window_menu_status' => $_POST['multi_window_menu_status'],
                'users_settings_menu_status' => $_POST['users_settings_menu_status'],
                'bluetooth_settings_menu_status' => $_POST['bluetooth_settings_menu_status'],
                'lockscreen_menu_status' => $_POST['lockscreen_menu_status'],
                'wifi_settings_menu_status' => $_POST['wifi_settings_menu_status'],
                'power_on_boot_status' => $_POST['power_on_boot_status'],
                'disable_all_bloatware_status' => $_POST['disable_all_bloatware_status'],
                'unknown_source_status' => $_POST['unknown_source_status'],
                'developer_mode_status' => $_POST['developer_mode_status'],
                'screenshot_status' => $_POST['screenshot_status'],
                'usb_debugging_status' => $_POST['usb_debugging_status'],
                'lte_mode_status' => $_POST['lte_mode_status'],
                'device_timeout' => $_POST['device_timeout'],
                'pull_interval' => $_POST['pull_interval'],
                'brightness_percentage' => $_POST['brightness_percentage'],
                'volume_percentage' => $_POST['volume_percentage'],
                'sleep_percentage' => $_POST['sleep_percentage'],
                'deep_sleep_percentage' => $_POST['deep_sleep_percentage'],
                'orientation' => $_POST['orientation'],
                'global_proxy' => $_POST['global_proxy'],
                'global_port' => $_POST['global_port'],
                'kiosk_type' => $_POST['kiosk_type'],
                'kiosk_password' => htmlentities($_POST['kiosk_password']),
                'single_app_kiosk' => htmlentities($_POST['single_app_kiosk']),
                'browser_kiosk' => htmlentities($_POST['browser_kiosk']),
                'registration_password' => htmlentities($_POST['registration_password']),
                'advanced_command' => htmlentities($_POST['advanced_command']),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);

    }

    public function profileget()
    {
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id == 3) {
                $response = UserProfile::where('created_by', Auth::getUser()->id)->where('profile_id', $_POST['profileId'])->get();
            } elseif (Auth::getUser()->role_id == 2) {
                $response = UserProfile::where('created_by', Auth::user()->id)->where('profile_id', $_POST['profileId'])->get();
            } elseif (Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                $response = UserProfile::where('profile_id', $_POST['profileId'])->get();
            } elseif(Auth::getUser()->role_id == 4){
                $response = UserProfile::where('created_by', Auth::getUser()->master_user)->where('profile_id', $_POST['profileId'])->get();
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function profilelist()
    {
        try {
            $response = [];
            $statusCode = 200;
            $controller = new Controller();
            if (Auth::getUser()->role_id == 3) {
                $response = UserProfile::where('created_by', Auth::getUser()->id)->where('profile_status', 1)->get();
            } elseif (Auth::getUser()->role_id == 2) {
                $idArray = $controller->AdminUserSubUsers();
                $response = UserProfile::join('users', 'users.id', '=', 'user_profiles.created_by')
                    ->where('profile_status', 1)
                    ->where('user_profiles.created_by', Auth::getUser()->id)
                    ->orwhereIn('user_profiles.created_by', array_values($idArray))
                    ->where('role_id', '>=', Auth::getUser()->role_id)
                    ->get(['users.id AS uid', 'user_profiles.*']);
            } elseif (Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                $response = UserProfile::where('profile_status', 1)->get();
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }


    public function profiledelete()
    {
        try {
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $profObj = UserProfile::find($_POST['profileId']);
                $response = $profObj->profile_name;
                UserProfile::where('id', $_POST['profileId'])->update(['profile_status' => 0]);
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function generateprofileid($id)
    {
        return 'OSP' . $id . 'UP' . date('y');
    }

    public function devicecreate()
    {
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $profileDeviceExist = ProfileDevice::where('device_serial_number', trim($_POST['device_serial_number']))->get();
                if ($profileDeviceExist->count()) {
                    ProfileDevice::where('device_serial_number', $_POST['device_serial_number'])->update([
                        'profile_id' => $_POST['profile_id'],
                        'device_identifier' => (!empty($_POST['device_identifier'])) ? htmlentities($_POST['device_identifier']) :'',
                        'device_additional_info' => (!empty($_POST['device_additional_info'])) ? htmlentities(trim($_POST['device_additional_info'])) : '',
                        'device_user_name' => (!empty($_POST['device_user_name'])) ? htmlentities($_POST['device_user_name']) : trim($_POST['device_serial_number']),
                        'device_password' => ((!empty($_POST['device_password']))) ? md5($_POST['device_password']) : md5('qwerty'),
                        'device_status' => 1,
                        'updated_at' => date('Y-m-d H:i:s'),
                        'device_created_by' => Auth::getUser()->id
                    ]);
                } else {
                    $response = ProfileDevice::insertGetId([
                        'profile_id' => $_POST['profile_id'],
                        'device_serial_number' =>(!empty($_POST['device_serial_number'])) ?  htmlentities(trim($_POST['device_serial_number'])) : '',
                        'device_additional_info' => (!empty($_POST['device_additional_info'])) ? htmlentities(trim($_POST['device_additional_info'])) : '',
                        'device_identifier' => (!empty($_POST['device_identifier'])) ? htmlentities($_POST['device_identifier']) :'',
                        'device_user_name' =>  (!empty($_POST['device_user_name'])) ? htmlentities($_POST['device_user_name']) : htmlentities(trim($_POST['device_serial_number'])),
                        'device_password' => md5('qwerty'),
                        'device_gcm_token' => 'ezLWJEAzIG4:APA91bGMBQklgqo_XWv0CMEeiK9BwyfKVPVZgZlTZVGGNIg0XAmiK31tuWuwvx3Cr9NbsUW-F7Xf8-1KEaV-nyFnfXtKRNdFmfjDn1DRdGi_SMp_hCelEx6mEJDOgjKFrU8IoqVHSTT4',
                        'device_status' => 1,
                        'provision_status' => 'unprovisioned',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        'device_created_by' => Auth::getUser()->id
                    ]);
                }
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }

        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function deviceget()
    {
        try {
            $response = [];
            $statusCode = 200;
            $profiles = ProfileDevice::where('profile_id', $_POST['profileId'])->where('device_status', 1)->with('UserProfile')->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($profiles, $statusCode);
    }

    public function devicedelete()
    {
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                ProfileDevice::where('id', $_POST['deviceId'])->update(['device_status' => 0]);
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function profilewificreate()
    {
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $wifiPasswordPurified = htmlentities($_POST['wifi_user_password']);
                $wifiPassword = $wifiPasswordPurified;
                //$wifiPassword = $this->encrypt_decrypt('encrypt', $wifiPasswordPurified);
                $profileWifiExist = ProfileWifiDetail::where('ssid', htmlentities(trim($_POST['ssid'])))
                    ->where('profile_id', $_POST['profile_id'])->get();
                if ($profileWifiExist->count()) {
                    ProfileWifiDetail::where('id', $profileWifiExist[0]->id)->update([
                        'wifi_user_name' => htmlentities($_POST['wifi_user_name']),
                        'wifi_user_password' => $wifiPassword,
                        'eap_method' => htmlentities($_POST['eap_method']),
                        'phase_2_authentication' => htmlentities($_POST['phase_2_authentication']),
                        'wifi_status' => 1,
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                    $response['save_success'] = 'Data saved successfully';
                } else {
                    ProfileWifiDetail::insert([
                        'profile_id' => $_POST['profile_id'],
                        'ssid' => htmlentities(trim($_POST['ssid'])),
                        'wifi_user_name' => htmlentities($_POST['wifi_user_name']),
                        'wifi_user_password' => $wifiPassword,
                        'eap_method' => htmlentities($_POST['eap_method']),
                        'phase_2_authentication' => htmlentities($_POST['phase_2_authentication']),
                        'wifi_status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                    $response['save_success'] = 'Data saved successfully';
                }
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }

        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function profilewifiget()
    {
        try {
            $response = [];
            $statusCode = 200;
            if($_POST['profile_type'] == 2){
                $response = IosWifiSetting::where('profile_id', $_POST['profileId'])->where('ios_wifi_settings_status', 1)->get();
            }else {
                $response = ProfileWifiDetail::where('profile_id', $_POST['profileId'])->where('wifi_status', 1)->with('UserProfile')->get();
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function profilewifidelete()
    {
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                if($_POST['profile_type'] == 2){
                    $response = IosWifiSetting::where('id', $_POST['wifiId'])->update(['ios_wifi_settings_status' => 0]);
                }else{
                    $response = ProfileWifiDetail::where('id', $_POST['wifiId'])->update(['wifi_status' => 0]);
                }
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function profilekioskcreate()
    {
        try {
            $response = [];
            $statusCode = 200;
            if ($_POST['kiosk_browser_priority']) {
                $maxPriority = $_POST['kiosk_browser_priority'];
            } else {
                $currentMaxPriority = ProfileDashboardKiosk::where('profile_id', $_POST['profile_id'])->orderBy('kiosk_browser_priority', 'desc')->get();
                if (sizeof($currentMaxPriority) > 0) {
                    $maxPriority = $currentMaxPriority[0]['kiosk_browser_priority'] + 1;
                } else {
                    $maxPriority = 1;
                }
            }
            $profileDashboardKioskExist = ProfileDashboardKiosk::where('kiosk_name', htmlentities($_POST['kiosk_name']))
                ->where('profile_id', $_POST['profile_id'])->get();
            if ($profileDashboardKioskExist->count()) {
                $response['data'] = ProfileDashboardKiosk::where('id', $profileDashboardKioskExist[0]->id)->update([
                    'kiosk_name' => htmlentities($_POST['kiosk_name']),
                    'kiosk_browser_type' => $_POST['kiosk_browser_type'],
                    'kiosk_browser_priority' => $maxPriority,
                    'kiosk_browser_row' => $_POST['kiosk_browser_row'],
                    'kiosk_browser_column' => $_POST['kiosk_browser_column'],
                    'dashboard_kiosk_status' => 1,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

            } else {
                $response['data'] = ProfileDashboardKiosk::insert([
                    'profile_id' => $_POST['profile_id'],
                    'kiosk_name' => htmlentities($_POST['kiosk_name']),
                    'kiosk_browser_type' => $_POST['kiosk_browser_type'],
                    'kiosk_browser_priority' => $maxPriority,
                    'kiosk_browser_row' => $_POST['kiosk_browser_row'],
                    'kiosk_browser_column' => $_POST['kiosk_browser_column'],
                    'dashboard_kiosk_status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
            if($response['data']){
                $response['save_success'] = 'Data saved successfully';
            }
            $profileDashboardKioskSettingsId = $this->saveProfileDashboardKioskSettings($_POST['profile_id'], $_POST['kiosk_screen_number'], $_POST['kiosk_special_screen_number'], $_POST['kiosk_special_screen_column_number']);
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function profilekiosksetting()
    {
        try {
            $response = [];
            $statusCode = 200;
            $profileDashboardKioskSettingsId = $this->saveProfileDashboardKioskSettings($_POST['profile_id'], $_POST['kiosk_screen_number'], $_POST['kiosk_special_screen_number'], $_POST['kiosk_special_screen_column_number']);
            if($profileDashboardKioskSettingsId){
                $response['save_success'] = 'Data saved successfully';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function saveProfileDashboardKioskSettings($profileId, $kioskScreenNumber, $kioskSpecialScreenNumber, $kioskSpecialScreenColumnNumber)
    {
        $profileDashboardKioskSettingsExist = ProfileDashboardKioskSetting::where('profile_id', $profileId)->get();
        if ($profileDashboardKioskSettingsExist->count()) {
            ProfileDashboardKioskSetting::where('id', $profileDashboardKioskSettingsExist[0]->id)->update([
                'kiosk_screen_number' => $kioskScreenNumber,
                'kiosk_special_screen_number' => $kioskSpecialScreenNumber,
                'kiosk_special_screen_column_number' => $kioskSpecialScreenColumnNumber,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            $profileDashboardKioskSettingsId = $profileDashboardKioskSettingsExist[0]->id;
        } else {
            $profileDashboardKioskSettingsId = ProfileDashboardKioskSetting::insertGetId([
                'profile_id' => $profileId,
                'kiosk_screen_number' => $kioskScreenNumber,
                'kiosk_special_screen_number' => $kioskSpecialScreenNumber,
                'kiosk_special_screen_column_number' => $kioskSpecialScreenColumnNumber,
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }
        return $profileDashboardKioskSettingsId;
    }

    public function profilekioskget()
    {
        try {
            $response = [];
            $statusCode = 200;
            if($_POST['profile_type'] == 2) {
                $response = IosKioskSetting::where('profile_id', $_POST['profileId'])->where('ios_kiosk_settings_status', 1)->get();
            }else{
                $response = ProfileDashboardKiosk::where('profile_id', $_POST['profileId'])->where('dashboard_kiosk_status', 1)->with('UserProfile')->orderBy('kiosk_browser_priority', 'asc')->get();
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function profilekioskdelete()
    {
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                if($_POST['profile_type'] == 2) {
                    $response = IosKioskSetting::where('id', $_POST['kioskId'])->update(['ios_kiosk_settings_status' => 0]);
                }else {
                    $response = ProfileDashboardKiosk::where('id', $_POST['kioskId'])->update(['dashboard_kiosk_status' => 0]);
                }
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function profiledashboardkiosksettingsget()
    {
        try {
            $response = [];
            $statusCode = 200;
            $response = ProfileDashboardKioskSetting::where('profile_id', $_POST['profileId'])->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function profileapplicationlist()
    {
        try {
            $response = [];
            $statusCode = 200;
            $response = ProfileApplication::where('profile_id', $_POST['profileId'])->where('profile_application_status', 1)->with('Application')->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function assignprofileapplication()
    {
        try {
            $response = [];
            $statusCode = 200;
            $profileApplicationExist = ProfileApplication::where('profile_id', $_POST['profile_id'])->where('application_id', $_POST['applicationId'])->with('Application')->get();
            if ($profileApplicationExist->count()) {
                $applicationsWithSamePackageName = Application::where('id', '!=', $_POST['applicationId'])->where('application_package', $profileApplicationExist[0]['Application']->application_package)->get();
                if ($applicationsWithSamePackageName->count()) {
                    foreach ($applicationsWithSamePackageName as $applicationWithSamePackageName) {
                        ProfileApplication::where('profile_id', $_POST['profile_id'])->where('application_id', $applicationWithSamePackageName->id)->delete();
                    }
                }
                ProfileApplication::where('profile_id', $_POST['profile_id'])->where('application_id', $_POST['applicationId'])->update([
                    'profile_application_status' => 1,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            } else {
                $applicationPackageName = Application::where('id', $_POST['applicationId'])->get();
                $applicationNameExist = Application::where('id', '!=', $_POST['applicationId'])->where('application_package', $applicationPackageName[0]->application_package)->get();
                if ($applicationNameExist->count()) {
                    foreach ($applicationNameExist as $profileApplicationOlderVersion) {
                        ProfileApplication::where('profile_id', $_POST['profile_id'])->where('application_id', $profileApplicationOlderVersion->id)->delete();
                    }
                }
                ProfileApplication::insert([
                    'profile_id' => $_POST['profile_id'],
                    'application_id' => $_POST['applicationId'],
                    'profile_application_status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }

        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function profileapplicationdelete()
    {
        try {
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $profAppObj = ProfileApplication::find($_POST['profileApplicationId']);
                $appId = $profAppObj->application_id;
                $appObj = Application::find($appId);
                $response = $appObj->application_name;
                $app_delete = ProfileApplication::where('id', $_POST['profileApplicationId'])->update(['profile_application_status' => 0]);
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function disableapplicationcreate()
    {
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $disableApplicationExist = DisableApplication::where('disabled_application_name', trim($_POST['disabled_application_name']))
                    ->where('profile_id', $_POST['profile_id'])
                    ->get();
                if (!$disableApplicationExist->count()) {
                    DisableApplication::insert([
                        'profile_id' => $_POST['profile_id'],
                        'disabled_application_name' => htmlentities(trim($_POST['disabled_application_name'])),
                        'disabled_application_status' => 1,
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
                    $response['save_success'] = 'Data saved successfully';
                }
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function disableapplicationget()
    {
        try {
            $response = [];
            $statusCode = 200;
            $response = DisableApplication::where('profile_id', $_POST['profileId'])->where('disabled_application_status', 1)->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function disableapplicationdelete()
    {
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                DisableApplication::where('id', $_POST['disabledApplicationId'])->update(['disabled_application_status' => 0]);
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function profilecopy()
    {
        try {
            $response = [];
            $statusCode = 200;
            $userProfileIdData = UserProfile::where('id', $_POST['profileId'])->first();
            $userProfileExists = UserProfile::where('profile_name', $userProfileIdData['profile_name'] . '_Copy')->first();
            if ($userProfileExists) {
                UserProfile::where('id', $userProfileExists['id'])->update([
                    'profile_status' => 1,
                    'created_by' => Auth::user()->id,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            } else {
                $userProfileCopyId = UserProfile::insertGetId([
                    'profile_name' => $userProfileIdData['profile_name'] . '_Copy',
                    'profile_status' => 1,
                    'created_by' => Auth::user()->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'mdm_display_status' => $userProfileIdData['mdm_display_status'],
                    'status_bar_status' => $userProfileIdData['status_bar_status'],
                    'home_key_status' => $userProfileIdData['home_key_status'],
                    'back_key_status' => $userProfileIdData['back_key_status'],
                    'recents_key_status' => $userProfileIdData['recents_key_status'],
                    'power_key_status' => $userProfileIdData['power_key_status'],
                    'firmware_upgrade_status' => $userProfileIdData['firmware_upgrade_status'],
                    'factory_reset_status' => $userProfileIdData['factory_reset_status'],
                    's_voice_status' => $userProfileIdData['s_voice_status'],
                    'android_settings_status' => $userProfileIdData['android_settings_status'],
                    'volume_key_status' => $userProfileIdData['volume_key_status'],
                    'multi_window_status' => $userProfileIdData['multi_window_status'],
                    'notifications_status' => $userProfileIdData['notifications_status'],
                    'lock_screen_status' => $userProfileIdData['lock_screen_status'],
                    'bluetooth_status' => $userProfileIdData['bluetooth_status'],
                    'gps_status' => $userProfileIdData['gps_status'],
                    'device_timeout' => $userProfileIdData['device_timeout'],
                    'pull_interval' => $userProfileIdData['pull_interval'],
                    'brightness_percentage' => $userProfileIdData['brightness_percentage'],
                    'volume_percentage' => $userProfileIdData['volume_percentage'],
                    'sleep_percentage' => $userProfileIdData['sleep_percentage'],
                    'deep_sleep_percentage' => $userProfileIdData['deep_sleep_percentage'],
                    'orientation' => $userProfileIdData['orientation'],
                    'backup_reset_menu_status' => $userProfileIdData['backup_reset_menu_status'],
                    'developer_mode_menu_status' => $userProfileIdData['developer_mode_menu_status'],
                    'airplane_mode_menu_status' => $userProfileIdData['airplane_mode_menu_status'],
                    'language_menu_status' => $userProfileIdData['language_menu_status'],
                    'multi_window_menu_status' => $userProfileIdData['multi_window_menu_status'],
                    'users_settings_menu_status' => $userProfileIdData['users_settings_menu_status'],
                    'bluetooth_settings_menu_status' => $userProfileIdData['bluetooth_settings_menu_status'],
                    'lockscreen_menu_status' => $userProfileIdData['lockscreen_menu_status'],
                    'wifi_settings_menu_status' => $userProfileIdData['wifi_settings_menu_status'],
                    'power_on_boot_status' => $userProfileIdData['power_on_boot_status'],
                    'disable_all_bloatware_status' => $userProfileIdData['disable_all_bloatware_status'],
                    'unknown_source_status' => $userProfileIdData['unknown_source_status'],
                    'developer_mode_status' => $userProfileIdData['developer_mode_status'],
                    'screenshot_status' => $userProfileIdData['screenshot_status'],
                    'usb_debugging_status' => $userProfileIdData['usb_debugging_status'],
                    'lte_mode_status' => $userProfileIdData['lte_mode_status'],
                    'placeholder1' => $userProfileIdData['placeholder1'],
                    'placeholder2' => $userProfileIdData['placeholder2'],
                    'placeholder3' => $userProfileIdData['placeholder3'],
                    'global_proxy' => $userProfileIdData['global_proxy'],
                    'global_port' => $userProfileIdData['global_port'],
                    'kiosk_type' => $userProfileIdData['kiosk_type'],
                    'kiosk_password' => $userProfileIdData['kiosk_password'],
                    'single_app_kiosk' => $userProfileIdData['single_app_kiosk'],
                    'browser_kiosk' => $userProfileIdData['browser_kiosk'],
                    'registration_password' => $userProfileIdData['registration_password'],
                    'advanced_command' => $userProfileIdData['advanced_command'],
                    'profile_counter' => (!empty($userProfileIdData['profile_counter'])) ? $userProfileIdData['profile_counter'] : 0,
                    'profile_type' => $userProfileIdData['profile_type']
                ]);

                $profile_id = $this->generateprofileid($userProfileCopyId);
                UserProfile::where('id', $userProfileCopyId)->update(['profile_id' => $profile_id]);
                array_push($response, [
                    'id' => $userProfileCopyId,
                    'profile_id' => $profile_id,
                ]);

                $this->insertWifiDetails($userProfileCopyId, $userProfileIdData->id);
                $this->insertProfileDashboardKioskSettings($userProfileCopyId, $userProfileIdData->id);
                $this->insertProfileDashboardKiosk($userProfileCopyId, $userProfileIdData->id);
                $this->insertDisableApplication($userProfileCopyId, $userProfileIdData->id);
                $this->insertProfileApplication($userProfileCopyId, $userProfileIdData->id);
                $this->insertProfileEmail($userProfileCopyId, $userProfileIdData->id);
                $this->insertProfileActiveSync($userProfileCopyId, $userProfileIdData->id);
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    /*
     * SSL Encryption method
     */
    private function encrypt_decrypt($action, $string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'OSP secret key';
        $iv = '2e9693f2a0c8f039';
        // hash
        $key = hash('sha256', $secret_key);
        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, true, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, true, $iv);
        }
        return $output;
    }

    public function insertWifiDetails($profileCopyId, $profileId)
    {
        $wifidetails = '';
        $profileData = ProfileWifiDetail::where('profile_id', $profileId)->get();
        if ($profileData) {
            foreach ($profileData as $profileData) {
                $wifidetails = ProfileWifiDetail::insertGetId([
                    'profile_id' => $profileCopyId,
                    'ssid' => $profileData->ssid,
                    'wifi_user_name' => $profileData->wifi_user_name,
                    'wifi_user_password' => $profileData->wifi_user_password,
                    'eap_method' => $profileData->eap_method,
                    'phase_2_authentication' => $profileData->phase_2_authentication,
                    'wifi_status' => $profileData->wifi_status,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }

        }
        return $wifidetails;
    }

    public function insertProfileDashboardKioskSettings($profileCopyId, $profileId)
    {
        $kioskSettingDetails = '';
        $profileData = ProfileDashboardKioskSetting::where('profile_id', $profileId)->get();
        if ($profileData) {
            foreach ($profileData as $profileData) {
                $kioskSettingDetails = ProfileDashboardKioskSetting::insertGetId([
                    'profile_id' => $profileCopyId,
                    'kiosk_screen_number' => $profileData->kiosk_screen_number,
                    'kiosk_special_screen_number' => $profileData->kiosk_special_screen_number,
                    'kiosk_special_screen_column_number' => $profileData->kiosk_special_screen_column_number,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        return $kioskSettingDetails;
    }

    public function insertProfileDashboardKiosk($profileCopyId, $profileId)
    {
        $kioskDetails = '';
        $profileData = ProfileDashboardKiosk::where('profile_id', $profileId)->get();
        if ($profileData) {
            foreach ($profileData as $profileData) {
                $kioskDetails = ProfileDashboardKiosk::insertGetId([
                    'profile_id' => $profileCopyId,
                    'kiosk_name' => $profileData->kiosk_name,
                    'kiosk_browser_type' => $profileData->kiosk_browser_type,
                    'kiosk_browser_priority' => $profileData->kiosk_browser_priority,
                    'kiosk_screen_number' => $profileData->kiosk_screen_number,
                    'kiosk_special_screen_number' => $profileData->kiosk_special_screen_number,
                    'kiosk_browser_row' => $profileData->kiosk_browser_row,
                    'kiosk_browser_column' => $profileData->kiosk_browser_column,
                    'dashboard_kiosk_status' => $profileData->dashboard_kiosk_status,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        return $kioskDetails;
    }


    public function insertDisableApplication($profileCopyId, $profileId)
    {
        $DisableApplication = '';
        $profileData = DisableApplication::where('profile_id', $profileId)->get();
        if ($profileData) {
            foreach ($profileData as $profileData) {
                $DisableApplication = DisableApplication::insertGetId([
                    'profile_id' => $profileCopyId,
                    'disabled_application_name' => $profileData->disabled_application_name,
                    'disabled_application_status' => $profileData->disabled_application_status,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }

        }
        return $DisableApplication;
    }

    public function insertProfileApplication($profileCopyId, $profileId)
    {
        $ProfileApplication = '';
        $applicationData = ProfileApplication::where('profile_id', $profileId)->get();
        if ($applicationData) {
            foreach ($applicationData as $applicationData) {
                $ProfileApplication = ProfileApplication::insertGetId([
                    'profile_id' => $profileCopyId,
                    'application_id' => $applicationData->application_id,
                    'profile_application_status' => $applicationData->profile_application_status,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        return $ProfileApplication;
    }

    public function insertProfileActiveSync($profileCopyId, $profileId)
    {
        $ProfileActiveSync = '';
        $syncData = ProfileActiveSyncSetting::where('profile_id', $profileId)->get();
        if ($syncData) {
            foreach ($syncData as $sync) {
                $ProfileActiveSync = ProfileActiveSyncSetting::insertGetId([
                    'profile_id' => $profileCopyId,
                    'active_sync_account_name' => $sync->active_sync_account_name,
                    'active_sync_host' => $sync->active_sync_host,
                    'active_sync_domain' => $sync->active_sync_domain,
                    'active_sync_user_name' => $sync->active_sync_user_name,
                    'active_sync_user_email' => $sync->active_sync_user_email,
                    'active_sync_user_password' => $sync->active_sync_user_password,
                    'active_sync_schedule' => $sync->active_sync_schedule,
                    'active_sync_ssl_status' => $sync->active_sync_ssl_status,
                    'active_sync_status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        return $ProfileActiveSync;
    }


    public function insertProfileEmail($profileCopyId, $profileId)
    {
        $ProfileApplication = '';
        $emailData = ProfileEmailConfigurationSetting::where('profile_id', $profileId)->get();
        if ($emailData) {
            foreach ($emailData as $email) {
                $ProfileApplication = ProfileEmailConfigurationSetting::insertGetId([
                    'profile_id' => $profileCopyId,
                    'account_description' => $email->account_description,
                    'user_display_name' => $email->user_display_name,
                    'user_email_address' => $email->user_email_address,
                    'incoming_mail_account_type' => $email->incoming_mail_account_type,
                    'incoming_mail_server' => $email->incoming_mail_server,
                    'incoming_mail_server_port' => $email->incoming_mail_server_port,
                    'incoming_mail_user_name' => $email->incoming_mail_user_name,
                    'incoming_mail_password' => $email->incoming_mail_password,
                    'outgoing_mail_account_type' => $email->outgoing_mail_account_type,
                    'outgoing_mail_server' => $email->outgoing_mail_server,
                    'outgoing_mail_server_port' => $email->outgoing_mail_server_port,
                    'outgoing_mail_user_name' => $email->outgoing_mail_user_name,
                    'outgoing_mail_password' => $email->outgoing_mail_password,
                    'incoming_mail_ssl_usage' => $email->incoming_mail_ssl_usage,
                    'incoming_mail_tls_usage' => $email->incoming_mail_tls_usage,
                    'incoming_mail_certificate_usage' => $email->incoming_mail_certificate_usage,
                    'outgoing_mail_ssl_usage' => $email->outgoing_mail_ssl_usage,
                    'outgoing_mail_tls_usage' => $email->outgoing_mail_tls_usage,
                    'outgoing_mail_certificate_usage' => $email->outgoing_mail_certificate_usage,
                    'email_status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        return $ProfileApplication;
    }

    public function editprofilename()
    {
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $userProfileExist = UserProfile::where('profile_name', $_POST['profile_name'])->get();
                if (!$userProfileExist->count()) {
                    UserProfile::where('id', $_POST['profile_id'])->update([
                        'profile_name' => $_POST['profile_name'],
                        'profile_type' => $_POST['profile_type'],
                        'updated_at' => date('Y-m-d H:i:s')

                    ]);
                } else {
                    $response['profileExist'] = 'Profile name already exist';
                }
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }

        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function updateprofileemail()
    {
        try {
            $ProfileEmailConfigurationSetting = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $ProfileEmailExist = ProfileEmailConfigurationSetting::where('user_email_address', $_POST['user_email_address'])->where('profile_id', $_POST['profile_id'])->get();
                if ($ProfileEmailExist->count()) {
                    ProfileEmailConfigurationSetting::where('id', $ProfileEmailExist[0]['id'])->update([
                        'account_description' => $_POST['account_description'],
                        'user_display_name' => $_POST['user_display_name'],
                        'user_email_address' => $_POST['user_email_address'],
                        'incoming_mail_account_type' => $_POST['incoming_mail_account_type'],
                        'incoming_mail_server' => $_POST['incoming_mail_server'],
                        'incoming_mail_server_port' => $_POST['incoming_mail_server_port'],
                        'incoming_mail_user_name' => $_POST['incoming_mail_user_name'],
                        'incoming_mail_password' => $_POST['incoming_mail_password'],
                        'outgoing_mail_account_type' => $_POST['outgoing_mail_account_type'],
                        'outgoing_mail_server' => $_POST['outgoing_mail_server'],
                        'outgoing_mail_server_port' => $_POST['outgoing_mail_server_port'],
                        'outgoing_mail_user_name' => $_POST['outgoing_mail_user_name'],
                        'outgoing_mail_password' => $_POST['outgoing_mail_password'],
                        'incoming_mail_ssl_usage' => $_POST['incoming_mail_ssl_usage'],
                        'incoming_mail_tls_usage' => $_POST['incoming_mail_tls_usage'],
                        'incoming_mail_certificate_usage' => $_POST['incoming_mail_certificate_usage'],
                        'outgoing_mail_ssl_usage' => $_POST['outgoing_mail_ssl_usage'],
                        'outgoing_mail_tls_usage' => $_POST['outgoing_mail_tls_usage'],
                        'outgoing_mail_certificate_usage' => $_POST['outgoing_mail_certificate_usage'],
                        'email_status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                } else {
                    $ProfileEmailConfigurationSetting = ProfileEmailConfigurationSetting::insertGetId([
                        'profile_id' => $_POST['profile_id'],
                        'account_description' => $_POST['account_description'],
                        'user_display_name' => $_POST['user_display_name'],
                        'user_email_address' => $_POST['user_email_address'],
                        'incoming_mail_account_type' => $_POST['incoming_mail_account_type'],
                        'incoming_mail_server' => $_POST['incoming_mail_server'],
                        'incoming_mail_server_port' => $_POST['incoming_mail_server_port'],
                        'incoming_mail_user_name' => $_POST['incoming_mail_user_name'],
                        'incoming_mail_password' => $_POST['incoming_mail_password'],
                        'outgoing_mail_account_type' => $_POST['outgoing_mail_account_type'],
                        'outgoing_mail_server' => $_POST['outgoing_mail_server'],
                        'outgoing_mail_server_port' => $_POST['outgoing_mail_server_port'],
                        'outgoing_mail_user_name' => $_POST['outgoing_mail_user_name'],
                        'outgoing_mail_password' => $_POST['outgoing_mail_password'],
                        'incoming_mail_ssl_usage' => $_POST['incoming_mail_ssl_usage'],
                        'incoming_mail_tls_usage' => $_POST['incoming_mail_tls_usage'],
                        'incoming_mail_certificate_usage' => $_POST['incoming_mail_certificate_usage'],
                        'outgoing_mail_ssl_usage' => $_POST['outgoing_mail_ssl_usage'],
                        'outgoing_mail_tls_usage' => $_POST['outgoing_mail_tls_usage'],
                        'outgoing_mail_certificate_usage' => $_POST['outgoing_mail_certificate_usage'],
                        'email_status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                }
            }

        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($ProfileEmailConfigurationSetting, $statusCode);
    }

    public function profileemailget()
    {
        try {
            $response = [];
            $statusCode = 200;

            if($_POST['profile_type'] == 1){
                $response = ProfileEmailConfigurationSetting::where('profile_id', $_POST['profileId'])->where('email_status', 1)->get();
            }else if($_POST['profile_type'] == 2) {
                $response = IosProfileEmailSetting::where('profile_id', $_POST['profileId'])->where('ios_email_status', 1)->get();
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function profileemaildelete()
    {
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                if($_POST['profile_type'] == 1){
                    $response = ProfileEmailConfigurationSetting::where('id', $_POST['profile_id'])->update(['email_status' => 0]);
                }elseif($_POST['profile_type'] == 2){
                    $response = IosProfileEmailSetting::where('id', $_POST['profile_id'])->update(['ios_email_status' => 0]);
                }

            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function updateprofileactivesync()
    {
        try {
            $ProfileActiveSyncSetting = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $ProfileSyncExist = ProfileActiveSyncSetting::where('active_sync_user_email', $_POST['active_sync_user_email'])->where('profile_id', $_POST['profile_id'])->get();
                if ($ProfileSyncExist->count()) {
                    ProfileActiveSyncSetting::where('id', $ProfileSyncExist[0]['id'])->update([
                        'active_sync_account_name' => $_POST['active_sync_account_name'],
                        'active_sync_host' => $_POST['active_sync_host'],
                        'active_sync_domain' => $_POST['active_sync_domain'],
                        'active_sync_user_name' => $_POST['active_sync_user_name'],
                        'active_sync_user_email' => $_POST['active_sync_user_email'],
                        'active_sync_user_password' => $_POST['active_sync_user_password'],
                        'active_sync_schedule' => $_POST['active_sync_schedule'],
                        'active_sync_ssl_status' => $_POST['active_sync_ssl_status'],
                        'active_sync_status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                } else {
                    $ProfileActiveSyncSetting = ProfileActiveSyncSetting::insertGetId([
                        'profile_id' => $_POST['profile_id'],
                        'active_sync_account_name' => $_POST['active_sync_account_name'],
                        'active_sync_host' => $_POST['active_sync_host'],
                        'active_sync_domain' => $_POST['active_sync_domain'],
                        'active_sync_user_name' => $_POST['active_sync_user_name'],
                        'active_sync_user_email' => $_POST['active_sync_user_email'],
                        'active_sync_user_password' => $_POST['active_sync_user_password'],
                        'active_sync_schedule' => $_POST['active_sync_schedule'],
                        'active_sync_ssl_status' => $_POST['active_sync_ssl_status'],
                        'active_sync_status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                }
            }

        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($ProfileActiveSyncSetting, $statusCode);
    }

    public function profileactivesyncget()
    {
        try {
            $response = [];
            $statusCode = 200;
            if($_POST['profile_type'] == 1){
                $response = ProfileActiveSyncSetting::where('profile_id', $_POST['profileId'])->where('active_sync_status', 1)->get();
            }else if($_POST['profile_type'] == 2){
                $response = IosProfileActiveSyncSetting::where('profile_id', $_POST['profileId'])->where('ios_active_sync_status', 1)->get();
            }

        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function profilesyncdelete()
    {
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                if($_POST['profile_type'] == 1){
                    ProfileActiveSyncSetting::where('id', $_POST['profile_id'])->update(['active_sync_status' => 0]);
                }elseif($_POST['profile_type'] == 2){
                    IosProfileActiveSyncSetting::where('id', $_POST['profile_id'])->update(['ios_active_sync_status' => 0]);
                }
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function iosprofileupdate()
    {
        try {
            $ProfileIosSetting = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $ProfileIosSettingExist = IosProfileSetting::where('profile_id', $_POST['profile_id'])->where('profile_id', $_POST['profile_id'])->get();
                if ($ProfileIosSettingExist->count()) {
                    $ProfileIosSetting = IosProfileSetting::where('id', $ProfileIosSettingExist[0]['id'])->update([
                        'app_install_allow_status' => $_POST['app_install_allow_status'],
                        'air_drop_block_status' => $_POST['air_drop_block_status'],
                        'camera_use_allow_status' => $_POST['camera_use_allow_status'],
                        'face_time_allow_status' => $_POST['face_time_allow_status'],
                        'screen_capture_allow_status' => $_POST['screen_capture_allow_status'],
                        'roaming_automatic_sync_allow_status' => $_POST['roaming_automatic_sync_allow_status'],
                        'touch_id_status' => $_POST['touch_id_status'],
                        'siri_allow_status' => $_POST['siri_allow_status'],
                        'device_locked_siri_allow_status' => $_POST['device_locked_siri_allow_status'],
                        'device_locked_passbook_allow_status' => $_POST['device_locked_passbook_allow_status'],
                        'voice_dialing_allow_status' => $_POST['voice_dialing_allow_status'],
                        'in_app_purchase_allow_status' => $_POST['in_app_purchase_allow_status'],
                        'force_user_credential_on_itune_purchase_allow_status' => $_POST['force_user_credential_on_itune_purchase_allow_status'],
                        'multiplayer_gaming_allow_status' => $_POST['multiplayer_gaming_allow_status'],
                        'adding_game_centre_friends_allow_status' => $_POST['adding_game_centre_friends_allow_status'],
                        'trust_enterprise_app_allow_status' => $_POST['trust_enterprise_app_allow_status'],
                        'modify_trust_enterprise_app_allow_status' => $_POST['modify_trust_enterprise_app_allow_status'],
                        'enterprise_book_backup_allow_status' => $_POST['enterprise_book_backup_allow_status'],
                        'managed_apps_to_sync_allow_status' => $_POST['managed_apps_to_sync_allow_status'],
                        'use_youtbe_allow_status' => $_POST['use_youtbe_allow_status'],
                        'use_itune_store_allow_status' => $_POST['use_itune_store_allow_status'],
                        'use_safari_allow_status' => $_POST['use_safari_allow_status'],
                        'safari_enable_autofill_status' => $_POST['safari_enable_autofill_status'],
                        'safari_force_fraud_warning_status' => $_POST['safari_force_fraud_warning_status'],
                        'safari_enable_javascript_status' => $_POST['safari_enable_javascript_status'],
                        'safari_block_popup_status' => $_POST['safari_block_popup_status'],
                        'icloud_backup_allow_status' => $_POST['icloud_backup_allow_status'],
                        'icloud_document_sync_allow_status' => $_POST['icloud_document_sync_allow_status'],
                        'icloud_photo_stream_allow_status' => $_POST['icloud_photo_stream_allow_status'],
                        'icloud_shared_photo_stream_allow_status' => $_POST['icloud_shared_photo_stream_allow_status'],
                        'icloud_photo_library_allow_status' => $_POST['icloud_photo_library_allow_status'],
                        'lock_screen_notification_allow_status' => $_POST['lock_screen_notification_allow_status'],
                        'today_view_in_lock_screen_allow_status' => $_POST['today_view_in_lock_screen_allow_status'],
                        'control_center_in_lock_screen_allow_status' => $_POST['control_center_in_lock_screen_allow_status'],
                        'over_the_air_PKI_updates_allow_status' => $_POST['over_the_air_PKI_updates_allow_status'],
                        'ad_tracking_limit_status' => $_POST['ad_tracking_limit_status'],
                        'diagnostic_data_sent_allow_status' => $_POST['diagnostic_data_sent_allow_status'],
                        'untrusted_TLS_certificate_allow_status' => $_POST['untrusted_TLS_certificate_allow_status'],
                        'force_encrypt_backup_status' => $_POST['force_encrypt_backup_status'],
                        'force_apple_watch_wrist_detection_status' => $_POST['force_apple_watch_wrist_detection_status'],
                        'rate_music_allow_status' => $_POST['rate_music_allow_status'],
                        'rate_ibookstore_erotica_allow_status' => $_POST['rate_ibookstore_erotica_allow_status'],
                        'cookie_accept_status' => $_POST['cookie_accept_status'],
                        'rating_region' => $_POST['rating_region'],
                        'movie_allow_status' => $_POST['movie_allow_status'],
                        'tv_shows_allow_status' => $_POST['tv_shows_allow_status'],
                        'apps_allow_status' => $_POST['apps_allow_status'],
                        'ios_settings_status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                } else {
                    $ProfileIosSetting = IosProfileSetting::insertGetId([
                        'profile_id' => $_POST['profile_id'],
                        'app_install_allow_status' => $_POST['app_install_allow_status'],
                        'air_drop_block_status' => $_POST['air_drop_block_status'],
                        'camera_use_allow_status' => $_POST['camera_use_allow_status'],
                        'face_time_allow_status' => $_POST['face_time_allow_status'],
                        'screen_capture_allow_status' => $_POST['screen_capture_allow_status'],
                        'roaming_automatic_sync_allow_status' => $_POST['roaming_automatic_sync_allow_status'],
                        'touch_id_status' => $_POST['touch_id_status'],
                        'siri_allow_status' => $_POST['siri_allow_status'],
                        'device_locked_siri_allow_status' => $_POST['device_locked_siri_allow_status'],
                        'device_locked_passbook_allow_status' => $_POST['device_locked_passbook_allow_status'],
                        'voice_dialing_allow_status' => $_POST['voice_dialing_allow_status'],
                        'in_app_purchase_allow_status' => $_POST['in_app_purchase_allow_status'],
                        'force_user_credential_on_itune_purchase_allow_status' => $_POST['force_user_credential_on_itune_purchase_allow_status'],
                        'multiplayer_gaming_allow_status' => $_POST['multiplayer_gaming_allow_status'],
                        'adding_game_centre_friends_allow_status' => $_POST['adding_game_centre_friends_allow_status'],
                        'trust_enterprise_app_allow_status' => $_POST['trust_enterprise_app_allow_status'],
                        'modify_trust_enterprise_app_allow_status' => $_POST['modify_trust_enterprise_app_allow_status'],
                        'enterprise_book_backup_allow_status' => $_POST['enterprise_book_backup_allow_status'],
                        'managed_apps_to_sync_allow_status' => $_POST['managed_apps_to_sync_allow_status'],
                        'use_youtbe_allow_status' => $_POST['use_youtbe_allow_status'],
                        'use_itune_store_allow_status' => $_POST['use_itune_store_allow_status'],
                        'use_safari_allow_status' => $_POST['use_safari_allow_status'],
                        'safari_enable_autofill_status' => $_POST['safari_enable_autofill_status'],
                        'safari_force_fraud_warning_status' => $_POST['safari_force_fraud_warning_status'],
                        'safari_enable_javascript_status' => $_POST['safari_enable_javascript_status'],
                        'safari_block_popup_status' => $_POST['safari_block_popup_status'],
                        'icloud_backup_allow_status' => $_POST['icloud_backup_allow_status'],
                        'icloud_document_sync_allow_status' => $_POST['icloud_document_sync_allow_status'],
                        'icloud_photo_stream_allow_status' => $_POST['icloud_photo_stream_allow_status'],
                        'icloud_shared_photo_stream_allow_status' => $_POST['icloud_shared_photo_stream_allow_status'],
                        'icloud_photo_library_allow_status' => $_POST['icloud_photo_library_allow_status'],
                        'lock_screen_notification_allow_status' => $_POST['lock_screen_notification_allow_status'],
                        'today_view_in_lock_screen_allow_status' => $_POST['today_view_in_lock_screen_allow_status'],
                        'control_center_in_lock_screen_allow_status' => $_POST['control_center_in_lock_screen_allow_status'],
                        'over_the_air_PKI_updates_allow_status' => $_POST['over_the_air_PKI_updates_allow_status'],
                        'ad_tracking_limit_status' => $_POST['ad_tracking_limit_status'],
                        'diagnostic_data_sent_allow_status' => $_POST['diagnostic_data_sent_allow_status'],
                        'untrusted_TLS_certificate_allow_status' => $_POST['untrusted_TLS_certificate_allow_status'],
                        'force_encrypt_backup_status' => $_POST['force_encrypt_backup_status'],
                        'force_apple_watch_wrist_detection_status' => $_POST['force_apple_watch_wrist_detection_status'],
                        'rate_music_allow_status' => $_POST['rate_music_allow_status'],
                        'rate_ibookstore_erotica_allow_status' => $_POST['rate_ibookstore_erotica_allow_status'],
                        'cookie_accept_status' => $_POST['cookie_accept_status'],
                        'rating_region' => $_POST['rating_region'],
                        'movie_allow_status' => $_POST['movie_allow_status'],
                        'tv_shows_allow_status' => $_POST['tv_shows_allow_status'],
                        'apps_allow_status' => $_POST['apps_allow_status'],
                        'ios_settings_status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                }
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($ProfileIosSetting, $statusCode);
    }

    public function getiossettings(){
        $iosProfileSetting = [];
            try {
                $statusCode = 200;
                if(!empty($_POST['profile_id'])){
                    $iosProfileSetting = IosProfileSetting::where('profile_id', $_POST['profile_id'])->where('profile_id', $_POST['profile_id'])->get();
                }
            } catch (Exception $e) {
                $statusCode = 404;
            }
            return response()->json($iosProfileSetting, $statusCode);
    }

    public function iosactivesyncupdate(){
        $iosActiveSync = [];
        try{
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $iosActiveSyncExist = IosProfileActiveSyncSetting::where('active_sync_user_email', $_POST['active_sync_user_email'])->where('profile_id', $_POST['profile_id'])->get();
                if ($iosActiveSyncExist->count()) {
                    $iosActiveSync = IosProfileActiveSyncSetting::where('id', $iosActiveSyncExist[0]['id'])->update([
                        'move_allow_status'=> $_POST['move_allow_status'],
                        'recent_address_syncing_allow_status'=> $_POST['recent_address_syncing_allow_status'],
                        'use_only_in_mail_status'=> $_POST['use_only_in_mail_status'],
                        'use_ssl_status' => $_POST['use_ssl_status'],
                        'use_mime_status' => $_POST['use_mime_status'],
                        'account_name' => $_POST['account_name'],
                        'active_sync_server_name' => $_POST['active_sync_server_name'],
                        'active_sync_domain' => $_POST['active_sync_domain'],
                        'active_sync_user_name' => $_POST['active_sync_user_name'],
                        'active_sync_user_email' => $_POST['active_sync_user_email'],
                        'active_sync_user_password' => $_POST['active_sync_user_password'],
                        'mail_sync_period' => $_POST['mail_sync_period'],
                        'identity_certificate' => $_POST['identity_certificate'],
                        'identity_certificate_ios4_compatible_status' => $_POST['identity_certificate_ios4_compatible_status'],
                        'ios_active_sync_status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                } else {
                    $iosActiveSync = IosProfileActiveSyncSetting::insertGetId([
                        'profile_id' => $_POST['profile_id'],
                        'move_allow_status'=> $_POST['move_allow_status'],
                        'recent_address_syncing_allow_status'=> $_POST['recent_address_syncing_allow_status'],
                        'use_only_in_mail_status'=> $_POST['use_only_in_mail_status'],
                        'use_ssl_status' => $_POST['use_ssl_status'],
                        'use_mime_status' => $_POST['use_mime_status'],
                        'account_name' => $_POST['account_name'],
                        'active_sync_server_name' => $_POST['active_sync_server_name'],
                        'active_sync_domain' => $_POST['active_sync_domain'],
                        'active_sync_user_name' => $_POST['active_sync_user_name'],
                        'active_sync_user_email' => $_POST['active_sync_user_email'],
                        'active_sync_user_password' => $_POST['active_sync_user_password'],
                        'mail_sync_period' => $_POST['mail_sync_period'],
                        'identity_certificate' => $_POST['identity_certificate'],
                        'identity_certificate_ios4_compatible_status' => $_POST['identity_certificate_ios4_compatible_status'],
                        'ios_active_sync_status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                }
            }
        }catch (Exception $ex){
            $statusCode = 404;
        }
        return response()->json($iosActiveSync, $statusCode);
    }

    public function iosprofileemailupdate(){
        $iosEmail = [];
        try{
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $iosEmailExist = IosProfileEmailSetting::where('user_email_address', $_POST['user_email_address'])->where('profile_id', $_POST['profile_id'])->get();
                if ($iosEmailExist->count()) {
                    $iosEmail = IosProfileEmailSetting::where('id', $iosEmailExist[0]['id'])->update([
                        'account_description' => $_POST['account_description'],
                        'account_type'=> $_POST['account_type'],
                        'user_display_name'=> $_POST['user_display_name'],
                        'user_email_address'=> $_POST['user_email_address'],
                        'allow_move_status'=> $_POST['allow_move_status'],
                        'incoming_mail_account_type'=> $_POST['incoming_mail_account_type'],
                        'incoming_mail_server'=> $_POST['incoming_mail_server'],
                        'incoming_mail_server_port'=> $_POST['incoming_mail_server_port'],
                        'incoming_mail_user_name'=> $_POST['incoming_mail_user_name'],
                        'incoming_mail_authentication_type'=> $_POST['incoming_mail_authentication_type'],
                        'incoming_mail_password'=> $_POST['incoming_mail_password'],
                        'incoming_mail_ssl_usage'=> $_POST['incoming_mail_ssl_usage'],
                        'outgoing_mail_account_type'=> $_POST['outgoing_mail_account_type'],
                        'outgoing_mail_server'=> $_POST['outgoing_mail_server'],
                        'outgoing_mail_server_port'=> $_POST['outgoing_mail_server_port'],
                        'outgoing_mail_user_name'=> $_POST['outgoing_mail_user_name'],
                        'outgoing_mail_authentication_type'=> $_POST['outgoing_mail_authentication_type'],
                        'outgoing_mail_password'=> $_POST['outgoing_mail_password'],
                        'recent_address_syncing_allow_status'=> $_POST['recent_address_syncing_allow_status'],
                        'only_in_mail_usage'=> $_POST['only_in_mail_usage'],
                        'outgoing_mail_ssl_usage'=> $_POST['outgoing_mail_ssl_usage'],
                        'outgoing_mail_mime_usage'=> $_POST['outgoing_mail_mime_usage'],
                        'ios_email_status'=> 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                } else {
                    $iosEmail = IosProfileEmailSetting::insertGetId([
                        'profile_id' => $_POST['profile_id'],
                        'account_description' => $_POST['account_description'],
                        'account_type'=> $_POST['account_type'],
                        'user_display_name'=> $_POST['user_display_name'],
                        'user_email_address'=> $_POST['user_email_address'],
                        'allow_move_status'=> $_POST['allow_move_status'],
                        'incoming_mail_account_type'=> $_POST['incoming_mail_account_type'],
                        'incoming_mail_server'=> $_POST['incoming_mail_server'],
                        'incoming_mail_server_port'=> $_POST['incoming_mail_server_port'],
                        'incoming_mail_user_name'=> $_POST['incoming_mail_user_name'],
                        'incoming_mail_authentication_type'=> $_POST['incoming_mail_authentication_type'],
                        'incoming_mail_password'=> $_POST['incoming_mail_password'],
                        'incoming_mail_ssl_usage'=> $_POST['incoming_mail_ssl_usage'],
                        'outgoing_mail_account_type'=> $_POST['outgoing_mail_account_type'],
                        'outgoing_mail_server'=> $_POST['outgoing_mail_server'],
                        'outgoing_mail_server_port'=> $_POST['outgoing_mail_server_port'],
                        'outgoing_mail_user_name'=> $_POST['outgoing_mail_user_name'],
                        'outgoing_mail_authentication_type'=> $_POST['outgoing_mail_authentication_type'],
                        'outgoing_mail_password'=> $_POST['outgoing_mail_password'],
                        'recent_address_syncing_allow_status'=> $_POST['recent_address_syncing_allow_status'],
                        'only_in_mail_usage'=> $_POST['only_in_mail_usage'],
                        'outgoing_mail_ssl_usage'=> $_POST['outgoing_mail_ssl_usage'],
                        'outgoing_mail_mime_usage'=> $_POST['outgoing_mail_mime_usage'],
                        'ios_email_status'=> 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                }
            }
        }catch (Exception $ex){
            $statusCode = 404;
        }
        return response()->json($iosEmail, $statusCode);
    }

    public function iosprofileldapupdate(){
        $iosLdapSetting = [];
        try{
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $iosLdapExist = IosLdapSetting::where('ldap_account_user_name', $_POST['ldap_account_user_name'])->where('profile_id', $_POST['profile_id'])->get();
                if ($iosLdapExist->count()) {
                    $iosLdapSetting = IosProfileEmailSetting::where('id', $iosLdapExist[0]['id'])->update([
                        'ldap_ssl_usage_status' => $_POST['ldap_ssl_usage_status'],
                        'ldap_account_description' => $_POST['ldap_account_description'],
                        'ldap_account_user_name' => $_POST['ldap_account_user_name'],
                        'ldap_account_password' => $_POST['ldap_account_password'],
                        'ldap_account_host_name' => $_POST['ldap_account_host_name'],
                        'ios_ldap_settings_status'=> 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                } else {
                    $iosLdapSetting = IosLdapSetting::insertGetId([
                        'profile_id' => $_POST['profile_id'],
                        'ldap_ssl_usage_status' => $_POST['ldap_ssl_usage_status'],
                        'ldap_account_description' => $_POST['ldap_account_description'],
                        'ldap_account_user_name' => $_POST['ldap_account_user_name'],
                        'ldap_account_password' => $_POST['ldap_account_password'],
                        'ldap_account_host_name' => $_POST['ldap_account_host_name'],
                        'ios_ldap_settings_status'=> 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                }
            }
        }catch (Exception $ex){
            $statusCode = 404;
        }
        return response()->json($iosLdapSetting, $statusCode);
    }

    public function profileldapget(){
        try {
            $ldap = [];
            $statusCode = 200;
            $ldap = IosLdapSetting::where('profile_id', $_POST['profileId'])->where('ios_ldap_settings_status', 1)->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($ldap, $statusCode);
    }

    public function profileldapdelete(){
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $response = IosLdapSetting::where('id', $_POST['profile_id'])->update(['ios_ldap_settings_status' => 0]);
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function iosprofilevpnupdate(){
        $iosVpnSetting = [];
        try{
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                //$iosVpnExist = IosVpnSetting::where('ldap_account_user_name', $_POST['ldap_account_user_name'])->where('profile_id', $_POST['profile_id'])->get();
                $iosVpnSetting = IosVpnSetting::insertGetId([
                        'profile_id' => $_POST['profile_id'],
                        'vpn_sent_all_traffic_status' => $_POST['vpn_sent_all_traffic_status'],
                        'vpn_connection_name' => $_POST['vpn_connection_name'],
                        'vpn_connection_type' => $_POST['vpn_connection_type'],
                        'vpn_server' => $_POST['vpn_server'],
                        'vpn_account' => $_POST['vpn_account'],
                        'vpn_user_authentication_type' => $_POST['vpn_user_authentication_type'],
                        'vpn_shared_secret' => $_POST['vpn_shared_secret'],
                        'vpn_proxy' => $_POST['vpn_proxy'],
                        'ios_vpn_settings_status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
            }
        }catch (Exception $ex){
            $statusCode = 404;
        }
        return response()->json($iosVpnSetting, $statusCode);
    }

    public function profilevpnget(){
        try {
            $Vpn = [];
            $statusCode = 200;
            $Vpn = IosVpnSetting::where('profile_id', $_POST['profileId'])->where('ios_vpn_settings_status', 1)->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($Vpn, $statusCode);
    }

    public function profilevpndelete(){
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $response = IosVpnSetting::where('id', $_POST['profile_id'])->update(['ios_vpn_settings_status' => 0]);
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function iosprofilecarddavupdate(){
        $iosCardDavSetting = [];
        try{
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                //$iosVpnExist = IosVpnSetting::where('ldap_account_user_name', $_POST['ldap_account_user_name'])->where('profile_id', $_POST['profile_id'])->get();
                $iosCardDavSetting = IosCarddavSetting::insertGetId([
                        'profile_id' => $_POST['profile_id'],
                        'carddav_account_description' => $_POST['carddav_account_description'],
                        'carddav_account_host_name' => $_POST['carddav_account_host_name'],
                        'carddav_port' => $_POST['carddav_port'],
                        'carddav_princpal_url' => $_POST['carddav_princpal_url'],
                        'carddav_account_user_name' => $_POST['carddav_account_user_name'],
                        'carddav_account_password' => $_POST['carddav_account_password'],
                        'carddav_ssl_usage_status' => $_POST['carddav_ssl_usage_status'],
                        'ios_carddav_settings_status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
            }
        }catch (Exception $ex){
            $statusCode = 404;
        }
        return response()->json($iosCardDavSetting, $statusCode);
    }

    public function profilecarddavget(){
        try {
            $dav = [];
            $statusCode = 200;
            $dav = IosCarddavSetting::where('profile_id', $_POST['profileId'])->where('ios_carddav_settings_status', 1)->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($dav, $statusCode);
    }

    public function profilecarddavdelete(){
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $response = IosCarddavSetting::where('id', $_POST['profile_id'])->update(['ios_carddav_settings_status' => 0]);
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function profileadvancedrestrictionupdate(){
        $iosAdvancedRestrictionSetting = [];
        try{
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                //$iosVpnExist = IosVpnSetting::where('ldap_account_user_name', $_POST['ldap_account_user_name'])->where('profile_id', $_POST['profile_id'])->get();
                $iosAdvancedRestrictionSetting = IosAdvancedRestrictionsSetting::insertGetId([
                    'profile_id' => $_POST['profile_id'],
                      'airdrop_allow_status' => $_POST['airdrop_allow_status'],
                      'app_cellular_data_modification_allow_status' => $_POST['app_cellular_data_modification_allow_status'],
                      'remove_app_allow_status' => $_POST['remove_app_allow_status'],
                      'book_store_allow_status' => $_POST['book_store_allow_status'],
                      'touch_id_add_remove_allow_status' => $_POST['touch_id_add_remove_allow_status'],
                      'imessage_allow_status' => $_POST['imessage_allow_status'],
                      'game_center_allow_status' => $_POST['game_center_allow_status'],
                      'itunes_pairing_allow_status' => $_POST['itunes_pairing_allow_status'],
                      'profile_installation_allow_status' => $_POST['profile_installation_allow_status'],
                      'podcasts_allow_status'  => $_POST['podcasts_allow_status'],
                      'lookup_definition_allow_status'  => $_POST['lookup_definition_allow_status'],
                      'predictive_keyboard_allow_status'  => $_POST['predictive_keyboard_allow_status'],
                      'auto_correction_allow_status'  => $_POST['auto_correction_allow_status'],
                      'spell_check_allow_status'  => $_POST['spell_check_allow_status'],
                      'apple_music_services_allow_status'  => $_POST['apple_music_services_allow_status'],
                      'itunes_radio_allow_status'  => $_POST['itunes_radio_allow_status'],
                      'ios_news_allow_status' => $_POST['ios_news_allow_status'],
                      'app_install_from_devices_allow_status' => $_POST['app_install_from_devices_allow_status'],
                      'keyboard_shortcut_allow_status' => $_POST['keyboard_shortcut_allow_status'],
                      'paired_watch_allow_status' => $_POST['paired_watch_allow_status'],
                      'account_modification_allow_status' => $_POST['account_modification_allow_status'],
                      'erase_cintent_and_settings_allow_status' => $_POST['erase_cintent_and_settings_allow_status'],
                      'user_generated_content_assistant_allow_status'  => $_POST['user_generated_content_assistant_allow_status'],
                      'find_my_friends_modify_allow_status'  => $_POST['find_my_friends_modify_allow_status'],
                      'force_use_of_priority_filter_status'  => $_POST['force_use_of_priority_filter_status'],
                      'spotlight_internet_results_allow_status'  => $_POST['spotlight_internet_results_allow_status'],
                      'restrictions_enabling_allow_status'  => $_POST['restrictions_enabling_allow_status'],
                      'passcode_modification_allow_status'  => $_POST['passcode_modification_allow_status'],
                      'device_name_modification_allow_status'  => $_POST['device_name_modification_allow_status'],
                      'wallpaper_modification_allow_status'  => $_POST['wallpaper_modification_allow_status'],
                      'notification_modification_allow_status'  => $_POST['notification_modification_allow_status'],
                      'automatic_apps_downloading_allow_status'  => $_POST['automatic_apps_downloading_allow_status'],
                      'autonomous_single_app_mode_apps'  => $_POST['autonomous_single_app_mode_apps'],
                      'ios_advanced_restrictions_settings_status'  => 1,
                      'created_at' => date('Y-m-d H:i:s'),
                      'updated_at'  => date('Y-m-d H:i:s')
                ]);
            }
        }catch (Exception $ex){
            $statusCode = 404;
        }
        return response()->json($iosAdvancedRestrictionSetting, $statusCode);
    }

    public function getadvancedrestriction(){
        $iosProfilerestrictionSetting = [];
        try {
            $statusCode = 200;
            if(!empty($_POST['profileId'])){
                $iosProfilerestrictionSetting = IosAdvancedRestrictionsSetting::where('profile_id', $_POST['profileId'])->where(['ios_advanced_restrictions_settings_status' => 1])->get();
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($iosProfilerestrictionSetting, $statusCode);
    }



    public function iosprofileapnupdate(){
        $iosApnSetting = [];
        try{
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $iosApnSetting = IosApnSetting::insertGetId([
                    'profile_id' => $_POST['profile_id'],
                    'apn_name' => $_POST['apn_name'],
                    'apn_user_name' => $_POST['apn_user_name'],
                    'apn_password' => $_POST['apn_password'],
                    'apn_proxy_server' => $_POST['apn_proxy_server'],
                    'apn_proxy_server_port' => $_POST['apn_proxy_server_port'],
                    'ios_apn_settings_status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
        }catch (Exception $ex){
            $statusCode = 404;
        }
        return response()->json($iosApnSetting, $statusCode);
    }

    public function profileapnget(){
        try {
            $apn = [];
            $statusCode = 200;
            $apn = IosApnSetting::where('profile_id', $_POST['profile_id'])->where('ios_apn_settings_status', 1)->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($apn, $statusCode);
    }

    public function profileapndelete(){
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $response = IosApnSetting::where('id', $_POST['profile_id'])->update(['ios_apn_settings_status' => 0]);
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }


    public function iosprofilekioskupdate(){
        $iosKioskSettingg = [];
        try{
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $iosKioskSettingg = IosKioskSetting::insertGetId([
                    'profile_id' => $_POST['profile_id'],
                    'kiosk_app' => $_POST['kiosk_app'],
                    'kiosk_app_name' => $_POST['kiosk_app_name'],
                    'kiosk_app_identifier' => $_POST['kiosk_app_identifier'],
                    'kiosk_disable_touch_status' => $_POST['kiosk_disable_touch_status'],
                    'kiosk_disable_device_rotation_status' => $_POST['kiosk_disable_device_rotation_status'],
                    'kiosk_disable_volume_button_status' => $_POST['kiosk_disable_volume_button_status'],
                    'kiosk_disable_ringer_switch_status' => $_POST['kiosk_disable_ringer_switch_status'],
                    'kiosk_disable_sleep_wake_button_status' => $_POST['kiosk_disable_sleep_wake_button_status'],
                    'kiosk_disable_auto_lock_status' => $_POST['kiosk_disable_auto_lock_status'],
                    'kiosk_enable_voice_over_status' => $_POST['kiosk_enable_voice_over_status'],
                    'kiosk_enable_zoom_status' => $_POST['kiosk_enable_zoom_status'],
                    'kiosk_enable_invert_colors_status' => $_POST['kiosk_enable_invert_colors_status'],
                    'kiosk_enable_assistive_touch_status' => $_POST['kiosk_enable_assistive_touch_status'],
                    'kiosk_enable_speak_selection_status' => $_POST['kiosk_enable_speak_selection_status'],
                    'kiosk_voice_over_status' => $_POST['kiosk_voice_over_status'],
                    'kiosk_zoom_status' => $_POST['kiosk_zoom_status'],
                    'kiosk_invert_colors_status' => $_POST['kiosk_invert_colors_status'],
                    'kiosk_assistive_touch_status' => $_POST['kiosk_assistive_touch_status'],
                    'ios_kiosk_settings_status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
        }catch (Exception $ex){
            $statusCode = 404;
        }
        return response()->json($iosKioskSettingg, $statusCode);
    }


    public function iosprofilepasswordupdate(){
        $iosPasswordSetting = [];
        try{
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $iosPasswordSetting = IosPasswordSetting::insertGetId([
                    'profile_id' => $_POST['profile_id'],
                    'allow_simple_ios_password_status' => $_POST['allow_simple_ios_password_status'],
                    'ios_password_alpha_numeric_value_require_status' => $_POST['ios_password_alpha_numeric_value_require_status'],
                    'ios_password_minmum_length' => $_POST['ios_password_minmum_length'],
                    'ios_password_minmum_complex_character_count' => $_POST['ios_password_minmum_complex_character_count'],
                    'ios_password_maximum_age' => $_POST['ios_password_maximum_age'],
                    'ios_device_auto_lock_status' => $_POST['ios_device_auto_lock_status'],
                    'ios_password_history_count' => $_POST['ios_password_history_count'],
                    'ios_device_lock_grace_period' => $_POST['ios_device_lock_grace_period'],
                    'ios_password_failed_attempts_count' => $_POST['ios_password_failed_attempts_count'],
                    'ios_password_settings_status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
        }catch (Exception $ex){
            $statusCode = 404;
        }
        return response()->json($iosPasswordSetting, $statusCode);
    }

    public function profilepasswordget(){
        try {
            $password = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $password = IosPasswordSetting::where('profile_id', $_POST['profile_id'])->where('ios_password_settings_status', 1)->get();
            } else {
                $password['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($password, $statusCode);
    }

    public function profilepassworddelete(){
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $response = IosPasswordSetting::where('id', $_POST['item_id'])->update(['ios_password_settings_status' => 0]);
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }



    public function iosprofilewebclipupdate(Request $request){
        $iosWebclipSetting = [];
        try{
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $response = [];
                if($request->file('file')){
                    $uploadedFile = $request->file('file');
                    $fileName = $_FILES['file']['name'];
                    $extension = $uploadedFile->getClientOriginalExtension();
                    $extArray = array('png','jpg','jpeg','JPG','JPEG','PNG');
                    if(in_array($extension, $extArray))
                    {
                        $uploadedFile->move(public_path().'/images/webclips/',$_POST['profile_id'].'_'.$fileName);
                        $iosWebclipSetting['icon'] = $fileName.'_'.$_POST['profile_id'];
                        $iosWebclipSetting = IosWebclipSetting::insertGetId([
                            'profile_id' => $_POST['profile_id'],
                            'webclip_removable_status' => $_POST['webclip_removable_status'],
                            'webclip_precomposed_icon_status' => $_POST['webclip_precomposed_icon_status'],
                            'webclip_fullscreen_status' => $_POST['webclip_fullscreen_status'],
                            'webclip_label' => $_POST['webclip_label'],
                            'webclip_url' => $_POST['webclip_url'],
                            'webclip_icon' => '/images/webclips/'.$_POST['profile_id'].'_'.$fileName,
                            'ios_webclip_settings_status' => 1,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);
                    }else{
                        $iosWebclipSetting ='Invalid Icon';
                    }
                }
            }
        }catch (Exception $ex){
            $statusCode = 404;
        }
        return response()->json($iosWebclipSetting, $statusCode);
    }

    public function profilewebclipget(){
        try {
            $webclip = [];
            $statusCode = 200;
            $webclip = IosWebclipSetting::where('profile_id', $_POST['profile_id'])->where('ios_webclip_settings_status', 1)->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($webclip, $statusCode);
    }

    public function profilewebclipdelete(){
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $response = IosWebclipSetting::where('id', $_POST['webclipId'])->update(['ios_webclip_settings_status' => 0]);
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function iosprofilewifiupdate(){
        $iosWifiSetting = [];
        try{
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $iosWifiSetting = IosWifiSetting::insertGetId([
                    'profile_id' => $_POST['profile_id'],
                    'wifi_auto_join_status' => $_POST['wifi_auto_join_status'],
                    'wifi_service_set_identifier' => $_POST['wifi_service_set_identifier'],
                    'wifi_hidden_network' => $_POST['wifi_hidden_network'],
                    'wifi_security_type' => $_POST['wifi_security_type'],
                    'wifi_security_password' => $_POST['wifi_security_password'],
                    'wifi_proxy' => $_POST['wifi_proxy'],
                    'ios_wifi_settings_status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
        }catch (Exception $ex){
            $statusCode = 404;
        }
        return response()->json($iosWifiSetting, $statusCode);
    }

    public function adminuserprofile($id = null){
        return view('configuration/adminuserprofile',compact('id'));
    }

    public function updateadminprofiletab(){
        $tabSetting = [];
        try{
            $statusCode = 200;
            if (Auth::getUser()->role_id == 1) {
                foreach($_POST['tab'] as $tab => $val){
                    $tabSettingExist = TabSetting::where('created_by', Auth::getUser()->id)->where('tab_name', $tab)->where('profile_type', $_POST['profile_type'])->get();
                    if ($tabSettingExist->count()) {
                        $tabSetting = TabSetting::where('id', $tabSettingExist[0]['id'])->update([
                            'tab_name' => $tab,
                            'tab_status' => $val,
                            'profile_type' => $_POST['profile_type'],
                            'created_by' => Auth::getUser()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);
                    }else{
                        $tabSetting = TabSetting::insertGetId([
                            'tab_name' => $tab,
                            'tab_status' => $val,
                            'profile_type' => $_POST['profile_type'],
                            'created_by' => Auth::getUser()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);
                    }
                }
            }
        }catch (Exception $ex){
            $statusCode = 404;
        }
        return response()->json($tabSetting, $statusCode);
    }

    public function getabsetting(){
        $tabSetting = [];
        try{
            $statusCode = 200;
            if (Auth::getUser()->role_id == 1) {
                $tabSetting = TabSetting::where('created_by', Auth::getUser()->id)->get();
            }
        }catch (Exception $ex){
            $statusCode = 404;
        }
        return response()->json($tabSetting, $statusCode);
    }

    public function certificatecreate(Request $request){
        try{
            $response = [];
            $statusCode = 200;
            if(Auth::getUser()->role_id < 4) {
                $fileName = $_FILES['file']['name'];
                    $uploadedFile = $request->file('file');
                    $extension = $uploadedFile->getClientOriginalExtension();
                    $actualFileName = explode('.' . $extension, $uploadedFile->getClientOriginalName());
                    $fileName = $actualFileName[0] . '_' . Auth::user()->id . '.' . $extension;
                    $extArray = array('png', 'jpeg', 'jpg', 'txt', 'csv', 'xls', 'doc', 'ula');
                    if (file_exists((resource_path() . '/uploads/' . $fileName))) {
                        $response[] = 'File Already Exist';
                    } else if (!(in_array($extension, $extArray))) {
                        $response[] = 'InValid File';
                    } else {
                        $uploadedFile->move(resource_path() . '/uploads/', $fileName);
                        $certificateExist = CredentialSetting::where('certificate_name', $fileName)
                            ->where('profile_id', $request->profile_id)
                            ->where('created_by', Auth::user()->id)
                            ->get();
                        if ($certificateExist->count()) {
                            CredentialSetting::where('id', $certificateExist[0]['id'])->update([
                                'certificate_name' => htmlentities($fileName),
                                'certificate_status' => 1,
                                'updated_at' => date('Y-m-d H:i:s')
                            ]);
                        } else {
                            CredentialSetting::insert([
                                'certificate_name' => htmlentities($fileName),
                                'certificate_status' => 1,
                                'profile_id' => $request->profile_id,
                                'created_by' => Auth::user()->id,
                                'created_at' => date('Y-m-d H:i:s')
                            ]);
                        }
                        $lastInsertId = CredentialSetting::latest('id')->first();
                        $response['lastId'] = $lastInsertId->id;
                    }
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        }catch (Exception $e){
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }
    
    public function getcertificatesetting(){
        $certificateSetting = [];
        try{
            $statusCode = 200;
                $certificateSetting = CredentialSetting::where('profile_id', $_POST['profileId'])->where('created_by', Auth::getUser()->id)->get();
        }catch (Exception $ex){
            $statusCode = 404;
        }
        return response()->json($certificateSetting, $statusCode);
    }

    public function profilecertificatedelete(){
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                CredentialSetting::where('id', $_POST['certificateId'])->update(['certificate_status' => 0]);
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function updateioswebfiler(){
        try {
            $response = [];
            $statusCode = 200;
            $urlList = [];
            if (Auth::getUser()->role_id < 4) {
                if(!empty($_POST['url'])){
                    $urlList = explode(',', $_POST['url']);
                }
                $count = 0;
                foreach($urlList as $url) {
                    if (!empty($url)) {
                        $urllistExist = IosWebfilerSetting::where('profile_id', $_POST['profile_id'])->where('webfiler_url', $url)->get();
                        if ($urllistExist->count()) {
                            $response = IosWebfilerSetting::where('id', $urllistExist[0]['id'])->update([
                                'profile_id' => $_POST['profile_id'],
                                'webfiler_url' => $url,
                                'webfiler_url_restrict_content_status' => ($_POST['restrict_inappropriate_content']) ? $_POST['restrict_inappropriate_content'] : '',
                                'webfiler_url_type' => $_POST['url_type'],
                                'webfiler_url_status' => 1,
                                'created_by' => Auth::getUser()->id,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s')
                            ]);
                        } else {
                            $response = IosWebfilerSetting::insertGetId([
                                'profile_id' => $_POST['profile_id'],
                                'webfiler_url' => $url,
                                'webfiler_url_restrict_content_status' => ($_POST['restrict_inappropriate_content']) ? $_POST['restrict_inappropriate_content'] : '',
                                'webfiler_url_type' => $_POST['url_type'],
                                'webfiler_url_status' => 1,
                                'created_by' => Auth::getUser()->id,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s')
                            ]);
                        }
                    }
                }
            }else{
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function getwebfilerurllist(){
        $webfilerSetting = [];
        try{
            $statusCode = 200;
                $webfilerSetting = IosWebfilerSetting::where('profile_id', $_POST['profile_id'])->where('created_by', Auth::getUser()->id)->where('webfiler_url_status', 1)->where('webfiler_url_type', $_POST['url_type'])->get();
        }catch (Exception $ex){
            $statusCode = 404;
        }
        return response()->json($webfilerSetting, $statusCode);
    }

    public function webfilerurldelete(){
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                if($_POST['currentTab'] == 'blackList'){
                    $response = IosWebfilerSetting::where('id', $_POST['itemId'])->where('webfiler_url_type', 1)->update(['webfiler_url_status' => 0]);
                }elseif($_POST['currentTab'] == 'whiteList'){
                    $response = IosWebfilerSetting::where('id', $_POST['itemId'])->where('webfiler_url_type', 2)->update(['webfiler_url_status' => 0]);
                }
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }
}