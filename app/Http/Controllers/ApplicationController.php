<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Filesystem\Filesystem;

use App\Http\Requests;
use App\Application;
use App\ProfileApplication;
use Validator;
use AWS;

class ApplicationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('application/applications');
    }
    protected function applicationValidator(array $data)
    {
        return Validator::make($data, [
            'file' => 'required|max:1000000',
        ]);
    }
    public function applicationcreate(Request $request)
    {
        try{
            $response = [];
            $statusCode = 200;
            if(Auth::getUser()->role_id < 4) {
                $fileName = $_FILES['file']['name'];
                if ($_FILES) {
                    $validator = $this->applicationValidator($request->all());
                }
                if ($validator->fails()) {
                    $response = $validator->errors();
                } else {
                    $uploadedFile = $request->file('file');
                    $extension = $uploadedFile->getClientOriginalExtension();
                    $actualFileName = explode('.' . $extension, $uploadedFile->getClientOriginalName());
                    $fileName = $actualFileName[0] . '_' . Auth::user()->id . '.' . $extension;
                    $extArray = array('apk', 'png', 'jpeg', 'jpg', 'txt', 'csv', 'xls', 'doc', 'mp4', 'avi', 'mp3', 'ogg', 'qmg', 'gif', '3gp', 'webm', 'mkv', 'ts', 'ula');
                    if (file_exists((resource_path() . '/uploads/' . $fileName))) {
                        $response[] = 'File Already Exist';
                    } else if (!(in_array($extension, $extArray))) {
                        $response[] = 'InValid File';
                    } else {
                        $uploadedFile->move(resource_path() . '/uploads/', $fileName);
                        $packageFileName = resource_path() . '/uploads/' . $fileName;
                        if ($extension == 'apk') {
                            $packageName = shell_exec('/usr/bin/aapt dump badging ' . $packageFileName . ' | grep package:\ name');
                            $versionCodeMatches = array();
                            $packageNameMatches = array();
                            preg_match("/name='(.*?)\'/s", $packageName, $packageNameMatches);
                            preg_match("/versionCode='(.*?)\'/s", $packageName, $versionCodeMatches);
                            $versionCode = $versionCodeMatches[1];
                            $packageName = $packageNameMatches[1];
                        } else {
                            $versionCode = 1;
                            $packageName = $fileName;
                        }
                        $applicationExist = Application::where('application_package', $packageName)
                            ->where('application_version', $versionCode)
                            ->where('created_by', Auth::user()->id)
                            ->get();
                        if ($applicationExist->count()) {
                            Application::where('id', $applicationExist[0]['id'])->update([
                                'application_name' => htmlentities($fileName),
                                'application_status' => 1,
                                'updated_at' => date('Y-m-d H:i:s')
                            ]);
                        } else {
                            Application::insert([
                                'application_name' => htmlentities($fileName),
                                'application_version' => htmlentities($versionCode),
                                'application_package' => htmlentities($packageName),
                                'application_status' => 1,
                                'created_by' => Auth::user()->id,
                                'created_at' => date('Y-m-d H:i:s')
                            ]);
                        }
                        /*
                         * Amazon S3 upload code
                         */
                        /* $s3 = \Storage::disk('s3');
                         $filePath = '/uploads/'.$uploadedFile->getClientOriginalName();
                         $s3->put($filePath, file_get_contents($uploadedFile), 'public'); */
                        $lastInsertId = Application::latest('id')->first();
                        $response['lastId'] = $lastInsertId->id;
                    }
                }
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        }catch (Exception $e){
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function applicationlist()
    {
        try{
            $response = [];
            $statusCode = 200;
            if(Auth::getUser()->role_id == 3) {
                $response = Application::where('created_by', Auth::getUser()->id)->where('application_status', 1)->orderBy('application_name','asc')->get();
            } elseif(Auth::getUser()->role_id == 2) {
                $response = Application::where('created_by', Auth::user()->id)->where('application_status', 1)->orderBy('application_name','asc')->get();
            } elseif(Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                $response = Application::where('application_status', 1)->orderBy('application_name','asc')->get();
            } elseif(Auth::getUser()->role_id == 4){
                $response = Application::where('created_by', Auth::getUser()->master_user)->where('application_status', 1)->orderBy('application_name','asc')->get();
            }
        }catch (Exception $e){
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function applicationdelete()
    {
        try{
            $statusCode = 200;
            if(Auth::getUser()->role_id < 4) {
                $appObj = Application::find($_POST['applicationId']);
                $response = $appObj->application_package;
                $fileName = $appObj->application_name;
                $app_delete = Application::where('id', $_POST['applicationId'])->update(['application_status' => 0]);
                if ($app_delete) {
                    rename(resource_path() . '/uploads/' . $fileName, resource_path() . '/uploads/' . strtotime(date('Y-m-d H:i:s')) . '-' . $fileName);
                }
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        }catch (Exception $e){
            $statusCode = 404;
        }
        return response()->json($response, $statusCode );
    }

    public function disableapplication()
    {
        return view('application/disableapplications');
    }
    
    public function allapplicationlist()
    {
        try{
            $response = [];
            $statusCode = 200;
            $response = DB::table('applications')->where('created_by', Auth::user()->id)->where('application_status', 1)
                    ->whereNotIn('id', function($q){
                $q->select('application_id')->from('profile_applications')->where('profile_application_status', 1)->where('profile_id', $_POST['profile_id']);
            })->get();
           // $response[] = Application::where('created_by', Auth::user()->id)->where('application_status', 1)->get();
        }catch (Exception $e){
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }
}