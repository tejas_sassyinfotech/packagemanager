<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Licence;

class LicenceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('licence/licences');
    }
    public function licensecreate()
    {
        try{
            $response = [];
            $statusCode = 200;
            $profileLicenceExist = Licence::where('licence_name', trim($_POST['licence_name']))
                                          ->where('profile_id', $_POST['profile_id'])->get();
            if ($profileLicenceExist->count()) {
                Licence::where('id', $profileLicenceExist[0]->id)->update([
                    'licence_code' => htmlentities($_POST['license_code']),
                    'licence_description' => htmlentities($_POST['licence_description']),
                    'maximum_quantity' => htmlentities($_POST['quantity']),
                    'used_quantity' => 0,
                    'licence_status' => 1,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            } else {
                Licence::insert([
                    'licence_name' => htmlentities($_POST['licence_name']),
                    'licence_code' => htmlentities($_POST['license_code']),
                    'licence_description' => htmlentities($_POST['licence_description']),
                    'profile_id' => $_POST['profile_id'],
                    'maximum_quantity' => htmlentities($_POST['quantity']),
                    'used_quantity' => 0,
                    'licence_status' => 1,
                    'created_by' => Auth::user()->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
        }catch (Exception $e){
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }
    
    public function licenselist()
    {
        try{
            $response = [];
            $statusCode = 200;
            if(Auth::getUser()->role_id == 4 || Auth::getUser()->role_id == 3) {
                $response = Licence::where('created_by', Auth::getUser()->master_user)->where('licence_status', 1)->with('UserProfile')->get();
            } elseif(Auth::getUser()->role_id == 2) {
                $response = Licence::where('created_by', Auth::user()->id)->where('licence_status', 1)->with('UserProfile')->get();
            } elseif(Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                $response = Licence::where('licence_status', 1)->with('UserProfile')->get();
            }
        }catch (Exception $e){
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }
    
    public function licencedelete()
    {
        try{
            $licenseObj = Licence::find($_POST['licenseId']);   
            $response = $licenseObj->licence_name;
            $statusCode = 200;
            Licence::where('id', $_POST['licenseId'])->update(['licence_status' => 0]);
        }catch (Exception $e){
            $statusCode = 404;
        }
        return response()->json($response, $statusCode );
    }

}
