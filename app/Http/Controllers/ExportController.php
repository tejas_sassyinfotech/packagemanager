<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Request;
use Excel;
use App\UserProfile;
use App\DisableApplication;
use App\ProfileDevice;
use App\DeviceWipe;
class ExportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function exportdisableaplication($profileId)
    {
        try{
            $response = [];
            $statusCode = 200;
            $data = DisableApplication::select('disabled_application_name')->where('profile_id', $profileId)->where('disabled_application_status', 1)->get()->toArray();
        }catch (Exception $e){
            $statusCode = 404;
        }
        return Excel::create('disabled_application_'.$profileId, function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->download('xlsx');
    }

    public function exportdevice($profileId = null)
    {
        try{
            $controller = new Controller();
            $response = [];
            $statusCode = 200;
            if($profileId) {
                $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->select('device_serial_number as Serial_Number','device_identifier as Device_Identifier','device_osp_client_version as MDM','profile_name as Profile_Name','device_profile_counter as Counter','device_battery_level as Battery','profile_devices.device_location as Location','profile_devices.device_additional_info as Additional_Info','profile_devices.updated_at as Last_Update')
                    ->where('profile_devices.profile_id', $profileId)
                    ->where('profile_devices.device_status', 1)
                    ->get()->toArray();
                if (Auth::getUser()->role_id == 3) {
                    $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->select('device_serial_number as Serial_Number','device_identifier as Device_Identifier','device_osp_client_version as MDM','profile_name as Profile_Name','device_profile_counter as Counter','device_battery_level as Battery','profile_devices.device_location as Location','profile_devices.device_additional_info as Additional_Info','profile_devices.updated_at as Last_Update')
                        ->where('profile_devices.profile_id', $profileId)
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->id)->get()->toArray();
                } elseif (Auth::getUser()->role_id == 2) {
                    $idArray = $controller->AdminProfileIds();
                    $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->select('device_serial_number as Serial_Number','device_identifier as Device_Identifier','device_osp_client_version as MDM','profile_name as Profile_Name','device_profile_counter as Counter','device_battery_level as Battery','profile_devices.device_location as Location','profile_devices.device_additional_info as Additional_Info','profile_devices.updated_at as Last_Update')
                        ->where('profile_devices.profile_id', $profileId)
                        ->whereIn('profile_devices.profile_id',array_values($idArray))
                        ->where('device_status', 1)
                        ->get()->toArray();
                } elseif (Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                    $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1)
                        ->select('device_serial_number as Serial_Number','device_identifier as Device_Identifier','device_osp_client_version as MDM','profile_name as Profile_Name','device_profile_counter as Counter','device_battery_level as Battery','profile_devices.device_location as Location','profile_devices.device_additional_info as Additional_Info','profile_devices.updated_at as Last_Update')
                        ->where('profile_devices.profile_id', $profileId)
                        ->get()->toArray();
                } elseif (Auth::getUser()->role_id == 4) {
                    $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->select('device_serial_number as Serial_Number','device_identifier as Device_Identifier','device_osp_client_version as MDM','profile_name as Profile_Name','device_profile_counter as Counter','device_battery_level as Battery','profile_devices.device_location as Location','profile_devices.device_additional_info as Additional_Info','profile_devices.updated_at as Last_Update')
                        ->where('profile_devices.profile_id', $profileId)
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->master_user)
                        ->get()->toArray();
                }
            }else {
                if (Auth::getUser()->role_id == 3) {
                    $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->select('device_serial_number as Serial_Number','device_identifier as Device_Identifier','device_osp_client_version as MDM','profile_name as Profile_Name','device_profile_counter as Counter','device_battery_level as Battery','profile_devices.device_location as Location','profile_devices.device_additional_info as Additional_Info','profile_devices.updated_at as Last_Update')
                        ->where('profile_devices.device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->id)
                        ->get()->toArray();
                } elseif (Auth::getUser()->role_id == 2) {
                    $idArray = $controller->AdminProfileIds();
                    $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->select('device_serial_number as Serial_Number','device_identifier as Device_Identifier','device_osp_client_version as MDM','profile_name as Profile_Name','device_profile_counter as Counter','device_battery_level as Battery','profile_devices.device_location as Location','profile_devices.device_additional_info as Additional_Info','profile_devices.updated_at as Last_Update')
                        ->whereIn('profile_devices.profile_id',array_values($idArray))
                        ->where('device_status', 1)
                        ->get()->toArray();
                } elseif (Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                    $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->select('device_serial_number as Serial_Number','device_identifier as Device_Identifier','device_osp_client_version as MDM','profile_name as Profile_Name','device_profile_counter as Counter','device_battery_level as Battery','profile_devices.device_location as Location','profile_devices.device_additional_info as Additional_Info','profile_devices.updated_at as Last_Update')
                        ->where('device_status', 1)
                        ->get()->toArray();
                } elseif (Auth::getUser()->role_id == 4) {
                    $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->select('device_serial_number as Serial_Number','device_identifier as Device_Identifier','device_osp_client_version as MDM','profile_name as Profile_Name','device_profile_counter as Counter','device_battery_level as Battery','profile_devices.device_location as Location','profile_devices.device_additional_info as Additional_Info','profile_devices.updated_at as Last_Update')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->master_user)
                        ->get()->toArray();

                }
            }

        }catch (Exception $e){
            $statusCode = 404;
        }
        return Excel::create('devices_'.$profileId, function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->download('xlsx');
    }

    public function deviceactivationexcel($fromDate=null,$toDate=null)
    {
        try {
            $data = [];
            $statusCode = 200;
            if (!empty($fromDate) && !empty($toDate)) {
                $fromDate = date('Y-m-d H:i:s', strtotime($fromDate));
                $toDate = date('Y-m-d H:i:s', strtotime($toDate));
                if(Auth::getUser()->role_id == 3) {
                    $data = ProfileDevice::whereBetween('device_activation_date', [$fromDate,$toDate])
                        ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->id)
                        ->select('device_serial_number as Serial_Number','device_identifier as Device_Identifier','device_osp_client_version as MDM','profile_name as Profile_Name','device_profile_counter as Counter','device_battery_level as Battery','profile_devices.updated_at as Last_Update','profile_devices.device_location as Location','profile_devices.device_additional_info as Additional_Info')
                        ->get()->toArray();
                } elseif(Auth::getUser()->role_id == 2) {
                    $data = ProfileDevice::whereBetween('device_activation_date', [$fromDate,$toDate])
                        ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->where('users.master_user', Auth::getUser()->id)
                        ->orWhere('user_profiles.created_by', Auth::getUser()->id)
                        ->where('users.role_id', '>=', Auth::getUser()->role_id)
                        ->select('device_serial_number as Serial_Number','device_identifier as Device_Identifier','device_osp_client_version as MDM','profile_name as Profile_Name','device_profile_counter as Counter','device_battery_level as Battery','profile_devices.updated_at as Last_Update','profile_devices.device_location as Location','profile_devices.device_additional_info as Additional_Info')
                        ->get()->toArray();
                }  elseif(Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                    $data = ProfileDevice::whereBetween('device_activation_date', [$fromDate,$toDate])
                        ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1)
                        ->select('device_serial_number as Serial_Number','device_identifier as Device_Identifier','device_osp_client_version as MDM','profile_name as Profile_Name','device_profile_counter as Counter','device_battery_level as Battery','profile_devices.updated_at as Last_Update','profile_devices.device_location as Location','profile_devices.device_additional_info as Additional_Info')
                        ->get()->toArray();
                } elseif(Auth::getUser()->role_id == 4) {
                    $data = ProfileDevice::whereBetween('device_activation_date', [$fromDate,$toDate])
                        ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by',Auth::getUser()->master_user)
                        ->select('device_serial_number as Serial_Number','device_identifier as Device_Identifier','device_osp_client_version as MDM','profile_name as Profile_Name','device_profile_counter as Counter','device_battery_level as Battery','profile_devices.updated_at as Last_Update','profile_devices.device_location as Location','profile_devices.device_additional_info as Additional_Info')
                        ->get()->toArray();
                }else{
                    $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by',Auth::getUser()->master_user)
                        ->select('device_serial_number as Serial_Number','device_identifier as Device_Identifier','device_osp_client_version as MDM','profile_name as Profile_Name','device_profile_counter as Counter','device_battery_level as Battery','profile_devices.device_location as Location','profile_devices.device_additional_info as Additional_Info','profile_devices.updated_at as Last_Update')
                        ->get()->toArray();
                }
            }else{
                $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=',  date('Y'))
                    ->select('device_serial_number as Serial_Number','device_identifier as Device_Identifier','device_osp_client_version as MDM','profile_name as Profile_Name','device_profile_counter as Counter','device_battery_level as Battery','profile_devices.device_location as Location','profile_devices.device_additional_info as Additional_Info','profile_devices.updated_at as Last_Update')
                    ->orderBy('device_activation_date', 'asc')->get();
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return Excel::create('activated_device_'.Auth::getUser()->id, function ($excel) use ($data) {
            $excel->sheet('Activation List', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->download('xlsx');
    }

    public function deviceregistrationexcel($profileId = null)
    {
        try {
            $data = [];
            $statusCode = 200;
            if (!empty($profileId)) {
                if(Auth::getUser()->role_id == 3) {
                    $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('device_registrations', 'profile_devices.id', '=', 'device_registrations.device_id')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->id)
                        ->where('profile_devices.profile_id', $profileId)
                        ->select('device_identifier as Device_Identifier','device_serial_number as Serial_Number',DB::raw('(CASE WHEN registration_status = 1 THEN "Active" ELSE "End" END) AS Status'),'patient_name as User','patient_name as Admin','start_date as Start_Date','end_date as End_Date')
                        ->get();
                } elseif(Auth::getUser()->role_id == 2) {
                    $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('device_registrations', 'profile_devices.id', '=', 'device_registrations.device_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->where('users.master_user', Auth::getUser()->id)
                        ->orWhere('user_profiles.created_by', Auth::getUser()->id)
                        ->where('users.role_id', '>', Auth::getUser()->role_id)
                        ->where('profile_devices.profile_id', $profileId)
                        ->select('device_identifier as Device_Identifier','device_serial_number as Serial_Number',DB::raw('(CASE WHEN registration_status = 1 THEN "Active" ELSE "End" END) AS Status'),'patient_name as User','patient_name as Admin','start_date as Start_Date','end_date as End_Date')
                        ->get();
                }  elseif(Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                    $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('device_registrations', 'profile_devices.id', '=', 'device_registrations.device_id')
                        ->where('device_status', 1)
                        ->where('profile_devices.profile_id', $profileId)
                        ->select('device_identifier as Device_Identifier','device_serial_number as Serial_Number',DB::raw('(CASE WHEN registration_status = 1 THEN "Active" ELSE "End" END) AS Status'),'patient_name as User','patient_name as Admin','start_date as Start_Date','end_date as End_Date')
                        ->get();
                } elseif(Auth::getUser()->role_id == 4) {
                    $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->join('device_registrations', 'profile_devices.id', '=', 'device_registrations.device_id')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->master_user)
                        ->where('profile_devices.profile_id', $profileId)
                        ->select('device_identifier as Device_Identifier','device_serial_number as Serial_Number',DB::raw('(CASE WHEN registration_status = 1 THEN "Active" ELSE "End" END) AS Status'),'patient_name as User','patient_name as Admin','start_date as Start_Date','end_date as End_Date')
                        ->get();
                }
            }else{
                $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->join('device_registrations', 'profile_devices.id', '=', 'device_registrations.device_id')
                    ->where('device_status', 1)
                    ->whereYear('start_date', '=',  date('Y'))
                    ->select('device_identifier as Device_Identifier','device_serial_number as Serial_Number',DB::raw('(CASE WHEN registration_status = 1 THEN "Active" ELSE "End" END) AS Status'),'patient_name as User','patient_name as Admin','start_date as Start_Date','end_date as End_Date')
                    ->orderBy('start_date', 'asc')->get();
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return Excel::create('device_registration_'.Auth::getUser()->id, function ($excel) use ($data) {
            $excel->sheet('Registration List', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->download('xlsx');
    }

    public function devicewipeexcel($profileId = null)
    {
        try {
            $data = [];
            $statusCode = 200;
            if (!empty($profileId)) {
                if(Auth::getUser()->role_id == 3) {
                    $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('device_wipes', 'profile_devices.id', '=', 'device_wipes.device_id')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->id)
                        ->where('profile_devices.profile_id', $profileId)
                        ->select('device_identifier as Device_Identifier','device_serial_number as Serial_Number','wipe_description as Wipe_Description','wipe_result as Wipe_Result','wipe_reason as Wipe_Reason','wipe_additional_info as Wipe_Additional_Info','wipe_initiated as Wipe_Initiated','wipe_date as Wipe_Date')
                        ->get();
                } elseif(Auth::getUser()->role_id == 2) {
                    $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('device_wipes', 'profile_devices.id', '=', 'device_wipes.device_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->where('users.master_user', Auth::getUser()->id)
                        ->orWhere('user_profiles.created_by', Auth::getUser()->id)
                        ->where('users.role_id', '>', Auth::getUser()->role_id)
                        ->where('profile_devices.profile_id', $profileId)
                        ->select('device_identifier as Device_Identifier','device_serial_number as Serial_Number','wipe_description as Wipe_Description','wipe_result as Wipe_Result','wipe_reason as Wipe_Reason','wipe_additional_info as Wipe_Additional_Info','wipe_initiated as Wipe_Initiated','wipe_date as Wipe_Date')
                        ->get();
                }  elseif(Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                    $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('device_wipes', 'profile_devices.id', '=', 'device_wipes.device_id')
                        ->where('device_status', 1)
                        ->where('profile_devices.profile_id', $profileId)
                        ->select('device_identifier as Device_Identifier','device_serial_number as Serial_Number','wipe_description as Wipe_Description','wipe_result as Wipe_Result','wipe_reason as Wipe_Reason','wipe_additional_info as Wipe_Additional_Info','wipe_initiated as Wipe_Initiated','wipe_date as Wipe_Date')
                        ->get();
                } elseif(Auth::getUser()->role_id == 4) {
                    $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->join('device_wipes', 'profile_devices.id', '=', 'device_wipes.device_id')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->master_user)
                        ->where('profile_devices.profile_id', $profileId)
                        ->select('device_identifier as Device_Identifier','device_serial_number as Serial_Number','wipe_description as Wipe_Description','wipe_result as Wipe_Result','wipe_reason as Wipe_Reason','wipe_additional_info as Wipe_Additional_Info','wipe_initiated as Wipe_Initiated','wipe_date as Wipe_Date')
                        ->get();
                }
            } else {
                $data = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->join('device_wipes', 'profile_devices.id', '=', 'device_wipes.device_id')
                    ->where('device_status', 1)
                    ->whereYear('start_date', '=',  date('Y'))
                    ->select('device_identifier as Device_Identifier','device_serial_number as Serial_Number','wipe_description as Wipe_Description','wipe_result as Wipe_Result','wipe_reason as Wipe_Reason','wipe_additional_info as Wipe_Additional_Info','wipe_initiated as Wipe_Initiated','wipe_date as Wipe_Date')
                    ->orderBy('start_date', 'asc')->get();
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return Excel::create('device_wipes_'.Auth::getUser()->id, function ($excel) use ($data) {
            $excel->sheet('Registration List', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->download('xlsx');
    }
}
