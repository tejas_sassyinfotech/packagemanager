<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Edujugon\PushNotification\PushNotification;
use App\Http\Requests;
use App\ProfileDevice;
use App\UserProfile;

class PushNotificationController extends Controller
{
    public function sendNotificationToDevice()
    {
        if($_POST['command'] == 'all') {
            $devicePushNotifivation = $this->getDeviceCollection($_POST['profile_id'], $_POST['command']);
            if ($devicePushNotifivation) {
                $this->setProfileCounter($_POST['profile_id']);
                return response()->json(['error' => 'push_notification_sent'], 200);
            } else {
                return response()->json(['error' => 'invalid_device_details'], 500);
            }
        } elseif($_POST['command'] == 'rebootall') {
            $devicePushNotifivation = $this->getDeviceCollection($_POST['profile_id'], $_POST['command']);
            if ($devicePushNotifivation) {
                $this->setProfileCounter($_POST['profile_id']);
                return response()->json(['error' => 'push_notification_sent'], 200);
            } else {
                return response()->json(['error' => 'invalid_device_details'], 500);
            }
        } else {
            $deviceToken = $this->getDeviceGCMToken($_POST['profile_id'], $_POST['device_id']);
            if ($deviceToken) {
                $message = $deviceToken->device_serial_number . ',' . $_POST['command'];
                // Send the notification to the device with a token of $deviceToken
                $push = new PushNotification('fcm');
                $push->setMessage(['message'=>$message])
                    ->setApiKey('AIzaSyAraU2rHyDzGKoO3U-qFTNGlejAnvpIJeg')
                    ->setDevicesToken($deviceToken->device_gcm_token)
                    ->setConfig(['dry_run' => false]);
                $push->send();
                return response()->json(['error' => 'push_notification_sent'], 200);
            } else {
                return response()->json(['error' => 'invalid_device_details'], 500);
            }
        }
    }

    public function getDeviceGCMToken($profileId, $deviceId)
    {
        $response = ProfileDevice::where('profile_id', $profileId)->where('id', $deviceId)->first();
        if($response) {
            return $response;
        } else {
            return false;
        }
    }

    public function getDeviceCollection($profileId, $command)
    {
        $messageToSent = 'all';
        $profileDetails = ProfileDevice::where('profile_id', $profileId)->get();
        if($profileDetails) {
            foreach ($profileDetails as $profileDevice) {
                $push = new PushNotification('fcm');
                if($command == 'rebootall') {
                    $messageToSent = $profileDevice->device_serial_number . ',' . 'restart';
                }
                $push->setMessage(['message' => $messageToSent])
                    ->setApiKey('AIzaSyAraU2rHyDzGKoO3U-qFTNGlejAnvpIJeg')
                    ->setDevicesToken($profileDevice->device_gcm_token)
                    ->setConfig(['dry_run' => false]);
                $push->send();
            }
            return 1;
        } else {
            return false;
        }
    }

    public function setProfileCounter($profileId)
    {
        $userProfileIncrement =  UserProfile::where('id', $profileId)->increment('profile_counter');
        return $userProfileIncrement;
    }
}
