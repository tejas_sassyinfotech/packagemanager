<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\ProfileDevice;
use App\DeviceLog;
use App\DeviceApplication;
use App\UserProfile;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Symfony\Component\HttpKernel\Profiler\Profile;

class DeviceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('device/devices');
    }

    public function devicelist()
    {
        try {
            $response = [];
            $statusCode = 200;
            $response = ProfileDevice::where('device_status', 1)->with('UserProfile')->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function userdevicelist($index = null)
    {
        try {
            $controller = new Controller();
            $response = [];
            $statusCode = 200;
            if($index){
                if (Auth::getUser()->role_id == 3) {
                    $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1)
                        ->whereYear('device_activation_date', '=', date('Y'))
                        ->where('user_profiles.created_by', Auth::getUser()->id)->get(['profile_devices.id AS did', 'user_profiles.*', 'profile_devices.*']);
                } elseif (Auth::getUser()->role_id == 2) {
                    $idArray = $controller->AdminProfileIds();
                    $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->whereIn('profile_devices.profile_id',array_values($idArray))
                        ->where('device_status', 1)
                        ->whereYear('device_activation_date', '=', date('Y'))
                        ->get(['profile_devices.id AS did', 'user_profiles.*', 'profile_devices.*']);
                   /* $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->whereYear('device_activation_date', '=', date('Y'))
                        ->where('users.master_user', Auth::getUser()->id)
                        ->orWhere('user_profiles.created_by', Auth::getUser()->id)
                        ->where('users.role_id', '>=', Auth::getUser()->role_id)
                        ->get(['profile_devices.id AS did', 'user_profiles.*', 'profile_devices.*']);*/
                } elseif (Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                    $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1)
                        ->whereYear('device_activation_date', '=', date('Y'))
                        ->get(['profile_devices.id AS did', 'user_profiles.*', 'profile_devices.*']);
                } elseif (Auth::getUser()->role_id == 4) {
                    $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->whereYear('device_activation_date', '=', date('Y'))
                        ->where('user_profiles.created_by', Auth::getUser()->master_user)
                        ->get(['profile_devices.id AS did', 'user_profiles.*', 'profile_devices.*']);
                }
            }else {
                if (Auth::getUser()->role_id == 3) {
                    $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->id)->get(['profile_devices.id AS did', 'user_profiles.*', 'profile_devices.*']);
                } elseif (Auth::getUser()->role_id == 2) {
                    $idArray = $controller->AdminProfileIds();
                    $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->whereIn('profile_devices.profile_id',array_values($idArray))
                        ->where('device_status', 1)
                        ->get(['profile_devices.id AS did', 'user_profiles.*', 'profile_devices.*']);
                    /*$response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->where('users.master_user', Auth::getUser()->id)
                        ->orWhere('user_profiles.created_by', Auth::getUser()->id)
                        ->where('users.role_id', '>=', Auth::getUser()->role_id)
                        ->get(['profile_devices.id AS did', 'user_profiles.*', 'profile_devices.*']);*/
                } elseif (Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                    $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1)
                        ->get(['profile_devices.id AS did', 'user_profiles.*', 'profile_devices.*']);
                } elseif (Auth::getUser()->role_id == 4) {
                    $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->master_user)
                        ->get(['profile_devices.id AS did', 'user_profiles.*', 'profile_devices.*']);

                }
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function userdevicemappoints()
    {
        try {
            $controller = new Controller();
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id == 3) {
                $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->select('device_identifier', 'device_location')
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->where('device_status', 1)
                    ->where('created_by', Auth::getUser()->master_user)->get()->toArray();
            } elseif (Auth::getUser()->role_id == 2) {
                $idArray = $controller->AdminProfileIds();
                $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->select('device_identifier', 'device_location')
                    ->whereIn('profile_devices.profile_id',array_values($idArray))
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->get()->toArray();

                /*$response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->select('device_identifier', 'device_location')
                    ->join('users', 'users.id', '=', 'user_profiles.created_by')
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->where('users.master_user', Auth::getUser()->id)
                    ->orWhere('user_profiles.created_by', Auth::getUser()->id)
                    ->where('users.role_id', '>=', Auth::getUser()->role_id)
                    ->get()->toArray();*/
            } elseif (Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->select('device_identifier', 'device_location')
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->get()->toArray();
            } elseif (Auth::getUser()->role_id == 4) {
                $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->join('users', 'users.id', '=', 'user_profiles.created_by')
                    ->select('device_identifier', 'device_location')
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->where('user_profiles.created_by', Auth::getUser()->master_user)
                    ->get()->toArray();
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function userdevicecount()
    {
        try {
            $controller = new Controller();
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id == 3) {
                $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->where('created_by', Auth::getUser()->id)->count();
            } elseif (Auth::getUser()->role_id == 2) {
                $idArray = $controller->AdminProfileIds();
                $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->whereIn('profile_devices.profile_id',array_values($idArray))
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->count();
                /*

                $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->join('users', 'users.id', '=', 'user_profiles.created_by')
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->where('users.master_user', Auth::getUser()->id)
                    ->orWhere('user_profiles.created_by', Auth::getUser()->id)
                    ->where('users.role_id', '>=', Auth::getUser()->role_id)
                    ->count();*/
            } elseif (Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->count();
            } elseif (Auth::getUser()->role_id == 4) {
                $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->join('users', 'users.id', '=', 'user_profiles.created_by')
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->where('user_profiles.created_by', Auth::getUser()->master_user)
                    ->count();
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function userdevicestatuscount()
    {
        try {
            $controller = new Controller();
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id == 3) {
                $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->where('device_status', 1)
                    ->where('provision_status', $_POST['provisionStatus'])
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->where('created_by', Auth::getUser()->id)->count();
            } elseif (Auth::getUser()->role_id == 2) {
                $idArray = $controller->AdminProfileIds();
                $response = ProfileDevice::whereIn('profile_devices.profile_id',array_values($idArray))
                    ->where('device_status', 1)
                    ->where('provision_status', $_POST['provisionStatus'])
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->count();
            } elseif (Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->where('provision_status', $_POST['provisionStatus'])->count();
            }elseif(Auth::getUser()->role_id == 4){
                    $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->whereYear('device_activation_date', '=', date('Y'))
                        ->where('user_profiles.created_by', Auth::getUser()->master_user)
                        ->where('provision_status', $_POST['provisionStatus'])
                        ->count();
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function getdevice()
    {
        return view('device/device');
    }

    public function devicelog()
    {
        try {
            $response = [];
            $statusCode = 200;
            $response = ProfileDevice::where('device_serial_number', $_POST['serialNumber'])->with('UserProfile')->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function devicetrace()
    {
        try {
            $response = [];
            $statusCode = 200;
            $response = DB::connection('mysql_external')->table('device_logs')->where('device_serial_number', $_POST['serialNumber'])
                ->orderBy('created_at', 'desc')
                ->limit(500)
                ->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function deletedevicetrace($deviceSerialNumber = null)
    {
        try {
            $response = [];
            $statusCode = 200;
            if ($deviceSerialNumber) {
                DB::connection('mysql_external')->table('device_logs')->where('device_serial_number', $deviceSerialNumber)->delete();
                $response = "Deleted logs of " . $deviceSerialNumber;
            } else {
                DB::connection('mysql_external')->table('device_logs')->truncate();
                $response = "Deleted all logs";
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function deviceapplication()
    {
        try {
            $response = [];
            $statusCode = 200;
            $response = DeviceApplication::where('device_serial_number', $_POST['serialNumber'])
                ->orderBy('created_at', 'desc')
                ->limit(150)
                ->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);

    }

    public function userdevicechart()
    {
        try {
            $controller = new Controller();
            $response = [];
            $responseMonth = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id == 3) {
                $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->where('device_status', 1)
                    ->where('created_by', Auth::getUser()->master_user)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->orderBy('device_activation_date', 'asc')->get();
            } elseif (Auth::getUser()->role_id == 2) {
                $idArray = $controller->AdminProfileIds();
                $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->whereIn('profile_devices.profile_id',array_values($idArray))
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->orderBy('device_activation_date', 'asc')
                    ->get();
/*

                $response = ProfileDevice::whereYear('device_activation_date', '=', date('Y'))
                    ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->join('users', 'users.id', '=', 'user_profiles.created_by')
                    ->where('device_status', 1)
                    ->where('users.master_user', Auth::getUser()->id)
                    ->orWhere('user_profiles.created_by', Auth::getUser()->id)
                    ->where('users.role_id', '>=', Auth::getUser()->role_id)
                    ->orderBy('device_activation_date', 'asc')
                    ->get();*/
            } elseif (Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->orderBy('device_activation_date', 'asc')->get();
            }elseif(Auth::getUser()->role_id == 4){
                $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->join('users', 'users.id', '=', 'user_profiles.created_by')
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->where('user_profiles.created_by', Auth::getUser()->master_user)
                    ->get();
            }
            if (!empty($response)) {
                $monthArray = array();
                foreach ($response as $response) {
                    $monthArray[] = date('F', strtotime($response['device_activation_date']));
                }
                $responseMonth = array_count_values($monthArray);
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return json_encode(array('month' => $responseMonth, 'statusCode' => $statusCode));
    }


    public function deviceosmodelchart()
    {
        try {
            $controller = new Controller();
            $responses = [];
            $responseModel = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id == 3) {
                $responses = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->where('created_by', Auth::getUser()->master_user)->get();
            } elseif (Auth::getUser()->role_id == 2) {

                $idArray = $controller->AdminProfileIds();
                $responses = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->whereIn('profile_devices.profile_id',array_values($idArray))
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->get();

                /*$responses = ProfileDevice::whereYear('device_activation_date', '=', date('Y'))
                    ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->join('users', 'users.id', '=', 'user_profiles.created_by')
                    ->where('device_status', 1)
                    ->where('users.master_user', Auth::getUser()->id)
                    ->orWhere('user_profiles.created_by', Auth::getUser()->id)
                    ->where('users.role_id', '>=', Auth::getUser()->role_id)
                    ->get();*/
            } elseif (Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                $responses = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->get();
            } elseif(Auth::getUser()->role_id == 4) {
                $responses = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->join('users', 'users.id', '=', 'user_profiles.created_by')
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->where('user_profiles.created_by', Auth::getUser()->master_user)
                    ->get();
            }
            if (!empty($responses)) {
                $modelArray = array();
                $OperatingSystemArray = array();
                foreach ($responses as $response) {
                    if (!empty($response['device_model_number'])) {
                        $modelArray[] = $response['device_model_number'];
                    }
                    if (!empty($response['device_operating_system'])) {
                        $OperatingSystemArray[] = $response['device_operating_system'];
                    }
                }
                $responseModel = array_count_values($modelArray);
                $responseOperatingSystem = array_count_values($OperatingSystemArray);
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return json_encode(array('model' => $responseModel, 'os' => $responseOperatingSystem, 'statusCode' => $statusCode));
    }


    public function dashboardfilterlist()
    {
        try {
            $responseTable = [];
            $responseDashboardCount = 0;
            $provisionCount = 0;
            $unprovisionCount = 0;
            $responseProvisonCount = 0;
            $responseUnprovisonCount = 0;
            $responseModel = 0;
            $responseOperatingSystem = 0;
            $statusCode = 200;
            if (Auth::getUser()->role_id == 3) {
                if(!empty($_POST['profileId']) || (!empty($_POST['fromDate']) && !empty($_POST['fromDate']))){
                    $query = DB::connection('mysql')->table('profile_devices')
                            ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                            ->where('device_status', 1)
                            ->where('user_profiles.created_by', Auth::getUser()->id);
                    if(!empty($_POST['profileId'])){
                        $query->where('profile_devices.profile_id', $_POST['profileId']);
                    }
                    if(!empty($_POST['fromDate']) && !empty($_POST['toDate'])){
                        $query->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                    }
                    $responseTable = $query->get();
                }else{
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->where('device_status', 1)
                    ->where('user_profiles.created_by', Auth::getUser()->id)->get();
                }
            } elseif (Auth::getUser()->role_id == 2) {
                if(!empty($_POST['profileId']) || (!empty($_POST['fromDate']) && !empty($_POST['fromDate']))){
                    $query = DB::connection('mysql')->table('profile_devices')
                        ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1);
                    if(!empty($_POST['profileId'])){
                        $query->where('profile_devices.profile_id', $_POST['profileId']);
                    }
                    if(!empty($_POST['fromDate']) && !empty($_POST['toDate'])){
                        $query->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                    }
                    $responseTable = $query->get();
                }else{

                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->where('users.master_user', Auth::getUser()->id)
                        ->orWhere('user_profiles.created_by', Auth::getUser()->id)
                        ->where('users.role_id', '>=', Auth::getUser()->role_id)
                        ->get();
                }
            } elseif (Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                if(!empty($_POST['profileId']) || (!empty($_POST['fromDate']) && !empty($_POST['fromDate']))){
                    $query = DB::connection('mysql')->table('profile_devices')
                            ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                            ->where('device_status', 1);
                    if(!empty($_POST['profileId'])){
                        $query->where('profile_devices.profile_id', $_POST['profileId']);
                    }
                    if(!empty($_POST['fromDate']) && !empty($_POST['toDate'])){
                        $query->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                    }
                    $responseTable = $query->get();
                }else{

                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1)
                        ->get();
                }
            } elseif (Auth::getUser()->role_id == 4) {
                if(!empty($_POST['profileId']) || (!empty($_POST['fromDate']) && !empty($_POST['fromDate']))){
                    $query = DB::connection('mysql')->table('profile_devices')
                            ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                            ->join('users', 'users.id', '=', 'user_profiles.created_by')
                            ->where('device_status', 1)
                            ->where('user_profiles.created_by', Auth::getUser()->master_user);
                    if(!empty($_POST['profileId'])){
                        $query->where('profile_devices.profile_id', $_POST['profileId']);
                    }
                    if(!empty($_POST['fromDate']) && !empty($_POST['toDate'])){
                        $query->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                    }
                    $responseTable = $query->get();
                }else{
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->join('users', 'users.id', '=', 'user_profiles.created_by')
                    ->where('device_status', 1)
                    ->where('user_profiles.created_by', Auth::getUser()->master_user)
                    ->get();
                }
            }
            if (!empty($responseTable)) {
                $responseDashboardCount = 0;
                foreach ($responseTable as $response) {
                    $response = get_object_vars($response);
                    if ($response['provision_status'] == 'Provisioned') {
                        $provisionCount = $provisionCount + 1;
                    }
                    if ($response['provision_status'] == 'unprovisioned') {
                        $unprovisionCount = $unprovisionCount + 1;
                    }
                    $responseDashboardCount = $responseDashboardCount + 1;
                }
                $responseProvisonCount = $provisionCount;
                $responseUnprovisonCount = $unprovisionCount;
            }

            if (!empty($responseTable)) {
                $modelArray = array();
                $OperatingSystemArray = array();
                foreach ($responseTable as $response) {
                    $response = get_object_vars($response);
                    if (!empty($response['device_model_number'])) {
                        $modelArray[] = $response['device_model_number'];
                    }
                    if (!empty($response['device_operating_system'])) {
                        $OperatingSystemArray[] = $response['device_operating_system'];
                    }
                }
                $responseModel = array_count_values($modelArray);
                $responseOperatingSystem = array_count_values($OperatingSystemArray);
            }

        } catch (Exception $e) {
            $statusCode = 404;
        }
        return json_encode(array('deviceTable' => $responseTable, 'Provisioned' => $responseProvisonCount, 'Unprovisioned' => $responseUnprovisonCount, 'dashboardCount' => $responseDashboardCount, 'model' => $responseModel, 'os' => $responseOperatingSystem, 'statusCode' => $statusCode));
    }

    public function dashboardfilterchart()
    {
        try {
            $responseTable = [];
            $responseMonth = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id == 3) {
                if(!empty($_POST['profileId']) || (!empty($_POST['fromDate']) && !empty($_POST['fromDate']))){
                    $query = DB::connection('mysql')->table('profile_devices')
                            ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                            ->where('device_status', 1)
                            ->where('user_profiles.created_by', Auth::getUser()->id)
                            ->orderBy('device_activation_date', 'asc');
                    if(!empty($_POST['profileId'])){
                        $query->where('profile_devices.profile_id', $_POST['profileId']);
                    }
                    if(!empty($_POST['fromDate']) && !empty($_POST['toDate'])){
                        $query->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                    }
                    $responseTable = $query->get();
                }else{
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->id)
                        ->orderBy('device_activation_date', 'asc')->get();
                }
            } elseif (Auth::getUser()->role_id == 2) {
                if(!empty($_POST['profileId']) || (!empty($_POST['fromDate']) && !empty($_POST['fromDate']))){
                    $query = DB::connection('mysql')->table('profile_devices')
                        ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->orderBy('device_activation_date', 'asc');
                    if(!empty($_POST['profileId'])){
                        $query->where('profile_devices.profile_id', $_POST['profileId']);
                    }
                    if(!empty($_POST['fromDate']) && !empty($_POST['toDate'])){
                        $query->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                    }
                    $responseTable = $query->get();
                }else{
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->where('users.master_user', Auth::getUser()->id)
                        ->orWhere('user_profiles.created_by', Auth::getUser()->id)
                        ->where('users.role_id', '>', Auth::getUser()->role_id)
                        ->orderBy('device_activation_date', 'asc')->get();
                }
            } elseif (Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {

                if(!empty($_POST['profileId']) || (!empty($_POST['fromDate']) && !empty($_POST['fromDate']))){
                    $query = DB::connection('mysql')->table('profile_devices')
                            ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                            ->where('device_status', 1)
                            ->orderBy('device_activation_date', 'asc');
                    if(!empty($_POST['profileId'])){
                        $query->where('profile_devices.profile_id', $_POST['profileId']);
                    }
                    if(!empty($_POST['fromDate']) && !empty($_POST['toDate'])){
                        $query->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                    }
                    $responseTable = $query->get();
                }else{
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1)
                        ->orderBy('device_activation_date', 'asc')->get();
                }
            } elseif (Auth::getUser()->role_id == 4) {
                if(!empty($_POST['profileId']) || (!empty($_POST['fromDate']) && !empty($_POST['fromDate']))){
                    $query = DB::connection('mysql')->table('profile_devices')
                            ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                            ->join('users', 'users.id', '=', 'user_profiles.created_by')
                            ->where('device_status', 1)
                            ->where('user_profiles.created_by', Auth::getUser()->master_user)
                            ->orderBy('device_activation_date', 'asc');
                    if(!empty($_POST['profileId'])){
                        $query->where('profile_devices.profile_id', $_POST['profileId']);
                    }
                    if(!empty($_POST['fromDate']) && !empty($_POST['toDate'])){
                        $query->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                    }
                    $responseTable = $query->get();
                }else{
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->master_user)
                        ->orderBy('device_activation_date', 'asc')->get();
                }
            }
            if (!empty($responseTable)) {
                $monthArray = array();
                foreach ($responseTable as $response) {
                    $response = get_object_vars($response);
                    $monthArray[] = date('F', strtotime($response['device_activation_date']));
                }
                $responseMonth = array_count_values($monthArray);
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return json_encode(array('month' => $responseMonth, 'statusCode' => $statusCode));
    }

    public function dashboardfiltermappoints()
    {
        $responseTable = [];
        try {
            $statusCode = 200;
            if (Auth::getUser()->role_id == 3) {
                if(!empty($_POST['profileId']) || (!empty($_POST['fromDate']) && !empty($_POST['fromDate']))){
                    $query = DB::connection('mysql')->table('profile_devices')
                            ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                            ->select('device_identifier', 'device_location')
                            ->where('device_status', 1)
                            ->where('user_profiles.created_by', Auth::getUser()->id);
                    if(!empty($_POST['profileId'])){
                        $query->where('profile_devices.profile_id', $_POST['profileId']);
                    }
                    if(!empty($_POST['fromDate']) && !empty($_POST['toDate'])){
                        $query->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                    }
                    $responseTable = $query->get();
                }else{
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->select('device_identifier', 'device_location')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->id)->get();
                }
            } elseif (Auth::getUser()->role_id == 2) {
                if(!empty($_POST['profileId']) || (!empty($_POST['fromDate']) && !empty($_POST['fromDate']))){
                    $query = DB::connection('mysql')->table('profile_devices')
                            ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                            ->join('users', 'users.id', '=', 'user_profiles.created_by')
                            ->select('device_identifier', 'device_location')
                            ->where('device_status', 1);
                    if(!empty($_POST['profileId'])){
                        $query->where('profile_devices.profile_id', $_POST['profileId']);
                    }
                    if(!empty($_POST['fromDate']) && !empty($_POST['toDate'])){
                        $query->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                    }
                    $responseTable = $query->get();
                }else{
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->select('device_identifier', 'device_location')
                        ->where('device_status', 1)
                        ->where('users.master_user', Auth::getUser()->id)
                        ->orWhere('user_profiles.created_by', Auth::getUser()->id)
                        ->where('users.role_id', '>=', Auth::getUser()->role_id)->get();
                }
            } elseif (Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                if(!empty($_POST['profileId']) || (!empty($_POST['fromDate']) && !empty($_POST['fromDate']))){
                    $query = DB::connection('mysql')->table('profile_devices')
                            ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                            ->select('device_identifier', 'device_location')
                            ->where('device_status', 1);
                    if(!empty($_POST['profileId'])){
                        $query->where('profile_devices.profile_id', $_POST['profileId']);
                    }
                    if(!empty($_POST['fromDate']) && !empty($_POST['toDate'])){
                        $query->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                    }
                    $responseTable = $query->get();
                }else{
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->select('device_identifier', 'device_location')
                        ->where('device_status', 1)->get();
                }
            } elseif (Auth::getUser()->role_id == 4) {

                if(!empty($_POST['profileId']) || (!empty($_POST['fromDate']) && !empty($_POST['fromDate']))){
                    $query = DB::connection('mysql')->table('profile_devices')
                            ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                            ->join('users', 'users.id', '=', 'user_profiles.created_by')
                            ->select('device_identifier', 'device_location')
                            ->where('device_status', 1)
                            ->where('user_profiles.created_by', Auth::getUser()->master_user);
                    if(!empty($_POST['profileId'])){
                        $query->where('profile_devices.profile_id', $_POST['profileId']);
                    }
                    if(!empty($_POST['fromDate']) && !empty($_POST['toDate'])){
                        $query->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                    }
                    $responseTable = $query->get();
                }else{
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->select('device_identifier', 'device_location')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->master_user)->get();
                }
            }

        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($responseTable, $statusCode);
    }


    public function downdevices()
    {
        $responseTable = [];
        $profileDevice = [];
        try {
            $controller = new Controller();
            $statusCode = 200;
            if (Auth::getUser()->role_id == 3) {
                if(!empty($_POST['profileId']) || (!empty($_POST['fromDate']) && !empty($_POST['fromDate']))){
                    $query = DB::connection('mysql')->table('profile_devices')
                            ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                            ->where('profile_devices.updated_at', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 24 HOUR)'))
                            ->where('device_status', 1)
                            ->where('user_profiles.created_by', Auth::getUser()->id);
                    $queryDevice = DB::connection('mysql')->table('profile_devices')
                        ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->id);
                    if(!empty($_POST['profileId'])){
                        $query->where('profile_devices.profile_id', $_POST['profileId']);
                        $queryDevice->where('profile_devices.profile_id', $_POST['profileId']);
                    }
                    if(!empty($_POST['fromDate']) && !empty($_POST['toDate'])){
                        $query->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                        $queryDevice->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                    }
                    $responseTable = $query->get();
                    $profileDevice = $queryDevice->get();
                }else{
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('profile_devices.updated_at', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 24 HOUR)'))
                        ->where('device_status', 1)
                        ->whereYear('device_activation_date', '=', date('Y'))
                        ->where('user_profiles.created_by', Auth::getUser()->id)
                        ->get();

                    $profileDevice = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1)
                        ->whereYear('device_activation_date', '=', date('Y'))
                        ->where('user_profiles.created_by', Auth::getUser()->id)
                        ->get();
                }
            } elseif (Auth::getUser()->role_id == 2) {
                if(!empty($_POST['profileId']) || (!empty($_POST['fromDate']) && !empty($_POST['fromDate']))){
                    $query = DB::connection('mysql')->table('profile_devices')
                            ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                            ->join('users', 'users.id', '=', 'user_profiles.created_by')
                            ->where('profile_devices.updated_at', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 24 HOUR)'))
                            ->where('device_status', 1);
                    $queryDevice = DB::connection('mysql')->table('profile_devices')
                            ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                            ->join('users', 'users.id', '=', 'user_profiles.created_by')
                            ->where('device_status', 1);
                    if(!empty($_POST['profileId'])){
                        $query->where('profile_devices.profile_id', $_POST['profileId']);
                        $queryDevice->where('profile_devices.profile_id', $_POST['profileId']);
                    }
                    if(!empty($_POST['fromDate']) && !empty($_POST['toDate'])){
                        $query->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                        $queryDevice->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                    }
                    $responseTable = $query->get();
                    $profileDevice = $queryDevice->get();
                }else{
                    $idArray = $controller->AdminProfileIds();
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->whereIn('profile_devices.profile_id',array_values($idArray))
                        ->where('profile_devices.updated_at', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 24 HOUR)'))
                        ->where('device_status', 1)
                        ->whereYear('device_activation_date', '=', date('Y'))
                        ->get();


                    $idArrayProfileDevice = $controller->AdminProfileIds();
                    $profileDevice = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->whereIn('profile_devices.profile_id',array_values($idArrayProfileDevice))
                        ->where('device_status', 1)
                        ->whereYear('device_activation_date', '=', date('Y'))
                        ->get();

/*
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('profile_devices.updated_at', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 24 HOUR)'))
                        ->where('device_status', 1)
                        ->whereYear('device_activation_date', '=', date('Y'))
                        ->where('users.master_user', Auth::getUser()->id)
                        ->orWhere('user_profiles.created_by', Auth::getUser()->id)
*/
                   /* $profileDevice = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->whereYear('device_activation_date', '=', date('Y'))
                        ->where('users.master_user', Auth::getUser()->id)
                        ->orWhere('user_profiles.created_by', Auth::getUser()->id)
                        ->where('users.role_id', '>=', Auth::getUser()->role_id)->get();*/
                }
            } elseif (Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                if(!empty($_POST['profileId']) || (!empty($_POST['fromDate']) && !empty($_POST['fromDate']))){
                    $query = DB::connection('mysql')->table('profile_devices')
                            ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                            ->where('profile_devices.updated_at', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 24 HOUR)'))
                            ->where('device_status', 1);
                    $queryDevice = DB::connection('mysql')->table('profile_devices')
                            ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                            ->where('device_status', 1)
                            ->whereYear('device_activation_date', '=', date('Y'));
                    if(!empty($_POST['profileId'])){
                        $query->where('profile_devices.profile_id', $_POST['profileId']);
                        $queryDevice->where('profile_devices.profile_id', $_POST['profileId']);
                    }
                    if(!empty($_POST['fromDate']) && !empty($_POST['toDate'])){
                        $query->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                        $queryDevice->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                    }
                    $responseTable = $query->get();
                    $profileDevice = $queryDevice->get();
                }else{
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('profile_devices.updated_at', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 24 HOUR)'))
                        ->where('device_status', 1)
                        ->whereYear('device_activation_date', '=', date('Y'))->get();

                    $profileDevice = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1)
                        ->whereYear('device_activation_date', '=', date('Y'))->get();
                }
            } elseif (Auth::getUser()->role_id == 4) {
                if(!empty($_POST['profileId']) || (!empty($_POST['fromDate']) && !empty($_POST['fromDate']))){
                    $query = DB::connection('mysql')->table('profile_devices')
                            ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                            ->join('users', 'users.id', '=', 'user_profiles.created_by')
                            ->where('profile_devices.updated_at', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 24 HOUR)'))
                            ->where('device_status', 1)
                            ->where('user_profiles.created_by', Auth::getUser()->master_user);
                    $queryDevice = DB::connection('mysql')->table('profile_devices')
                            ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                            ->join('users', 'users.id', '=', 'user_profiles.created_by')
                            ->where('device_status', 1)
                            ->where('user_profiles.created_by', Auth::getUser()->master_user);
                    if(!empty($_POST['profileId'])){
                        $query->where('profile_devices.profile_id', $_POST['profileId']);
                        $queryDevice->where('profile_devices.profile_id', $_POST['profileId']);
                    }
                    if(!empty($_POST['fromDate']) && !empty($_POST['toDate'])){
                        $query->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                        $queryDevice->whereBetween('device_activation_date', [date("Y-m-d", strtotime($_POST['fromDate'])), date("Y-m-d", strtotime($_POST['toDate']))]);
                    }
                    $responseTable = $query->get();
                    $profileDevice = $queryDevice->get();
                }else{
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('profile_devices.updated_at', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 24 HOUR)'))
                        ->where('device_status', 1)
                        ->whereYear('device_activation_date', '=', date('Y'))
                        ->where('user_profiles.created_by', Auth::getUser()->master_user)->get();

                    $profileDevice = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->whereYear('device_activation_date', '=', date('Y'))
                        ->where('user_profiles.created_by', Auth::getUser()->master_user)->get();
                }
            }
        } catch (Exception $ex) {
            $statusCode = 404;
        }
        return json_encode(array('down' => count($responseTable), 'up' => count($profileDevice) - count($responseTable), 'statusCode' => $statusCode));
    }

    public function devicedelete()
    {
        $response = [];
        try {
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $response = ProfileDevice::where('id', $_POST['deviceId'])->update(['device_status' => 0]);
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function deviceactivation()
    {
        return view('device/deviceactivationlist');
    }

    public function deviceactivationlist()
    {
        try {
            $response = [];
            $statusCode = 200;
            if (!empty($_POST['fromDate']) && !empty($_POST['toDate'])) {
                $fromDate = date('Y-m-d H:i:s', strtotime($_POST['fromDate']));
                $toDate = date('Y-m-d H:i:s', strtotime($_POST['toDate']));
                if (Auth::getUser()->role_id == 3) {
                    $response = ProfileDevice::whereBetween('device_activation_date', [$fromDate, $toDate])
                        ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->id)
                        ->get(['user_profiles.updated_at AS pud', 'user_profiles.*', 'profile_devices.*']);
                } elseif (Auth::getUser()->role_id == 2) {
                    $response = ProfileDevice::whereBetween('device_activation_date', [$fromDate, $toDate])
                        ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->where('users.master_user', Auth::getUser()->id)
                        ->orWhere('user_profiles.created_by', Auth::getUser()->id)
                        ->where('users.role_id', '>=', Auth::getUser()->role_id)
                        ->get(['user_profiles.updated_at AS pud', 'user_profiles.*', 'profile_devices.*']);
                } elseif (Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                    $response = ProfileDevice::whereBetween('device_activation_date', [$fromDate, $toDate])
                        ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1)
                        ->get(['user_profiles.updated_at AS pud', 'user_profiles.*', 'profile_devices.*']);
                } elseif (Auth::getUser()->role_id == 4) {
                    $response = ProfileDevice::whereBetween('device_activation_date', [$fromDate, $toDate])
                        ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->master_user)
                        ->get(['user_profiles.updated_at AS pud', 'user_profiles.*', 'profile_devices.*']);
                } else {
                    $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->master_user)
                        ->get(['user_profiles.updated_at AS pud', 'user_profiles.*', 'profile_devices.*']);
                }
            } else {
                $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->where('device_status', 1)
                    ->whereYear('device_activation_date', '=', date('Y'))
                    ->orderBy('device_activation_date', 'asc')->get(['user_profiles.updated_at AS pud', 'user_profiles.*', 'profile_devices.*']);
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function searchdevicelist()
    {
        try {
            $responseTable = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id == 3) {
                if(!empty($_POST['profile_id'])  || !empty($_POST['device_serial_number']) || !empty($_POST['device_identifier'])){
                    $query = DB::connection('mysql')->table('profile_devices')
                        ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->id);
                    if(!empty($_POST['profile_id'])){
                        $query->where('profile_devices.profile_id', $_POST['profile_id']);
                    }
                    if(!empty($_POST['device_serial_number'])){
                        $query->where('profile_devices.device_serial_number', $_POST['device_serial_number']);
                    }
                    if(!empty($_POST['device_identifier'])){
                        $query->where('profile_devices.device_identifier', $_POST['device_identifier']);
                    }
                    $responseTable = $query->get(['profile_devices.id AS did', 'user_profiles.*', 'profile_devices.*']);
                }else{
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->id)->get(['profile_devices.id AS did', 'user_profiles.*', 'profile_devices.*']);
                }
            } elseif (Auth::getUser()->role_id == 2) {
                if(!empty($_POST['profile_id'])  || !empty($_POST['device_serial_number']) || !empty($_POST['device_identifier'])){
                    $query = DB::connection('mysql')->table('profile_devices')
                        ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1);
                    if(!empty($_POST['profile_id'])){
                        $query->where('profile_devices.profile_id', $_POST['profile_id']);
                    }
                    if(!empty($_POST['device_serial_number'])){
                        $query->where('profile_devices.device_serial_number', $_POST['device_serial_number']);
                    }
                    if(!empty($_POST['device_identifier'])){
                        $query->where('profile_devices.device_identifier', $_POST['device_identifier']);
                    }
                    $responseTable = $query->get(['profile_devices.id AS did', 'user_profiles.*', 'profile_devices.*']);
                }else{
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->where('users.master_user', Auth::getUser()->id)
                        ->orWhere('user_profiles.created_by', Auth::getUser()->id)
                        ->where('users.role_id', '>=', Auth::getUser()->role_id)
                        ->get(['profile_devices.id AS did', 'user_profiles.*', 'profile_devices.*']);
                }
            } elseif (Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                if(!empty($_POST['profile_id']) || !empty($_POST['device_serial_number']) || !empty($_POST['device_identifier'])) {
                    $query = DB::connection('mysql')->table('profile_devices')
                        ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1);
                    if (!empty($_POST['profile_id'])) {
                        $query->where('profile_devices.profile_id', $_POST['profile_id']);
                    }
                    if (!empty($_POST['device_serial_number'])) {
                        $query->where('profile_devices.device_serial_number', $_POST['device_serial_number']);
                    }
                    if(!empty($_POST['device_identifier'])){
                        $query->where('profile_devices.device_identifier', $_POST['device_identifier']);
                    }
                    $responseTable = $query->get(['profile_devices.id AS did', 'user_profiles.*', 'profile_devices.*']);
                }else{
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->where('device_status', 1)->get(['profile_devices.id AS did', 'user_profiles.*', 'profile_devices.*']);
                }
            } elseif (Auth::getUser()->role_id == 4) {
                if(!empty($_POST['profile_id']) || !empty($_POST['device_serial_number']) || !empty($_POST['device_identifier'])) {
                    $query = DB::connection('mysql')->table('profile_devices')
                        ->join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->master_user);
                    if(!empty($_POST['profile_id'])){
                        $query->where('profile_devices.profile_id', $_POST['profile_id']);
                    }
                    if(!empty($_POST['device_serial_number'])){
                        $query->where('profile_devices.device_serial_number', $_POST['device_serial_number']);
                    }
                    if(!empty($_POST['device_identifier'])){
                        $query->where('profile_devices.device_identifier', $_POST['device_identifier']);
                    }
                    $responseTable = $query->get(['profile_devices.id AS did', 'user_profiles.*', 'profile_devices.*']);
                }else{
                    $responseTable = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('users', 'users.id', '=', 'user_profiles.created_by')
                        ->where('device_status', 1)
                        ->where('user_profiles.created_by', Auth::getUser()->master_user)
                        ->get(['profile_devices.id AS did', 'user_profiles.*', 'profile_devices.*']);
                }
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($responseTable, $statusCode);
    }
}