<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Role;
use App\Http\Requests;
use Validator;

class UserManagementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('usermanagement/users');
    }

    public function userlist()
    {
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id == 2 || Auth::getUser()->role_id == 3 ) {
                $response = User::where('master_user', Auth::user()->id)->where('user_status', 1)->with('Role')->get();
            } elseif (Auth::getUser()->role_id == 1 || Auth::getUser()->role_id == 5) {
                $response = User::where('user_status', 1)->with('Role')->get();
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function userlistbyrole()
    {
        try {
            $response = [];
            $statusCode = 200;
            $response = User::where('role_id', $_POST['roleId'])->where('user_status',1)->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function useradd()
    {
        return view('usermanagement/useradd');
    }

    public function rolelist()
    {
        try {
            $response = [];
            $statusCode = 200;
            if (Auth::getUser()->role_id == 1) {
                $response = Role::get();
            } else {
                $response = Role::where('id', '>', Auth::getUser()->role_id)->get();
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function testuserlist()
    {
        try {
            $response = [];
            $statusCode = 200;
            $response = User::where('master_user', $_POST['adminId'])->where('user_status',1)->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
        ]);
    }

    protected function usereditvalidator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
        ]);
    }

    public function userroleadd(Request $request)
    {
        try {
            $response = [];
            $statusCode = 200;
            $masterUser = 0;
            if ($_POST['role_id'] == 4) {
                $masterUser = $_POST['testuser_id'];
            } elseif ($_POST['role_id'] == 3) {
                $masterUser = $_POST['admin_id'];
            } elseif ($_POST['role_id'] == 5) {
                $masterUser = Auth::getUser()->id;
            }

            $validator = $this->validator($request->all());
            if ($validator->fails()) {
                $response = $validator->errors();
            } else if ($_POST['role_id'] == 3 && $_POST['admin_id'] == 0) {
                $response['admin_null'] = 'Sorry, You can not create user without select Administrator';
            } else if ($_POST['role_id'] == 4 && $_POST['admin_id'] == 0) {
                $response['admin_null'] = 'Sorry, You can not create user without select Administrator';
            } elseif ($_POST['role_id'] == 4 && $_POST['testuser_id'] == 0) {
                $response['testuser_null'] = 'Sorry, You can not create user Without select Test User';
            } else {
                $userExists = User::where('email',$_POST['email'])->where('user_status',0)->get()->first();
                if(empty($userExists)){
                    $response_id = User::insertGetId([
                        'name' => htmlentities($_POST['name']),
                        'email' => htmlentities($_POST['email']),
                        'password' => bcrypt('qwerty'),
                        'role_id' => $_POST['role_id'],
                        'master_user' => $masterUser,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                }else{
                    $response_id = $userExists['id'];
                    User::where('id', $userExists['id'])->update(['user_status' => 1,'name' => htmlentities($_POST['name'])]);
                }
                if ($response_id) {
                    $response['success'] = 'User Added successfully';
                }
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function useredit($userid)
    {
        $userDetails = User::where('id', $userid)->first();
        $userDetails['master_user'] = User::where('id', $userDetails->master_user)->first();
        if ($userDetails->role_id == 4) {
            $userDetails['test_user'] = User::where('id', $userDetails['master_user']->master_user)->first();
        }
        return view('usermanagement/useredit')->with('userDetails', $userDetails);
    }

    public function userroleupdate(Request $request)
    {
        try {
            $response = [];
            $statusCode = 200;
            $masterUser = 0;
            $password = '';
            if ($_POST['role_id'] == 4) {
                $masterUser = $_POST['testuser_id'];
            } elseif ($_POST['role_id'] == 3) {
                $masterUser = $_POST['admin_id'];
            }
            if ($_POST['user_password']) {
                $password = $_POST['user_password'];
            }
            $validator = $this->usereditvalidator($request->all());
            if ($validator->fails()) {
                $response = $validator->errors();
            } else if ($_POST['user_password'] && !preg_match('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()+]+.*)[0-9a-zA-Z\w!@#$%^&*()+]{8,}$/', $_POST['user_password'])) {
                $response['admin_null'] = 'Password must contain 8 characters with at least one capital letter and a special character';
            } else if ($_POST['role_id'] == 3 && $_POST['admin_id'] == 0) {
                $response['admin_null'] = 'Sorry, You can not create user without select Administrator';
            } else if ($_POST['role_id'] == 4 && $_POST['admin_id'] == 0) {
                $response['admin_null'] = 'Sorry, You can not create user without select Administrator';
            } elseif ($_POST['role_id'] == 4 && $_POST['testuser_id'] == 0) {
                $response['testuser_null'] = 'Sorry, You can not create user Without select Test User';
            } else {
                $userObj = User::where('id', $_POST['user_id'])->first();
                $userObj->name = htmlentities($_POST['name']);
                $userObj->email = htmlentities($_POST['email']);
                $userObj->role_id = $_POST['role_id'];
                if ($_POST['admin_id'] || $_POST['testuser_id']) {
                    $userObj->master_user = $masterUser;
                }
                if ($password) {
                    $userObj->password = bcrypt($password);
                }
                $saveid = $userObj->save();
                if ($saveid) {
                    $response['success'] = 'User Update successfully';
                }
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function userdelete()
    {
        $response = [];
        try {
            $statusCode = 200;
            if (Auth::getUser()->role_id < 4) {
                $response = User::where('id', $_POST['userId'])->update(['user_status' => 0]);
            } else {
                $response['authorisation'] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function userview($id = null)
    {
        try {
            $response = [];
            $responseMasterUser = [];
            if (Auth::getUser()->role_id == 5 || Auth::getUser()->role_id == 4 || Auth::getUser()->role_id == 3) {
                $response = User::where('id', $id)->where('user_status', 1)->with('Role')->first()->toArray();
                if ($response['master_user']) {
                    $responseMasterUser = User::where('id', $response['master_user'])->where('user_status', 1)->with('Role')->first()->toArray();
                }
            }
        } catch (Exception $e) {
        }
        return view('usermanagement/user', compact('response', 'responseMasterUser'));
    }
}

