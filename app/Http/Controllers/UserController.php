<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    protected function editprofilevalidator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
        ]);
    }
    protected function editprofileuniqueEmailvalidator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
        ]);
    }
    protected function changepasswordvalidator(array $data)
    {
        return Validator::make($data, [
            'password' => 'required|min:6',
            'password_confirmation' => 'required|min:6|same:password'
            
        ]);
    }
    public function profile() {
       
        return view('user/profile');
    }
    public function editprofile() {
        
        return view('user/editprofile');
    }
    public function editpassword(){
        
        return view('user/editpassword');
    }
    public function update(Request $request) {
        
        //Update User Profile
        $id = Auth::getUser()->id;
        $email = Auth::getUser()->email;
        $userObj = User::find($id);
        $validator = $this->editprofilevalidator($request->all());
        $userObj->name = strip_tags($request->input('name'));
        $userObj->email =  strip_tags($request->input('email'));
        if(($request->input('email') == $email)) {
            $validator = $this->editprofilevalidator($request->all());
        }
        else {
            $validator = $this->editprofileuniqueEmailvalidator($request->all());
        }
      
        if($validator->fails()) {
                return redirect('/editprofile')
                            ->withErrors($validator);       
        } else {
                $userObj->save();
                $request->session()->flash('alert-success', 'User details updated successfully');
                return redirect('/editprofile');
        }
    }
    public function changepassword(Request $request){
        
        //Changed Password
        $error_flag = 0;
        $id = Auth::getUser()->id;  
        $oldpassword = Auth::getUser()->password;
        $userObj = User::find($id);
        
        $validator = $this->changepasswordvalidator($request->all());
        $userObj->password =  bcrypt($request->input('password')); 
        if(!(Hash::check($request->input('oldpassword'), $oldpassword))) {
            $error_flag = 1;
            $request->session()->flash('oldpassword-error', 'Old password do not match');
            
        }
        if($validator->fails() || $error_flag) {
                return redirect('/editpassword')
                            ->withErrors($validator);
                            
        } else {
                $userObj->save();
                $request->session()->flash('alert-success', 'Password Changed successfully, Please Sign In again');
                Auth::logout();
                return redirect('/login');
                
        }
    }
    
}
