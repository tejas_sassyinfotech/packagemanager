<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ProfileDevice;

class WipeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('wipe/wipes');
    }

    public function profilewipelist()
    {
        try{
            $response = [];
            $statusCode = 200;
            $response = ProfileDevice::join('device_wipes', 'profile_devices.id', '=', 'device_wipes.device_id')
                ->select('device_wipes.*', 'profile_devices.device_serial_number','profile_devices.device_identifier')
                ->where('profile_id', $_POST['profileId'])->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }
}
