<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\ProfileDevice;
use App\DeviceRegistration;
use App\DeviceLog;
use App\DeviceApplication;
use App\DeviceSubscription;
use App\DeviceSystemInfo;
use App\DevicePackageInfo;
use App\DeviceWipe;
use Symfony\Component\HttpKernel\Profiler\Profile;

class ApiController extends Controller
{

    public function __construct()
    {
        //
    }

    public function getDeviceProfile($token)
    {
        try{
            $response = [];
            $statusCode = 200;
            $response = $this->getDeviceProfileForCheck(htmlentities($token));
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function getDeviceProfileWifi($token)
    {
        try{
            $response = [];
            $statusCode = 200;
            $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                        ->join('profile_wifi_details', 'user_profiles.id', '=', 'profile_wifi_details.profile_id')
                        ->select('profile_wifi_details.*')
                        ->where('active_access_token', $token)->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function getDeviceProfileDisabledApplication($token)
    {
        try{
            $response = [];
            $statusCode = 200;
            $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                ->join('disable_applications', 'user_profiles.id', '=', 'disable_applications.profile_id')
                ->select('disable_applications.*')
                ->where('active_access_token', $token)->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function getDeviceProfileKiosk($token)
    {
        try{
            $response = [];
            $statusCode = 200;
            $profileDetails = $this->getDeviceProfileForCheck($token)->toArray();
            if($profileDetails[0]['userprofile']['kiosk_type'] == 1) {
                $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                    ->join('profile_dashboard_kiosks', 'user_profiles.id', '=', 'profile_dashboard_kiosks.profile_id')
                    ->select('profile_dashboard_kiosks.*','user_profiles.kiosk_type')
                    ->where('active_access_token', $token)
                    ->orderBy('kiosk_browser_priority','asc')->get();
            } else {
                $response = ProfileDevice::where('active_access_token', $token)->with('userprofile')->get();
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function getDeviceProfileKioskSetting($token)
    {
        try{
            $response = [];
            $statusCode = 200;
            $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                ->join('profile_dashboard_kiosk_settings', 'user_profiles.id', '=', 'profile_dashboard_kiosk_settings.profile_id')
                ->select('profile_dashboard_kiosk_settings.*','user_profiles.kiosk_type')
                ->where('active_access_token', $token)->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function getDeviceProfileApplication($token)
    {
        try{
            $response = [];
            $statusCode = 200;
            $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                ->join('profile_applications', 'user_profiles.id', '=', 'profile_applications.profile_id')
                ->join('applications', 'applications.id', '=', 'profile_applications.application_id')
                ->select('applications.*', 'profile_applications.profile_application_status')
                ->where('active_access_token', $token)->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function getDeviceProfileLicences($token)
    {
        try{
            $response = [];
            $statusCode = 200;
            $response = ProfileDevice::join('user_profiles', 'user_profiles.id', '=', 'profile_devices.profile_id')
                ->join('licences', 'user_profiles.id', '=', 'licences.profile_id')
                ->select('licences.*')
                ->where('active_access_token', $token)->get();
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function setDeviceFCMDetails($device_gcm_token, $token)
    {
        try{
            $response = [];
            $statusCode = 200;
            ProfileDevice::where('active_access_token', $token)->update([
                'device_gcm_token' => htmlentities($device_gcm_token)
            ]);
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function setDeviceDetails($device_battery_level, $device_activation_date,
                                     $device_osp_client_version, $device_operating_system, $device_model_number, $device_mac_address,
                                     $device_ip_address, $device_location, $device_gateway_ip, $device_profile_counter, $token)
    {
        try{
            $response = [];
            $statusCode = 200;
            ProfileDevice::where('active_access_token', $token)->update([
                'device_battery_level' => $device_battery_level,
                'device_activation_date' => $device_activation_date,
                'device_osp_client_version' => htmlentities($device_osp_client_version),
                'device_operating_system' => htmlentities($device_operating_system),
                'device_model_number' => htmlentities($device_model_number),
                'device_mac_address' => htmlentities($device_mac_address),
                'device_ip_address' => htmlentities($device_ip_address),
                'device_location' => htmlentities($device_location),
                'device_gateway_ip' => htmlentities($device_gateway_ip),
                'device_profile_counter' => $device_profile_counter,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function setDeviceProvisionStatus($provision_status, $token)
    {
        try{
            $response = [];
            $statusCode = 200;
            ProfileDevice::where('active_access_token', $token)->update([
                'provision_status' => htmlentities($provision_status),
                'device_activation_date' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function setDeviceUserRegistration($registration_first_name, $registration_last_name, $nurse_signature, $token)
    {
        try{
            $response = [];
            $statusCode = 200;
            $profileDetails = $this->getDeviceProfileForCheck($token)->toArray();
            if($profileDetails[0]['id']) {
                $deviceRegistration = DeviceRegistration::where('device_id', $profileDetails[0]['id'])->where('registration_status', 1)->first();
                if(!$deviceRegistration) {
                    DeviceRegistration::insert([
                        'device_id' => $profileDetails[0]['id'],
                        'first_name' => htmlentities($registration_first_name),
                        'last_name' => htmlentities($registration_last_name),
                        'nurse_signature' => htmlentities($nurse_signature),
                        'patient_name' => $registration_first_name.' '.$registration_last_name,
                        'registration_status' => 1,
                        'start_date' => date('Y-m-d H:i:s'),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                    $response = 'Registration done successfully...';
                } else {
                    $response = 'Record Already exist';
                }
            } else {
                return response()->json('Record not found', 404);
            }
        }catch (Exception $e){
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function setDeviceWipe($wipe_description, $wipe_result, $wipe_reason, $wipe_additional_info, $wipe_initiated, $wipe_date, $token)
    {
        try{
            $response = [];
            $statusCode = 200;
            $profileDetails = $this->getDeviceProfileForCheck($token)->toArray();
            if($profileDetails[0]['id']) {
                $deviceWipe = DeviceWipe::where('device_id', $profileDetails[0]['id'])->where('wipe_status', 1)->first();
                if(!$deviceWipe) {
                    DeviceWipe::insert([
                        'device_id' => $profileDetails[0]['id'],
                        'wipe_description' => htmlentities($wipe_description),
                        'wipe_result' => htmlentities($wipe_result),
                        'wipe_reason' => htmlentities($wipe_reason),
                        'wipe_additional_info' => htmlentities($wipe_additional_info),
                        'wipe_initiated' => htmlentities($wipe_initiated),
                        'wipe_date' => $wipe_date,
                        'wipe_status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                    $response = 'Device wipe entry done successfully...';
                } else {
                    $response = 'Device wipe entry Already exist';
                }
            } else {
                return response()->json('Record not found', 404);
            }
        }catch (Exception $e){
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function deleteUserRegistration($token)
    {
        try {
            $response = [];
            $statusCode = 200;
            $profileDetails = $this->getDeviceProfileForCheck($token)->toArray();
            if ($profileDetails[0]['id']) {
                $deviceRegistration = DeviceRegistration::where('device_id', $profileDetails[0]['id'])->where('registration_status', 1)->first();
                if($deviceRegistration) {
                    DeviceRegistration::where('id', $deviceRegistration->id)->update([
                        'registration_status' => 0,
                        'end_date' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                    $response = 'Registration deleted successfully...';
                } else {
                    return response()->json('Record not found', 404);
                }
            } else {
                return response()->json('Record not found', 404);
            }
        } catch (Exception $e){
                $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function deviceUserRegistrationList($token)
    {
        try{
            $response = [];
            $statusCode = 200;
            $profileDetails = $this->getDeviceProfileForCheck($token)->toArray();
            if ($profileDetails[0]['id']) {
                $profileDevices = ProfileDevice::where('profile_id', $profileDetails[0]['profile_id'])->get();
                foreach($profileDevices as $profileDevice) {
                    $deviceRegistrations = DeviceRegistration::where('device_id', $profileDevice->id)
                        ->orderBy('created_at', 'desc')
                        ->limit(2)
                        ->get();
                    foreach($deviceRegistrations as $deviceRegistration) {
                        array_push($response, array(
                            'id' => $deviceRegistration->id,
                            'device_id' => $deviceRegistration->device_id,
                            'first_name' => $deviceRegistration->first_name,
                            'last_name' => $deviceRegistration->last_name,
                            'nurse_signature' => $deviceRegistration->nurse_signature,
                            'patient_name' => $deviceRegistration->patient_name,
                            'registration_status' => $deviceRegistration->registration_status,
                            'start_date' => $deviceRegistration->start_date,
                            'end_date' => $deviceRegistration->end_date,
                            'created_at' => $deviceRegistration->created_at,
                            'updated_at' => $deviceRegistration->updated_at,
                            'device_identifier' => $profileDevice->device_identifier,
                            'device_serial_number' => $profileDevice->device_serial_number
                        ));
                    }
                }
            } else {
                return response()->json('Record not found', 404);
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function getDeviceRegistrationHistory($token)
    {
        try{
            $response = [];
            $statusCode = 200;
            $profileDetails = $this->getDeviceProfileForCheck($token)->toArray();
            if($profileDetails[0]['id']) {
                $response = DeviceRegistration::where('device_id', $profileDetails[0]['id'])->orderBy('start_date', 'desc')->get();
            } else {
                return response()->json('Record not found', 404);
            }
        }catch (Exception $e){
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);

    }

    public function getDeviceProfileForCheck($token)
    {
        return ProfileDevice::where('active_access_token', $token)->with('userprofile')->get();
    }

    public function getApplicationFile($fileName)
    {
        return response()->download(resource_path() . '/uploads/'.$fileName);
    }

    public function setDeviceLog($device_serial_number, $device_activity, $activity_result)
    {
        try{
            $response = [];
            $statusCode = 200;
            DB::connection('mysql_external')->table('device_logs')->insert([
                'device_serial_number' => htmlentities($device_serial_number),
                'device_activity' => htmlentities($device_activity),
                'activity_result' => htmlentities($activity_result),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            $response = 'Device log saved successfully...';
        } catch (Exception $e){
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }
    
    public function setDeviceApplication($device_details_json, $token)
    {
        try{
            $device_details = json_decode($device_details_json, TRUE);
            $response = [];
            $statusCode = 200;
            $profileDetails = $this->getDeviceProfileForCheck($token)->toArray();
            foreach($device_details as $device_detail) {
                $deviceApplicationExist = DeviceApplication::where('device_serial_number', htmlentities($profileDetails[0]['device_serial_number']))->
                                                             where('device_application_package_name', htmlentities($device_detail['package_name']))->
                                                             where('device_application_version', htmlentities($device_detail['application_version']))->get();
                if ($deviceApplicationExist->count()) {
                    DeviceApplication::where('id', $deviceApplicationExist[0]->id)->update([
                        'device_application_additional_info' => $device_detail['additionalInfo'],
                        'device_application_sdk_version' => $device_detail['targetSDK'],
                        'device_application_active_status' => 1,
                        'device_application_date_installed' => $device_detail['date_installed'],
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                    $response = 'Device application entry updated successfully...';
                } else {
                    DeviceApplication::insert([
                        'device_serial_number' => htmlentities($profileDetails[0]['device_serial_number']),
                        'device_application_package_name' => htmlentities($device_detail['package_name']),
                        'device_application_version' => htmlentities($device_detail['application_version']),
                        'device_application_additional_info' => $device_detail['additionalInfo'],
                        'device_application_sdk_version' => $device_detail['targetSDK'],
                        'device_application_active_status' => 1,
                        'device_application_date_installed' => $device_detail['date_installed'],
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
                    $response = 'Device application saved successfully...';
                }
            }
        } catch (Exception $e){
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function deleteSpecificDeviceApplication($device_serial_number, $device_application_package_name, $device_application_version)
    {
        try{
            $response = [];
            $statusCode = 200;
            DeviceApplication::where('device_serial_number', htmlentities($device_serial_number))->
                               where('device_application_package_name', htmlentities($device_application_package_name))->
                               where('device_application_version', htmlentities($device_application_version))->delete();
            $response = 'Device application deleted successfully...';
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function deleteDeviceApplication($device_serial_number)
    {
        try{
            $response = [];
            $statusCode = 200;
            DeviceApplication::where('device_serial_number', htmlentities($device_serial_number))->delete();
            $response = 'Device applications deleted successfully...';
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function setDeviceProfile($device_serial_number, $device_password, $suscription_code, $profile_id)
    {
        try{
            $response = [];
            $statusCode = 200;
            $deviceExist = ProfileDevice::where('device_serial_number', $device_serial_number)->first();
            if(!$deviceExist) {
                $codeSubscription = DeviceSubscription::where('subscription_code', strtoupper($suscription_code))
                                                        ->where('subscription_code_status', 1)->get();
                if($codeSubscription->count()) {
                    $remainingAllotment = $codeSubscription[0]->allotment - $codeSubscription[0]->allotted;
                    if ($remainingAllotment > 0) {
                        ProfileDevice::insert([
                            'profile_id' => $profile_id,
                            'device_identifier' => htmlentities($device_serial_number),
                            'device_serial_number' => htmlentities($device_serial_number),
                            'device_user_name' => htmlentities($device_serial_number),
                            'device_password' => $device_password,
                            'device_status' => 1,
                            'subscription_code_used' => $suscription_code,
                            'provision_status' => 'unprovisioned',
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);
                        $remainingAllotmentUpdated = $codeSubscription[0]->allotted + 1;
                        DeviceSubscription::where('subscription_code', strtoupper($suscription_code))->update(['allotted' => $remainingAllotmentUpdated]);
                        $response = 'Registration done successfully...';
                    } else {
                        $response = 'Code expired';
                    }
                } else {
                    $response = 'No subscription found';
                }
            } else {
                $response = 'Device Already exist';
            }
        }catch (Exception $e){
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function setDevicePackageInfo($device_package_info_json, $token)
    {
        try{
            $device_package_info_details = json_decode($device_package_info_json, TRUE);
            $response = [];
            $statusCode = 200;
            $profileDetails = $this->getDeviceProfileForCheck($token)->toArray();
            foreach($device_package_info_details as $device_package_info_detail) {
                $devicePackageExist = DevicePackageInfo::where('device_serial_number', htmlentities($profileDetails[0]['device_serial_number']))->
                                                         where('device_package_name', htmlentities($device_package_info_detail['device_package_name']))->get();
                if ($devicePackageExist->count()) {
                    DevicePackageInfo::where('id', $devicePackageExist[0]->id)->update([
                        'apk_size' => htmlentities($device_package_info_detail['apk_size']),
                        'apk_path' => htmlentities($device_package_info_detail['apk_path']),
                        'apk_version' => htmlentities($device_package_info_detail['apk_version']),
                        'bloatware_status' => $device_package_info_detail['bloatware_status'],
                        'package_permissions' => htmlentities($device_package_info_detail['package_permissions']),
                        'package_activities' => htmlentities($device_package_info_detail['package_permissions']),
                        'package_services' => htmlentities($device_package_info_detail['package_services']),
                        'package_libraries' => htmlentities($device_package_info_detail['package_libraries']),
                        'package_signature' => htmlentities($device_package_info_detail['package_signature']),
                        'package_info_1' => htmlentities($device_package_info_detail['package_info_1']),
                        'package_info_2' => htmlentities($device_package_info_detail['package_info_2']),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                    $response = 'Device Package Info entry updated successfully...';
                } else {
                    DevicePackageInfo::insert([
                        'device_serial_number' => htmlentities($profileDetails[0]['device_serial_number']),
                        'device_package_name' => htmlentities($device_package_info_detail['device_package_name']),
                        'apk_size' => htmlentities($device_package_info_detail['apk_size']),
                        'apk_path' => htmlentities($device_package_info_detail['apk_path']),
                        'apk_version' => htmlentities($device_package_info_detail['apk_version']),
                        'bloatware_status' => $device_package_info_detail['bloatware_status'],
                        'package_permissions' => htmlentities($device_package_info_detail['package_permissions']),
                        'package_activities' => htmlentities($device_package_info_detail['package_permissions']),
                        'package_services' => htmlentities($device_package_info_detail['package_services']),
                        'package_libraries' => htmlentities($device_package_info_detail['package_libraries']),
                        'package_signature' => htmlentities($device_package_info_detail['package_signature']),
                        'package_info_1' => htmlentities($device_package_info_detail['package_info_1']),
                        'package_info_2' => htmlentities($device_package_info_detail['package_info_2']),
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
                    $response = 'Device Package Info saved successfully...';
                }
            }
        }catch (Exception $e){
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function setDeviceSystemInfo($device_name ,$device_manufacture ,$device_code_name ,$device_model ,$device_id ,$device_build_version ,$device_kernal_version ,$device_rooted_status ,$device_display_info ,$device_mac_address ,$device_ip_address ,$package_libraries ,$device_imei_number ,$device_operator ,$device_country_code ,$device_sim_country_code ,$device_mcc_mnc ,$device_sensor_info ,$device_storage ,$device_processor ,$system_info, $token)
    {
        try{
            $response = [];
            $statusCode = 200;
            $profileDetails = $this->getDeviceProfileForCheck($token)->toArray();
            $devicePackageExist = DeviceSystemInfo::where('device_serial_number', htmlentities($profileDetails[0]['device_serial_number']))->get();
            if ($devicePackageExist->count()) {
                DeviceSystemInfo::where('id', $devicePackageExist[0]->id)->update([
                    'device_name' => htmlentities($device_name),
                    'device_manufacture' => htmlentities($device_manufacture),
                    'device_code_name' => htmlentities($device_code_name),
                    'device_model' => htmlentities($device_model),
                    'device_id' => htmlentities($device_id),
                    'device_build_version' => htmlentities($device_build_version),
                    'device_kernal_version' => htmlentities($device_kernal_version),
                    'device_rooted_status' => htmlentities($device_rooted_status),
                    'device_display_info' => htmlentities($device_display_info),
                    'device_mac_address' => htmlentities($device_mac_address),
                    'device_ip_address' => htmlentities($device_ip_address),
                    'package_libraries' => htmlentities($package_libraries),
                    'device_imei_number' => htmlentities($device_imei_number),
                    'device_operator' => htmlentities($device_operator),
                    'device_country_code' => htmlentities($device_country_code),
                    'device_sim_country_code' => htmlentities($device_sim_country_code),
                    'device_mcc_mnc' => htmlentities($device_mcc_mnc),
                    'device_sensor_info' => htmlentities($device_sensor_info),
                    'device_storage' => htmlentities($device_storage),
                    'device_processor' => htmlentities($device_processor),
                    'system_info' => htmlentities($system_info),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
                $response = 'Device System Info entry updated successfully...';
            } else {
                DeviceSystemInfo::insert([
                    'device_serial_number' => htmlentities($profileDetails[0]['device_serial_number']),
                    'device_name' => htmlentities($device_name),
                    'device_manufacture' => htmlentities($device_manufacture),
                    'device_code_name' => htmlentities($device_code_name),
                    'device_model' => htmlentities($device_model),
                    'device_id' => htmlentities($device_id),
                    'device_build_version' => htmlentities($device_build_version),
                    'device_kernal_version' => htmlentities($device_kernal_version),
                    'device_rooted_status' => htmlentities($device_rooted_status),
                    'device_display_info' => htmlentities($device_display_info),
                    'device_mac_address' => htmlentities($device_mac_address),
                    'device_ip_address' => htmlentities($device_ip_address),
                    'package_libraries' => htmlentities($package_libraries),
                    'device_imei_number' => htmlentities($device_imei_number),
                    'device_operator' => htmlentities($device_operator),
                    'device_country_code' => htmlentities($device_country_code),
                    'device_sim_country_code' => htmlentities($device_sim_country_code),
                    'device_mcc_mnc' => htmlentities($device_mcc_mnc),
                    'device_sensor_info' => htmlentities($device_sensor_info),
                    'device_storage' => htmlentities($device_storage),
                    'device_processor' => htmlentities($device_processor),
                    'system_info' => htmlentities($system_info),
                    'created_at' => date('Y-m-d H:i:s')
                ]);
                $response = 'Device System Info saved successfully...';
            }
        }catch (Exception $e){
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }
}
