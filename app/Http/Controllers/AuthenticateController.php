<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\UserProfile;
use App\ProfileDevice;

class AuthenticateController extends Controller
{
    public function __construct()
    {
        //
    }

    /**
     * Return api authentication status
     *
     * @return Response
     */
    public function authenticate($deviceUserName, $devicePassword)
    {
        try {
            // verify the credentials and create a token for the user
            if (! $token =  $this->attempt($deviceUserName, $devicePassword)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (Exception $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }

    public function attempt($deviceUserName, $devicePassword)
    {
        $deviceData = ProfileDevice::where('device_user_name', $deviceUserName)->where('device_password', $devicePassword)->first();
        if($deviceData) {
            if ($deviceData->demo_device_status == 1 && $deviceData->active_access_token) {
                $accessToken = $deviceData->active_access_token;
            } else {
                $accessToken = bin2hex(random_bytes(32));
                ProfileDevice::where('id', $deviceData->id)->update([
                    'active_access_token' => $accessToken,
                    'last_logged_in' => date('Y-m-d H:i:s')
                ]);
            }
            return $accessToken;
        } else {
            return false;
        }
    }

    public function logout($token)
    {
        try {
            ProfileDevice::where('active_access_token', $token)->update([
                'active_access_token' => ''
            ]);
        }  catch (Exception $e) {
            // something went wrong
            return response()->json(['error' => 'invalid_token'], 500);
        }
        return response()->json(['error' => 'logged_out_successfully'], 400);
    }

    /*
     *  Device google login
     */
    public function getDeviceLogin($serial_number, $token)
    {
        try{
            $response = [];
            $statusCode = 200;
            $url = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='.$token;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec ($ch);
            curl_close ($ch);
            $user = json_decode($output);
            $userDetails = User::where('email',$user->email)->get()->first();
            if($userDetails){
                $deviceExist = ProfileDevice::where('device_serial_number', $serial_number)->get()->first();
                if($deviceExist) {
                    $response = 'user_and_device_already_exist';
                } else {
                    $profileId = $this->createDummyUserProfile($userDetails->id);
                    $this->createDeviceUnderDummyUserProfile($profileId, $userDetails->id, $serial_number);
                    $response = 'device_registration_done_successfully';
                }
            } else {
                $deviceUser = $this->createDeviceuser($user);
                $deviceExist = ProfileDevice::where('device_serial_number', $serial_number)->get()->first();
                if($deviceExist) {
                    $response = 'user_created_and_device_already_exist';
                } else {
                    $profileId = $this->createDummyUserProfile($deviceUser);
                    $this->createDeviceUnderDummyUserProfile($profileId, $deviceUser, $serial_number);
                    $response = 'device_registration_done_successfully';
                }
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function createDeviceuser($user){
        $googleLoginMasterUser = User::where('email', 'admin@london.com')->get()->first();
        return User::insertGetId([
            'name' => $user->name,
            'email' => $user->email,
            'master_user' => $googleLoginMasterUser->id,
            'role_id' => 3,
            'password' => bcrypt('qwerty')
        ]);
    }

    public function createDummyUserProfile($userId){
        $profileId = 'DEFAULT_PROFILE_'.$userId;
        $dummyProfileExist = UserProfile::where('created_by', $userId)->where('default_profile_status', 1)->get()->first();
        if($dummyProfileExist) {
            return $dummyProfileExist->id;
        } else {
            return UserProfile::insertGetId([
                'profile_name' => 'Default profile_'.$userId,
                'profile_id' => $profileId,
                'default_profile_status' => 1,
                'profile_status' => 1,
                'profile_type' => 1,
                'default_profile_status' => 1,
                'created_by' => $userId,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
    }

    public function createDeviceUnderDummyUserProfile($profileId, $userId, $serialNumber){
        return ProfileDevice::insertGetId([
            'profile_id' => $profileId,
            'device_serial_number' =>$serialNumber,
            'device_identifier' => $serialNumber,
            'device_user_name' =>  $serialNumber,
            'device_password' => md5('qwerty'),
            'device_status' => 1,
            'provision_status' => 'unprovisioned',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'device_created_by' => $userId
        ]);
    }
}