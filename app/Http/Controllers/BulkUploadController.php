<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Request;
use Excel;
use App\UserProfile;
use App\DisableApplication;
use App\ProfileDevice;
class BulkUploadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function importdisableaplication()
    {
        try {
            $response = [];
            $statusCode = 200;
            if(Auth::getUser()->role_id < 4) {
                if (Request::hasFile('import_file')) {
                    $issueOnRecord = 0;
                    $path = Request::file('import_file')->getRealPath();
                    $extension = Request::file('import_file')->getClientOriginalExtension();
                    $extArray = array('xlsx', 'xls',);
                    if (!(in_array($extension, $extArray))) {
                        $response[] = 'Invalid File';
                    } else {
                        $data = Excel::load($path, function ($reader) {
                        })->get();
                        if (!empty($data) && $data->count()) {
                            foreach ($data as $key => $value) {
                                $userProfileDetails = UserProfile::where('id', $_POST['profileid'])->get();
                                if ($userProfileDetails->count()) {
                                    if (!empty($value->disabled_application_name)) {
                                        $disableApplicationExist = DisableApplication::where('disabled_application_name', trim($value->disabled_application_name))
                                            ->where('profile_id', $userProfileDetails[0]->id)
                                            ->get();
                                        if (!$disableApplicationExist->count()) {
                                            $insert[] = [
                                                'profile_id' => $userProfileDetails[0]->id,
                                                'disabled_application_name' => trim($value->disabled_application_name),
                                                'disabled_application_status' => 1,
                                                'created_at' => date('Y-m-d H:i:s')
                                            ];
                                        }
                                    } else {
                                        $issueOnRecord = 1;
                                    }
                                }
                            }
                            if (!empty($insert)) {
                                if (DisableApplication::insert($insert)) {
                                    if ($issueOnRecord == 1) {
                                        $response[] = 'Upload done, but not all records...';
                                    } else {
                                        $response[] = 'Upload done successfully...';
                                    }
                                } else {
                                    $response[] = 'Some error occured...';
                                }

                            } else {
                                $response[] = 'No records to insert...';
                            }
                        } else {
                            $response[] = 'No records to insert...';
                        }
                    }
                }
            } else {
                $response[] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function importdevices()
    {
        try{
            $response = [];
            $statusCode = 200;
            if(Auth::getUser()->role_id < 4) {
                if (Request::hasFile('import_file')) {
                    $issueOnRecord = 0;
                    $path = Request::file('import_file')->getRealPath();
                    $extension = Request::file('import_file')->getClientOriginalExtension();
                    $extArray = array('xlsx', 'xls',);
                    if (!(in_array($extension, $extArray))) {
                        $response[] = 'Invalid File';
                    } else {
                        $data = Excel::load($path, function ($reader) {
                        })->get();
                        if (!empty($data) && $data->count()) {
                            foreach ($data as $key => $value) {
                                $userProfileDetails = UserProfile::where('id', $_POST['profileid'])->get();
                                if ($userProfileDetails->count()) {
                                    if (!empty($value->device_serial_number)) {
                                        $profileDeviceExist = ProfileDevice::where('device_serial_number', trim($value->device_serial_number))->get();
                                        if (!$profileDeviceExist->count()) {
                                            $insert[] = [
                                                'profile_id' => $userProfileDetails[0]->id,
                                                'device_identifier' => $value->device_identifier,
                                                'device_serial_number' => $value->device_serial_number,
                                                'device_additional_info' => $value->device_additional_info,
                                                'device_user_name' => (!empty($value->device_user_name)) ? $value->device_user_name : $value->device_serial_number,
                                                'device_password' => ($value->device_password) ? md5($value->device_password) : md5('qwerty'),
                                                'device_status' => 1,
                                                'provision_status' => 'unprovisioned',
                                                'created_at' => date('Y-m-d H:i:s'),
                                                'device_created_by' => Auth::getUser()->id
                                            ];
                                        } else {
                                            ProfileDevice::where('device_serial_number', trim($value->device_serial_number))->update([
                                                'device_identifier' =>($value->device_identifier) ? htmlentities($value->device_identifier) : $profileDeviceExist[0]->device_identifier,
                                                'device_additional_info' => ($value->device_additional_info) ? htmlentities($value->device_additional_info) : $profileDeviceExist[0]->device_additional_info,
                                                'device_user_name' => ($value->device_user_name) ? htmlentities($value->device_user_name) : $profileDeviceExist[0]->device_user_name,
                                                'device_password' => ($value->device_password) ? md5($value->device_password) : $profileDeviceExist[0]->device_user_name,
                                                'device_status' => 1,
                                                'updated_at' => date('Y-m-d H:i:s'),
                                                'device_created_by' => Auth::getUser()->id
                                            ]);
                                        }
                                    }
                                }
                            }
                            if (!empty($insert)) {
                                if (ProfileDevice::insert($insert)) {
                                    if ($issueOnRecord == 1) {
                                        $response[] = 'Upload done, but not all records...';
                                    } else {
                                        $response[] = 'Upload done successfully...';
                                    }
                                } else {
                                    $response[] = 'Some error occured...';
                                }
                            } else {
                                $response[] = 'No records to insert...';
                            }
                        } else {
                            $response[] = 'No records to insert...';
                        }
                    }
                }
            } else {
                $response[] = 'You are not authorised to do this action';
            }
        } catch (Exception $e) {
            $statusCode = 404;
        }
        return response()->json($response, $statusCode);
    }

    public function getSampleFile($fileName)
    {
        return response()->download(resource_path() . '/samples/'.$fileName);
    }
}