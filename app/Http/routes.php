<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'Auth\AuthController@getLogin');
Route::get('/dashboard', 'HomeController@index');
Route::get('/survey', 'HomeController@survey');
Route::get('/surveylist', 'HomeController@usersurveylist');
Route::post('/savesurvey','HomeController@savesurvey');
Route::auth();
Route::post('/register', 'Auth\AuthController@register');
Route::post('/profilecopy',[
    'middleware' => ['auth'],
    'uses' => 'ConfigurationController@profilecopy'
]);
Route::get('/acltest', [
    'middleware' => ['auth', 'roles'], // A 'roles' middleware must be specified
    'uses' => 'TestController@index',
    'roles' => ['Administrator'] // Only an administrator, or a manager can access this route
]);
Route::get('/profile','UserController@profile');
Route::get('/editprofile','UserController@editprofile');
Route::post('/update','UserController@update');
Route::get('/editpassword','UserController@editpassword');
Route::post('/changepassword','UserController@changepassword');
Route::get('/applications','ApplicationController@index');
Route::post('/applicationlist',[
    'middleware' => ['auth'],
    'uses' => 'ApplicationController@applicationlist'
]);
Route::post('/applicationdelete',[
    'middleware' => ['auth'],
    'uses' => 'ApplicationController@applicationdelete'
]);
Route::post('/applicationcreate',[
    'middleware' => ['auth'],
    'uses' => 'ApplicationController@applicationcreate'
]);
Route::get('/configurations','ConfigurationController@index');
Route::post('/profilelist',[
    'middleware' => ['auth'],
    'uses' => 'ConfigurationController@profilelist'
]);
Route::post('/profiledelete',[
    'middleware' => ['auth'],
    'uses' => 'ConfigurationController@profiledelete'
]);
Route::get('/userprofile/{profileId?}','ConfigurationController@profile');
Route::post('/editprofilename','ConfigurationController@editprofilename');
Route::post('/profilecreate',[
    'middleware' => ['auth'],
    'uses' => 'ConfigurationController@profilecreate'
]);
Route::post('/profileupdate',[
    'middleware' => ['auth','profile.auth'],
    'uses' => 'ConfigurationController@profileupdate'
]);
Route::post('/updateprofileemail',[
    'uses' => 'ConfigurationController@updateprofileemail'
]);
Route::post('/profileemailget',[
    'uses' => 'ConfigurationController@profileemailget'
]);
Route::post('/profileemaildelete',[
    'uses' => 'ConfigurationController@profileemaildelete'
]);
Route::post('/profilesyncdelete',[
    'uses' => 'ConfigurationController@profilesyncdelete'
]);
Route::post('/updateprofileactivesync',[
    'uses' => 'ConfigurationController@updateprofileactivesync'
]);
Route::post('/profileactivesyncget',[
    'uses' => 'ConfigurationController@profileactivesyncget'
]);
Route::post('/populateprofile',[
    'middleware' => ['auth'],
    'uses' => 'ConfigurationController@profileget'
]);
Route::post('/devicecreate',[
    'middleware' => ['auth'],
    'uses' => 'ConfigurationController@devicecreate'
]);
Route::post('/deviceget',[
    'middleware' => ['auth'],
    'uses' => 'ConfigurationController@deviceget'
]);
Route::post('/devicedelete',[
    'middleware' => ['auth'],
    'uses' => 'ConfigurationController@devicedelete'
]);
Route::get('/devices','DeviceController@index');
Route::post('/devicetrace', [
    'middleware' => ['auth'],
    'uses' => 'DeviceController@devicetrace'
]);
Route::post('/deviceapplications', [
    'middleware' => ['auth'],
    'uses' => 'DeviceController@deviceapplication'
]);
Route::get('/deletedevicetrace/{device_serial_number?}', [
    'middleware' => ['auth'],
    'uses' => 'DeviceController@deletedevicetrace'
]);
Route::post('/devicelist',[
    'middleware' => ['auth'],
    'uses' => 'DeviceController@devicelist'
]);
Route::post('/dashboardlist/{dashboard?}',[
    'middleware' => ['auth'],
    'uses' => 'DeviceController@userdevicelist'
]);
Route::post('/dashboardfilterlist',[
    'middleware' => ['auth'],
    'uses' => 'DeviceController@dashboardfilterlist'
]);
Route::post('/dashboardfilterchart',[
    'middleware' => ['auth'],
    'uses' => 'DeviceController@dashboardfilterchart'
]);
Route::post('/dashboardmappoints',[
    'middleware' => ['auth'],
    'uses' => 'DeviceController@userdevicemappoints'
]);
Route::post('/dashboardfiltermappoints',[
    'middleware' => ['auth'],
    'uses' => 'DeviceController@dashboardfiltermappoints'
]);
Route::post('/downdevices',[
    'middleware' => ['auth'],
    'uses' => 'DeviceController@downdevices'
]);
Route::post('/dashboardChart',[
    'middleware' => ['auth'],
    'uses' => 'DeviceController@userdevicechart'
]);
Route::post('/dashboardOsModelChart',[
    'middleware' => ['auth'],
    'uses' => 'DeviceController@deviceosmodelchart'
]);

Route::post('/dashboardlistcount',[
    'middleware' => ['auth'],
    'uses' => 'DeviceController@userdevicecount'
]);
Route::post('/devicestatuscount',[
    'middleware' => ['auth'],
    'uses' => 'DeviceController@userdevicestatuscount'
]);
Route::post('/profilewificreate',[
    'middleware' => ['auth','profile.auth'],
    'uses' => 'ConfigurationController@profilewificreate'
]);
Route::post('/profilewifiget',[
    'middleware' => ['auth'],
    'uses' => 'ConfigurationController@profilewifiget'
]);
Route::post('/profilewifidelete',[
    'middleware' => ['auth'],
    'uses' => 'ConfigurationController@profilewifidelete'
]);
Route::post('/profilekioskcreate',[
    'middleware' => ['auth','profile.auth'],
    'uses' => 'ConfigurationController@profilekioskcreate'
]);
Route::post('/profilekiosksetting',[
    'middleware' => ['auth','profile.auth'],
    'uses' => 'ConfigurationController@profilekiosksetting'
]);
Route::post('/profilekioskget',[
    'middleware' => ['auth'],
    'uses' => 'ConfigurationController@profilekioskget'
]);
Route::post('/profilekioskdelete',[
    'middleware' => ['auth'],
    'uses' => 'ConfigurationController@profilekioskdelete'
]);
Route::post('/profiledashboardkiosksettingsget',[
    'middleware' => ['auth'],
    'uses' => 'ConfigurationController@profiledashboardkiosksettingsget'
]);
Route::post('/profileapplicationlist',[
    'middleware' => ['auth'],
    'uses' => 'ConfigurationController@profileapplicationlist'
]);
Route::post('/assignprofileapplication',[
    'middleware' => ['auth','profile.auth'],
    'uses' => 'ConfigurationController@assignprofileapplication'
]);
Route::post('/profileapplicationdelete',[
    'middleware' => ['auth'],
    'uses' => 'ConfigurationController@profileapplicationdelete'
]);
Route::post('/disableapplicationcreate',[
    'middleware' => ['auth','profile.auth'],
    'uses' => 'ConfigurationController@disableapplicationcreate'
]);
Route::post('/disableapplicationget',[
    'middleware' => ['auth'],
    'uses' => 'ConfigurationController@disableapplicationget'
]);
Route::post('/disableapplicationdelete',[
    'middleware' => ['auth'],
    'uses' => 'ConfigurationController@disableapplicationdelete'
]);
Route::get('/licences', 'LicenceController@index');
Route::post('/licensecreate',[
    'middleware' => ['auth'],
    'uses' => 'LicenceController@licensecreate'
]);
Route::post('/licenselist',[
    'middleware' => ['auth'],
    'uses' => 'LicenceController@licenselist'
]);
Route::post('/licencedelete',[
    'middleware' => ['auth'],
    'uses' => 'LicenceController@licencedelete'
]);
Route::get('/registrations','RegistrationController@index');
Route::get('/wipes','WipeController@index');
Route::get('/devices/{serial}',[
    'middleware' => ['auth'],
    'uses' => 'DeviceController@getdevice'
]);
Route::post('/devicelog',[
    'middleware' => ['auth'],
    'uses' => 'DeviceController@devicelog'
]);
Route::post('/allapplicationlist',[
    'middleware' => ['auth'],
    'uses' => 'ApplicationController@allapplicationlist'
]);
Route::post('/allprofilelist',[
    'middleware' => ['auth'],
    'uses' => 'ConfigurationController@profilelist'
]);
Route::post('/profileregistrationlist',[
    'middleware' => ['auth'],
    'uses' => 'RegistrationController@profileregistrationlist'
]);
Route::post('/devicewipelist',[
    'middleware' => ['auth'],
    'uses' => 'WipeController@profilewipelist'
]);
Route::post('/notification', 'PushNotificationController@sendNotificationToDevice');
Route::post('/bulkuploaddisableapps',[
    'middleware' => ['auth'],
    'uses' => 'BulkUploadController@importdisableaplication'
]);
Route::post('/bulkuploaddevices',[
    'middleware' => ['auth'],
    'uses' => 'BulkUploadController@importdevices'
]);
Route::get('/samplefile/{fileName}', [
    'middleware' => ['api.auth'],
    'uses' => 'BulkUploadController@getSamplefile'
]);
Route::get('/exportdisableaplication/{profileId}', [
    'middleware' => ['api.auth'],
    'uses' => 'ExportController@exportdisableaplication'
]);
Route::get('/exportdevice/{profileId?}', [
    'middleware' => ['api.auth'],
    'uses' => 'ExportController@exportdevice'
]);
Route::get('/usermanagement',[
    'middleware' => ['auth'],
    'uses' => 'UserManagementController@index'
]);
Route::get('/usermanagement/{userId}',[
    'middleware' => ['auth'],
    'uses' => 'UserManagementController@userview'
]);
Route::post('/userlist',[
    'middleware' => ['auth'],
    'uses' => 'UserManagementController@userlist'
]);
Route::post('/searchdevicelist',[
    'middleware' => ['auth'],
    'uses' => 'DeviceController@searchdevicelist'
]);

Route::get('/useradd','UserManagementController@useradd');
Route::get('/useredit/{id}','UserManagementController@useredit');
Route::post('/userroleadd','UserManagementController@userroleadd');
Route::post('/userroleupdate','UserManagementController@userroleupdate');
Route::post('/rolelist','UserManagementController@rolelist');
Route::post('/testuserlist','UserManagementController@testuserlist');
Route::post('/userlistbyrole','UserManagementController@userlistbyrole');
Route::post('/iosprofileupdate','ConfigurationController@iosprofileupdate');
Route::post('/getiossettings','ConfigurationController@getiossettings');
Route::post('/iosactivesyncupdate','ConfigurationController@iosactivesyncupdate');
Route::post('/iosprofileemailupdate','ConfigurationController@iosprofileemailupdate');
Route::post('/iosprofileldapupdate','ConfigurationController@iosprofileldapupdate');
Route::post('/profileldapget','ConfigurationController@profileldapget');
Route::post('/profileldapdelete','ConfigurationController@profileldapdelete');
Route::post('/iosprofilevpnupdate','ConfigurationController@iosprofilevpnupdate');
Route::post('/profilevpnget','ConfigurationController@profilevpnget');
Route::post('/profilevpndelete','ConfigurationController@profilevpndelete');
Route::post('/iosprofilecarddavupdate','ConfigurationController@iosprofilecarddavupdate');
Route::post('/profilecarddavget','ConfigurationController@profilecarddavget');
Route::post('/profilecarddavdelete','ConfigurationController@profilecarddavdelete');
Route::post('/profileadvancedrestrictionupdate','ConfigurationController@profileadvancedrestrictionupdate');
Route::post('/getadvancedrestriction','ConfigurationController@getadvancedrestriction');
Route::post('/iosprofileapnupdate','ConfigurationController@iosprofileapnupdate');
Route::post('/profileapnget','ConfigurationController@profileapnget');
Route::post('/profileapndelete','ConfigurationController@profileapndelete');
Route::post('/iosprofilekioskupdate','ConfigurationController@iosprofilekioskupdate');
Route::post('/profilepasswordget','ConfigurationController@profilepasswordget');
Route::post('/profilepassworddelete','ConfigurationController@profilepassworddelete');
Route::post('/profilewebclipget','ConfigurationController@profilewebclipget');
Route::post('/iosprofilewebclipupdate','ConfigurationController@iosprofilewebclipupdate');
Route::post('/profilewebclipdelete','ConfigurationController@profilewebclipdelete');
Route::post('/iosprofilewifiupdate','ConfigurationController@iosprofilewifiupdate');
Route::post('/updateadminprofiletab','ConfigurationController@updateadminprofiletab');
Route::post('/iosprofilepasswordupdate','ConfigurationController@iosprofilepasswordupdate');

Route::post('/userdelete','UserManagementController@userdelete');
Route::post('/devicedelete','DeviceController@devicedelete');
Route::get('auth/google', 'Auth\AuthController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\AuthController@handleGoogleCallback');

Route::get('/adminuserprofile','ConfigurationController@adminuserprofile');
Route::post('/getabsetting','ConfigurationController@getabsetting');
Route::post('/certificatecreate','ConfigurationController@certificatecreate');
Route::post('/getcertificatesetting','ConfigurationController@getcertificatesetting');
Route::post('/profilecertificatedelete','ConfigurationController@profilecertificatedelete');
Route::post('/updateioswebfiler','ConfigurationController@updateioswebfiler');
Route::post('/getwebfilerurllist','ConfigurationController@getwebfilerurllist');
Route::post('/webfilerurldelete','ConfigurationController@webfilerurldelete');
Route::get('/deviceactivation','DeviceController@deviceactivation');
Route::post('/deviceactivationlist','DeviceController@deviceactivationlist');
Route::get('/deviceactivationexcel/{fromDate?}/{toDate?}','ExportController@deviceactivationexcel');
Route::get('/deviceregistrationexcel/{profileId?}','ExportController@deviceregistrationexcel');
Route::get('/devicewipeexcel/{profileId?}','ExportController@devicewipeexcel');

/*
 * API Routing section
 */
Route::group(['prefix' => '/api/v1'], function() {
    Route::get('/authenticate/{deviceuser}/{devicepassword}', [
        'middleware' => ['addheader'],
        'uses' => 'AuthenticateController@authenticate'
    ]);
    Route::get('/logout/{token}', [
        'middleware' => ['addheader'],
        'uses' => 'AuthenticateController@logout'
    ]);
    Route::get('/register/{deviceserialnumber}/{devicepassword}/{suscriptioncode}/{profileid}', [
        'middleware' => ['addheader'],
        'uses' => 'ApiController@setDeviceProfile'
    ]);
    Route::get('/deviceprofiledetails/{token}', [
        'middleware' => ['api.auth', 'addheader'],
        'uses' => 'ApiController@getDeviceProfile'
    ]);
    Route::get('/deviceprofilewifidetails/{token}', [
        'middleware' => ['api.auth', 'addheader'],
        'uses' => 'ApiController@getDeviceProfileWifi'
    ]);
    Route::get('/deviceprofiledisabledapplications/{token}', [
        'middleware' => ['api.auth', 'addheader'],
        'uses' => 'ApiController@getDeviceProfileDisabledApplication'
    ]);
    Route::get('/deviceprofilekiosks/{token}', [
        'middleware' => ['api.auth', 'addheader'],
        'uses' => 'ApiController@getDeviceProfileKiosk'
    ]);
    Route::get('/deviceprofilekiosksettings/{token}', [
        'middleware' => ['api.auth', 'addheader'],
        'uses' => 'ApiController@getDeviceProfileKioskSetting'
    ]);
    Route::get('/deviceprofileapplications/{token}', [
        'middleware' => ['api.auth', 'addheader'],
        'uses' => 'ApiController@getDeviceProfileApplication'
    ]);
    Route::get('/setdevicefcmcode/{device_gcm_token}/{token}', [
        'middleware' => ['api.auth', 'addheader'],
        'uses' => 'ApiController@setDeviceFCMDetails'
    ]);
    Route::get('/setdevicedetails/{device_battery_level}/{device_activation_date}/{device_osp_client_version}/{device_operating_system}/{device_model_number}/{device_mac_address}/{device_ip_address}/{device_location}/{device_gateway_ip}/{device_profile_counter}/{token}', [
        'middleware' => ['api.auth', 'addheader'],
        'uses' => 'ApiController@setDeviceDetails'
    ]);
    Route::get('/setdeviceprovisionstatus/{provision_status}/{token}', [
        'middleware' => ['api.auth', 'addheader'],
        'uses' => 'ApiController@setDeviceProvisionStatus'
    ]);
    Route::get('/setdeviceuserregistration/{first_name}/{last_name}/{nurse_signature}/{token}', [
        'middleware' => ['api.auth', 'addheader'],
        'uses' => 'ApiController@setDeviceUserRegistration'
    ]);
    Route::get('setDeviceWipe/{wipe_description}/{wipe_result}/{wipe_reason}/{wipe_additional_info}/{wipe_initiated}/{wipe_date}/{token}', [
        'middleware' => ['api.auth', 'addheader'],
        'uses' => 'ApiController@setDeviceWipe'
    ]);
    Route::get('/deletedeviceuserregistration/{token}', [
        'middleware' => ['api.auth', 'addheader'],
        'uses' => 'ApiController@deleteUserRegistration'
    ]);
    Route::get('/deviceuserregistrations/{token}', [
        'middleware' => ['api.auth', 'addheader'],
        'uses' => 'ApiController@deviceUserRegistrationList'
    ]);
    Route::get('/deviceuserregistrationhistory/{token}', [
        'middleware' => ['api.auth', 'addheader'],
        'uses' => 'ApiController@getDeviceRegistrationHistory'
    ]);
    Route::get('/getapplicationfile/{fileName}/{token}', [
        'middleware' => ['api.auth'],
        'uses' => 'ApiController@getApplicationFile'
    ]);
    Route::get('/deviceprofilelicences/{token}', [
        'middleware' => ['api.auth'],
        'uses' => 'ApiController@getDeviceProfileLicences'
    ]);
    Route::get('/setdevicelog/{device_serial_number}/{device_activity}/{activity_result}/{token}', [
        'middleware' => ['api.auth'],
        'uses' => 'ApiController@setDeviceLog'
    ]);
    Route::get('/setdeviceapplication/{device_details_json}/{token}', [
        'middleware' => ['api.auth'],
        'uses' => 'ApiController@setDeviceApplication'
    ]);
    Route::get('/deletespecificdeviceapplication/{device_serial_number}/{device_application_package_name}/{device_application_version}/{token}', [
        'middleware' => ['api.auth'],
        'uses' => 'ApiController@deleteSpecificDeviceApplication'
    ]);
    Route::get('/deletedeviceapplication/{device_serial_number}/{token}', [
        'middleware' => ['api.auth'],
        'uses' => 'ApiController@deleteDeviceApplication'
    ]);
    Route::get('/setdevicepackageinfo/{device_package_info_json}/{token}', [
        'middleware' => ['api.auth'],
        'uses' => 'ApiController@setDevicePackageInfo'
    ]);
    Route::get('/setdevicesysteminfo/{device_name}/{device_manufacture}/{device_code_name}/{device_model}/{device_id}/{device_build_version}/{device_kernal_version}/{device_rooted_status}/{device_display_info}/{device_mac_address}/{device_ip_address}/{package_libraries}/{device_imei_number}/{device_operator}/{device_country_code}/{device_sim_country_code}/{device_mcc_mnc}/{device_sensor_info}/{device_storage}/{device_processor}/{system_info}/{token}', [
        'middleware' => ['api.auth'],
        'uses' => 'ApiController@setDeviceSystemInfo'
    ]);
    Route::get('/getdevicelogin/{serial_number}/{token}', [
        'uses' => 'AuthenticateController@getDeviceLogin'
    ]);
});