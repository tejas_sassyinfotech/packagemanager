<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IosAdvancedRestrictionsSetting extends Model
{
    protected $table = 'ios_advanced_restrictions_settings';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
