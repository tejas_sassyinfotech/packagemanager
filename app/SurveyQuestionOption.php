<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestionOption extends Model
{
    protected $table = 'survey_question_options';

    public function surveyquestion()
    {
        return $this->hasOne('\App\SurveyQuestion', 'id', 'survey_question_id');
    }

    public function surveyquestionoption()
    {
        return $this->hasMany('\App\SurveyQuestionAnswer', 'survey_question_option_id', 'id');
    }
}
