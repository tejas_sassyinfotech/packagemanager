<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileApplication extends Model
{
    protected $table = 'profile_applications';

    public function userprofile()
    {
        return $this->hasMany('\App\UserProfile', 'id', 'profile_id');
    }

    public function application()
    {
        return $this->hasOne('\App\Application', 'id', 'application_id');
    }
}
