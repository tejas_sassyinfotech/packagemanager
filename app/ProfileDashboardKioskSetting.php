<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileDashboardKioskSetting extends Model
{
    protected $table = 'profile_dashboard_kiosk_settings';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
