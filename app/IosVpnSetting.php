<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IosVpnSetting extends Model
{
    protected $table = 'ios_vpn_settings';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
