<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileDevice extends Model
{
    protected $table = 'profile_devices';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
