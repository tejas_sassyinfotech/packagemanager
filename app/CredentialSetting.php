<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CredentialSetting extends Model
{
    protected $table = 'ios_credential_settings';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
