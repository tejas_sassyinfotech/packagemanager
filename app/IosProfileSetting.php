<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IosProfileSetting extends Model
{
    protected $table = 'ios_profile_settings';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
