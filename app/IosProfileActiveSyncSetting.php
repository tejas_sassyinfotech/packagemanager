<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IosProfileActiveSyncSetting extends Model
{
    protected $table = 'ios_profile_active_sync_settings';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
