<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileEmailConfigurationSetting extends Model
{
    protected $table = 'profile_email_configuration_settings';

    public function userprofile()
    {
        return $this->hasOne('\App\UserProfile', 'id', 'profile_id');
    }
}
