<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table = 'user_profiles';
    
    public function profiledevices()
    {
        return $this->hasMany('\App\ProfileDevice', 'profile_id', 'id');
    }

    public function profilewifidetails()
    {
        return $this->hasMany('\App\ProfileWifiDetail', 'profile_id', 'id');
    }

    public function profiledashboardkiosks()
    {
        return $this->hasMany('\App\ProfileDashboardKiosk', 'profile_id', 'id');
    }

    public function disableapplications()
    {
        return $this->hasMany('\App\DisableApplication', 'profile_id', 'id');
    }

    public function licences()
    {
        return $this->hasMany('\App\Licence', 'profile_id', 'id');
    }

    public function profiledashboardkiosksetting()
    {
        return $this->hasOne('\App\ProfileDashboardKioskSettings', 'profile_id', 'id');
    }
}
