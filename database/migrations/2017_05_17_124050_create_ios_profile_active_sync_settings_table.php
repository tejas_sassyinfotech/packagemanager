<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIosProfileActiveSyncSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ios_profile_active_sync_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->string('account_name')->nullable();
            $table->string('active_sync_server_name')->nullable();
            $table->integer('move_allow_status')->default(0)->nullable();
            $table->integer('recent_address_syncing_allow_status')->default(0)->nullable();
            $table->integer('use_only_in_mail_status')->default(0)->nullable();
            $table->integer('use_ssl_status')->default(0)->nullable();
            $table->integer('use_mime_status')->default(0)->nullable();
            $table->string('active_sync_domain')->nullable();
            $table->string('active_sync_user_name')->nullable();
            $table->string('active_sync_user_email')->nullable();
            $table->string('active_sync_user_password')->nullable();
            $table->string('mail_sync_period')->nullable();
            $table->string('identity_certificate')->nullable();
            $table->integer('identity_certificate_ios4_compatible_status')->default(0)->nullable();
            $table->integer('ios_active_sync_status')->default(1)->nullable();
            $table->timestamps();
            $table->foreign('profile_id')->references('id')->on('user_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ios_profile_active_sync_settings');
    }
}
