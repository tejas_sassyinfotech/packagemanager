<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIosApnSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ios_apn_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->string('apn_name')->nullable();
            $table->string('apn_user_name')->nullable();
            $table->string('apn_password')->nullable();
            $table->string('apn_proxy_server')->nullable();
            $table->integer('apn_proxy_server_port')->default(0)->nullable();
            $table->integer('ios_apn_settings_status')->default(1)->nullable();
            $table->timestamps();
            $table->foreign('profile_id')->references('id')->on('user_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ios_apn_settings');
    }
}
