<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserTableDeleteAccessToken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('api_password');
            $table->dropColumn('active_access_token');
            $table->dropColumn('last_logged_in');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('api_password')->after('role_id')->nullable();
            $table->string('active_access_token')->after('api_password')->nullable();
            $table->timestamp('last_logged_in')->after('active_access_token')->nullable();
        });
    }
}
