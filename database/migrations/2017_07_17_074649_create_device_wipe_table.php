<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceWipeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_wipes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('device_id')->unsigned();
            $table->string('wipe_description')->nullable();
            $table->string('wipe_result')->nullable();
            $table->string('wipe_reason')->nullable();
            $table->string('wipe_additional_info')->nullable();
            $table->string('wipe_initiated')->nullable();
            $table->integer('wipe_status')->nullable();
            $table->timestamp('wipe_date')->nullable();
            $table->timestamps();
            $table->foreign('device_id')->references('id')->on('profile_devices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Scheme::drop('device_wipes');
    }
}
