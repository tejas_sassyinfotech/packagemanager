<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIosVpnSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ios_vpn_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->string('vpn_connection_name')->nullable();
            $table->string('vpn_connection_type')->nullable();
            $table->string('vpn_server')->nullable();
            $table->string('vpn_account')->nullable();
            $table->string('vpn_user_authentication_type')->nullable();
            $table->string('vpn_shared_secret')->nullable();
            $table->string('vpn_sent_all_traffic_status')->nullable();
            $table->string('vpn_proxy')->nullable();
            $table->integer('ios_vpn_settings_status')->default(1)->nullable();
            $table->timestamps();
            $table->foreign('profile_id')->references('id')->on('user_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ios_vpn_settings');
    }
}
