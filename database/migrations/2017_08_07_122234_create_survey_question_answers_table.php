<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_question_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survey_id')->unsigned();
            $table->integer('survey_question_id')->unsigned();
            $table->integer('survey_question_option_id')->unsigned()->nullable();
            $table->string('survey_question_answer_desc');
            $table->integer('created_by');
            $table->timestamps();
            $table->foreign('survey_id')->references('id')->on('user_surveys');
            $table->foreign('survey_question_id')->references('id')->on('survey_questions');
            $table->foreign('survey_question_option_id')->references('id')->on('survey_question_options');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('survey_question_answers');
    }
}
