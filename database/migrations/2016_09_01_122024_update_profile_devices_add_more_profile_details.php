<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProfileDevicesAddMoreProfileDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_devices', function (Blueprint $table) {
            $table->string('device_gcm_token')->after('device_password')->nullable();
            $table->integer('device_battery_level')->after('device_gcm_token')->nullable();
            $table->timestamp('device_activation_date')->after('device_battery_level')->nullable();
            $table->string('device_osp_client_version')->after('device_activation_date')->nullable();
            $table->string('device_operating_system')->after('device_osp_client_version')->nullable();
            $table->string('device_model_number')->after('device_operating_system')->nullable();
            $table->string('device_mac_address')->after('device_model_number')->nullable();
            $table->string('device_ip_address')->after('device_mac_address')->nullable();
            $table->string('device_location')->after('device_ip_address')->nullable();
            $table->string('device_gateway_ip')->after('device_location')->nullable();
            $table->integer('device_profile_counter')->after('device_gateway_ip')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_devices', function ($table) {
            $table->dropColumn('device_gcm_token');
            $table->dropColumn('device_battery_level');
            $table->dropColumn('device_activation_date');
            $table->dropColumn('device_osp_client_version');
            $table->dropColumn('device_operating_system');
            $table->dropColumn('device_model_number');
            $table->dropColumn('device_mac_address');
            $table->dropColumn('device_ip_address');
            $table->dropColumn('device_location');
            $table->dropColumn('device_gateway_ip');
            $table->dropColumn('device_profile_counter');
        });
    }
}
