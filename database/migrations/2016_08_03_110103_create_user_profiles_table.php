<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('profile_name')->unique();
            $table->string('profile_id')->unique();
            $table->integer('mdm_display_status')->nullable();
            $table->integer('status_bar_status')->nullable();
            $table->integer('home_key_status')->nullable();
            $table->integer('back_key_status')->nullable();
            $table->integer('recents_key_status')->nullable();
            $table->integer('power_key_status')->nullable();
            $table->integer('firmware_upgrade_status')->nullable();
            $table->integer('factory_reset_status')->nullable();
            $table->integer('s_voice_status')->nullable();
            $table->integer('android_settings_status')->nullable();
            $table->integer('volume_key_status')->nullable();
            $table->integer('multi_window_status')->nullable();
            $table->integer('notifications_status')->nullable();
            $table->integer('lock_screen_status')->nullable();
            $table->integer('bluetooth_status')->nullable();
            $table->integer('gps_status')->nullable();
            $table->integer('device_timeout')->nullable();
            $table->integer('pull_interval')->nullable();
            $table->integer('brightness_percentage')->nullable();
            $table->integer('volume_percentage')->nullable();
            $table->integer('sleep_percentage')->nullable();
            $table->integer('deep_sleep_percentage')->nullable();
            $table->string('orientation')->nullable();
            $table->integer('backup_reset_menu_status')->nullable();
            $table->integer('developer_mode_menu_status')->nullable();
            $table->integer('airplane_mode_menu_status')->nullable();
            $table->integer('language_menu_status')->nullable();
            $table->integer('multi_window_menu_status')->nullable();
            $table->string('users_settings_menu_status')->nullable();
            $table->integer('bluetooth_settings_menu_status')->nullable();
            $table->integer('lockscreen_menu_status')->nullable();
            $table->integer('wifi_settings_menu_status')->nullable();
            $table->integer('power_on_boot_status')->nullable();
            $table->integer('disable_all_bloatware_status')->nullable();
            $table->integer('global_proxy')->nullable();
            $table->integer('global_port')->nullable();
            $table->integer('kiosk_type')->nullable();
            $table->string('kiosk_password')->nullable();
            $table->string('single_app_kiosk')->nullable();
            $table->string('browser_kiosk')->nullable();
            $table->string('registration_password')->nullable();
            $table->string('advanced_command')->nullable();
            $table->integer('profile_status')->nullable();
            $table->integer('profile_counter')->default(0);
            $table->integer('created_by')->unsigned();
            $table->timestamps();
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_profiles');
    }
}
