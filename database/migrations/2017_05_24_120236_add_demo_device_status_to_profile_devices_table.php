<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDemoDeviceStatusToProfileDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_devices', function (Blueprint $table) {
            $table->integer('demo_device_status')->after('provision_status')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_devices', function ($table) {
            $table->dropColumn('demo_device_status');
        });
    }
}
