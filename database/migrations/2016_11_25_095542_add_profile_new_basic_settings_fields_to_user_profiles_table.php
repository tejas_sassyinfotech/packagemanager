<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProfileNewBasicSettingsFieldsToUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_profiles', function(Blueprint $table)
        {
            $table->integer('unknown_source_status')->after('disable_all_bloatware_status')->nullable();
            $table->integer('developer_mode_status')->after('unknown_source_status')->nullable();
            $table->integer('screenshot_status')->after('developer_mode_status')->nullable();
            $table->integer('usb_debugging_status')->after('screenshot_status')->nullable();
            $table->integer('lte_mode_status')->after('usb_debugging_status')->nullable();
            $table->string('placeholder1')->after('lte_mode_status')->nullable();
            $table->string('placeholder2')->after('placeholder1')->nullable();
            $table->string('placeholder3')->after('placeholder2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_profiles', function(Blueprint $table)
        {
            $table->dropColumn('unknown_source_status');
            $table->dropColumn('developer_mode_status');
            $table->dropColumn('screenshot_status');
            $table->dropColumn('usb_debugging_status');
            $table->dropColumn('lte_mode_status');
            $table->dropColumn('placeholder1');
            $table->dropColumn('placeholder2');
            $table->dropColumn('placeholder3');
        });
    }
}
