<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceSystemInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_system_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('device_serial_number')->nullable();
            $table->string('device_name')->nullable();
            $table->string('device_manufacture')->nullable();
            $table->string('device_code_name')->nullable();
            $table->string('device_model')->nullable();
            $table->string('device_id')->nullable();
            $table->string('device_build_version')->nullable();
            $table->longText('device_kernal_version')->nullable();
            $table->integer('device_rooted_status')->default(1)->nullable();
            $table->longText('device_display_info')->nullable();
            $table->string('device_mac_address')->nullable();
            $table->string('device_ip_address')->nullable();
            $table->string('package_libraries')->nullable();
            $table->string('device_imei_number')->nullable();
            $table->string('device_operator')->nullable();
            $table->string('device_country_code')->nullable();
            $table->string('device_sim_country_code')->nullable();
            $table->string('device_mcc_mnc')->nullable();
            $table->longText('device_sensor_info')->nullable();
            $table->longText('device_storage')->nullable();
            $table->longText('device_processor')->nullable();
            $table->longText('system_info')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Scheme::drop('device_system_info');
    }
}
