<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsFromApiToDeviceApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('device_applications', function (Blueprint $table) {
            $table->string('device_application_sdk_version')->after('device_application_version')->nullable();
            $table->string('device_application_additional_info')->after('device_application_sdk_version')->nullable();
            $table->integer('device_application_active_status')->after('device_application_additional_info')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('device_applications', function ($table) {
            $table->dropColumn('device_application_sdk_version');
            $table->dropColumn('device_application_additional_info');
            $table->dropColumn('device_application_active_status');
        });
    }
}
