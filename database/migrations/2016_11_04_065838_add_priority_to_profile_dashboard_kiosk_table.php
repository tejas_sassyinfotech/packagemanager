<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriorityToProfileDashboardKioskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_dashboard_kiosks', function (Blueprint $table) {
            $table->integer('kiosk_browser_priority')->after('kiosk_browser_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_dashboard_kiosks', function ($table) {
            $table->dropColumn('kiosk_browser_priority');
        });
    }
}
