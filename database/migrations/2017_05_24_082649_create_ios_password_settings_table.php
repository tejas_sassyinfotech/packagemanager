<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIosPasswordSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ios_password_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->integer('allow_simple_ios_password_status')->default(1)->nullable();
            $table->integer('ios_password_alpha_numeric_value_require_status')->default(0)->nullable();
            $table->integer('ios_password_minmum_length')->nullable();
            $table->integer('ios_password_minmum_complex_character_count')->nullable();
            $table->integer('ios_password_maximum_age')->nullable();
            $table->string('ios_device_auto_lock_status')->nullable();
            $table->integer('ios_password_history_count')->nullable();
            $table->string('ios_device_lock_grace_period')->nullable();
            $table->integer('ios_password_failed_attempts_count')->nullable();
            $table->integer('ios_password_settings_status')->default(1)->nullable();
            $table->timestamps();
            $table->foreign('profile_id')->references('id')->on('user_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Scheme::drop('ios_password_settings');
    }
}
