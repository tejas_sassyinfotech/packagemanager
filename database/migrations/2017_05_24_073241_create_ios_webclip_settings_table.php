<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIosWebclipSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ios_webclip_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->string('webclip_label')->nullable();
            $table->string('webclip_url')->nullable();
            $table->integer('webclip_removable_status')->default(1)->nullable();
            $table->integer('webclip_precomposed_icon_status')->default(0)->nullable();
            $table->integer('webclip_fullscreen_status')->default(0)->nullable();
            $table->string('webclip_icon')->nullable();
            $table->integer('ios_webclip_settings_status')->default(1)->nullable();
            $table->timestamps();
            $table->foreign('profile_id')->references('id')->on('user_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ios_webclip_settings');
    }
}
