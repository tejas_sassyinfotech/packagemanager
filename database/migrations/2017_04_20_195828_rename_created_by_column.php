<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameCreatedByColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_devices',function (Blueprint $table) {
            $table->renameColumn('created_by', 'device_created_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_devices', function(Blueprint $table) {
            $table->rename('device_created_by', 'created_by');
        });
    }
}
