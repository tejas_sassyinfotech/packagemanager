<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicePackageInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_package_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('device_serial_number')->nullable();
            $table->string('device_package_name')->nullable();
            $table->string('apk_size')->nullable();
            $table->string('apk_path')->nullable();
            $table->string('apk_version')->nullable();
            $table->integer('bloatware_status')->default(1)->nullable();
            $table->longText('package_permissions')->nullable();
            $table->longText('package_activities')->nullable();
            $table->longText('package_services')->nullable();
            $table->longText('package_libraries')->nullable();
            $table->string('package_signature')->nullable();
            $table->longText('package_info_1')->nullable();
            $table->longText('package_info_2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Scheme::drop('device_package_info');
    }
}
