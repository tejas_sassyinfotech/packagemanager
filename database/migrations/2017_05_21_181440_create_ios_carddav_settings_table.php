<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIosCarddavSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ios_carddav_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->string('carddav_account_description')->nullable();
            $table->string('carddav_account_host_name')->nullable();
            $table->string('carddav_port')->nullable();
            $table->string('carddav_princpal_url')->nullable();
            $table->string('carddav_account_user_name')->nullable();
            $table->string('carddav_account_password')->nullable();
            $table->integer('carddav_ssl_usage_status')->default(1)->nullable();
            $table->integer('ios_carddav_settings_status')->default(1)->nullable();
            $table->timestamps();
            $table->foreign('profile_id')->references('id')->on('user_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ios_carddav_settings');
    }
}
