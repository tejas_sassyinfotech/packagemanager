<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProfileDevices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_devices', function(Blueprint $table)
        {
            $table->string('device_user_name')->after('device_serial_number');
            $table->string('device_password')->after('device_user_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_devices', function ($table) {
            $table->dropColumn('device_user_name');
            $table->dropColumn('device_password');
        });
    }
}
