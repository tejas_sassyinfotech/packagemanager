<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIosWifiSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ios_wifi_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->string('wifi_service_set_identifier')->nullable();
            $table->integer('wifi_auto_join_status')->default(1)->nullable();
            $table->string('wifi_hidden_network')->nullable();
            $table->string('wifi_security_type')->nullable();
            $table->string('wifi_security_password')->nullable();
            $table->string('wifi_proxy')->nullable();
            $table->integer('ios_wifi_settings_status')->default(1)->nullable();
            $table->timestamps();
            $table->foreign('profile_id')->references('id')->on('user_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ios_wifi_settings');
    }
}
