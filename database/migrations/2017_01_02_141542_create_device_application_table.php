<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('device_serial_number');
            $table->string('device_application_package_name');
            $table->string('device_application_version');
            $table->timestamp('device_application_date_installed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('device_applications');
    }
}
