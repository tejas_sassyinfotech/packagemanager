<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIosProfileEmailSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ios_profile_email_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->string('account_description')->nullable();
            $table->string('account_type')->nullable();
            $table->string('user_display_name')->nullable();
            $table->string('user_email_address')->nullable();
            $table->integer('allow_move_status')->default(0)->nullable();
            $table->string('incoming_mail_account_type')->nullable();
            $table->string('incoming_mail_server')->nullable();
            $table->string('incoming_mail_server_port')->nullable();
            $table->string('incoming_mail_user_name')->nullable();
            $table->string('incoming_mail_authentication_type')->nullable();
            $table->string('incoming_mail_password')->nullable();
            $table->integer('incoming_mail_ssl_usage')->default(1)->nullable();
            $table->string('outgoing_mail_account_type')->nullable();
            $table->string('outgoing_mail_server')->nullable();
            $table->string('outgoing_mail_server_port')->nullable();
            $table->string('outgoing_mail_user_name')->nullable();
            $table->string('outgoing_mail_authentication_type')->nullable();
            $table->integer('outgoing_mail_password')->default(1)->nullable();
            $table->integer('recent_address_syncing_allow_status')->default(0)->nullable();
            $table->integer('only_in_mail_usage')->default(0)->nullable();
            $table->integer('outgoing_mail_ssl_usage')->default(1)->nullable();
            $table->integer('outgoing_mail_mime_usage')->default(0)->nullable();
            $table->integer('ios_email_status')->default(1)->nullable();
            $table->timestamps();
            $table->foreign('profile_id')->references('id')->on('user_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ios_profile_email_settings');
    }
}
