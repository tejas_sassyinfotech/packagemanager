<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileActiveSyncSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_active_sync_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->string('active_sync_account_name')->nullable();
            $table->string('active_sync_host')->nullable();
            $table->string('active_sync_domain')->nullable();
            $table->string('active_sync_user_name')->nullable();
            $table->string('active_sync_user_email')->nullable();
            $table->string('active_sync_user_password')->nullable();
            $table->string('active_sync_schedule')->nullable();
            $table->integer('active_sync_ssl_status')->nullable();
            $table->timestamps();
            $table->foreign('profile_id')->references('id')->on('user_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profile_active_sync_settings');
    }
}
