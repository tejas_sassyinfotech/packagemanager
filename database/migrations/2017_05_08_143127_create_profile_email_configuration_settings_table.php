<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileEmailConfigurationSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_email_configuration_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->string('account_description')->nullable();
            $table->string('user_display_name')->nullable();
            $table->string('user_email_address')->nullable();
            $table->string('incoming_mail_account_type')->nullable();
            $table->string('incoming_mail_server')->nullable();
            $table->string('incoming_mail_server_port')->nullable();
            $table->string('incoming_mail_user_name')->nullable();
            $table->string('incoming_mail_password')->nullable();
            $table->integer('incoming_mail_ssl_usage')->default(1)->nullable();
            $table->integer('incoming_mail_tls_usage')->default(0)->nullable();
            $table->string('outgoing_mail_account_type')->nullable();
            $table->string('outgoing_mail_server')->nullable();
            $table->string('outgoing_mail_server_port')->nullable();
            $table->string('outgoing_mail_user_name')->nullable();
            $table->string('outgoing_mail_password')->nullable();
            $table->integer('outgoing_mail_ssl_usage')->default(1)->nullable();
            $table->integer('outgoing_mail_tls_usage')->default(0)->nullable();
            $table->integer('email_status')->default(1)->nullable();
            $table->timestamps();
            $table->foreign('profile_id')->references('id')->on('user_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profile_email_configuration_settings');
    }
}
