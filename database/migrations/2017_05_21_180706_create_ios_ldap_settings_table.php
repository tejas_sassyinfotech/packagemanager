<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIosLdapSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ios_ldap_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->string('ldap_account_description')->nullable();
            $table->string('ldap_account_user_name')->nullable();
            $table->string('ldap_account_password')->nullable();
            $table->string('ldap_account_host_name')->nullable();
            $table->string('ldap_ssl_usage_status')->nullable();
            $table->integer('ios_ldap_settings_status')->default(1)->nullable();
            $table->timestamps();
            $table->foreign('profile_id')->references('id')->on('user_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ios_ldap_settings');
    }
}
