<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tab_name')->nullable();
            $table->integer('tab_status')->default(1)->comment('1 - Show, 0 - Hide')->nullable();
            $table->integer('profile_type')->default(0)->comment('1 - Android, 2 - IOS')->nullable();
            $table->integer('created_by')->unsigned();
            $table->timestamps();
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Scheme::drop('tab_settings');
    }
}
