<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubscriptionCodeUsedToProfileDeviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_devices', function (Blueprint $table) {
            $table->string('subscription_code_used')->after('demo_device_status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_devices', function ($table) {
            $table->dropColumn('subscription_code_used');
        });
    }
}
