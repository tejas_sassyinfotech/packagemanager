<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProfileDeviceAddAccessToken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_devices', function (Blueprint $table) {
            $table->string('active_access_token')->after('device_password')->nullable();
            $table->timestamp('last_logged_in')->after('active_access_token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_devices', function ($table) {
            $table->dropColumn('active_access_token');
            $table->dropColumn('last_logged_in');
        });
    }
}
