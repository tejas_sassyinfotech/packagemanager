<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subscription_code')->nullable();
            $table->integer('allotment')->default(0)->nullable();
            $table->integer('allotted')->default(0)->nullable();
            $table->integer('subscription_code_status')->default(1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Scheme::drop('device_subscriptions');
    }
}
