<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceLogTableOnDb2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_external')->create('device_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('device_serial_number');
            $table->string('device_activity');
            $table->string('activity_result');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_external')->drop('device_logs');
    }
}
