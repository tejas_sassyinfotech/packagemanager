<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCredentialSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ios_credential_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('certificate_name')->nullable();
            $table->integer('certificate_status')->default(1)->comment('1 - Show, 0 - Hide')->nullable();
            $table->integer('profile_id')->unsigned();
            $table->integer('created_by')->unsigned();
            $table->timestamps();
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Scheme::drop('credential_settings');
    }
}
