<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCertificateUsageStatusToProfileEmailConfigurationSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_email_configuration_settings', function (Blueprint $table) {
            $table->integer('incoming_mail_certificate_usage')->after('incoming_mail_tls_usage')->nullable();
            $table->integer('outgoing_mail_certificate_usage')->after('outgoing_mail_tls_usage')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_email_configuration_settings', function ($table) {
            $table->dropColumn('incoming_mail_certificate_usage');
            $table->dropColumn('outgoing_mail_certificate_usage');
        });
    }
}
