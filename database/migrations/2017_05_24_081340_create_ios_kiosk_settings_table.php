<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIosKioskSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ios_kiosk_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->string('kiosk_app')->nullable();
            $table->string('kiosk_app_name')->nullable();
            $table->integer('kiosk_disable_touch_status')->default(0)->nullable();
            $table->integer('kiosk_disable_device_rotation_status')->default(0)->nullable();
            $table->integer('kiosk_disable_volume_button_status')->default(0)->nullable();
            $table->integer('kiosk_disable_ringer_switch_status')->default(0)->nullable();
            $table->integer('kiosk_disable_sleep_wake_button_status')->default(0)->nullable();
            $table->integer('kiosk_disable_auto_lock_status')->default(0)->nullable();
            $table->integer('kiosk_enable_voice_over_status')->default(0)->nullable();
            $table->integer('kiosk_enable_zoom_status')->default(0)->nullable();
            $table->integer('kiosk_enable_invert_colors_status')->default(0)->nullable();
            $table->integer('kiosk_enable_assistive_touch_status')->default(0)->nullable();
            $table->integer('kiosk_enable_speak_selection_status')->default(0)->nullable();
            $table->integer('kiosk_voice_over_status')->default(0)->nullable();
            $table->integer('kiosk_zoom_status')->default(0)->nullable();
            $table->integer('kiosk_invert_colors_status')->default(0)->nullable();
            $table->integer('kiosk_assistive_touch_status')->default(0)->nullable();
            $table->integer('ios_kiosk_settings_status')->default(1)->nullable();
            $table->timestamps();
            $table->foreign('profile_id')->references('id')->on('user_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ios_kiosk_settings');
    }
}
