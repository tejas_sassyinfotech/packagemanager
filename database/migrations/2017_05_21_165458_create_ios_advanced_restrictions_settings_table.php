<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIosAdvancedRestrictionsSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ios_advanced_restrictions_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->string('airdrop_allow_status')->default(1)->nullable();
            $table->string('app_cellular_data_modification_allow_status')->default(1)->nullable();
            $table->integer('remove_app_allow_status')->default(1)->nullable();
            $table->integer('book_store_allow_status')->default(1)->nullable();
            $table->integer('touch_id_add_remove_allow_status')->default(0)->nullable();
            $table->integer('imessage_allow_status')->default(1)->nullable();
            $table->integer('game_center_allow_status')->default(1)->nullable();
            $table->string('itunes_pairing_allow_status')->default(1)->nullable();
            $table->string('profile_installation_allow_status')->default(1)->nullable();
            $table->string('podcasts_allow_status')->default(1)->nullable();
            $table->string('lookup_definition_allow_status')->default(1)->nullable();
            $table->string('predictive_keyboard_allow_status')->default(1)->nullable();
            $table->string('auto_correction_allow_status')->default(1)->nullable();
            $table->string('spell_check_allow_status')->default(1)->nullable();
            $table->string('apple_music_services_allow_status')->default(1)->nullable();
            $table->integer('itunes_radio_allow_status')->default(1)->nullable();
            $table->integer('ios_news_allow_status')->default(1)->nullable();
            $table->integer('app_install_from_devices_allow_status')->default(1)->nullable();
            $table->integer('keyboard_shortcut_allow_status')->default(1)->nullable();
            $table->integer('paired_watch_allow_status')->default(1)->nullable();
            $table->integer('account_modification_allow_status')->default(1)->nullable();
            $table->integer('erase_cintent_and_settings_allow_status')->default(1)->nullable();
            $table->integer('user_generated_content_assistant_allow_status')->default(1)->nullable();
            $table->integer('find_my_friends_modify_allow_status')->default(1)->nullable();
            $table->integer('force_use_of_priority_filter_status')->default(0)->nullable();
            $table->integer('spotlight_internet_results_allow_status')->default(1)->nullable();
            $table->integer('restrictions_enabling_allow_status')->default(1)->nullable();
            $table->integer('passcode_modification_allow_status')->default(1)->nullable();
            $table->integer('device_name_modification_allow_status')->default(1)->nullable();
            $table->integer('wallpaper_modification_allow_status')->default(1)->nullable();
            $table->integer('notification_modification_allow_status')->default(1)->nullable();
            $table->integer('automatic_apps_downloading_allow_status')->default(1)->nullable();
            $table->string('autonomous_single_app_mode_apps')->nullable();
            $table->integer('ios_advanced_restrictions_settings_status')->default(1)->nullable();
            $table->timestamps();
            $table->foreign('profile_id')->references('id')->on('user_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ios_advanced_restrictions_settings');
    }
}
