<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScreenNumberAndSpecialScreenNumberFieldsToProfileDashboardKiosksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_dashboard_kiosks', function (Blueprint $table) {
            $table->integer('kiosk_screen_number')->after('kiosk_browser_priority')->nullable();
            $table->integer('kiosk_special_screen_number')->after('kiosk_screen_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_dashboard_kiosks', function ($table) {
            $table->dropColumn('kiosk_screen_number');
            $table->dropColumn('kiosk_special_screen_number');
        });
    }
}
