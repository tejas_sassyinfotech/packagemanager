<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIosWebfilerSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ios_webfiler_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->string('webfiler_url')->nullable();
            $table->integer('webfiler_url_restrict_content_status')->default(0)->comment('1 - active, 0 - inactive')->nullable();
            $table->integer('webfiler_url_type')->default(1)->comment('1 - blacklist, 2 - Whitelist')->nullable();
            $table->integer('webfiler_url_status')->default(1)->comment('1 - Show, 0 - Hide')->nullable();
            $table->integer('created_by')->unsigned();
            $table->timestamps();
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ios_webfiler_settings');
    }
}
