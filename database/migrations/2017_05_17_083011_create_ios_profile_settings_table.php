<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIosProfileSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ios_profile_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->integer('app_install_allow_status')->default(1)->nullable();
            $table->integer('air_drop_block_status')->default(0)->nullable();
            $table->integer('camera_use_allow_status')->default(1)->nullable();
            $table->integer('face_time_allow_status')->default(1)->nullable();
            $table->integer('screen_capture_allow_status')->default(1)->nullable();
            $table->integer('roaming_automatic_sync_allow_status')->default(1)->nullable();
            $table->integer('touch_id_status')->default(1)->nullable();
            $table->integer('siri_allow_status')->default(1)->nullable();
            $table->integer('device_locked_siri_allow_status')->default(1)->nullable();
            $table->integer('voice_dialing_allow_status')->default(1)->nullable();
            $table->integer('device_locked_passbook_allow_status')->default(0)->nullable();
            $table->integer('in_app_purchase_allow_status')->default(1)->nullable();
            $table->integer('force_user_credential_on_itune_purchase_allow_status')->default(1)->nullable();
            $table->integer('multiplayer_gaming_allow_status')->default(1)->nullable();
            $table->integer('adding_game_centre_friends_allow_status')->default(1)->nullable();
            $table->integer('trust_enterprise_app_allow_status')->default(1)->nullable();
            $table->integer('modify_trust_enterprise_app_allow_status')->default(1)->nullable();
            $table->integer('enterprise_book_backup_allow_status')->default(1)->nullable();
            $table->integer('managed_apps_to_sync_allow_status')->default(1)->nullable();
            $table->integer('use_youtbe_allow_status')->default(1)->nullable();
            $table->integer('use_itune_store_allow_status')->default(0)->nullable();
            $table->integer('use_safari_allow_status')->default(1)->nullable();
            $table->integer('safari_enable_autofill_status')->default(1)->nullable();
            $table->integer('safari_force_fraud_warning_status')->default(0)->nullable();
            $table->integer('safari_enable_javascript_status')->default(1)->nullable();
            $table->integer('safari_block_popup_status')->default(1)->nullable();
            $table->string('cookie_accept_status')->nullable();
            $table->integer('icloud_backup_allow_status')->default(1)->nullable();
            $table->integer('icloud_document_sync_allow_status')->default(0)->nullable();
            $table->integer('icloud_photo_stream_allow_status')->default(1)->nullable();
            $table->integer('icloud_shared_photo_stream_allow_status')->default(0)->nullable();
            $table->integer('icloud_photo_library_allow_status')->default(1)->nullable();
            $table->integer('lock_screen_notification_allow_status')->default(0)->nullable();
            $table->integer('today_view_in_lock_screen_allow_status')->default(0)->nullable();
            $table->integer('control_center_in_lock_screen_allow_status')->default(1)->nullable();
            $table->integer('over_the_air_PKI_updates_allow_status')->default(1)->nullable();
            $table->string('ad_tracking_limit_status')->nullable();
            $table->integer('diagnostic_data_sent_allow_status')->default(1)->nullable();
            $table->integer('untrusted_TLS_certificate_allow_status')->default(0)->nullable();
            $table->integer('force_encrypt_backup_status')->default(1)->nullable();
            $table->integer('force_apple_watch_wrist_detection_status')->default(0)->nullable();
            $table->integer('rate_music_allow_status')->default(1)->nullable();
            $table->integer('rate_ibookstore_erotica_allow_status')->default(0)->nullable();
            $table->string('rating_region')->nullable();
            $table->string('movie_allow_status')->nullable();
            $table->string('tv_shows_allow_status')->nullable();
            $table->string('apps_allow_status')->nullable();
            $table->integer('ios_settings_status')->default(1)->nullable();
            $table->timestamps();
            $table->foreign('profile_id')->references('id')->on('user_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ios_profile_settings');
    }
}
