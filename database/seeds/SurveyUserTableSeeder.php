<?php

use Illuminate\Database\Seeder;

class SurveyUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Role::create([
            'id'            => 6,
            'name'          => 'SurveyUser',
            'description'   => 'Able to view all surveys.'
        ]);
    }
}
