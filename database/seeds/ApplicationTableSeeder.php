<?php

use Illuminate\Database\Seeder;

class ApplicationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('applications')->truncate();
        \App\Application::create([
            'id' => 1,
            'application_name'     => 'test_name',
            'application_version'  => 'test_version',
            'application_package'  => 'test_package',
            'application_status'   => 1,
            'created_by'           => 1
        ]);
    }
}
