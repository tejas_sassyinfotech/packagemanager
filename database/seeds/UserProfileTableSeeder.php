<?php

use Illuminate\Database\Seeder;

class UserProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\UserProfile::create([
            'profile_name'   => 'test_name',
            'profile_status' => 1,
            'created_by'     => 1
        ]);
    }
}
