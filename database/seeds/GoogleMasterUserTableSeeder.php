<?php

use Illuminate\Database\Seeder;

class GoogleMasterUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create(array(
            'name'     => 'londonadmin',
            'email'    => 'admin@london.com',
            'password' => Hash::make('qwerty'),
            'role_id'  => '2'
        ));
    }
}
