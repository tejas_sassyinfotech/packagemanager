<?php

use Illuminate\Database\Seeder;

class RoleUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Role::where('id',1)->update([
            'name' => 'Super User',
        ]);

        \App\Role::where('id',2)->update([
            'name' => 'Administrator',
        ]);

        \App\Role::where('id',3)->update([
            'name' => 'Test User',
        ]);

        \App\Role::where('id',4)->update([
            'name' => 'Viewer',
        ]);

        \App\Role::where('id',5)->delete();

    }
}
