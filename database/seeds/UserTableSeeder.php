<?php
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{

    public function run()
    {
        //DB::table('users')->delete();
        \App\User::create(array(
            'name'     => 'syam',
            'email'    => 'osp@trivand.com',
            'password' => Hash::make('qwerty'),
            'role_id'  => '1'
        ));
    }

}
