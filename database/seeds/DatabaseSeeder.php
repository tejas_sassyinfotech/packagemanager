<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
//use database\seeds\UserTableSeeder;
//use database\seeds\RoleTableSeeder;
//use database\seeds\UserProfileTableSeeder;
//use database\seeds\ApplicationTableSeeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
        $this->call('RoleTableSeeder');
        $this->call('UserTableSeeder');
        $this->call('GoogleMasterUserTableSeeder');
        $this->call('UserProfileTableSeeder');
        $this->call('ApplicationTableSeeder');
    }
}
