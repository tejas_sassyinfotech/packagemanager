@extends('layouts.adminltemain')
@section('content-main')
   <script src="/js/device.js"></script>
   <script src="/js/pushnotification.js"></script>
   <link href="/css/progressbar-style.css" rel="stylesheet">
    <div class="pageheader">
        <section class="content-header">
            <h2><i class="fa fa-tablet"></i>Devices</h2>
            <ol class="breadcrumb">
                <li><a href="/dashboard">{{ env('OSP_LABEL') }}</a></li>
                <li class="active">Devices</li>
            </ol>
        </section>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="tab-pane" id="devicesettings">
                                      @if ( Auth::user()->role_id < 4)
                                     <form id="devices"  value="config" class="form-horizontal form-bordered" method="post" >
                                     <br>
                                     <div class="row">

                                         <div class="col-md-6">
                                               <div class="form-group">
                                                   <label class="col-sm-5 control-label">SELECT PROFILE</label>
                                                   <div class="col-sm-7">
                                                       <select id="addProfile" name="profileid" class="select2 profileidpersist form-control" required data-placeholder="Choose One">
                                                           <option value="">CHOOSE FROM PROFILE LIST</option>
                                                       </select>
                                                       <div id="deviceperror" style="display:none; color:red"></div>
                                                   </div>
                                               </div>

                                         </div>

                                         <div class="col-md-6">
                                            <div class="form-group">
                                            <div class="col-md-12">
                                               <button id="searchdevice" value="add" type="button" class="btn btn-primary btn-sm">SEARCH</button>
                                               <a class="btn btn-primary btn-sm" id="devicemgtlink" href="/exportdevice">EXPORT</a>
                                               <button id="reboot" value="reboot" command="rebootall" type="button" class="btn btn-primary btn-sm pushnotification">REBOOT</button>
                                               </div>
                                            </div>
                                         </div>

                                     </div>

                                     <div class="row">

                                          {{--<div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label">DEVICE IDENTIFIER</label>
                                                    <div class="col-sm-7">
                                                       <label class="sr-only" for="deviceid">DEVICE IDENTIFIER</label>
                                                       <input type="deviceid" class="form-control" id="deviceid" placeholder="">
                                                    </div>
                                                </div>
                                          </div>--}}

                                          <div class="col-md-6">

                                          <div class="form-group">
                                             <label class="col-sm-5 control-label">BULK UPLOAD</label>
                                              <div class="col-sm-7">
                                                <input type="file" name="import_file" id="bulkuploaddisableapps" />
                                                <a href="/samplefile/devices_list.xlsx">download sample file</a>
                                                {{--<input type="hidden" name="profileid" class="profileidpersist" value="">--}}
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                              </div>

                                          </div>

                                         </div>

                                          <div class="col-md-6">
                                            <div class="form-group">
                                            <div class="col-md-6">
                                                <input class="btn btn-primary btn-sm" type='submit' id='_bulksubmit' value='BULK UPLOAD'>
                                                </div>
                                            </div>
                                          </div>

                                      </div>

                                      <div class="row">

                                        <div class="col-md-6">


                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">ADD DEVICE</label>

                                                <div class="col-sm-7">
                                                <input type="deviceid" class="form-control" id="deviceid" placeholder="DEVICE IDENTIFIER">
                                               </div>


                                            </div>


                                        </div>
                                        <div class="col-md-6">

                                                <div class="form-group">
                                                       <label class="sr-only" for="deviceid">DEVICE IDENTIFIER</label>
                                                    <div class="col-sm-7">
                                                      <label class="sr-only" for="serialno">SERIAL NUMBER</label>
                                                      <input type="serialno" class="form-control" id="serialno" placeholder="SERIAL NUMBER">

                                                    </div>
                                                    <div class="col-sm-5">
                                                    <button id="adddevice" value="add" type="button" class="btn btn-primary btn-sm">ADD</button>
                                                    </div>
                                                </div>


                                          </div>

                                       </div>

                                        {{--<div class="form-group">
                                            <label class="col-sm-3 control-label">ADDITIONAL INFO</label>
                                            <div class="col-sm-4">
                                              <label class="sr-only" for="serialno">ADDITIONAL INFO</label>
                                              <input type="additionalinfo" class="form-control" id="additionalinfo" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">DEVICE USER NAME</label>
                                            <div class="col-sm-4">
                                              <label class="sr-only" for="serialno">DEVICE USER NAME</label>
                                              <input type="deviceusername" class="form-control" id="deviceusername" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">DEVICE PASSWORD</label>
                                            <div class="col-sm-4">
                                              <label class="sr-only" for="serialno">DEVICE PASSWORD</label>
                                              <input type="password" class="form-control" id="devicepassword" placeholder="">
                                            </div>
                                            </div>--}}

                                      </form>
                                      <br />
                                      @endif
                                      @if ( Auth::user()->role_id < 4)
                                       {{--<button value="SAVE" class="btn btn-primary savesettings" currenttab="devicesettings">SAVE</button>--}}@endif
                                      </div>





            <div class="panel-heading">
              <div class="panel-btns">
                <!-- <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a> -->
                <div class="panel-body">
                  <h5 class="subtitle mb5">Device List</h5>
                  <br />
                  <div class="table-responsive">
                    <table class="table table-striped" id="devicetable">
                      <thead>
                         <tr>
                            <th id="group">Serial Number </th>
                            <th>Device Identifier</th>
                            <th>MDM</th>
                            <th>Profile Name</th>
                            <th>Counter</th>
                            <th>Battery</th>
                            <th>Location</th>
                            <th>Last Update</th>
                            <th class="icons-device"></th>
                         </tr>
                      </thead>
                      <tbody>
                      </tbody>
                   </table>
                  </div><!-- panel-body -->
                </div><!-- panel -->
            </div>
            </div>
        </div>
    </div>
@endsection