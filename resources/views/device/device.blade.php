@extends('layouts.adminltemain')
@section('content-main')
    <script src="/js/device.js"></script>
    <script src="/js/pushnotification.js"></script>
    <link href="/css/progressbar-style.css" rel="stylesheet">
    <div class="pageheader">
        <section class="content-header">
            <h2><i class="fa fa-tablet"></i>Devices</h2>
            <ol class="breadcrumb">
                <li><a href="/dashboard">{{ env('OSP_LABEL') }}</a></li>
                <li class="active">Devices</li>
            </ol>
        </section>

      {{--<h2><i class="fa fa-tablet"></i> Devices </h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="/dashboard">OSP</a></li>

        </ol>
      </div>--}}
    </div>
    <div class="contentpanel">
      <div class="panel panel-default">
                        <div class="panel-heading">
                            <h5 class="bug-key-title"></h5>
                            <div class="panel-title" id="serialnumber"></div>
                        </div><!-- panel-heading-->
                        <div class="panel-body">
                        @if ( Auth::user()->role_id < 4)
                            <div class="btn-group mr10" >
                                <button class="btn btn-success pushnotification" type="button" command="restart"  value='1' id="restart"><i class="fa fa-retweet mr5"></i> Restart</button>
                                <button class="btn btn-primary pushnotification" type="button" command="wipe" value='2' id="wipe"><i class="fa  fa-eraser"></i> Wipe</button>
                                <button class="btn btn-warning pushnotification" data-toggle="modal" data-target="#myModal"  type="button" command="factoryreset" id="reset"><i class="fa fa-magic mr5"></i> Factory Reset</button>
                                <button class="btn btn-darkblue pushnotification" type="button" command="un-enroll" id="enroll"><i class="glyphicon glyphicon-arrow-down"></i> UN-ENROLL</button>
                                <button class="btn btn-danger pushnotification" type="button" command="kiosk" id="del"><i class="glyphicon glyphicon-lock"></i> Disable KIOSK</button>
                                <input type="hidden" id="profileid" > <input type="hidden" id="deviceid" >
                            </div>

                            <div class="btn-group mr10" >
                                <button class="btn btn-primary pushnotification" type="button" command="refresh" id="refresh"><i class="fa fa-spinner mr5"></i>Refresh</button>
                            </div>
                            <br /><br />
                        @endif
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5 class="subtitle subtitle-lined">Details</h5>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-xs-6">Device Identifier</div>
                                                <div class="col-xs-6" id="devicename"></div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-6">Profile</div>
                                                <div class="col-xs-6" id="profilename"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-6">Status</div>
                                                <div class="col-xs-6" id="devicestatus"></div>
                                            </div>
                                             <div class="row">
                                                <div class="col-xs-6">Battery Level</div>
                                                <div class="col-xs-6" id="devicebattery"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-6">Activation Date</div>
                                                <div class="col-xs-6" id="deviceactivationdate"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-6">Last Seen On</div>
                                                <div class="col-xs-6" id="lastseen"></div>
                                            </div>
                                             <div class="row">
                                                <div class="col-xs-6">OSP Client Version</div>
                                                <div class="col-xs-6" id="deviceospversion"></div>
                                            </div>

                                        </div><!-- col-sm-6 -->
                                        <div class="col-sm-6">
                                        	<div class="row">
                                                <div class="col-xs-6">Model Number</div>
                                                <div class="col-xs-6" id="devicemodelnumber"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-6">MAC Address</div>
                                                <div class="col-xs-6" id="devicemacaddress"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-6">IP Address</div>
                                                <div class="col-xs-6" id="deviceipaddress"></div>
                                            </div>
                                             <div class="row">
                                                <div class="col-xs-6">Location</div>
                                                <div class="col-xs-6" id="devicelocation"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-6">Operating System</div>
                                                <div class="col-xs-6" id="deviceos"></div>
                                            </div>
                                             <div class="row">
                                                <div class="col-xs-6">Gateway IP Address</div>
                                                <div class="col-xs-6" id="devicegateway"></div>
                                            </div>
                                             <div class="row">
                                                <div class="col-xs-6">Profile Counter</div>
                                                <div class="col-xs-6" id="deviceprofilecounter"></div>
                                            </div>



                                    </div><!-- row -->
                                    </div>
                                    <br /><br />

                                </div>
                            </div>
</div><!-- panel-body -->
      </div>
        <div class="contentpanel">
      <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="" class="panel-close">&times;</a>
            <a href="" class="minimize">&minus;</a> -->
      <div class="panel-body">
          <h5 class="subtitle mb5">TRACE</h5>

          <br />
          <div class="table-responsive">
            <table class="table table-striped" id="devicelogtable">
              <thead>
                 <tr>
                   <th>Date</th>
                    <th>Activity</th>
                    <th>Result</th>
                    <th>Local/Remote</th>
                    <th>Data</th>
                 </tr>
              </thead>
              <tbody>


              </tbody>
           </table>


        </div><!-- panel-body -->
      </div><!-- panel -->
      </div>
    </div>
    </div>
    <br>
    <div class="panel-heading">
          <div class="panel-btns">
            <div class="panel-body">
              <h5 class="subtitle mb5">APPLICATIONS</h5>
              <br />
              <div class="table-responsive">
                <table class="table table-striped" id="deviceapplicationtable">
                  <thead>
                     <tr>
                       <th>Packge Name</th>
                        <th>Version</th>
                        <th>Date Installed</th>
                     </tr>
                  </thead>
                  <tbody>
                  </tbody>
               </table>


            </div><!-- panel-body -->
            </div><!-- panel -->

                    </div><!-- contentpanel -->
            </div>
@endsection