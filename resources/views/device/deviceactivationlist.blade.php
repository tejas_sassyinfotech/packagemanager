@extends('layouts.adminltemain')
@section('content-main')
    <script src="/js/device.js"></script>
    <div class="pageheader">
        <section class="content-header">
            <h2><i class="fa fa-tablet"></i>Device Activation List</h2>
                <ol class="breadcrumb">
                    <li><a href="/dashboard">{{ env('OSP_LABEL') }}</a></li>
                    <li class="active">Device Activation List</li>
                </ol>
        </section>
    </div>
    <div class="contentpanel">
          <div class="panel panel-default">
                  <div class="panel-heading">
                    <!-- <div class="panel-btns">
                      <a href="" class="panel-close">&times;</a>
                      <a href="" class="minimize">&minus;</a>
                    </div> -->
                     <div class="form-group">
                        <div class="col-md-3">
                            <div class="form-group">
                            <input class="fromDate form-control" placeholder="Start Date" type="text">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                            <input class="toDate form-control" placeholder="End Date" type="text">
                            </div>
                        </div>
                      <button id="deviceSearch" value="search" type="button" class="btn btn-primary">SEARCH</button>
                      <a id="exportToExcel" href="/deviceactivationexcel/" type="button" class="btn btn-primary">EXPORT</a>
                      <div class="row"><div class="col-md-12"><div id="filter_error" style="color: red; class="error"></div></div></div>
                      </div><!-- form-group -->
                  </div><!-- panel -->
          </div>
          <div class="contentpanel"></div>
          <div class="panel panel-default">
                  <div class="panel-heading">
                    <!-- <div class="panel-btns">
                      <a href="" class="panel-close">&times;</a>
                      <a href="" class="minimize">&minus;</a>
                      </div> -->
                  <div class="panel-body">
                    <br />
                    <h5 class="subtitle mb5">Device List</h5>
                  <br />
                  <div class="table-responsive">
                    <table class="table table-striped" id="deviceActivationListtable">
                      <thead>
                         <tr>
                            <th id="group">Serial Number </th>
                            <th>Device Identifier</th>
                            <th>MDM</th>
                            <th>Profile Name</th>
                            <th>Counter</th>
                            <th>Battery</th>
                            <th>Location</th>
                            <th>Additional Info</th>
                            <th>Last Update</th>
                            {{--<th></th>--}}
                         </tr>
                      </thead>
                      <tbody id="deviceActivationBody">
                      </tbody>
                   </table>
                  </div>
                </div><!-- panel -->
            </div>
            </div>

                </div>
@endsection