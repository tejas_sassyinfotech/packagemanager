@extends('layouts.adminltemain')
@section('content-main')
    <script src="/js/pushnotification.js"></script>
        <script src="/js/profile.js"></script>
        <link href="/css/progressbar-style.css" rel="stylesheet">
        <div class="pageheader">
            <section class="content-header">
                <h2><i class="fa fa-edit"></i>Configuration<span>Profiles</span></h2>
                    <ol class="breadcrumb">
                        <li><a href="/dashboard">{{ env('OSP_LABEL') }}</a></li>
                        <li class="active"><a href="/configurations">Configuration</a></li>
                        <li class="active">Profile</li>
                    </ol>
            </section>
          </div>
    <div class="contentpanel">
        <h5 class="subtitle mb5">PROFILE</h5>
        <div class="panel-footer">
            <div class="form-group">
               <div class="col-sm-12">
                    <label class="col-sm-4 control-label">PROFILE NAME</label>
                    <div class="col-sm-5">
                        <input type="text" name="profilename" id="profilename" class="form-control"/>
                        <input type="hidden" name="userprofileid" id="userprofileid" />
                        <input type="hidden" name="tabShow" id="tabShow" value="<?php echo (!empty($id)) ? 1 : 0 ?> "/>
                        <input type="hidden" name="profileid" id="profileid" />
                        <span class="help-block"></span>
                        <?php $checked =  (empty($id)) ? 'checked' : '' ?>
                    </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">TYPE</label>
                          <div class="col-sm-4">
                          <?php if ($id != ''){
                            $disabled = 'disabled';
                          }else {
                            $disabled = '';
                           } ?>
                            <select id="profile_type" name="profile_type" class="select2 form-control"  <?php echo $disabled;?> data-placeholder="Choose One">
                                <option value="">Please Select</option>
                                <option value="1">Android</option>
                                <option value="2">IOS</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-sm-offset-4">
                         <p id="_profileerror" class="text-danger" style="display: none;"></p>
                         <p id="_profilesuccess" class="success-message" style="display: none;"></p>
                         </div>
                       {{-- @endif--}}
               </div>
             </div>
			 <div class="row">
				<div class="col-sm-6 text-right">
				  <button id="_create" value="SAVE" class="btn btn-primary">SAVE</button>
			    @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)
				    @if ($id != '')
                  	    <button onClick="javascript:void();" id="_editProfile" value="config"  class="btn btn-primary editProfile">Change Profile Name</button>
                  	@endif
                @endif
				</div>
				 @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)
				 <div class="col-sm-6 text-left" id="settingsapply" style="display: none;">
				  <button onClick="javascript:void();" id="_apply" value="config"  class="btn btn-primary">Apply</button>

				</div>
				@endif
			 </div>
		  </div><!-- panel-footer -->
          <br />
        <!-- Nav tabs -->
        <div id="settings" style="display: none;">
        <ul class="nav nav-tabs">
            <?php $Anactive ='';
            $countAn = 0;
            if(sizeof(($tabNameArray))){
            foreach($tabNameArray as $tabkey => $tabValue){
                            if($tabkey < sizeof($tabNameArray)-1){
                                $valueAn = $tabNameArray[$tabkey+1]['tab_name'];
                            }
                            if($tabValue['tab_status'] != 1){
                                $Anstyle = 'display:none;';
                            }else{
                                $Anstyle = '';
                            }
                            if($countAn == 0){
                                $Anactive = 'active';
                            }else{
                                $Anactive = '';
                            }
                            if($tabValue['tab_name'] == 'home'){
                                $tabValueName = 'Settings';
                            }else if($tabValue['tab_name'] == 'packagedisabler'){
                                $tabValueName = 'Disable Applications';
                            }else{
                                $tabValueName = ucfirst(str_replace("settings","",preg_replace('~\B[A-Z]~x', ' $0', $tabValue['tab_name'])));
                            }?>
                            <li class="<?php echo $Anactive; ?>"><a href="#<?php echo $tabValue['tab_name'];?>" class="<?php echo $tabValue['tab_name'];?>" data-toggle="tab" style="<?php echo $Anstyle; ?>" next_to="<?php echo $valueAn; ?>"><strong><?php echo $tabValueName; ?></strong></a></li>
                        <?php
                        $countAn = $countAn + 1;}
            }else{
            ?>

             <li class="active"><a href="#home" class="home" data-toggle="tab" next_to="wifi"><strong>Settings</strong></a></li>
          <li><a href="#wifi" class="wifi" data-toggle="tab"  next_to="packagedisabler"><strong>WiFi</strong></a></li>
          <li><a href="#packagedisabler" class="packagedisabler" data-toggle="tab" next_to="kiosksettings"><strong>Disable Applications</strong></a></li>
          <!--<li><a href="#browser" data-toggle="tab"><strong>Browser</strong></a></li>-->
          <li><a href="#kiosksettings" class="kiosksettings" data-toggle="tab" next_to="applicationsettings"><strong>KIOSK</strong></a></li>
          <li><a href="#applicationsettings" class="applicationsettings" data-toggle="tab" next_to="emailsettings"><strong>Applications</strong></a></li>
          <li><a href="#emailsettings" class="emailsettings" data-toggle="tab" next_to="activeSyn"><strong>Email</strong></a></li>
            <li><a href="#activeSyn"  class="activeSyn" data-toggle="tab" next_to="commands"><strong>Active Sync</strong></a></li>
          <li><a href="#commands" class="commands" data-toggle="tab" next_to="home"><strong>Advanced</strong></a></li>
          <?php } ?>
        </ul>
        <div class="tab-content mb30">

        <div class="tab-pane active" id="home">
                    <div class="panel-body panel-body-nopadding">

                  <form id="config"  value="config" class="form-horizontal form-bordered" method="post" action="function.php">
                       <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox"> MDM CLIENT DISPLAY</label>
        				  <div class="col-sm-6">
        					 <div class="checkbox block"><label><input type="checkbox" name="status" id="mdm_display_status" value="0"> Enable</label></div>
        				  </div>
        				</div>
                        <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">STATUS BAR</label>
        				  <div class="col-sm-6">
        					 <div class="checkbox block"><label><input type="checkbox" name="status" id="status_bar_status" value="0"> Disable</label></div>
        				  </div>
        				</div>
                        <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">HOME KEY</label>
        				  <div class="col-sm-6">
        					 <div class="checkbox block"><label><input type="checkbox" name="homekey" id="home_key_status" value="0"> Disable</label></div>
        				  </div>
        				</div>
                     <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">BACK KEY</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="back_key_status" value="0"> Disable</label></div>
                                </div>
        				</div>
                              <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">RECENTS KEY</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="recents_key_status" value="0"> Disable</label></div>
                                            </div>
                         </div>

                         <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">POWER KEY</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="power_key_status" value="0"> Disable</label></div>

        				  </div>


        				</div>
                              <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox" >FIRMWARE UPGRADE</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="firmware_upgrade_status" value="0"> Disable</label></div>
                                </div>
        				</div>

                     <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">FACTORY RESET</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="factory_reset_status" value="0"> Disable</label></div>

        				  </div>
        				</div>
                                <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">S-VOICE</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="s_voice_status" value="0"> Disable</label></div>

        				  </div>
        				</div>
                      <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">ANDROID SETTINGS</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="android_settings_status" value="0"> Disable</label></div>

        				  </div>
        				</div>
                      <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">VOLUME KEY</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="volume_key_status" value="0"> Disable</label></div>

        				  </div>
        				</div>

                      <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">MULTI-WINDOW</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="multi_window_status" value="0"> Disable</label></div>

        				  </div>
        				</div>
        				 <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">NOTIFICATIONS</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="notifications_status" value="0"> Disable</label></div>

        				  </div>
        				</div>
        				 <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">LOCK SCREEN ( Once disabled cannot be enabled remotely )</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="lock_screen_status" value="0"> Disable</label></div>

        				  </div>
        				</div>
        				 <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">BLUETOOTH</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="bluetooth_status" value="0"> Disable</label></div>

        				  </div>
        				</div>
        				<div class="form-group">
                          <label class="col-sm-3 control-label" for="checkbox">WIFI</label>
                          <div class="col-sm-6">

                             <div class="checkbox block"><label><input type="checkbox" id="wifi_status" value="0"> Disable</label></div>

                          </div>
                        </div>
        				 <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">GPS</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="gps_status" value="0"> Disable</label></div>

        				  </div>
        				</div>

                    <div class="form-group">
                      <label class="col-sm-3 control-label">DEVICE TIMEOUT ( Minimum 15 sec )</label>
                      <div class="col-sm-9">
                        <input type="text" name="timeout" id="device_timeout" value="1500"/>
                        <span class="help-block">Second</span>
                      </div>
                    </div>
                     <div class="form-group">
                      <label class="col-sm-3 control-label">PULL INTERVAL( Minimum 15 min )</label>
                      <div class="col-sm-9">
                        <input type="text" name="timeout" id="pull_interval" value="360"/>
                        <span class="help-block">Minutes</span>
                      </div>
                    </div>

                     <div class="form-group">
                      <label class="col-sm-3 control-label">BRIGHTNESS ( 0-100 )</label>
                      <div class="col-sm-9">
                        <input type="text" name="brightness" id="brightness_percentage" value="50"/>
                        <span class="help-block">%</span>
                      </div>
                    </div>
                     <div class="form-group">
                      <label class="col-sm-3 control-label">VOLUME ( 0-100 )</label>
                      <div class="col-sm-9">
                        <input type="text" name="volume" id="volume_percentage" value="0"/>
                        <span class="help-block">%</span>
                      </div>
                    </div>
                     <div class="form-group">
                      <label class="col-sm-3 control-label">SLEEP ( 0-100 )</label>
                      <div class="col-sm-9">
                        <input type="text" name="sleep" id="sleep_percentage" value="0"/>
                        <span class="help-block">%</span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">DEEP SLEEP ( 0-100 )</label>
                      <div class="col-sm-9">
                        <input type="text" name="deepsleep" id="deep_sleep_percentage" value="0"/>
                        <span class="help-block">%</span>
                      </div>
                    </div>
                    <div class="form-group">
                          <label class="col-sm-3 control-label">ORIENTATION</label>
                          <div class="col-sm-6">
                            <select id="orientation" class="select2" required data-placeholder="Choose One">

                              <option value="0">AUTO</option>
                              <option value="1">PORTRAIT</option>
                              <option value="2">LANDSCAPE</option>

                            </select>


                    </div>
                    </div>

                    <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">Settings Menu (Backup/Reset)</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="backup_reset_menu_status" value="0"> HIDE</label></div>

        				  </div>
        				</div>


                    <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">Settings Menu (Developer Mode)</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="developer_mode_menu_status" value="0"> HIDE</label></div>

        				  </div>
        				</div>


                    <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">Settings Menu (Airplane Mode)</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="airplane_mode_menu_status" value="0"> HIDE</label></div>

        				  </div>
        				</div>

        			  <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">Settings Menu (Langauge)</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="language_menu_status" value="0"> HIDE</label></div>

        				  </div>
        				</div>

                    <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">Settings Menu (Multi-Window)</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="multi_window_menu_status" value="0"> HIDE</label></div>

        				  </div>
        				</div>


                    <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">Settings Menu (users)</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="users_settings_menu_status" value="0"> HIDE</label></div>

        				  </div>
        				</div>

        			  <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">Settings Menu (Bluetooth)</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="bluetooth_settings_menu_status" value="0"> HIDE</label></div>

        				  </div>
        				</div>
        			  <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">Settings Menu (Lockscreen)</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="lockscreen_menu_status" value="0"> HIDE</label></div>

        				  </div>
        				</div>
                    <div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">Settings Menu (WiFi)</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="wifi_settings_menu_status" value="0"> HIDE</label></div>

        				  </div>
        				</div>
        				<div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">Power On Boot</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="power_on_boot_status" value="0"> Enable</label></div>

        				  </div>
        				</div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label" for="checkbox">Disable All Bloatware</label>
                          <div class="col-sm-6">

                             <div class="checkbox block"><label><input type="checkbox" id="disable_all_bloatware_status" value="0"> Enable</label></div>

                          </div>
                        </div>
        				<div class="form-group">
        				  <label class="col-sm-3 control-label" for="checkbox">UNKNOWN SOURCE</label>
        				  <div class="col-sm-6">

        					 <div class="checkbox block"><label><input type="checkbox" id="unknown_source_status" value="0"> Enable</label></div>

        				  </div>
        				</div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label" for="checkbox">DEVELOPER MODE</label>
                          <div class="col-sm-6">

                             <div class="checkbox block"><label><input type="checkbox" id="developer_mode_status" value="0"> Enable</label></div>

                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label" for="checkbox">SCREENSHOT</label>
                          <div class="col-sm-6">

                             <div class="checkbox block"><label><input type="checkbox" id="screenshot_status" value="0"> Disable</label></div>

                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label" for="checkbox">USB DEBUGGING</label>
                          <div class="col-sm-6">

                             <div class="checkbox block"><label><input type="checkbox" id="usb_debugging_status" value="0"> Enable</label></div>

                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label" for="checkbox">LTE MODE</label>
                          <div class="col-sm-6">

                             <div class="checkbox block"><label><input type="checkbox" id="lte_mode_status" value="0"> Disable</label></div>

                          </div>
                        </div>
                  </form>
                  @if ( Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5 )<button value="SAVE" class="btn btn-primary savesettings" currenttab="home">SAVE</button>@endif
                </div><!-- panel-body -->


                </div>

        <div class="tab-pane" id="wifi">
            <form id="config"  value="config" class="form-horizontal form-bordered" method="post" action="form-validation.html">
            @if ( Auth::user()->role_id < 4 ||  Auth::user()->role_id == 5)
            <div class="form-group">
              <label class="col-sm-3 control-label">AP SETTINGS</label>
              <div class="col-sm-5">
                <input type="text" id="ssid" placeholder="SSID" class="form-control mb15">
                <p id="_wifierror" class="text-danger" style="display: none;"></p>
                <input type="text" id="wifiusername" placeholder="Identity" class="form-control mb15">
                <input type="password" id="wifipassword" placeholder="Password" class="form-control mb15">
         <!--       <input type="text" id="eap" placeholder="EAP Method" class="form-control mb15">
                <input type="text" id="phase2" placeholder="Phase 2 Authentication" class="form-control mb15"> -->
                <p id="_wifisuccess" class="success-message" style="display: none"></p>
              </div>
               <button id="addWifi" value="add" type="button" class="btn btn-primary">ADD</button>
            </div>

            <div class="form-group">
                  <label class="col-sm-3 control-label">EAP METHOD</label>
                  <div class="col-sm-5">
                    <select id="eapmethod" class="select2" required data-placeholder="Choose One">
                      <option value="Open">OPEN</option>
                      <option value="EAP-TLS">EAP-TLS</option>
                      <option value="WEP">WEP</option>
                      <option value="WPA">WPA</option>
                      <option value="PSK">WPA2-PSK</option>
                      <option value="EAP-LEAP">EAP-LEAP</option>
                      <option value="EAP-FAST">EAP-FAST</option>
                      <option value="EAP-PEAP">EAP-PEAP</option>
                      <option value="EAP-TTLS">EAP-TTLS</option>
                    </select>

                  </div>
                </div><!-- form-group -->

              <div class="form-group">
                  <label class="col-sm-3 control-label">PHASE 2 AUTHENTICATION </label>
                  <div class="col-sm-5">
                    <select id="phase2auth" class="select2" required data-placeholder="Choose One">
                      <option value="NONE">NONE</option>
                      <option value="PAP">PAP</option>
                      <option value="MSCHAP">MSCHAP</option>
                      <option value="MSCHAPV2">MSCHAPV2</option>
                      <option value="GTC">GTC</option>
                    </select>
                  </div>
                </div><!-- form-group -->
                 @endif
                  <div class="form-group">
                  @if ( Auth::user()->role_id < 4 ||  Auth::user()->role_id == 5)
            <div class="form-group">
              <label class="col-sm-3 control-label">GLOBAL PROXY</label>
              <div class="col-sm-9">
                <input type="text" name="proxy" id="proxy" />
                <span class="help-block"></span>
              </div>
            </div>

            <div class="form-group">
               <label class="col-sm-3 control-label">GLOBAL PORT</label>
              <div class="col-sm-9">
                <input type="text" name="port" id="port" />
                <span class="help-block"></span>
              </div>
            </div>

                <div class="form-group">
                </div>
                @endif
                 </div>
            </form>


             <br />
          <div class="table-responsive">
            <table class="table" id="wifitable">
              <thead>
                 <tr>
                    <th>SSID</th>
                    <th>Identity</th>
                	<th>EAP Method</th>
                    <th>Phase 2 Authentication</th>
                    @if (Auth::user()->role_id < 4)
            		<th>Action</th>
            		@endif
                 </tr>
              </thead>
              <tbody>

              </tbody>
           </table>
           </div>
           @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id !=5)<button value="SAVE" success="_wifisuccess" class="btn btn-primary savesettings" currenttab="wifi">SAVE</button>@endif
        </div>

          <div class="tab-pane" id="packagedisabler">
                     @if ( Auth::user()->role_id < 4)
                   <form id="bulkupload"  value="bulkupload" class="form-horizontal form-bordered" method="post" enctype="multipart/form-data" action="">
                      <div class="form-group">
                          <label class="col-sm-3 control-label">DISABLE PACKAGE</label>
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <input type="hidden" name="profileid" class="profileidpersist" value="">
                          <div class="col-sm-4">
                            <label class="sr-only" for="packagename">Package Name</label>
                            <input type="packagename" class="form-control" id="disablepackagename" placeholder="com.android.email">
                            <p id="_disableappsuccess" class="success-message" style="display: none"></p>
                          </div>
                          <button id="disableapp" value="add" type="button" class="btn btn-primary">ADD</button>
                      </div>

                      <div class="form-group">
                            <label class="col-sm-3 control-label">BULK UPLOAD</label>
                            <div class="col-sm-4">
                            <input type="file" name="import_file" id="bulkuploaddisableapps" />
                            <a href="/samplefile/disable_app_list.xlsx">download sample file</a>
                            <div id="disableapperror" style="display:none;color:red"></div>
                            </div>
                            <div class="col-sm-2 text-right">
                            <input class="btn btn-primary btn-sm" type='submit' id='_bulksubmit' value='BULK UPLOAD'>
                            </div>
                            <div class="col-sm-3">
                            <a class="btn btn-primary btn-sm" id="disableapplink" href="#">EXPORT</a>
                            </div>
                      </div>
                      <hr>
                    </form>
                    <br />
                    @endif
                    <div class="table-responsive">
                      <table class="table" id="packagedisabletable">
                        <thead>
                           <tr>
                              <th>Package Name</th>
                              @if (Auth::user()->role_id < 4)
                      		<th>Action</th>
                                @endif
                           </tr>
                        </thead>
                        <tbody>
                        </tbody>
                     </table>
                    </div>
                     @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" success="_disableappsuccess" class="btn btn-primary savesettings" currenttab="packagedisabler">SAVE</button>@endif
          </div>
          <div class="tab-pane" id="kiosksettings">
                     @if ( Auth::user()->role_id < 4)
                             <form id="config"  value="config" class="form-horizontal form-bordered" method="post" action="toggleFields()" >
                               <div class="form-group">
                                      <label class="col-sm-3 control-label">KIOSK TYPE</label>
                                      <div class="col-sm-6">
                                        <select id="kiosk_type" class="select2" required data-placeholder="Choose One">
                                          <option value="0">NONE</option>
                                          <option value="3">1.SINGLE APP KIOSK</option>
                                          <option value="2">2.BROWSER KIOSK</option>
                                          <option value="1">3.DASHBOARD KIOSK</option>
                                        </select>
                                      </div>
                                </div><!-- form-group -->
                                 <div class="form-group">
                                      <label class="col-sm-3 control-label">KIOSK PASSWORD</label>
                                  <div class="col-sm-6">
                                    <input type="passwd" class="form-control" id="kiosk_password" placeholder="ENTER KIOSK PASSWORD">
                                  </div>
                                </div><!-- form-group -->
                                 <div class="form-group">
                                 <label class="col-sm-3 control-label">1. SINGLE APP KIOSK</label>
                                  <div class="col-sm-6">
                                  <label class="sr-only" for="singleapp">singleapp</label>
                                  <input type="singleapp" class="form-control" id="single_app_kiosk" placeholder="ENTER a package name ( com.microsoft.word )">
                                   <label id="SA"></label>
                                  <label id="help1"><font color="orange"> <i>NOTE: Please disable STATUS BAR, BACK KEY, RECENTS KEY </i></font></label>
                                </div>
                                </div>
                                 <div class="form-group">
                                 <label class="col-sm-3 control-label">2. BROWSER KIOSK</label>
                                  <div class="col-sm-6">
                                  <label class="sr-only" for="singleapp">browserkiosk</label>
                                  <input type="url" class="form-control" id="browser_kiosk" placeholder="ENTER an URL starting with HTTP or HTTPS ( http://www.bbc.co.uk )">
                                   <label id="BU"></label>
                                   <label id="help2"><font color="orange"><i>NOTE: Please disable STATUS BAR, HOME KEY, BACK KEY, RECENTS KEY</i></font></label>
                                </div>
                                </div>
                                <div>
                                 <label class="col-sm-3 control-label" style="padding-top: 20px;">3. DASHBOARD KIOSK</label>
                                </div>
                                <div class="col-lg-12" style="padding-top: 20px;">
                                 <label class="col-sm-3 control-label">NUMBER OF SCREENS</label>
                                  <div class="col-sm-4">
                                      <input type="text" class="form-control" id="screennumber">
                                  </div>
                                </div>
                                <div class="col-lg-12" style="padding-top: 20px;">
                                 <label class="col-sm-3 control-label">SPECIAL SCREEN NUMBER</label>
                                  <div class="col-sm-4">
                                      <input type="text" class="form-control" id="splscreennumber">
                                  </div>
                                </div>
                                <div class="col-lg-12" style="padding-top: 20px;">
                                 <label class="col-sm-3 control-label">NUMBER OF COLUMNS</label>
                                  <div class="col-sm-4">
                                      <input type="text" class="form-control" id="numberofcolumns">
                                      <p id="_kiosksuccess" class="success-message" style="display:none"></p>
                                  </div>
                                  <div class="col-sm-4">
                                     <button id="addKioskSettings" value="add" type="button" class="btn btn-primary">ADD/EDIT SETTINGS</button>
                                  </div>
                                </div>
                                <div class="form-group">
                                <div class="col-lg-12" style="padding-top: 20px;">
                                 <label class="col-sm-3 control-label">Package Name / URL</label>
                                 <div class="col-sm-4">
                                 <input type="text" class="form-control" id="kioskdashboard" placeholder="com.microsoft.word OR http://www.bbc.com">
                                 </div>
                                  <div class="col-sm-4">
                                    <select id="kioskbrowser" class="select2" required data-placeholder="Choose One">
                                        <option value="2">APPLICATION</option>
                                        <option value="1">OSP BROWSER</option>
                                        <option value="0">STANDARD BROWSER</option>
                                    </select>
                                  </div>
                                </div>
                                </div>
                                <div class="col-lg-12" style="padding-top: 20px;">
                                 <label class="col-sm-3 control-label">SCREEN</label>
                                  <div class="col-sm-4">
                                  <input type="text" class="form-control" id="kioskpriority">
                                  </div>
                                </div>
                                <div class="col-lg-12" style="padding-top: 20px;">

                                 <label class="col-sm-3 control-label">ROW</label>
                                  <div class="col-sm-4">
                                  <input type="text" class="form-control" id="kioskrow">
                                </div>
                                </div>
                                <div class="col-lg-12" style="padding-top: 20px; padding-bottom: 20px; border-bottom: 1px solid #d3d7db; margin-bottom: 20px;">
                                 <label class="col-sm-3 control-label">COLUMN</label>
                                  <div class="col-sm-4">
                                    <input type="text" class="form-control" id="kioskcolumn">
                                    <p id="_kioskaddsuccess" class="success-message" style="display:none"></p>
                                  </div>
                                 <div class="col-sm-4">
                                    <button id="addKiosk" value="add" type="button" class="btn btn-primary">ADD</button>
                                 </div>
                                </div>

                                <hr>
                              </form>
                              <br />
                              @endif
                              <div class="table-responsive col-sm-12">
                                <table class="table" id="kiosktable">
                                  <thead>
                                     <tr>
                                        <th>Package Name / URL</th>
                                        <th>Type</th>
                                        <th>Screen</th>
                                        <th>Row</th>
                                        <th>Column</th>
                                        @if (Auth::user()->role_id < 4)
                                		<th>Action</th>
                                		@endif
                                     </tr>
                                  </thead>
                                  <tbody>
                                  </tbody>
                               </table>
                        </div>
                         @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" success="_kioskaddsuccess" class="btn btn-primary savesettings" currenttab="kiosksettings">SAVE</button>@endif
                    </div>

          <div class="tab-pane" id="applicationsettings">
                    <form id="config"  value="config" class="form-horizontal form-bordered" enctype="multipart/form-data" method="post" action="" >
                      {{ csrf_field() }}
                    @if ( Auth::user()->role_id < 4)
                    <div class="form-group">
                     <label class="col-sm-3 control-label">SELECT APPLICATION</label>
                        <div class="col-sm-6">
                               <select id="addapplications" class="select2" required data-placeholder="Choose One">
                                 <option value="0">CHOOSE FROM APP LIST</option>
                               </select>
                          <button id="assignapp" value="add" type="button" class="btn btn-primary">ADD AND APPLY</button>
                       </div>
                       </div>
                    @endif
                    <!--<div class="form-group">
                        <label class="col-sm-3 control-label">PACKAGE NAME (optional)</label>
                        <div class="col-sm-6">
                            <input type="text" name="package" id="package" />
                            <span class="help-block"></span>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">VERSION CODE (optional)</label>
                        <div class="col-sm-6">
                            <input type="text" name="vcode" id="vcode">
                            <span class="help-block"></span>
                        </div>
                    </div> -->
                   <div class="form-group">
                    @if ( Auth::user()->role_id < 4)
                    <label class="col-sm-3 control-label">UPLOAD NEW APP / WALLPAPER</label>
                    @endif
                     <div class="col-sm-6">
                     @if ( Auth::user()->role_id < 4)
                   <p>
                     <input type='file' id='_file' name="file"><p></p> <input class="btn btn-primary btn-sm" type='submit' id='_submit' value='UPLOAD AND APPLY'>
                     <div id='err-msg' style="display:none;color:red">No file selected</div>
                     <input type="text" id="errormsg" placeholder="" readonly style="border: none;color:red;" >
                   </p>

                    <h3 id="status"></h3>
                    <div id='err' style="display:none;color:red"></div>
                    <div class="col-sm-6">
                    <div class="w3-progress-container">
                        <div id="myBar-profile" class="w3-progressbar w3-green" style="width:0%"></div>
                      </div>
                    </div>
                  @endif
              </div>


                    
                     <div class="table-responsive col-sm-12">
                       <table class="table" id="applicationstable">
                         <thead>
                            <tr>
                            	<th id="group">Application Name</th>
                            	<th>Package Name</th>
                            	<th>Version</th>
                            	@if (Auth::user()->role_id < 4)
                       		<th>Action</th>
                       		@endif
                            </tr>
                         </thead>
                         <tbody>

                         </tbody>
                      </table>


               	  </div>
                     </div>
                  </form>
                  <div id='profileapp-message' style="display:none;color:green"></div></br>
                   @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary savesettings" currenttab="applicationsettings">SAVE</button>@endif
                </div>

          <div class="tab-pane" id="emailsettings">
                       @if ( Auth::user()->role_id < 4)
                               <form id="configEmail"  value="config" class="form-horizontal form-bordered" method="post" action="" >
                                   <div class="form-group">
                                        <label class="col-sm-3 control-label">Account Description</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control emailRequired" id="account_description" placeholder="ENTER ACCOUNT DESCRIPTION">
                                    </div>
                                  </div><!-- form-group -->
                                   <div class="form-group">
                                   <label class="col-sm-3 control-label">User Display Name</label>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" name="user_display_name" id="user_display_name" placeholder="ENTER USER DISPLAY NAME">
                                  </div>
                                  </div>
                                   <div class="form-group">
                                   <label class="col-sm-3 control-label">Email Address</label>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="user_email_address" name="user_email_address" placeholder="ENTER EMAIL ADDRESS">
                                  </div>
                                  </div>
                                  <div>
                                   <label class="col-sm-3 control-label" style="padding-top: 20px;">INCOMING MAIL</label>
                                  </div>
                                  <div class="col-lg-12" style="padding-top: 20px;">
                                   <label class="col-sm-3 control-label">incoming Account Type</label>
                                    <div class="col-sm-4">
                                        <select id="incoming_mail_account_type" name="incoming_mail_account_type" class="select2 form-control" data-placeholder="Choose One">
                                            <option value="">Please select</option>
                                            <option value="POP">POP</option>
                                            <option value="SMTP">SMTP</option>
                                        </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-12" style="padding-top: 20px;">
                                   <label class="col-sm-3 control-label">Incoming Mail Server</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control emailRequired" id="incoming_mail_server" name="incoming_mail_server">
                                    </div>
                                  </div>
                                  <div class="col-lg-12" style="padding-top: 20px;">
                                   <label class="col-sm-3 control-label">Incoming Server port</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="incoming_mail_server_port" name="incoming_mail_server_port">
                                    </div>
                                  </div>
                                  <div class="col-lg-12" style="padding-top: 20px;">
                                   <label class="col-sm-3 control-label">User Name</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="incoming_mail_user_name" name="incoming_mail_user_name">
                                    </div>
                                  </div>
                                  <div class="col-lg-12" style="padding-top: 20px;">
                                   <label class="col-sm-3 control-label">Password</label>
                                    <div class="col-sm-4">
                                      <input type="password" class="form-control" id="incoming_mail_password" name="incoming_mail_password">
                                    </div>
                                  </div>


                                  <div class="form-group">
                                    <label class="col-sm-2 control-label" for="checkbox">USE SSL</label>
                                    <div class="col-sm-1">
                                       <div class="checkbox block"><label><input name="incoming_mail_ssl_usage" id="incoming_mail_ssl_usage" value="0" type="checkbox"></label></div>
                                    </div>
                                   <label class="col-sm-2 control-label" for="checkbox">USE TLS</label>
                                    <div class="col-sm-1">
                                       <div class="checkbox block"><label><input name="incoming_mail_tls_usage" id="incoming_mail_tls_usage" value="0" type="checkbox"></label></div>
                                    </div>
                                    <label class="col-sm-2 control-label" for="checkbox">USE Certificate</label>
                                        <div class="col-sm-1">
                                           <div class="checkbox block"><label><input name="incoming_mail_certificate_usage" id="incoming_mail_certificate_usage" value="0" type="checkbox"></label></div>
                                        </div>
                                  </div>
                          <div>
                           <label class="col-sm-3 control-label" style="padding-top: 20px;">OUTGOING MAIL</label>
                          </div>
                          <div class="col-lg-12" style="padding-top: 20px;">
                           <label class="col-sm-3 control-label">Outgoing Account Type</label>
                            <div class="col-sm-4">
                                <select id="outgoing_mail_account_type" name="outgoing_mail_account_type" class="select2 form-control" data-placeholder="Choose One">
                                    <option value="">Please select</option>
                                    <option value="POP">POP</option>
                                    <option value="SMTP">SMTP</option>
                                </select>
                            </div>
                          </div>
                          <div class="col-lg-12" style="padding-top: 20px;">
                           <label class="col-sm-3 control-label">Outgoing Mail Server</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control emailRequired" id="outgoing_mail_server" name="outgoing_mail_server">
                            </div>
                          </div>
                          <div class="col-lg-12" style="padding-top: 20px;">
                           <label class="col-sm-3 control-label">Outgoing Server port</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="outgoing_mail_server_port" name="outgoing_mail_server_port">
                            </div>
                          </div>
                          <div class="col-lg-12" style="padding-top: 20px;">
                           <label class="col-sm-3 control-label">User Name</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="outgoing_mail_user_name" name="outgoing_mail_user_name">
                            </div>
                          </div>
                          <div class="col-lg-12" style="padding-top: 20px;">
                           <label class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-4">
                              <input type="password" class="form-control" id="outgoing_mail_password" name="outgoing_mail_password">
                            </div>
                          </div>


                          <div class="form-group">
                            <label class="col-sm-2 control-label" for="checkbox">USE SSL</label>
                            <div class="col-sm-1">
                               <div class="checkbox block"><label><input name="outgoing_mail_ssl_usage" id="outgoing_mail_ssl_usage" value="0" type="checkbox"></label></div>
                            </div>
                           <label class="col-sm-2 control-label" for="checkbox">USE TLS</label>
                            <div class="col-sm-1">
                               <div class="checkbox block"><label><input name="outgoing_mail_tls_usage" id="outgoing_mail_tls_usage" value="0" type="checkbox"></label></div>
                            </div>
                            <label class="col-sm-2 control-label" for="checkbox">USE Certificate</label>
                                <div class="col-sm-1">
                                   <div class="checkbox block"><label><input name="outgoing_mail_certificate_usage" id="outgoing_mail_certificate_usage " value="0" type="checkbox"></label></div>
                                </div>
                          </div>
                                </form>
                                <br />
                                @endif
                                <div class="table-responsive col-sm-12">
                                  <table class="table" id="androidemailtable">
                                    <thead>
                                       <tr>
                                          <th>Account Description</th>
                                          <th>Display Name </th>
                                          <th>Email Address</th>
                                          @if (Auth::user()->role_id < 4)
                                          <th>Action</th>
                                          @endif
                                       </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                 </table>
                          </div>
                           @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary saveEmailsettings" currenttab="emailsettings">SAVE</button>@endif
                      </div>

            <div class="tab-pane" id="activeSyn">
                         @if ( Auth::user()->role_id < 4)
                                 <form id="config"  value="config" class="form-horizontal form-bordered" method="post" action="" >
                                     <div class="form-group">
                                          <label class="col-sm-3 control-label">Account Name</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control" name="active_sync_account_name" id="active_sync_account_name" placeholder="ENTER ACCOUNT NAME">
                                      </div>
                                    </div><!-- form-group -->
                                     <div class="form-group">
                                     <label class="col-sm-3 control-label">Exchange ActiveSync Host</label>
                                      <div class="col-sm-6">
                                      <input type="text" class="form-control syncRequired" name="active_sync_host" id="active_sync_host" placeholder="ENTER ACTIVESYNC HOST">
                                    </div>
                                    </div>
                                     <div class="form-group">
                                     <label class="col-sm-3 control-label">Domain</label>
                                      <div class="col-sm-6">
                                      <input type="text" class="form-control" id="active_sync_domain" name="active_sync_domain" placeholder="ENTER DOMAIN">
                                    </div>
                                    </div>
                                     <div class="form-group">
                                     <label class="col-sm-3 control-label">User</label>
                                      <div class="col-sm-6">
                                      <input type="text" class="form-control" id="active_sync_user_name" name="active_sync_user_name" placeholder="ENTER USER NAME">
                                    </div>
                                    </div>

                                    <div class="form-group">
                                     <label class="col-sm-3 control-label">Email Address</label>
                                      <div class="col-sm-6">
                                      <input type="text" class="form-control" id="active_sync_user_email" name="active_sync_user_email" placeholder="ENTER EMAIL ADDRESS">
                                    </div>
                                    </div>
                                     <div class="form-group">
                                     <label class="col-sm-3 control-label">Password</label>
                                      <div class="col-sm-6">
                                      <input type="password" class="form-control" id="active_sync_user_password" name="active_sync_user_password" placeholder="ENTER PASSWORD">
                                    </div>
                                    </div>
                                     <div class="form-group">
                                     <label class="col-sm-3 control-label">Sync Schedule</label>
                                      <div class="col-sm-6">
                                        <select id="active_sync_schedule" name="active_sync_schedule" class="select2 form-control" data-placeholder="Choose One">
                                            <option value="">Please select</option>
                                            <option value="Auto">Auto</option>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-3 control-label" for="checkbox">USE SSL</label>
                                      <div class="col-sm-1">
                                         <div class="checkbox block"><label><input name="active_sync_ssl_status" id="active_sync_ssl_status" value="0" type="checkbox"></label></div>
                                      </div>

                                    </div>
                                  </form>
                                  <br />
                                  @endif
                                  <div class="table-responsive col-sm-12">
                                    <table class="table" id="androidActiveSyntable">
                                      <thead>
                                         <tr>
                                            <th>Account name</th>
                                            <th>Host</th>
                                            <th>Domain</th>
                                            <th>Email</th>
                                            @if (Auth::user()->role_id < 4)
                                            <th>Action</th>
                                            @endif
                                         </tr>
                                      </thead>
                                      <tbody>
                                      </tbody>
                                   </table>
                            </div>
                             @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary saveactiveSyncsettings" currenttab="activeSyn">SAVE</button>@endif
                        </div>

      <div class="tab-pane" id="commands">
                  <form id="config"  value="config" class="form-horizontal form-bordered" method="post" action="function.php">


                  <div class="form-group">
                    <label class="col-sm-3 control-label">Registration Password</label>
                    <div class="col-sm-6">
                      <input type="regpasswd" id='registration_password'  placeholder="Password" title="Tooltip goes here" data-toggle="tooltip" data-trigger="hover" class="form-control tooltips" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label">COMMAND</label>
                    <div class="col-sm-6">
                      <input type="command" id='advanced_command' placeholder="enable:" class="form-control">
                      <span class="help-block"></span>
                    </div>
                  </div>
                   </form>
                     @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary savesettings" currenttab="commands">SAVE</button>@endif
                   </div>
        </div>
        </div>


        <div id="IOSsettings" style="display: none">
            <ul class="nav nav-tabs">
            <?php $activeios ='';
                  $count = 0;
                  if(sizeof(($tabIOSArray))){
                    foreach($tabIOSArray as $key => $tabioskey){
                        if($key < sizeof($tabIOSArray)-1){
                                $value = $tabIOSArray[$key+1]['tab_name'];
                        }

                            if($tabioskey['tab_status'] != 1){
                                $style = 'display:none;';
                            }else{
                                $style = '';
                            }
                            if($count == 0){
                                $activeios = 'active';
                            }else{
                                $activeios = '';
                            }
                            $count = $count + 1;
                            if($tabioskey['tab_name'] == 'iosHome'){
                                $tabioskeyName = 'Settings';
                            }else if($tabioskey['tab_name'] == 'iosKiosksettings'){
                                $tabioskeyName = 'KIOSK';
                            }else if($tabioskey['tab_name'] == 'iosAdvancedrestriction'){
                                $tabioskeyName = 'Advanced Restriction';
                            }else if($tabioskey['tab_name'] == 'iosCardDAV'){
                                $tabioskeyName = 'Card DAV';
                            }else{
                                $tabioskeyName = ucfirst(str_replace("ios","", str_replace("settings","",preg_replace('~\B[A-Z]~x', ' $0', $tabioskey['tab_name']))));
                            }?>
                        <li class="<?php echo $activeios; ?>"><a href="#<?php echo $tabioskey['tab_name'];?>" class="<?php echo $tabioskey['tab_name'];?>" data-toggle="tab" style="<?php echo $style; ?>" next_to="<?php echo $value; ?>"><strong><?php echo $tabioskeyName;  ?></strong></a></li>
                        <?php
                        }
                  }else{ ?>

                  <li class="active"><a href="#iosHome" class="iosHome" data-toggle="tab" next_to="iosActiveSyn"><strong>Settings</strong></a></li>
                  <li><a href="#iosActiveSyn" class="iosActiveSyn" data-toggle="tab" next_to="iosAdvancedrestriction"><strong>Active Sync</strong></a></li>
                  <li><a href="#iosAdvancedrestriction" class="iosAdvancedrestriction" data-toggle="tab" next_to="iosApn"><strong>Advanced Restrictions</strong></a></li>
                  <li><a href="#iosApn" class="iosApn" data-toggle="tab" next_to="iosCredentials"><strong>APN</strong></a></li>
                  <li><a href="#iosCredentials" class="iosCredentials" data-toggle="tab" next_to="iosEmailsettings"><strong>Credentials</strong></a></li>
                  <li><a href="#iosEmailsettings" class="iosEmailsettings" data-toggle="tab" next_to="iosKiosksettings"><strong>Email</strong></a></li>
                  <li><a href="#iosKiosksettings" class="iosKiosksettings" data-toggle="tab" next_to="iosLdap"><strong>KIOSK</strong></a></li>
                  <li><a href="#iosLdap" class="iosLdap" data-toggle="tab" next_to="iosPassword"><strong>LDap</strong></a></li>
                  <li><a href="#iosPassword" class="iosPassword" data-toggle="tab" next_to="iosVpn"><strong>Password</strong></a></li>
                  <li><a href="#iosVpn" class="iosVpn" data-toggle="tab" next_to="iosWebfilter"><strong>VPN</strong></a></li>
                  <li><a href="#iosWebfilter" class="iosWebfilter" data-toggle="tab" next_to="iosWifi"><strong>Web Filer</strong></a></li>
                  <li><a href="#iosWifi" class="iosWifi" data-toggle="tab" next_to="iosCardDAV"><strong>WiFi</strong></a></li>
                  <li><a href="#iosCardDAV" class="iosCardDAV" data-toggle="tab" next_to="iosWebclip"><strong>Card DAV</strong></a></li>
                  <li><a href="#iosWebclip" class="iosWebclip" data-toggle="tab" next_to="iosHome"><strong>Web Clip</strong></a></li>
                  <?php } ?>
            </ul>
            <div class="tab-content mb30">
                <div class="tab-pane active" id="iosHome">
                    <div class="panel-body panel-body-nopadding">
                        <form id="configiosHome"  value="config" class="form-horizontal form-bordered" method="post" action="">
                            <div class="">
                                <label class="control-label" style="padding-top: 20px;">DEVICE FUNCTIONALITY</label>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow installing app</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="app_install_allow_status" id="app_install_allow_status" value="1"></label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Block Sharing Managed Document using AirDrop</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" name="air_drop_block_status" id="air_drop_block_status" value="0"></label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow use of camera</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="camera_use_allow_status" id="camera_use_allow_status" value="1"></label></div>
                                </div>
                                <div class="col-sm-6 col-sm-offset-4 face_time_allow_status" style="display: none">
                                    <div class="checkbox block"><label><input type="checkbox" name="face_time_allow_status" id="face_time_allow_status" value="0">Allow face time</label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow screen capture</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="screen_capture_allow_status" id="screen_capture_allow_status" value="1"></label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow automatic sync while roaming</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> name="roaming_automatic_sync_allow_status" id="roaming_automatic_sync_allow_status" value="1"></label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Touch ID</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> name="touch_id_status" id="touch_id_status" value="1"></label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox" >Allow siri</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="siri_allow_status" name="siri_allow_status" value="1"></label></div>
                                </div>
                                <div class="col-sm-6 col-sm-offset-4 device_locked_siri_allow_status" style="display: none;">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> name="device_locked_siri_allow_status" id="device_locked_siri_allow_status" value="1">Allow siri while device locked</label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow voice dialing</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> name="voice_dialing_allow_status" id="voice_dialing_allow_status" value="1"></label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow passbook while locked device</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" id="device_locked_passbook_allow_status" name="device_locked_passbook_allow_status" value="0"></label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow In-app purchase</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="in_app_purchase_allow_status" name="in_app_purchase_allow_status" value="1"></label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Force users to enter iTune store password for all purchase</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> name="force_user_credential_on_itune_purchase_allow_status" id="force_user_credential_on_itune_purchase_allow_status" value="1"></label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow multiplayer gaming</label>
                                <div class="col-sm-6">
                                     <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> name="multiplayer_gaming_allow_status" id="multiplayer_gaming_allow_status" value="1"></label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow adding Game Centre friends</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> name="adding_game_centre_friends_allow_status" id="adding_game_centre_friends_allow_status" value="1"></label></div>
                                </div>
                            </div>
                            <div>
                                <label class="control-label" style="padding-top: 20px;">APPLICATIONS</label>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow trust enterprise app</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> name="trust_enterprise_app_allow_status" id="trust_enterprise_app_allow_status" value="1"></label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow modify enterprise app trust</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="modify_trust_enterprise_app_allow_status" name="modify_trust_enterprise_app_allow_status" value="1"> </label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow enterprise book backup</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="enterprise_book_backup_allow_status" name="enterprise_book_backup_allow_status" value="1"> </label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow managed apps to sync</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> name="managed_apps_to_sync_allow_status" id="managed_apps_to_sync_allow_status" value="1"> </label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow use of youtube</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> name="use_youtbe_allow_status" id="use_youtbe_allow_status" value="1"> </label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow use of iTunes store</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="use_itune_store_allow_status" name="use_itune_store_allow_status" value="1"> </label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow safari</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="use_safari_allow_status" name="use_safari_allow_status" value="1"> </label></div>
                                </div>
                                <div class="safari_div  col-sm-offset-4" use_safari_allow_status_div>
                                    <div class="col-sm-7">
                                        <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="safari_enable_autofill_status" name="safari_enable_autofill_status" value="1"> Enable autofill</label></div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="checkbox block"><label><input type="checkbox" id="safari_force_fraud_warning_status" name="safari_force_fraud_warning_status" value="0"> Force fraud warning</label></div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="safari_enable_javascript_status" name="safari_enable_javascript_status" value="1"> Enable javascript</label></div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="safari_block_popup_status" name="safari_block_popup_status" value="1">Block pop-ups </label></div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <label class="control-label" style="padding-top: 20px;">ACCEPT COOKIES</label>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-6">
                                    <select id="cookie_accept_status" name="cookie_accept_status" class="select2 form-control" required data-placeholder="Choose One">
                                        <option value="">Please select</option>
                                        <option value="Always">Always</option>
                                    </select>
                                </div>
                            </div>
                            <div>
                                <label class="control-label" style="padding-top: 20px;">iCLOUD</label>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow backup</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="icloud_backup_allow_status" name="icloud_backup_allow_status" value="1"> </label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow document sync</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="icloud_document_sync_allow_status" name="icloud_document_sync_allow_status" value="1"> </label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow photo stream(disallowing can cause data loss)</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="icloud_photo_stream_allow_status" name="icloud_photo_stream_allow_status" value="1"> </label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow shared photo streams</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="icloud_shared_photo_stream_allow_status" name="icloud_shared_photo_stream_allow_status" value="1"> </label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow iCloud photo library</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="icloud_photo_library_allow_status" name="icloud_photo_library_allow_status" value="1"> </label></div>
                                </div>
                            </div>
                            <div>
                                <label class="control-label" style="padding-top: 20px;">SECURITY AND PRIVACY</label>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow lock screen notifications</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="lock_screen_notification_allow_status" name="lock_screen_notification_allow_status" value="1"> </label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow today view in lock screen</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="today_view_in_lock_screen_allow_status" name="today_view_in_lock_screen_allow_status" value="1"> </label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow control center in lock screen</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="control_center_in_lock_screen_allow_status" name="control_center_in_lock_screen_allow_status" value="1"> </label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow over-the-air PKI updates</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="over_the_air_PKI_updates_allow_status" name="over_the_air_PKI_updates_allow_status" value="1"> </label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Limit Ad tracking</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" id="ad_tracking_limit_status" name="ad_tracking_limit_status" value="0"> </label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow diagnostic data to be sent  to Apple</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="diagnostic_data_sent_allow_status" name="diagnostic_data_sent_allow_status" value="1"> </label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow user to accept untrusted TLS certificate</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked;?> id="untrusted_TLS_certificate_allow_status" name="untrusted_TLS_certificate_allow_status" value="1"> </label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Force encrypted backup</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" id="force_encrypt_backup_status" name="force_encrypt_backup_status" value="0"> </label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Force Apple watch wrist detection</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" id="force_apple_watch_wrist_detection_status" name="force_apple_watch_wrist_detection_status" value="0"> </label></div>
                                </div>
                            </div>
                            <div>
                                <label class="control-label" style="padding-top: 20px;">CONTENT RATINGS</label>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow explicit music,podcasts & iTunes U</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> id="rate_music_allow_status" name="rate_music_allow_status" value="1"> </label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow iBookstore erotica</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input type="checkbox" id="rate_ibookstore_erotica_allow_status" name="rate_ibookstore_erotica_allow_status" value="0"> </label></div>
                                </div>
                            </div>
                            <div>
                                <label class="control-label" style="padding-top: 20px;">RATING REGION</label>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-6">
                                    <select id="rating_region" name="rating_region" class="select2 form-control" required data-placeholder="Choose One">
                                        <option value="">Please select</option>
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div>
                                <label class="control-label" style="padding-top: 20px;">ALLOWED CONTENT RATING</label>
                            </div>
                            <div class="content_rating_div">
                                <div class="row" style="padding-bottom: 7px;">
                                    <label class="col-sm-4 control-label">Movies</label>
                                    <div class="col-sm-6">
                                        <select id="movie_allow_status" name="movie_allow_status" class="select2 form-control" required data-placeholder="Choose One">
                                            <option value="Open">Please select</option>
                                            <option value="Allow all movies">Allow all movies</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row" style="padding-bottom: 7px;">
                                    <label class="col-sm-4 control-label">TV Shows</label>
                                    <div class="col-sm-6">
                                        <select id="tv_shows_allow_status" name="tv_shows_allow_status" class="select2 form-control" required data-placeholder="Choose One">
                                            <option value="Open">Please select</option>
                                            <option value="Allow all TV shows">Allow all TV shows</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row" style="padding-bottom: 7px;">
                                    <label class="col-sm-4 control-label">Apps</label>
                                    <div class="col-sm-6">
                                        <select id="apps_allow_status" name="apps_allow_status" class="select2 form-control" required data-placeholder="Choose One">
                                            <option value="Open">Please select</option>
                                            <option value="Allow all apps">Allow all apps</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                          @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary saveIOSsettings" currenttab="iosHome">SAVE</button>@endif
                    </div><!-- panel-body -->
                </div>
                <div class="tab-pane" id="iosActiveSyn">
                    @if ( Auth::user()->role_id < 4)
                        <form id="configiosActiveSyn"  value="config" class="form-horizontal form-bordered" method="post" action="" >
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Account Name</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="account_name" id="account_name" placeholder="ENTER ACCOUNT NAME">
                                </div>
                            </div><!-- form-group -->
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Exchange Active Sync Server Name</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control iosSyncRequired" name="active_sync_server_name" id="active_sync_server_name" placeholder="ENTER ACTIVESYNC HOST">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow Move</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input name="move_allow_status" id="move_allow_status" value="0" type="checkbox"></label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Allow recent address syncing</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input name="recent_address_syncing_allow_status" id="recent_address_syncing_allow_status" value="0" type="checkbox"></label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Use only in mail</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input name="use_only_in_mail_status" id="use_only_in_mail_status" value="0" type="checkbox"></label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">USE SSL</label>
                                <div class="col-sm-6">
                                     <div class="checkbox block"><label><input name="use_ssl_status" id="use_ssl_status" value="0" type="checkbox"></label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">USE S/MIME</label>
                                <div class="col-sm-6">
                                     <div class="checkbox block"><label><input name="use_mime_status" id="use_mime_status" value="0" type="checkbox"></label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Domain</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="active_sync_domain" name="active_sync_domain" placeholder="ENTER DOMAIN">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">User</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="active_sync_user_name" name="active_sync_user_name" placeholder="ENTER USER NAME">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Email Address</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="active_sync_user_email" name="active_sync_user_email" placeholder="ENTER EMAIL ADDRESS">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Password</label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" id="active_sync_user_password" name="active_sync_user_password" placeholder="ENTER PASSWORD">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Past days of mail to Sync</label>
                                <div class="col-sm-6">
                                    <select id="mail_sync_period" name="mail_sync_period" class="select2 form-control" data-placeholder="Choose One">
                                        <option value="">Please select</option>
                                        <option value="Three days">Three days</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Identify certificate</label>
                                <div class="col-sm-6">
                                    <select id="identity_certificate" name="identity_certificate" class="select2 form-control" data-placeholder="Choose One">
                                        <option value="">Please select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="checkbox">Make identity certificate compatible with IOS 4</label>
                                <div class="col-sm-6">
                                    <div class="checkbox block"><label><input name="identity_certificate_ios4_compatible_status" id="identity_certificate_ios4_compatible_status" value="0" type="checkbox"></label></div>
                                </div>
                            </div>
                        </form>
                            <br />
                        @endif
                            <div class="table-responsive col-sm-12">
                                <table class="table" id="iosActiveSyntable">
                                    <thead>
                                        <tr>
                                            <th>Account name</th>
                                            <th>Host</th>
                                            <th>Domain</th>
                                            <th>Email</th>
                                            @if (Auth::user()->role_id < 4)
                                            <th>Action</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                     @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary saveIOSsettings{{--saveIOSactiveSyncsettings--}}" currenttab="iosActiveSyn">SAVE</button>@endif
                </div>
                <div class="tab-pane" id="iosAdvancedrestriction">
                    <div class="panel-body panel-body-nopadding">

                  <form id="configiosRestriction"  value="config" class="form-horizontal form-bordered" method="post" action="">
                        <div class="">
                        <label class="control-label" style="padding-top: 20px;">DEVICE FUNCTIONALITY</label>
                        </div>
                       <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow AirDrop(iOS 7.0+)</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="airdrop_allow_status" id="airdrop_allow_status" value="0"></label></div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow app cellular data modification(iOS 7.0+)</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="app_cellular_data_modification_allow_status" id="app_cellular_data_modification_allow_status" value="0"></label></div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow removing apps</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="remove_app_allow_status" id="remove_app_allow_status" value="0"></label></div>
                          </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="checkbox">Allow book store(ios 6.0+)</label>
                            <div class="col-sm-6">
                               <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="book_store_allow_status" id="book_store_allow_status" value="0"></label></div>
                            </div>
                          </div>
                     <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow adding or removing touch ID</label>
                          <div class="col-sm-6">

                             <div class="checkbox block"><label><input type="checkbox" name="touch_id_add_remove_allow_status" id="touch_id_add_remove_allow_status" value="0"></label></div>
                                </div>
                        </div>
                              <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow iMessage(iOS 6.0+)</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="imessage_allow_status" id="imessage_allow_status" value="0"></label></div>
                          </div>
                         </div>

                         <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow games center(iOS 6.0+)</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="game_center_allow_status" id="game_center_allow_status" value="0"></label></div>
                          </div>
                        </div>
                              <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox" >Allow pairing with iTunes(iOS 7.0+)</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> id="itunes_pairing_allow_status" name="itunes_pairing_allow_status" value="0"></label></div>
                                </div>
                                </div>

                          <div class="form-group">
                            <label class="col-sm-4 control-label" for="checkbox">Allow configuration profile installation(iOS 6.0+)</label>
                            <div class="col-sm-6">
                               <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="profile_installation_allow_status" id="profile_installation_allow_status" value="0"></label></div>
                            </div>
                          </div>

                     <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow podcasts(iOs 8.0+)</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="podcasts_allow_status" id="podcasts_allow_status" value="0"></label></div>
                          </div>
                        </div>
                                <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow definition lookup(iOs 8.1.3+)</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> id="lookup_definition_allow_status" name="lookup_definition_allow_status" value="0"></label></div>
                          </div>
                        </div>
                      <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow predictive keyboard(iOS 8.1.3+)</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> id="ios_predictive_keyboard" name="ios_predictive_keyboard" value="0"></label></div>
                          </div>
                        </div>
                      <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow auto-correction</label>
                          <div class="col-sm-6">

                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="auto_correction_allow_status" id="auto_correction_allow_status" value="0"></label></div>

                          </div>
                        </div>

                      <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow spell-check</label>
                          <div class="col-sm-6">

                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?>  name="spell_check_allow_status" id="spell_check_allow_status" value="0"></label></div>

                          </div>
                        </div>
                         <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow apple music services(iOS 9.3+)</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="apple_music_services_allow_status" id="apple_music_services_allow_status" value="0"></label></div>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow iTunes radio(iOS 9.3+)</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="itunes_radio_allow_status" id="itunes_radio_allow_status" value="0"></label></div>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow News(iOS 9.0+)</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="ios_news_allow_status" id="ios_news_allow_status" value="0"></label></div>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow App installation from devices(iOS 9.0+)</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="app_install_from_devices_allow_status" id="app_install_from_devices_allow_status" value="0"></label></div>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow keyboard shortcuts(iOS 9.0+)</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="keyboard_shortcut_allow_status" id="keyboard_shortcut_allow_status" value="0"></label></div>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow paired watch(iOS 9.0+)</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="paired_watch_allow_status" id="paired_watch_allow_status" value="0"></label></div>
                          </div>
                        </div>

                        <div>
                            <label class="control-label" style="padding-top: 20px;">SECURITY AND PRIVACY</label>
                        </div>
                            <div class="form-group">
                              <label class="col-sm-4 control-label" for="checkbox">Allow account modification</label>
                              <div class="col-sm-6">
                                 <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="account_modification_allow_status" id="account_modification_allow_status" value="0"></label></div>
                              </div>
                            </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow erase content and settings</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> id="erase_cintent_and_settings_allow_status" name="erase_cintent_and_settings_allow_status" value="0"> </label></div>
                          </div>
                        </div>

                    <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow assistant user generated content(iOS 7.0+)</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> id="user_generated_content_assistant_allow_status" name="user_generated_content_assistant_allow_status" value="0"> </label></div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow modify find my friends(iOS 7.0+)</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> name="find_my_friends_modify_allow_status" id="find_my_friends_modify_allow_status" value="0"> </label></div>
                          </div>
                        </div>


                    <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Force use of profanity filter</label>
                          <div class="col-sm-6">

                             <div class="checkbox block"><label><input type="checkbox" name="force_use_of_priority_filter_status" id="force_use_of_priority_filter_status" value="0"> </label></div>

                          </div>
                        </div>
                      <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow spotlight internet results</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> id="spotlight_internet_results_allow_status" name="spotlight_internet_results_allow_status" value="0"> </label></div>
                          </div>
                        </div>
                    <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow restrictions enabling</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> id="restrictions_enabling_allow_status" name="restrictions_enabling_allow_status" value="0"> </label></div>
                          </div>
                        </div>
                        <div class="form-group">
                              <label class="col-sm-4 control-label" for="checkbox">Allow passcode modification(iOS 9.0+)</label>
                              <div class="col-sm-6">
                                 <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> id="passcode_modification_allow_status" name="passcode_modification_allow_status" value="0"> </label></div>
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="col-sm-4 control-label" for="checkbox">Allow device name modification(iOS 9.0+)</label>
                              <div class="col-sm-6">
                                 <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> id="device_name_modification_allow_status" name="device_name_modification_allow_status" value="0"> </label></div>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-4 control-label" for="checkbox">Allow wallpaper modification(iOS 9.3+)</label>
                              <div class="col-sm-6">
                                 <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> id="wallpaper_modification_allow_status" name="wallpaper_modification_allow_status" value="0"> </label></div>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-4 control-label" for="checkbox">Allow notification modification(iOS 9.3+)</label>
                              <div class="col-sm-6">
                                 <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> id="notification_modification_allow_status" name="notification_modification_allow_status" value="0"> </label></div>
                              </div>
                            </div>


                    <div>
                        <label class="control-label" style="padding-top: 20px;">APPLICATION CONFIGURATION</label>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label" for="checkbox">Allow automatic apps downloading (iOS 9.0+)</label>
                      <div class="col-sm-6">
                         <div class="checkbox block"><label><input type="checkbox" <?php echo $checked; ?> id="automatic_apps_downloading_allow_status" name="automatic_apps_downloading_allow_status" value="0"> </label></div>
                      </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-4 control-label">Autonomous single app mode apps</label>
                         <div class="col-sm-6">
                             <input type="text" class="form-control" name="autonomous_single_app_mode_apps" id="autonomous_single_app_mode_apps" placeholder="">
                         </div>
                    </div>
                  </form>
                  @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary saveIOSsettings" currenttab="iosAdvancedrestriction">SAVE</button>@endif
                </div><!-- panel-body -->


                </div>
            <div class="tab-pane" id="iosApn">
                     @if ( Auth::user()->role_id < 4)
                             <form id="configiosApn"  value="config" class="form-horizontal form-bordered" method="post" action="">
                                 <div class="form-group">
                                      <label class="col-sm-4 control-label">Access point name(APN)</label>
                                  <div class="col-sm-6">
                                    <input type="text" class="form-control apnRequired" name="apn_name" id="apn_name" placeholder="ENTER ACCESS POINT NAME">
                                  </div>
                                </div><!-- form-group -->
                                 <div class="form-group">
                                 <label class="col-sm-4 control-label">Access point user name</label>
                                  <div class="col-sm-6">
                                  <input type="text" class="form-control" name="apn_user_name" id="apn_user_name" placeholder="ENTER ACCESS POINT USER NAME">
                                </div>
                                </div>
                                 <div class="form-group">
                                 <label class="col-sm-4 control-label">Access point password</label>
                                  <div class="col-sm-6">
                                  <input type="password" class="form-control" id="apn_password" name="apn_password" placeholder="ENTER ACCESS POINT PASSWORD">
                                </div>
                                </div>
                                 <div class="form-group">
                                 <label class="col-sm-4 control-label">Proxy server</label>
                                  <div class="col-sm-6">
                                  <input type="text" class="form-control" id="apn_proxy_server" name="apn_proxy_server" placeholder="ENTER PROXY SERVER">
                                </div>
                                </div>

                                <div class="form-group">
                                 <label class="col-sm-4 control-label">Proxy server port</label>
                                  <div class="col-sm-6">
                                  <input type="text" class="form-control" id="apn_proxy_server_port" name="apn_proxy_server_port" placeholder="ENTER PROXY SERVER PORT">
                                </div>
                                </div>
                             </form>
                              <br />
                              @endif
                              <div class="table-responsive col-sm-12">
                                <table class="table" id="iosApntable">
                                  <thead>
                                     <tr>
                                        <th>Name</th>
                                        <th>User Name</th>
                                        <th>Server port</th>
                                        @if (Auth::user()->role_id < 4)
                                        <th>Action</th>
                                        @endif
                                     </tr>
                                  </thead>
                                  <tbody>
                                  </tbody>
                               </table>
                        </div>
                         @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary saveIOSsettings" currenttab="iosApn">SAVE</button>@endif
                    </div>
            <div class="tab-pane" id="iosCredentials">
                                         @if ( Auth::user()->role_id < 4)
                                                 <form id="config"  value="config" class="form-horizontal form-bordered" method="post" action="" >
                                                     <label class="col-sm-3 control-label">UPLOAD CERTIFICATE</label>
                                                     <input type="hidden" name="profileCertificateId" id="profileCertificateId" value="">
                                                     <div class="col-sm-9">
                                                         <p>
                                                         <input id="_fileCertificate" name="fileCertificate" type="file" value="">
                                                         </p>
                                                         <p></p>
                                                         <input id="credential_submit" class="btn btn-primary btn-sm" value="Add Certificate" type="button">
                                                         <div id="err-msg" style=" color:red"></div>
                                                     </div>
                                                 </form>
                                                  <br /> <br />
                                                  @endif
                                                    <div class="table-responsive col-sm-12">
                                                         <table class="table" id="ioscertificatetable">
                                                           <thead>
                                                              <tr>
                                                                 <th>Certificate Name</th>
                                                                 @if (Auth::user()->role_id < 4)
                                                                 <th>Action</th>
                                                                 @endif
                                                              </tr>
                                                           </thead>
                                                           <tbody>
                                                           </tbody>
                                                        </table>
                                                     </div>
                                             @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary saveIOSsettings" currenttab="iosCredentials">SAVE</button>@endif
                                        </div>
            <div class="tab-pane" id="iosEmailsettings">
             @if ( Auth::user()->role_id < 4)
                     <form id="configiosEmail"  value="config" class="form-horizontal form-bordered" method="post" action="" >
                         <div class="form-group">
                              <label class="col-sm-4 control-label">Account Description</label>
                          <div class="col-sm-6">
                            <input type="text" class="form-control iosemailRequired" name="account_description" id="account_description" placeholder="ENTER ACCOUNT DESCRIPTION">
                          </div>
                        </div><!-- form-group -->
                        <div class="form-group">
                         <label class="col-sm-4 control-label">Account Type</label>
                          <div class="col-sm-6">
                              <select id="account_type" name="account_type" class="select2 form-control" data-placeholder="Choose One">
                                  <option value="">Please select</option>
                                  <option value="POP">POP</option>
                                  <option value="SMTP">SMTP</option>
                              </select>
                          </div>
                        </div>
                         <div class="form-group">
                         <label class="col-sm-4 control-label">User Display Name</label>
                          <div class="col-sm-6">
                          <input type="text" class="form-control" name="user_display_name" id="user_display_name" placeholder="ENTER USER DISPLAY NAME">
                        </div>
                        </div>
                         <div class="form-group">
                         <label class="col-sm-4 control-label">Email Address</label>
                          <div class="col-sm-6">
                          <input type="text" class="form-control" id="user_email_address" name="user_email_address" placeholder="ENTER EMAIL ADDRESS">
                        </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">Allow Move</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input type="checkbox" id="allow_move_status" name="allow_move_status" value="0"> </label></div>
                          </div>
                        </div>
                        <div>
                         <label class="col-sm-4 control-label" style="padding-top: 20px;">INCOMING MAIL</label>
                        </div>

                        <div class="col-lg-12" style="padding-top: 20px;">
                         <label class="col-sm-4 control-label">Incoming Mail Server</label>
                          <div class="col-sm-6">
                              <input type="text" class="form-control iosemailRequired" id="incoming_mail_server" name="incoming_mail_server">
                          </div>
                        </div>
                        <div class="col-lg-12" style="padding-top: 20px;">
                         <label class="col-sm-4 control-label">Incoming Server port</label>
                          <div class="col-sm-6">
                              <input type="text" class="form-control" id="incoming_mail_server_port" name="incoming_mail_server_port">
                          </div>
                        </div>
                        <div class="col-lg-12" style="padding-top: 20px;">
                         <label class="col-sm-4 control-label">User Name</label>
                          <div class="col-sm-6">
                              <input type="text" class="form-control" id="incoming_mail_user_name" name="incoming_mail_user_name">
                          </div>
                        </div>
                        <div class="col-lg-12" style="padding-top: 20px;">
                         <label class="col-sm-4 control-label">Authentication Type</label>
                          <div class="col-sm-6">
                              <select id="incoming_mail_authentication_type" name="incoming_mail_authentication_type" class="select2 form-control" data-placeholder="Choose One">
                                  <option value="">Please select</option>
                                  <option value="Password">Password</option>
                              </select>
                          </div>
                        </div>
                        <div class="col-lg-12" style="padding-top: 20px;">
                         <label class="col-sm-4 control-label">Password</label>
                          <div class="col-sm-6">
                            <input type="password" class="form-control" id="incoming_mail_password" name="incoming_mail_password">
                          </div>
                        </div>


                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="checkbox">USE SSL</label>
                          <div class="col-sm-6">
                             <div class="checkbox block"><label><input name="incoming_mail_ssl_usage" <?php echo $checked;?> id="incoming_mail_ssl_usage" value="0" type="checkbox"></label></div>
                          </div>
                        </div>
                <div>
                 <label class="col-sm-4 control-label" style="padding-top: 20px;">OUTGOING MAIL</label>
                </div>
                <div class="col-lg-12" style="padding-top: 20px;">
                 <label class="col-sm-4 control-label">Outgoing Mail Server</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control iosemailRequired" id="outgoing_mail_server" name="outgoing_mail_server">
                  </div>
                </div>
                <div class="col-lg-12" style="padding-top: 20px;">
                 <label class="col-sm-4 control-label">Outgoing Server port</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="outgoing_mail_server_port" name="outgoing_mail_server_port">
                  </div>
                </div>
                <div class="col-lg-12" style="padding-top: 20px;">
                 <label class="col-sm-4 control-label">User Name</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="outgoing_mail_user_name" name="outgoing_mail_user_name">
                  </div>
                </div>
                <div class="col-lg-12" style="padding-top: 20px;">
                 <label class="col-sm-4 control-label">Authentication Type</label>
                  <div class="col-sm-6">
                      <select id="outgoing_mail_authentication_type" name="outgoing_mail_authentication_type" class="select2 form-control" data-placeholder="Choose One">
                          <option value="">Please select</option>
                          <option value="Password">Password</option>
                      </select>
                  </div>
                </div>


                  <div class="form-group" style="padding-top: 20px;">
                                  <label class="col-sm-4 control-label" for="checkbox">Outgoing password same as incoming</label>
                                  <div class="col-sm-6">
                                     <div class="checkbox block"><label><input name="outgoing_mail_password" <?php echo $checked;?> id="outgoing_mail_password" value="0" type="checkbox"></label></div>
                                  </div>
                                  </div>
                                  <div class="form-group">
                                  <label class="col-sm-4 control-label" for="checkbox">Allow recent address syncing</label>
                                  <div class="col-sm-6">
                                     <div class="checkbox block"><label><input name="recent_address_syncing_allow_status" id="recent_address_syncing_allow_status" value="0" type="checkbox"></label></div>
                                  </div>
                                  </div>
                                   <div class="form-group">
                                    <label class="col-sm-4 control-label" for="checkbox">Use only in Mail</label>
                                  <div class="col-sm-6">
                                     <div class="checkbox block"><label><input name="only_in_mail_usage" id="only_in_mail_usage" value="0" type="checkbox"></label></div>
                                  </div>
                                  </div>
                                   <div class="form-group">
                                  <label class="col-sm-4 control-label" for="checkbox">USE SSL</label>
                                      <div class="col-sm-6">
                                         <div class="checkbox block"><label><input name="outgoing_mail_ssl_usage" <?php echo $checked;?> id="outgoing_mail_ssl_usage" value="0" type="checkbox"></label></div>
                                      </div>
                                      </div>
                                       <div class="form-group">
                                  <label class="col-sm-4 control-label" for="checkbox">USE S/MIME</label>
                                      <div class="col-sm-6">
                                         <div class="checkbox block"><label><input name="outgoing_mail_mime_usage" id="outgoing_mail_mime_usage" value="0" type="checkbox"></label></div>
                                      </div>
                                </div>
                      </form>
                      <br />
                      @endif
                      <div class="table-responsive col-sm-12">
                        <table class="table" id="iosEmailtable">
                          <thead>
                             <tr>
                                <th>Account Description</th>
                                <th>Display Name </th>
                                <th>Email Address</th>
                                @if (Auth::user()->role_id < 4)
                                <th>Action</th>
                                @endif
                             </tr>
                          </thead>
                          <tbody>
                          </tbody>
                       </table>
                </div>
                 @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary saveIOSsettings" currenttab="iosEmailsettings">SAVE</button>@endif
            </div>
            <div class="tab-pane" id="iosLdap">
                 @if ( Auth::user()->role_id < 4)
                         <form id="configiosLdap"  value="config" class="form-horizontal form-bordered" method="post" action="" >
                             <div class="form-group">
                                  <label class="col-sm-4 control-label">Account Description</label>
                              <div class="col-sm-6">
                                <input type="text" class="form-control lDap_required" name="ldap_account_description" id="ldap_account_description" placeholder="ENTER ACCOUNT DESCRIPTION">
                              </div>
                            </div><!-- form-group -->
                             <div class="form-group">
                             <label class="col-sm-4 control-label">Account User Name</label>
                              <div class="col-sm-6">
                              <input type="text" class="form-control" name="ldap_account_user_name" id="ldap_account_user_name" placeholder="ENTER ACCOUNT USER NAME">
                            </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Account Password</label>
                                <div class="col-sm-6">
                                <input type="password" class="form-control" name="ldap_account_password" id="ldap_account_password" placeholder="ENTER ACCOUNT PASSWORD">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-4 control-label">Account Host Name</label>
                                <div class="col-sm-6">
                                <input type="text" class="form-control iosLdapRequired" name="ldap_account_host_name" id="ldap_account_host_name" placeholder="ENTER ACCOUNT HOST NAME">
                                </div>
                            </div>

                            <div class="form-group">
                              <label class="col-sm-4 control-label" for="checkbox">USE SSL</label>
                              <div class="col-sm-6">
                                 <div class="checkbox block"><label><input name="ldap_ssl_usage_status" <?php echo $checked;?> id="ldap_ssl_usage_status" value="0" type="checkbox"></label></div>
                              </div>
                            </div>
                          </form>
                          <br />
                          @endif
                          <div class="table-responsive col-sm-12">
                            <table class="table" id="iosLdaptable">
                              <thead>
                                 <tr>
                                    <th>Account Description</th>
                                    <th>User Name</th>
                                    <th>Host Name</th>
                                    @if (Auth::user()->role_id < 4)
                                    <th>Action</th>
                                    @endif
                                 </tr>
                              </thead>
                              <tbody>
                              </tbody>
                           </table>
                    </div>
                     @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary saveIOSsettings" currenttab="iosLdap">SAVE</button>@endif
                </div>
            <div class="tab-pane" id="iosPassword">
                 @if ( Auth::user()->role_id < 4 ||  Auth::user()->role_id == 5)
                         <form id="configiosPassword"  value="config" class="form-horizontal form-bordered" method="post" action="" >
                             <div class="form-group">
                               <label class="col-sm-4 control-label" for="checkbox">Allow simple value</label>
                               <div class="col-sm-6">
                                  <div class="checkbox block"><label><input name="allow_simple_ios_password_status" id="allow_simple_ios_password_status" <?php echo $checked; ?> value="0" type="checkbox"></label></div>
                               </div>
                             </div>
                             <div class="form-group">
                               <label class="col-sm-4 control-label" for="checkbox">Required alpha numeric value</label>
                               <div class="col-sm-6">
                                  <div class="checkbox block"><label><input name="ios_password_alpha_numeric_value_require_status" id="ios_password_alpha_numeric_value_require_status" value="0" type="checkbox"></label></div>
                               </div>
                             </div>
                             <div class="form-group">
                                  <label class="col-sm-4 control-label">Minimum passcode lenght</label>
                              <div class="col-sm-6">
                                <input type="text" class="form-control password_required" name="ios_password_minmum_length" id="ios_password_minmum_length" placeholder="">
                              </div>
                            </div><!-- form-group -->
                             <div class="form-group">
                             <label class="col-sm-4 control-label">Minimum complex character</label>
                              <div class="col-sm-6">
                              <input type="text" class="form-control" name="ios_password_minmum_complex_character_count" id="ios_password_minmum_complex_character_count" placeholder="">
                            </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Maximum passcode age in days(1-730 days)</label>
                                <div class="col-sm-6">
                                <input type="text" class="form-control" name="ios_password_maximum_age" id="ios_password_maximum_age" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                             <label class="col-sm-4 control-label">Auto lock</label>
                              <div class="col-sm-6">
                                  <select id="ios_device_auto_lock_status" name="ios_device_auto_lock_status" class="select2 form-control" data-placeholder="Choose One">
                                      <option value="">Please select</option>
                                      <option value="Never">Never</option>
                                  </select>
                              </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Passcode history(1-50 passcodes)</label>
                                <div class="col-sm-6">
                                <input type="text" class="form-control" name="ios_password_history_count" id="ios_password_history_count" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                             <label class="col-sm-4 control-label">Grace period for device lock</label>
                              <div class="col-sm-6">
                                  <select id="ios_device_lock_grace_period" name="ios_device_lock_grace_period" class="select2 form-control" data-placeholder="Choose One">
                                      <option value="">Please select</option>
                                      <option value="None">None</option>
                                  </select>
                              </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Failed attempt</label>
                                <div class="col-sm-6">
                                <input type="text" class="form-control" name="ios_password_failed_attempts_count" id="ios_password_failed_attempts_count" placeholder="">
                                </div>
                            </div>

                          </form>
                          <br />
                          @endif
                          <div class="table-responsive col-sm-12">
                            <table class="table" id="iosPasswordtable">
                              <thead>
                                 <tr>
                                    <th>Minimum Lenght</th>
                                    <th>Auto Lock</th>
                                    <th>Lock Grace Period</th>
                                    @if (Auth::user()->role_id < 4)
                                    <th>Action</th>
                                    @endif
                                 </tr>
                              </thead>
                              <tbody>
                              </tbody>
                           </table>
                    </div>
                     @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary saveIOSsettings" currenttab="iosPassword">SAVE</button>@endif
                </div>
            <div class="tab-pane" id="iosVpn">
                             @if ( Auth::user()->role_id < 4)
                                     <form id="configiosVpn"  value="config" class="form-horizontal form-bordered" method="post" action="" >
                                         <div class="form-group">
                                              <label class="col-sm-4 control-label">Connection name</label>
                                          <div class="col-sm-6">
                                            <input type="text" class="form-control" name="vpn_connection_name" id="vpn_connection_name" placeholder="">
                                          </div>
                                        </div><!-- form-group -->
                                        <div class="form-group">
                                         <label class="col-sm-4 control-label">Connection Type</label>
                                          <div class="col-sm-6">
                                              <select id="vpn_connection_type" name="vpn_connection_type" class="select2 form-control" data-placeholder="Choose One">
                                                  <option value="">Please select</option>
                                                  <option value="L2TP">L2TP</option>
                                              </select>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Server</label>
                                            <div class="col-sm-6">
                                            <input type="text" class="form-control vpn_required" name="vpn_server" id="vpn_server" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Account</label>
                                            <div class="col-sm-6">
                                            <input type="text" class="form-control" name="vpn_account" id="vpn_account" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                           <label class="col-sm-2 col-sm-offset-2" for="radio">User Authentication</label>
                                           <input type="radio" name="vpn_user_authentication_type" value="password"> Password
                                           <input type="radio" name="vpn_user_authentication_type" <?php echo $checked; ?> value="RSAsecureId"> RSA Secure Id
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Shared secret</label>
                                            <div class="col-sm-6">
                                            <input type="text" class="form-control" name="vpn_shared_secret" id="vpn_shared_secret" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                           <label class="col-sm-4 control-label" for="checkbox">Send all traffic</label>
                                           <div class="col-sm-6">
                                              <div class="checkbox block"><label><input name="vpn_sent_all_traffic_status" id="vpn_sent_all_traffic_status" value="0" type="checkbox"></label></div>
                                           </div>
                                         </div>
                                        <div class="form-group">
                                         <label class="col-sm-4 control-label">Proxy</label>
                                          <div class="col-sm-6">
                                              <select id="vpn_proxy" name="vpn_proxy" class="select2 form-control" data-placeholder="Choose One">
                                                  <option value="">Please select</option>
                                                  <option value="None">None</option>
                                              </select>
                                          </div>
                                        </div>
                                     </form>
                                      <br />
                                      @endif
                                      <div class="table-responsive col-sm-12">
                                        <table class="table" id="iosVpntable">
                                          <thead>
                                             <tr>
                                                <th>Connection Name</th>
                                                <th>Connection Type</th>
                                                <th>Server</th>
                                                @if (Auth::user()->role_id < 4)
                                                <th>Action</th>
                                                @endif
                                             </tr>
                                          </thead>
                                          <tbody>
                                          </tbody>
                                       </table>
                                </div>
                                 @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary saveIOSsettings" currenttab="iosVpn">SAVE</button>@endif
                            </div>
            <div class="tab-pane" id="iosWifi">
                 @if ( Auth::user()->role_id < 4)
                         <form id="configiosWifi"  value="config" class="form-horizontal form-bordered" method="post" action="" >
                             <div class="form-group">
                                  <label class="col-sm-4 control-label">Service set identifer</label>
                              <div class="col-sm-6">
                                <input type="text" class="form-control ios_wifi_required" name="wifi_service_set_identifier" id="wifi_service_set_identifier" placeholder="">
                              </div>
                            </div><!-- form-group -->
                            <div class="form-group">
                               <label class="col-sm-4 control-label" for="checkbox">Auto Join</label>
                               <div class="col-sm-6">
                                  <div class="checkbox block"><label><input name="wifi_auto_join_status" id="wifi_auto_join_status" <?php echo $checked; ?> value="0" type="checkbox"></label></div>
                               </div>
                             </div>
                             <div class="form-group">
                                 <label class="col-sm-4 control-label">Hidden Network</label>
                                 <div class="col-sm-6">
                                 <input type="text" class="form-control" name="wifi_hidden_network" id="wifi_hidden_network" placeholder="">
                                 </div>
                             </div>
                            <div class="form-group">
                             <label class="col-sm-4 control-label">Security Type</label>
                              <div class="col-sm-6">
                                  <select id="wifi_security_type" name="wifi_security_type" class="select2 form-control" data-placeholder="Choose One">
                                      <option value="">None</option>
                                      <option value="WEP">WEP</option>
                                      <option value="WPA/WPA2">WPA/WPA2</option>
                                      <option value="Any(Personal)">Any(Personal)</option>
                                      <option value="WEP Enterprise">WEP Enterprise</option>
                                      <option value="WPA/WPA2 Enterprise">WPA/WPA2 Enterprise</option>
                                      <option value="Any(Enterprise)">Any(Enterprise)</option>
                                  </select>
                              </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Password</label>
                                <div class="col-sm-6">
                                <input type="password" class="form-control" name="wifi_security_password" id="wifi_security_password" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                             <label class="col-sm-4 control-label">Proxy</label>
                              <div class="col-sm-6">
                                  <select id="wifi_proxy" name="wifi_proxy" class="select2 form-control" data-placeholder="Choose One">
                                      <option value="">Please select</option>
                                      <option value="None">None</option>
                                  </select>
                              </div>
                            </div>
                         </form>
                          <br />
                          @endif
                          <div class="table-responsive col-sm-12">
                            <table class="table" id="iosWifitable">
                              <thead>
                                 <tr>
                                    <th>Identifer</th>
                                    <th>Hidden Network</th>
                                    <th>Security Type</th>
                                    @if (Auth::user()->role_id < 4)
                                    <th>Action</th>
                                    @endif
                                 </tr>
                              </thead>
                              <tbody>
                              </tbody>
                           </table>
                    </div>
                     @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary saveIOSsettings" currenttab="iosWifi">SAVE</button>@endif
                </div>
            <div class="tab-pane" id="iosCardDAV">
                @if ( Auth::user()->role_id < 4)
                     <form id="configiosDav"  value="config" class="form-horizontal form-bordered" method="post" action="" >
                         <div class="form-group">
                              <label class="col-sm-4 control-label">Account Description</label>
                          <div class="col-sm-6">
                            <input type="text" class="form-control" name="carddav_account_description" id="carddav_account_description" placeholder="">
                          </div>
                        </div><!-- form-group -->
                         <div class="form-group">
                             <label class="col-sm-4 control-label">Account Host Name</label>
                             <div class="col-sm-6">
                             <input type="text" class="form-control dav_required" name="carddav_account_host_name" id="carddav_account_host_name" placeholder="">
                             </div>
                         </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Port</label>
                            <div class="col-sm-6">
                            <input type="text" class="form-control" name="carddav_port" id="carddav_port" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Principal URL</label>
                            <div class="col-sm-6">
                            <input type="text" class="form-control" name="carddav_princpal_url" id="carddav_princpal_url" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Account User Name</label>
                            <div class="col-sm-6">
                            <input type="text" class="form-control" name="carddav_account_user_name" id="carddav_account_user_name" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Account Password</label>
                            <div class="col-sm-6">
                            <input type="password" class="form-control" name="carddav_account_password" id="carddav_account_password" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                           <label class="col-sm-4 control-label" for="checkbox">Use SSL</label>
                           <div class="col-sm-6">
                              <div class="checkbox block"><label><input name="carddav_ssl_usage_status" <?php echo $checked; ?> id="carddav_ssl_usage_status" value="0" type="checkbox"></label></div>
                           </div>
                         </div>
                     </form>
                      <br />
                      @endif
                      <div class="table-responsive col-sm-12">
                        <table class="table" id="iosCardDavtable">
                          <thead>
                             <tr>
                                <th>Description</th>
                                <th>Host Name</th>
                                <th>User Name</th>
                                @if (Auth::user()->role_id < 4)
                                <th>Action</th>
                                @endif
                             </tr>
                          </thead>
                          <tbody>
                          </tbody>
                       </table>
                </div>
                 @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary saveIOSsettings" currenttab="iosCardDAV">SAVE</button>@endif
            </div>
            <div class="tab-pane" id="iosWebclip">
                @if ( Auth::user()->role_id < 4)
                     <form id="configiosWebclip"  value="config" class="form-horizontal form-bordered" method="post" action="" >
                         <div class="form-group">
                              <label class="col-sm-4 control-label">Label</label>
                          <div class="col-sm-6">
                            <input type="text" class="form-control webclip_required" name="webclip_label" id="webclip_label" placeholder="">
                          </div>
                        </div><!-- form-group -->
                         <div class="form-group">
                             <label class="col-sm-4 control-label">URL</label>
                             <div class="col-sm-6">
                             <input type="text" class="form-control webclip_required" name="webclip_url" id="webclip_url" placeholder="">
                             </div>
                         </div>
                        <div class="form-group">
                           <label class="col-sm-4 control-label" for="checkbox">Removable</label>
                           <div class="col-sm-6">
                              <div class="checkbox block"><label><input name="webclip_removable_status" id="webclip_removable_status" <?php echo $checked; ?> value="0" type="checkbox"></label></div>
                           </div>
                         </div>
                        <div class="form-group">
                           <label class="col-sm-4 control-label" for="checkbox">Precomposed Icon</label>
                           <div class="col-sm-6">
                              <div class="checkbox block"><label><input name="webclip_precomposed_icon_status" id="webclip_precomposed_icon_status" value="0" type="checkbox"></label></div>
                           </div>
                         </div>
                        <div class="form-group">
                           <label class="col-sm-4 control-label" for="checkbox">Full Screen</label>
                           <div class="col-sm-6">
                              <div class="checkbox block"><label><input name="webclip_fullscreen_status" id="webclip_fullscreen_status" value="0" type="checkbox"></label></div>
                           </div>
                         </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="text">Select Icon</label>
                            <div class="col-sm-3">
                            <div id="webclip_icon_show"></div>
                                <input type="file" class="form-control btn btn-primary" name="webclip_icon" id="webclip_icon" placeholder="" value="Icon">
                            </div>
                        </div>
                        <div class="invalid_icon hidden">Invalid icon</div>
                     </form>
                      <br />
                      @endif
                      <div class="table-responsive col-sm-12">
                        <table class="table" id="iosWebcliptable">
                          <thead>
                             <tr>
                                <th>Label</th>
                                <th>Url</th>
                                <th>Icon</th>
                                @if (Auth::user()->role_id < 4)
                                <th>Action</th>
                                @endif
                             </tr>
                          </thead>
                          <tbody>
                          </tbody>
                       </table>
                </div>
                 @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary saveIOSsettings" currenttab="iosWebclip">SAVE</button>@endif
            </div>
            <div class="tab-pane" id="iosKiosksettings">
                <div class="panel-body panel-body-nopadding">
                    <form id="configiosKiosk"  value="config" class="form-horizontal form-bordered" method="post" action="">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Select App</label>
                            <div class="col-sm-6">
                                <select id="kiosk_app" name="kiosk_app" class="select2 form-control" data-placeholder="Choose One">
                                    <option value="">Please select</option>
                                </select>
                            </div>
                        </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">App Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control kiosk_required" name="kiosk_app_name" id="kiosk_app_name" placeholder="ENTER APP NAME">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">App Identifier</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control kiosk_required" name="kiosk_app_identifier" id="kiosk_app_identifier" placeholder="ENTER APP NAME">
                        </div>
                    </div>
                    <div class="">
                        <label class="control-label" style="padding-top: 20px;">OPTIONS(iOS 7.0+)</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Disable touch</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" name="kiosk_disable_touch_status" id="kiosk_disable_touch_status" value="0"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Disable device rotation</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" name="kiosk_disable_device_rotation_status" id="kiosk_disable_device_rotation_status" value="0"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Diasble volume buttons</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" name="kiosk_disable_volume_button_status" id="kiosk_disable_volume_button_status" value="0"></label></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Disable ringer switch</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" name="kiosk_disable_ringer_switch_status" id="kiosk_disable_ringer_switch_status" value="0"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-4 control-label" for="checkbox">Disable sleep wake button</label>
                    <div class="col-sm-6">
                    <div class="checkbox block"><label><input type="checkbox" name="kiosk_disable_sleep_wake_button_status" id="kiosk_disable_sleep_wake_button_status" value="0"></label></div>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-4 control-label" for="checkbox">Disable auto lock</label>
                    <div class="col-sm-6">
                    <div class="checkbox block"><label><input type="checkbox" name="kiosk_disable_auto_lock_status" id="kiosk_disable_auto_lock_status" value="0"></label></div>
                    </div>
                    </div>

                    <div class="form-group">
                    <label class="col-sm-4 control-label" for="checkbox">Enable voice over</label>
                    <div class="col-sm-6">
                    <div class="checkbox block"><label><input type="checkbox" name="kiosk_enable_voice_over_status" id="kiosk_enable_voice_over_status" value="0"></label></div>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-4 control-label" for="checkbox">Enable zoom</label>
                    <div class="col-sm-6">
                    <div class="checkbox block"><label><input type="checkbox" id="kiosk_enable_zoom_status" name="kiosk_enable_zoom_status" value="0"></label></div>
                    </div>
                    </div>

                    <div class="form-group">
                    <label class="col-sm-4 control-label" for="checkbox">Enable invert colors</label>
                    <div class="col-sm-6">
                    <div class="checkbox block"><label><input type="checkbox" name="kiosk_enable_invert_colors_status" id="kiosk_enable_invert_colors_status" value="0"></label></div>
                    </div>
                    </div>



                    <div class="form-group">
                    <label class="col-sm-4 control-label" for="checkbox">Enable assistive touch</label>
                    <div class="col-sm-6">
                    <div class="checkbox block"><label><input type="checkbox" name="kiosk_enable_assistive_touch_status" id="kiosk_enable_assistive_touch_status" value="0"></label></div>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-4 control-label" for="checkbox">Enable speak selection</label>
                    <div class="col-sm-6">
                    <div class="checkbox block"><label><input type="checkbox" id="kiosk_enable_speak_selection_status" name="kiosk_enable_speak_selection_status" value="0"></label></div>
                    </div>
                    </div>
                    <div class="">
                    <label class="control-label" style="padding-top: 20px;">USER ENABLED OPTIONS(iOS 7.0+)</label>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-4 control-label" for="checkbox">Voice over</label>
                    <div class="col-sm-6">
                    <div class="checkbox block"><label><input type="checkbox" id="kiosk_voice_over_status" name="kiosk_voice_over_status" value="0"></label></div>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-4 control-label" for="checkbox">Zoom</label>
                    <div class="col-sm-6">

                    <div class="checkbox block"><label><input type="checkbox" name="kiosk_zoom_status" id="kiosk_zoom_status" value="0"></label></div>

                    </div>
                    </div>

                    <div class="form-group">
                    <label class="col-sm-4 control-label" for="checkbox">Invert colors</label>
                    <div class="col-sm-6">

                    <div class="checkbox block"><label><input type="checkbox" name="kiosk_invert_colors_status" id="kiosk_invert_colors_status" value="0"></label></div>

                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-4 control-label" for="checkbox">Assistive touch</label>
                    <div class="col-sm-6">
                    <div class="checkbox block"><label><input type="checkbox" name="kiosk_assistive_touch_status" id="kiosk_assistive_touch_status" value="0"></label></div>
                    </div>
                    </div>
                    </form>
                    <div class="table-responsive col-sm-12">
                        <table class="table" id="iosKiosktable">
                          <thead>
                             <tr>
                                <th>App name</th>
                                <th>Identifer</th>
                                @if (Auth::user()->role_id < 4)
                                <th>Action</th>
                                @endif
                             </tr>
                          </thead>
                          <tbody>
                          </tbody>
                       </table>
                    </div>
                    @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary saveIOSsettings" currenttab="iosKiosksettings">SAVE</button>@endif
                    </div><!-- panel-body -->
                    </div>
            <div class="tab-pane" id="iosWebfilter">
                     @if ( Auth::user()->role_id < 4 ||  Auth::user()->role_id == 5)
                                <div class="form-group">
                                    <label class="col-sm-2 col-sm-offset-3" for="radio">Filter Type</label>
                                    <input type="radio" name="ios_webfiler_type" id="ios_webfiler_type_bl" class="ios_webfiler_type" value="1"> Black List
                                    <input type="radio" name="ios_webfiler_type" id="ios_webfiler_type_wl" class="ios_webfiler_type" value="2"> White List
                                </div>
                                <div id="blacklist_div" style="display: none;">
                                <form id="config"  value="config" class="form-horizontal form-bordered" method="post" action="" >
                                 <div class="">
                                    <label class="control-label" style="padding-top: 20px;">BLACKLIST WEB URLs</label>
                                 </div>
                                <div class="form-group">
                                      <label class="col-sm-4 control-label">Specify the URLs which need to be blocked on browser.</label>
                                  <div class="col-sm-6">
                                    <textarea class="form-control" name="blacklist_url_name" id="blacklist_url_name" placeholder="Enter URLs separated by ,or; Only valid URLs can be added*"></textarea>
                                  </div>
                                  {{--<button id="saveBlacklistURL" class="btn btn-primary">Add</button>--}}
                                </div><!-- form-group -->
                                <p id="_urlblerror" class="text-danger" style="text-align:center; display: none">Please enter the valid url</p>
                                <div class="">
                                    <label class="control-label" style="padding-top: 20px;">BLACKLIST BY CONTENTS</label>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="checkbox">Restrict inappropriate Content</label>
                                    <div class="col-sm-6">
                                        <div class="checkbox block"><label><input type="checkbox" name="blacklist_restricted_inappropriate_content" id="blacklist_restricted_inappropriate_content" value="0"></label></div>
                                    </div>
                                </div>
                                <div class="table-responsive col-sm-12">
                                      <table class="table" id="iosWebfilerBlacklistUrlTable">
                                        <thead>
                                            <tr>
                                                <th>Blacklisted URLs</th>
                                                @if (Auth::user()->role_id < 4)
                                                <th>Action</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                     </table>
                                </div>
                            </form>
                            @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary saveWebFilerSettings" currentSelectedValue="blackList">SAVE</button>@endif
                            </div>
                                <div id="whitelist_div" style="display: none;">
                                <form id="config"  value="config" class="form-horizontal form-bordered" method="post" action="">
                                   <div class="">
                                      <label class="control-label" style="padding-top: 20px;">WHITELIST WEB URLs</label>
                                   </div>
                                  <div class="form-group">
                                        <label class="col-sm-4 control-label">Specify the URLs which need to be whitelist on brower.</label>
                                    <div class="col-sm-6">
                                      <textarea class="form-control" name="whitelist_url_name" id="whitelist_url_name" placeholder="Enter URLs separated by ,or; Only valid URLs can be added*"></textarea>
                                    </div>
                                    {{--<button id="saveWhitelistURL" class="btn btn-primary">Add</button>--}}
                                  </div><!-- form-group -->
                                  <p id="_urlwlerror" class="text-danger" style="text-align:center; display: none">Please enter the valid url</p>
                                  <div class="table-responsive col-sm-12">
                                        <table class="table" id="iosWebfilerWhitelistUrlTable">
                                          <thead>
                                             <tr>
                                                <th>Whitelisted URLs</th>
                                                <th>Action</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                          </tbody>
                                       </table>
                                  </div>

                              </form>
                              <br />
                         @if (Auth::user()->role_id < 4 &&  Auth::user()->role_id != 5)<button value="SAVE" class="btn btn-primary saveWebFilerSettings" currentSelectedValue="whiteList">SAVE</button>@endif
                         </div>
                         @endif

                    </div>
        </div>
      </div>


</div>
@endsection