@extends('layouts.adminltemain') 
@section('content-main')
        <script src="/js/profile.js"></script>
        <link href="/css/progressbar-style.css" rel="stylesheet">
        <div class="pageheader">
            <section class="content-header">
                <h2><i class="fa fa-edit"></i>Configuration<span>Choose Profile Tabs</span></h2>
                    <ol class="breadcrumb">
                        <li><a href="/dashboard">{{ env('OSP_LABEL') }}</a></li>
                        <li class="active"><a href="/configurations">Configuration</a></li>
                        <li class="active">Choose Profile Tabs</li>
                    </ol>
            </section>
          </div>
    <div class="contentpanel">
        <h5 class="subtitle mb5">PROFILE TABS</h5>
        <div class="panel-footer">
            <div class="form-group">
               <div class="col-sm-12">
                        <input type="hidden" name="userprofileid" id="userprofileid" />
                        <input type="hidden" name="tabShow" id="tabShow" value="<?php echo (!empty($id)) ? 1 : 0 ?> "/>
                        <input type="hidden" name="profileid" id="profileid" />
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <span class="help-block"></span>
                        <?php $checked =  (empty($id)) ? 'checked' : '' ?>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">PROFILE TYPE</label>
                          <div class="col-sm-4">
                          <?php if ($id != ''){
                            $disabled = 'disabled';
                          }else {
                            $disabled = '';
                           } ?>

                            <select id="profile_type_admin" name="profile_type_admin" class="select2 form-control"  <?php echo $disabled;?> data-placeholder="Choose One">
                                <option value="">Please Select</option>
                                <option value="1">Anroid</option>
                                <option value="2">IOS</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-sm-offset-4">
                         <p id="_profileerror" class="text-danger" style="display: none;"></p>
                         <p id="_profilesuccess" class="success-message" style="display: none;"></p>
                         </div>
                       {{-- @endif--}}
               </div>
             </div>
		  </div><!-- panel-footer -->
          <br />
        <!-- Nav tabs -->
        <div id="adminsettings" style="display: none;">
            <div class="tab-content mb30">
                 <form id="configandroid"  value="config" class="form-horizontal form-bordered" method="post" action="">
                    <div class="">
                        <label class="control-label" style="padding-top: 20px;">ANDROID TAB</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Settings</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?>  name="home" id="home" class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Wifi</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="wifi" id="wifi" class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Disable Application Setting</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="packagedisabler" id="packagedisabler" class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Kiosk Setting</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="kiosksettings" id="kiosksettings" class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Application Setting</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="applicationsettings" id="applicationsettings" class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Email Setting</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="emailsettings" id="emailsettings" class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Active Sync Setting</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="activeSyn" id="activeSyn" class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Advanced Setting</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="commands" id="commands" class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                </form>
                <button value="SAVE" class="btn btn-primary" id="adminAndroidSetting" currentTab="androidTab">SAVE</button>
            </div>
        </div>


        <div id="adminIOSsettings" style="display: none">
            <div class="tab-content mb30">
                <form id="configios"  value="config" class="form-horizontal form-bordered" method="post" action="">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="">
                        <label class="control-label" style="padding-top: 20px;">IOS TAB</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Settings</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="iosHome" id="iosHome" class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Active Sync Settings</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="iosActiveSyn" id="iosActiveSyn"  class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Advanced Restriction Settings</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="iosAdvancedrestriction"  class="ischecked" id="iosAdvancedrestriction" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">APN Setting</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="iosApn" id="iosApn"  class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Credential Setting</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="iosCredentials" id="iosCredentials"  class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Email Setting</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="iosEmailsettings" id="iosEmailsettings"  class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Kiosk Setting</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="iosKiosksettings" id="iosKiosksettings"  class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">LDAP Setting</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="iosLdap" id="iosLdap"  class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Password Setting</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="iosPassword" id="iosPassword"  class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">VPN Setting</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="iosVpn" id="iosVpn"  class="ischecked" value="1"></label></div>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Web Filer Setting</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="iosWebfilter" id="iosWebfilter"  class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Wifi Setting</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="iosWifi" id="iosWifi"  class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Card DAV Setting</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="iosCardDAV" id="iosCardDAV"  class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="checkbox">Web Clip Setting</label>
                        <div class="col-sm-6">
                            <div class="checkbox block"><label><input type="checkbox" <?php echo $checked ?> name="iosWebclip" id="iosWebclip"  class="ischecked" value="1"></label></div>
                        </div>
                    </div>
                </form>
                <button value="SAVE" class="btn btn-primary" id="adminIOSSetting" currentTab="IOSTab">SAVE</button>
            </div>
        </div>
</div>
@endsection