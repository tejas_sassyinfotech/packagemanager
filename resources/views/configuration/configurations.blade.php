@extends('layouts.adminltemain')
@section('content-main')
    <script src="/js/configuration.js"></script>
    <div class="pageheader">
        <section class="content-header">
            <h2><i class="fa fa-edit"></i>Configuration<span>Profiles</span></h2>
                <ol class="breadcrumb">
                    <li><a href="/dashboard">{{ env('OSP_LABEL') }}</a></li>
                    <li class="active"><a href="/configurations">Configuration</a></li>
                </ol>
        </section>


    {{--<h2><i class="fa fa-edit"></i> Configuration<span>Profiles</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="/dashboard">OSP</a></li>
          <li class="active"><a href="/configurations">Configuration</a></li>
        </ol>
      </div>--}}
    </div>
    <div class="contentpanel">
          @if ( Auth::user()->role_id < 4)
          <div class="panel panel-default">
              <div class="panel-heading">
                  <div class="panel-btns">
                    <!-- <a href="" class="panel-close">&times;</a>
                    <a href="" class="minimize">&minus;</a> -->
                  </div>
                  <h4 class="panel-title">NEW PROFILE</h4>
              </div>
              <div class="panel-body">
                    <button type="profile" class="btn btn-primary" onclick="location.href='/userprofile';" >Create Profile</button>
              </div><!-- panel-body -->
          </div><!-- panel -->
          @endif
          <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                            <!-- <a href="" class="panel-close">&times;</a>
                            <a href="" class="minimize">&minus;</a> -->
                        <div class="panel-body">
                          <h5 class="subtitle mb5">Profiles</h5>
                          <br />
                          <div id='profile-message' style="display:none;color:green"></div></br>
                          <div class="table-responsive">
                            <table class="table table-striped" id="tableprofile">
                              <thead>
                                 <tr>
                                    <th id="group">Profile Name</th>
                                    <th>Profile Counter</th>
                                    <th>Profile Type</th>
                                    <th>Last udpated on </th>
                                    <th class="icon-policies">Action</th>
                                 </tr>
                              </thead>
                              <tbody>
                              </tbody>
                           </table>
                         </div><!-- panel-body -->
                        </div>
                    </div>
                </div>
          </div>
      </div>
@endsection