@extends('layouts.adminltelogin')

@section('content')
<div class="signinpanel">

        <div class="row">

           {{-- <div class="col-md-7">

                <div class="signin-info">
                    <div class="logopanel">
                        <h1><span>[</span> OSP <span>]</span></h1>
                    </div><!-- logopanel -->

                    <div class="mb20"></div>

                    <h5><strong>Welcome to OSP</strong></h5>
                    <div class="mb20"></div>
                    <strong>Not a member? <a href="/register">Sign Up</a></strong>
                </div><!-- signin0-info -->

            </div><!-- col-sm-7 -->
            <div class="col-md-5">
                @if(Session::has('alert-success'))
                    <div class="alert alert-success"><em> {!! session('alert-success') !!}</em></div>
                @endif
                @if (Session::has('success'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! Session::get('success') !!}</li>
                        </ul>
                    </div>
                @endif
                <form role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}
                    <h4 class="nomargin">Sign In</h4>
                    <p class="mt5 mb20">Login to access your account.</p>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="username" class="form-control uname" placeholder="Username" name="email" />
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" class="form-control pword" placeholder="Password" name="password" />
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <a href="{{ url('/password/reset') }}"><small>Forgot Your Password?</small></a>
                    <button type="submit" class="btn btn-success btn-block">Sign In</button>
                </form>
            </div><!-- col-sm-5 -->--}}





            <div class="login-box">
              <div class="login-logo">
                <h1><span>{{ env('OSP_LABEL') }}</span></h1>
                <h5><strong>Welcome to {{ env('OSP_LABEL') }}</strong></h5>
              </div>
              <div class="mb20"></div>

              <div class="mb20"></div>
              <!-- /.login-logo -->
              <div class="login-box-body">
                <p class="login-box-msg">Sign in to start your session</p>
                @if(Session::has('alert-success'))
                                    <div class="alert alert-success"><em> {!! session('alert-success') !!}</em></div>
                                @endif
                                @if (Session::has('success'))
                                    <div class="alert alert-success">
                                        <ul>
                                            <li>{!! Session::get('success') !!}</li>
                                        </ul>
                                    </div>
                                @endif
                                <form role="form" method="POST" action="{{ url('/login') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input type="username" class="form-control uname" placeholder="Username" name="email" />
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <input type="password" class="form-control pword" placeholder="Password" name="password" />
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    @if (env('PASSWORD_RESET_STATUS') == 1)
                                        <a href="{{ url('/password/reset') }}"><small>Forgot Your Password?</small></a>
                                    @endif
                                    <div>
                                        <button type="submit" class="btn btn-success btn-block">Sign In</button>
                                    </div>
                                    @if (env('GOOGLE_LOGIN_STATUS') == 1)
                                    <div style="text-align: center">
                                        OR
                                    </div>
                                    <div>
                                    <a class="google-login btn btn-block btn-social btn-google" href="{{ url('auth/google') }}">
                                                {{--<span class="fa fa-google"></span>--}} Sign in with Google
                                    </a>
                                    </div>
                                    @endif
                                    </form>
                                    @if (env('PASSWORD_RESET_STATUS') == 1)
                                        <strong>Not a member? <a href="/register">Sign Up</a></strong>
                                    @endif
              </div>
              <!-- /.login-box-body -->
            </div>















        </div><!-- row -->

        <div class="signup-footer">
            <div class="pull-left">
                &copy; {{ date('Y') }}. All Rights Reserved. {{ env('OSP_LABEL') }}
            </div>
        </div>

    </div><!-- signin -->
@endsection