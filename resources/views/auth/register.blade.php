@extends('layouts.adminltelogin')

@section('content')
<div class="signinpanel">
<div class="register-box">
  <div class="register-logo">
    <h1><span>{{ env('OSP_LABEL') }}</span></h1>
    <h5><strong>Welcome to {{ env('OSP_LABEL') }}</strong></h5>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Sign Up</p>

    <form role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}
                        {{--<h3 class="nomargin">Sign Up</h3>--}}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <div class="mb10">
                                    <label class="control-label">Name</label>
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="control-label">Email Address</label>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="control-label">Password</label>
                                    <input id="password" type="password" class="form-control" name="password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                            </div>
                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label class="control-label">Retype Password</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                            </div>
                            <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                {!! app('captcha')->display(); !!}
                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="help-block">
                                            <strong>Captcha required</strong>
                                        </span>
                                    @endif
                            </div>
                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn btn-success btn-block">Sign Up</button>
                                </div>
                            </div>
                    </form>

    <strong>Already a member? <a href="/">Sign In</a></strong>
  </div>
  <!-- /.form-box -->
</div>
        <div class="signup-footer">
            <div class="pull-left">
                &copy; {{ date('Y') }}. All Rights Reserved. {{ env('OSP_LABEL') }}
            </div>
        </div>

    </div><!-- signin -->
@endsection



