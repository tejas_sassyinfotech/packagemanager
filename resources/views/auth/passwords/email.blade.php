@extends('layouts.adminltelogin')
@section('content')
<!-- Forget Password -->
   <div class="signinpanel">
    <div class="register-box">
      <div class="register-logo">
        <h1><span>{{ env('OSP_LABEL') }}</span></h1>
        <h5><strong>Welcome to AI</strong></h5>
      </div>

      <div class="register-box-body">
        <p class="login-box-msg">Reset Password</p>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                      {{ csrf_field() }}
                    <label class="control-label">E-Mail Address</label>
                    <div class="form-group">
                        <div class="mb10 col-md-12">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-success btn-block"><i class="fa fa-btn fa-envelope"></i> Send Password Reset Link</button>
                        </div>
                    </div>
                </form>


        <strong>Already a member? <a href="/">Sign In</a></strong>
      </div>
      <!-- /.form-box -->
    </div>
            <div class="signup-footer">
                <div class="pull-left">
                    &copy; {{ date('Y') }}. All Rights Reserved. OSP
                </div>
            </div>

        </div>
  
@endsection
