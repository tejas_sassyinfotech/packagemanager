@extends('layouts.adminltemain')
@section('content-main'){{--
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
  <script src="http://cdn.oesmith.co.uk/morris-0.4.1.min.js"></script>--}}
<script src="/js/dashboardcount.js"></script>
<div class="pageheader">
    <section class="content-header">
        <h2><i class="fa fa-home"></i>Dashboard</h2>
            <ol class="breadcrumb">
                <li><a href="/dashboard">{{ env('OSP_LABEL') }}</a></li>
                <li class="active">Dashboard</li>
            </ol>
    </section>
    </div>
        <div class="contentpanel">
        <div class="row tp-forms">

            <div class="col-md-3">
                    <div class="form-group">
                        <select id="addProfile" name="profileid" class="select2 profileidpersist form-control" required data-placeholder="Choose One">
                            <option value="">Choose Profile</option>
                        </select>
                    </div>
                </div>

            <div class="col-md-3">
                {{--<div class="col-md-5"><label>From Date</label></div>--}}
                <div class="form-group">
                        <input class="fromDate form-control" type="text" placeholder="Start Date">
                </div>
            </div>

            <div class="col-md-3">
               {{--<div class="col-md-5"> <label>To Date</label></div>--}}
                <div class="form-group">
                        <input class="toDate form-control" type="text" placeholder="End Date">
                </div>
            </div>

            <div class="col-md-3">
                <button id="FilterDevice" class="btn btn-primary">Filter</button>
            </div>

        </div>
        <div class="row"><div class="col-md-12"><div id="filter_error" style="color: red; class="error"></div></div></div>
              <div class="row">

                <div class="col-sm-6 col-md-4">
                  <div class="info-box">
                              <span class="info-box-icon bg-aqua"><i class="fa fa-tablet"></i></span>

                              <div class="info-box-content">
                                <span class="info-box-text">Devices Provisioned</span>
                                <span class="info-box-number" id="onlinestat"></span>
                              </div>
                              <!-- /.info-box-content -->
                            </div>

                </div><!-- col-sm-6 -->

                <div class="col-sm-6 col-md-4">
                  <div class="info-box">
                              <span class="info-box-icon bg-red"><i class="fa fa-tablet"></i></span>

                              <div class="info-box-content">
                                <span class="info-box-text">Devices Not Provisioned</span>
                                <span class="info-box-number" id="offlinestat"></span>
                              </div>
                              <!-- /.info-box-content -->
                            </div>
                </div><!-- col-sm-6 -->

                <div class="col-sm-6 col-md-4">
                  <div class="info-box">
                              <span class="info-box-icon bg-green"><i class="fa fa-tablet"></i></span>

                              <div class="info-box-content">
                                <span class="info-box-text">Total Devices</span>
                                <span class="info-box-number" id="totalstat"></span>
                              </div>
                              <!-- /.info-box-content -->
                            </div>
                </div><!-- col-sm-6 -->
              </div><!-- row -->
         </div>

        <div class="row">

            <div class="col-lg-9">
                <div class="box box-primary">
                    <div class="box-header with-border"><h3 class="box-title">Monthly Recap Report - Monthly Device Activations</h3></div>
                    <div class="box-body chart-responsive table-body">
                        <div id="line-chart" class="chart chart-line table-wrap"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="box box-primary">
                    <div class="box-header with-border"><h3 class="box-title">Device Model</h3></div>
                    <div class="box-body chart-responsive table-body">
                        <div id="device-model-chart" class="chart-model table-wrap"></div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-lg-9 connectedSortable ui-sortable">
                <div class="box box-primary">
                    <div id="map_wrapper">
                        <div id="map_canvas" class="mapping"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 connectedSortable ui-sortable">
                <div class="box box-primary">
                    <div class="box-header with-border"><h3 class="box-title">Operating System</h3></div>
                    <div class="box-body chart-responsive table-body">
                        <div id="device-os-chart" class="chart-model table-wrap"></div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
                <div class="col-lg-4 connectedSortable ui-sortable">
                    <div class="box box-primary">
                        <div class="box-header with-border"><h3 class="box-title">Website metrics</h3></div>
                        <div class="box-body chart-responsive table-body">
                            <div id="Website-chart" class="chart-model table-wrap"><img src="/images/1.jpg" height="230" width=""></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 connectedSortable ui-sortable">
                        <div class="box box-primary">
                            <div class="box-header with-border"><h3 class="box-title">Device Status</h3></div>
                            <div class="box-body chart-responsive table-body">
                                <div id="device-status-chart" class="chart-model table-wrap"></div>
                            </div>
                        </div>
                </div>
                <div class="col-lg-4 connectedSortable ui-sortable">
                    <div class="box box-primary">
                        <div class="box-header with-border"><h3 class="box-title">App Metrics</h3></div>
                        <div class="box-body chart-responsive table-body">
                            <div id="app-chart" class="chart-model table-wrap"><img src="/images/2.jpg" height="203" width=""></div>
                        </div>
                    </div>
                </div>
        </div>

        <div class="row">
                <div class="col-lg-6 connectedSortable ui-sortable">
                                        <div class="box box-primary">
                                            <div class="box-header with-border"><h3 class="box-title">Daily sessions</h3></div>
                                            <div class="box-body chart-responsive table-body">
                                                <div id="Website-chart" class="chart-model table-wrap"><img src="/images/3.jpg" height="180" width="500"></div>
                                            </div>
                                        </div>
                                    </div>

                            <div class="col-lg-6 connectedSortable ui-sortable">
                                                    <div class="box box-primary">
                                                        <div class="box-header with-border"><h3 class="box-title">Average sessions per hour</h3></div>
                                                        <div class="box-body chart-responsive table-body">
                                                            <div id="app-chart" class="chart-model table-wrap"><img src="/images/4.jpg" height="180" width="500"></div>
                                                        </div>
                                                    </div>
                                                </div>
                </div>


        <div id="TableContent"></div>

                <div class="row">
                    <div class="col-sm-12">

                        <div class="box-body box box-info">
                            <div class="table-responsive">
                                <table class="table no-margin table table-striped" id="tableconfig">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Device Identifier </th>
                                            <th>Serial Number</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tableFilterBody">
                                    </tbody>
                                </table>
                            </div>
                            <div class="view-more-device text-center">
                                <a href="/devices" class="hidden" id="viewMore"><b>View More</b></a>
                            </div>
                        </div>

                    </div>
                </div>

@endsection
