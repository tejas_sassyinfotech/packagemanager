@extends('layouts.adminltemain')

@section('content-main')
<div class="contentpanel">

      <div class="row">

        <div class="col-sm-6 col-md-3">
          <div class="panel panel-success panel-stat">
            <div class="panel-heading">

              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <img src="images/is-user.png" alt="" />
                  </div>
                  <div  class="col-xs-8">
                    <small class="stat-label">Devices Provisioned</small>
                    <h1 id="onlinestat"></h1>
                  </div>
                </div><!-- row -->

                <div class="mb15"></div>

                <div class="row">
                  <div class="col-xs-6">
                   <!-- <small class="stat-label">Pages / Visit</small>
                    <h4>7.80</h4>-->
                  </div>

                  <div class="col-xs-6">
                    <!--<small class="stat-label">% New Visits</small>
                    <h4>76.43%</h4>-->
                  </div>
                </div><!-- row -->
              </div><!-- stat -->

            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->

        <div class="col-sm-6 col-md-3">
          <div class="panel panel-danger panel-stat">
            <div class="panel-heading">

              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <img src="images/is-document.png" alt="" />
                  </div>
                  <div class="col-xs-8">
                    <small class="stat-label">Devices Not Provisioned</small>
                    <h1 id="offlinestat"></h1>
                  </div>
                </div><!-- row -->

                <div class="mb15"></div>

                <!--<small class="stat-label">Avg. Visit Duration</small>
                <h4>01:80:22</h4>-->

              </div><!-- stat -->

            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->

        <div class="col-sm-6 col-md-3">
          <div class="panel panel-primary panel-stat">
            <div class="panel-heading">

              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <img src="images/is-document.png" alt="" />
                  </div>
                  <div class="col-xs-8">
                    <small class="stat-label">Total Devices</small>
                    <h1 id="totalstat"></h1>
                  </div>
                </div><!-- row -->

                <div class="mb15"></div>

              <!--  <small class="stat-label">% Bounce Rate</small>
                <h4>34.23%</h4> -->

              </div><!-- stat -->

            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->


      </div><!-- row -->





 <div id="TableContent"></div>
      <div class="row">

        <div class="col-sm-9">

          <div class="table-responsive">
            <table  id="tableconfig" class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>Device Identifier </th>
                <th>Serial Number</th>
                <th>Status</th>
              </tr>
            </thead>
          </table>
          </div><!-- table-responsive -->

        </div><!-- col-sm-7 -->


      </div><!-- row -->


  <div class="rightpanel">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs nav-justified">
        <li class="active"><a href="#rp-alluser" data-toggle="tab"><i class="fa fa-users"></i></a></li>
        <li><a href="#rp-favorites" data-toggle="tab"><i class="fa fa-heart"></i></a></li>
        <li><a href="#rp-history" data-toggle="tab"><i class="fa fa-clock-o"></i></a></li>
        <li><a href="#rp-settings" data-toggle="tab"><i class="fa fa-gear"></i></a></li>
    </ul>



  </div><!-- rightpanel -->

 </div>
@endsection
