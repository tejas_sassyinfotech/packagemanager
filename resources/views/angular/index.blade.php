<!DOCTYPE html>
<html ng-app="simpleApp">
    <head>
        <title>Laravel</title>

        <link rel="stylesheet" href="{{URL::asset('bootstrap.min.css')}}">
        <script type="text/javascript" src="{{URL::asset('angular.min.js')}}"></script>

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container" ng-controller="TodoCtrl">
            <div class="content">
                <div class="title">Laravel 5.1</div>
                 <button class="btn-large" ng-click="greet()"><i class="icon-trash"></i>Press</button>
            </div>
        </div>
    </body>
</html>
<script>
angular.module('simpleApp',[]).controller('simpleController',simpleController)
function TodoCtrl($scope, $window) {
   $scope.greet = function() {
    ($window.mockWindow || $window).alert('Hello');
  }
}
</script>
