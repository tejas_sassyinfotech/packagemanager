@extends('...layouts.surveymain')
@section('content-main'){{--
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
  <script src="http://cdn.oesmith.co.uk/morris-0.4.1.min.js"></script>--}}
<div class="pageheader">
    <section class="content-header">
        <h2><i class="fa fa-home"></i>Survey</h2>
            <ol class="breadcrumb">
                <li><a href="/survey">{{ env('OSP_LABEL') }}</a></li>
                <li class="active">Survey</li>
            </ol>
    </section>
    </div>
        <div class="contentpanel">
            <form role="form" method="POST" action="{{ url('/savesurvey') }}">
                 {{ csrf_field() }}
                </br>
                <input type="hidden" name="survey_id" value="<?php echo $surveyId; ?>">
                <?php foreach($questions as $question) { ?>
                <label class="control-label"><?php echo $question['survey_question']; ?></label>
                <div class="row">
                    <div class="form-group">
                         @if($question['survey_question_type'] == 'radio')
                            @if($question['survey_question_option_desc_status'] == 1)
                                <div class="col-md-6">
                                    <input id="<?php echo $question['id']; ?>" type="text" class="form-control"  name="radio[text][<?php echo $question['id']; ?>]">
                                </div>
                            @endif
                            @if($question['SurveyQuestionOption'])
                                @foreach($question['SurveyQuestionOption'] as $option)
                                    <div class="col-md-6">
                                        <input type="radio" name="radio[option][<?php echo $question['id']; ?>]" value="<?php echo $option['id']?>"> <?php echo $option['survey_question_option']; ?>
                                    </div>
                                @endforeach
                            @endif
                         @elseif($question['survey_question_type'] == 'text')
                             <div class="col-md-6">
                                 <input id="<?php echo $question['id']; ?>" type="text" class="form-control"  name="text[<?php echo $question['id']; ?>]">
                             </div>
                         @elseif($question['survey_question_type'] == 'select')
                            <div class="col-md-6">
                                <select name="select[<?php echo $question['id']; ?>]" id="<?php echo $question['id']; ?>" class="form-control">
                                @if($question['SurveyQuestionOption'])
                                    @foreach($question['SurveyQuestionOption'] as $optionselect)
                                        <option value="<?php echo $optionselect['survey_question_option']?>"><?php echo $optionselect['survey_question_option']?></option>
                                    @endforeach
                                @endif
                                </select>
                              </div>
                         @endif
                    </div>
                    </div>
                <?php
                    } ?>
                <div class="form-group">
                    <input type="submit" class="btn btn-success pull-right" value="Save">
                    <input type="button" class="btn btn-success pull-left" value="Cancel">
                </div>
            </form>
        </div>

@endsection
