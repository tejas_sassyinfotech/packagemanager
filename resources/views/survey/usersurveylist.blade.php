@extends('layouts.surveymain')
@section('content-main')
    {{--<script src="/js/usermanagement.js"></script>--}}
     <script src="/js/jquery.datatables.min.js"></script>

    <div class="pageheader">
        <section class="content-header">
            <h2><i class="fa fa-home"></i>Survey</h2>
                <ol class="breadcrumb">
                    <li><a href="/survey">{{ env('OSP_LABEL') }}</a></li>
                    <li class="active">Survey</li>
                </ol>
        </section>
        </div>
    <div class="contentpanel">
          <div class="panel panel-default">
          @if ( Auth::user()->role_id == 6)
              <div class="panel-heading">
                  <div class="panel-btns">
                  </div>
                  <h4 class="panel-title">ATTEND SURVEY</h4>
              </div>
              <div class="panel-body">
                    <button class="btn btn-primary" onclick="location.href='/survey';" >Attend
                    </button>
              </div>
              @endif
              <!-- panel-body -->
          </div><!-- panel -->
          <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                            <!-- <a href="" class="panel-close">&times;</a>
                            <a href="" class="minimize">&minus;</a> -->
                        <div class="panel-body">
                          <h5 class="subtitle mb5">Surveys</h5>
                          <br />
                          <div id='profile-message' style="display:none;color:green"></div></br>
                          <div class="table-responsive">
                            <table class="table table-striped" id="tablesurvey">
                              <thead>
                                 <tr>
                                    <th id="group">Survey Name</th>
                                    <th>Attend by</th>
                                    <th>Attended on</th>
                                    <th>Action</th>
                                 </tr>
                              </thead>
                              <tbody>
                                @foreach($surveys as $survey)
                                  <th>{{$survey['UserSurvey']['survey_name']}}</th>
                                  <th>{{  Auth::user()->name }}</th>
                                  <th>{{$survey['created_at']}}</th>
                                  <th></th>
                                @endforeach
                              </tbody>
                           </table>
                         </div><!-- panel-body -->
                        </div>
                    </div>
                </div>
          </div>
      </div>
@endsection