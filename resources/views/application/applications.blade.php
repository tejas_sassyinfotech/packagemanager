@extends('layouts.adminltemain')
@section('content-main')
    <script src="/js/application.js"></script>
   <link href="/css/progressbar-style.css" rel="stylesheet">
    <div class="pageheader">
        <section class="content-header">
            <h1><i class="fa fa-android"></i>Applications</h1>
                <ol class="breadcrumb">
                   <li><a href="/dashboard">{{ env('OSP_LABEL') }}</a></li>
                   <li class="active">Applications</li>
                </ol>
        </section>
    </div>
    <div class="contentpanel">
          <div class="panel panel-default">
              <div class="panel-heading">
                   <div class="panel-btns">
                       <!-- <a href="" class="panel-close">&times;</a>
                       <a href="" class="minimize">&minus;</a> -->
                   </div>
                  <form id="applicationdetails" role="form" method="POST" enctype="multipart/form-data" action="">
                        {{ csrf_field() }}
                       <!-- <div class="form-group">
                           <div class="col-sm-4">
                               <label class="col-sm-4 control-label">PACKAGE NAME (optional)</label>
                               <div class="col-sm-6">
                                   <input type="text" name="package" id="package" />
                                   <span class="help-block"></span>
                               </div>
                           </div>
                       </div>
                       <div class="form-group">
                           <div class="col-sm-4">
                               <label class="col-sm-4 control-label">VERSION CODE (optional)</label>
                               <div class="col-sm-6">
                                   <input type="text" name="vcode" id="vcode">
                                   <span class="help-block"></span>
                               </div>
                           </div>
                       </div> -->
                       @if ( Auth::user()->role_id < 4)
                        <div class="form-group">
                            <div class="col-sm-4">
                                <p>
                                    UPLOAD APPLICATION : <input type='file' id='_file' name="file"></p><p><input class="btn btn-primary btn-sm" type='submit' id='_submit' value='UPLOAD'>
                                    <input type="text" id="errormsg" placeholder="" readonly style="border: none;color:red;" >
                                    <div id='err-message' style="display:none;color:red"></div>
                                    <div id='success-message' style="color:green"></div>
                                </p>
                            </div>
                        </div>
                        <p id="loaded_n_total"></p>
                        <p></p>
                        <div class="col-sm-4">
                       <div class="w3-progress-container">
                        <div id="myBar" class="w3-progressbar w3-green" style="width:0%"></div>
                      </div>
                      </div>
                      @endif
          <div class="panel panel-default">
                <div class="panel-heading">
                      <div class="panel-btns">
                        <!-- <a href="" class="panel-close">&times;</a>
                        <a href="" class="minimize">&minus;</a> -->
                        <div class="panel-body">
                          <h5 class="subtitle mb5">Application List</h5>
                          <br />
                         
                         <div id='del-message' style="display:none;color:green"></div></br>
                              <div class="table-responsive">
                                <table class="table" id="applicationstable">
                                  <thead>
                                     <tr>
                                        <th id="group">Application Name</th>
                                        <th>Version</th>
                                        <th>Package Name</th>
                                        <th>Date Uploaded</th>
                                        @if ( Auth::user()->role_id < 4)
                                        <th>Action</th>
                                        @endif
                                     </tr>
                                  </thead>
                                  <tbody>
                                  </tbody>
                               </table>
                              </div><!-- panel-body -->
                        </div><!-- panel -->
                  </div>
              </div>
          </div>
      </div>
@endsection