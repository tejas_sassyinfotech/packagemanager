@extends('layouts.main')

@section('content-main')
<div class="pageheader">
      <h2><i class="fa fa-android"></i>Applications<span></span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="/dashboard">OSP</a></li>
          <li class="active">Applications</li>
        </ol>
      </div>
</div>
<section>
    <div class="contentpanel">

        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
                </div>
                <form role="form" method="POST" enctype="multipart/form-data" name="application_form" id="application_form" >
                   
                    <div class="form-group">
                  <div class="col-sm-4">
                    <label class="col-sm-4 control-label">PACKAGE NAME (optional)</label>
                    <div class="col-sm-6">
                      <input type="text" name="application_package" id="package1" />
                      <span class="help-block"></span>
                    </div>
                  </div>
                </div>
                    <div class="form-group">
                  <div class="col-sm-4">
                    <label class="col-sm-4 control-label">VERSION CODE (optional)</label>
                    <div class="col-sm-6">
                      <input type="text" name="application_version" id="vcode" />
                      <span class="help-block"></span>
                    </div>
                  </div>
                </div>
                    <div class="form-group">
                <div class="col-sm-4">
                    <p>
                         SELECT FILE: <input type='file' id='_file' name="file"><p></p> <input class="btn btn-primary btn-sm" type='button' id='_submit' value='UPLOAD'>

                         <input type="text" id="errormsg" placeholder="" readonly style="border: none;color:red;" >

                    </p>
                </div>
                </div>

                    <p id="loaded_n_total"></p>
                    <p></p>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" id='_progress' role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                          <span class="sr-only">0% Complete (success)</span>
                        </div>
                </div>
                </form>
            </div><!-- panel -->
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>

            <div class="panel-body">
                <h5 class="subtitle mb5">Application List</h5>

                <br />
                <div class="table-responsive">
                   <table class="table" id="table1">
                     <thead>
                        <tr>
                               <th id="group">Application Name</th>
                               <th>Version Code</th>
                           <th>Package Name</th>
                           <th>Date Uploaded</th>
                               <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>

                     </tbody>
                  </table>
                </div><!-- panel-body -->
            </div><!-- panel -->

        </div><!-- contentpanel -->

      </div><!-- mainpanel -->
        </div>

    </div>
</section>

@endsection
