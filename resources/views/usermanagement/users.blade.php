@extends('layouts.adminltemain')
@section('content-main')
    <script src="/js/usermanagement.js"></script>
     <script src="/js/jquery.datatables.min.js"></script>

    <div class="pageheader">
        <section class="content-header">
            <h2><i class="fa fa-user"></i>User Management<span>Users</span></h2>
                <ol class="breadcrumb">
                    <li><a href="/dashboard">{{ env('OSP_LABEL') }}</a></li>
                    <li class="active">User Management</li>
                </ol>
        </section>
    {{--<h2><i class="fa fa-edit"></i> User Management<span>Users</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="/dashboard">OSP</a></li>
          <li class="active"><a href="/configurations">Configuration</a></li>
        </ol>
      </div>--}}
    </div>
    <div class="contentpanel">
          <div class="panel panel-default">
          @if ( Auth::user()->role_id < 3)
              <div class="panel-heading">
                  <div class="panel-btns">
                  </div>
                  <h4 class="panel-title">ADD NEW USER</h4>
              </div>
              <div class="panel-body">
                    <button type="profile" class="btn btn-primary" onclick="location.href='/useradd';" >Create User</button>
              </div>
              @endif
              <!-- panel-body -->
          </div><!-- panel -->
          <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                            <!-- <a href="" class="panel-close">&times;</a>
                            <a href="" class="minimize">&minus;</a> -->
                        <div class="panel-body">
                          <h5 class="subtitle mb5">USERS</h5>
                          <br />
                          <div id='profile-message' style="display:none;color:green"></div></br>
                          <div class="table-responsive">
                            <table class="table table-striped" id="tableuser">
                              <thead>
                                 <tr>
                                    <th id="group">Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                 </tr>
                              </thead>
                              <tbody>
                              </tbody>
                           </table>
                         </div><!-- panel-body -->
                        </div>
                    </div>
                </div>
          </div>
      </div>
@endsection