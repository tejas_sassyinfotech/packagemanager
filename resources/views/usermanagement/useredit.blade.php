@extends('layouts.adminltemain')
@section('content-main')
   <script src="/js/usermanagement.js"></script>
   <link href="/css/progressbar-style.css" rel="stylesheet">
    <div class="pageheader">
        <section class="content-header">
            <h2><i class="fa fa-user"></i>Edit User</h2>
                <ol class="breadcrumb">
                    <li><a href="/dashboard">{{ env('OSP_LABEL') }}</a></li>
                    <li class="active">Edit User</li>
                </ol>
        </section>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
                <div class="panel-heading">User Info
                  <div class="panel-btns">
                    <!-- <a href="" class="panel-close">&times;</a>
                    <a href="" class="minimize">&minus;</a> -->
                  </div>
                </div>
                <div class="panel-body">
                    <form id="license"  value="license"  method="post" >
                                          {{ csrf_field() }}
                                      <div class="form-group">
                                      <label class="col-sm-3 control-label"></label>
                                          <div class="col-sm-5">
                                            <select id="rolelist" class="form-control mb15" required data-placeholder="Choose One" onchange="valueselect(this.value)">
                                               <option value="{{$userDetails->role_id}}">{{$userDetails->role->name}}</option>
                                            </select>
                                             @if(Auth::getUser()->role_id !=2 )
                                             <select id="adminlist" class="form-control mb15" required data-placeholder="Choose One" style="display: none;">
                                               <option value="0">-Select Administrator-</option>
                                            </select>
                                            <div class="adminlist_error" style="color: red"></div>
                                             @else
                                                <input id="adminlist" type="hidden" value="{{Auth::getUser()->id}}">
                                                @endif
                                            <select id="testuserlist" class="form-control mb15" required data-placeholder="Choose One" style="display: none;">
                                               <option value="0">-Select Test User-</option>
                                            </select>
                                             <div class="testuserlist_error" style="color: red"></div>
                                            @if($userDetails['master_user'])
                                              <select id="adminlist" class="form-control mb15 admin-list" required data-placeholder="Choose One" @if($userDetails->role_id == 4 || $userDetails->role_id == 3 && Auth::getUser()->role_id !=2) style="display: block;" @else  style="display: none;" @endif>
                                                <option value="{{$userDetails['master_user']->id}}">{{$userDetails['master_user']->name}}</option>
                                             </select>
                                             <select id="testuserlist" class="form-control mb15" required data-placeholder="Choose One" style="display: none;">
                                                <option value="0">-Select Test User-</option>
                                             </select>
                                             @endif
                                             @if($userDetails['test_user'])
                                            <select id="testuserlist" class="form-control mb15 test-list" required data-placeholder="Choose One" @if($userDetails->role_id == 4) style="display: block;" @else  style="display: none;" @endif>
                                               <option value="{{$userDetails['test_user']->id}}">{{$userDetails['test_user']->name}}</option>
                                            </select>
                                            @endif
                                            <?php if($userDetails->name){
                                                $readonly = 'readonly';
                                            }else{
                                                $readonly='';
                                            }?>

                                            <input type="text" id="username" <?php echo $readonly; ?> value="{{$userDetails->name}}" class="form-control mb15">
                                            <div class="username_error" style="color: red"></div>
                                            <input type="text" id="useremail" value="{{$userDetails->email}}" class="form-control mb15">
                                            <div class="useremail_error" style="color: red"></div>
                                            <input type="password" id="userpassword" placeholder="Password" class="form-control mb15">
                                            <input type="hidden" id="userid" value="{{$userDetails->id}}" class="form-control mb15">
                                            <input type="hidden" id="test-user-id" name="test-user" value="">
                                            <div class="success-message" style="color: #009900"></div>
                                          </div>
                                           <button id="updateuser" value="Update" type="button" class="btn btn-primary">Update</button>
                                           <input type="hidden" class="log-user-id" value="{{Auth::getUser()->role_id}}">
                                        </div>
                                      </form>
                </div> <!-- panel -->
        </div>
    </div>
@endsection