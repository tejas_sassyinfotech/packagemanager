@extends('layouts.adminltemain')
@section('content-main')
   <script src="/js/usermanagement.js"></script>
   <script src="/js/jquery.datatables.min.js"></script>
   <link href="/css/progressbar-style.css" rel="stylesheet">
    <div class="pageheader">
        <section class="content-header">
            <h2><i class="fa fa-user"></i>Add User</h2>
            <ol class="breadcrumb">
                <li><a href="/dashboard">{{ env('OSP_LABEL') }}</a></li>
                <li class="active">Add User</li>
            </ol>
        </section>


      {{--<h2><i class="fa fa-android"></i>  Add User</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="/dashboard">OSP</a></li>
          <li class="active">Add User</li>
        </ol>
      </div>--}}
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
                <div class="panel-heading">User Info
                  <div class="panel-btns">
                    <!-- <a href="" class="panel-close">&times;</a>
                    <a href="" class="minimize">&minus;</a> -->
                  </div>
                </div><!-- panel -->
                <div class="panel-body">
                    <form id="license"  value="license"  method="post" >
                                          {{ csrf_field() }}
                                      <div class="form-group">
                                          <label class="col-sm-3 control-label">{{--User Info--}}</label>
                                          <div class="col-sm-5">
                                            <select id="rolelist" class="form-control mb15" required data-placeholder="Choose One">
                                               <option value="0">-Select Role-</option>
                                            </select>
                                            @if(Auth::getUser()->role_id !=2 )
                                            <select id="adminlist" class="form-control mb15" required data-placeholder="Choose One" style="display: none;"  >
                                               <option value="0">-Select Administrator-</option>
                                               <div class="adminlist_error" style="color: red"></div>
                                            </select>
                                            @else
                                            <input id="adminlist" type="hidden" value="{{Auth::getUser()->id}}">
                                            @endif

                                            <select id="testuserlist" class="form-control mb15" required data-placeholder="Choose One" style="display: none;">
                                               <option value="0">-Select Test User-</option>
                                            </select>
                                             <div class="testuserlist_error" style="color: red"></div>
                                            <input type="text" id="username" placeholder="Name" class="form-control mb15">
                                            <div class="username_error" style="color: red"></div>
                                            <input type="text" id="useremail" placeholder="Email" class="form-control mb15">
                                            <div class="useremail_error" style="color: red"></div>
                                            <div class="success-message" style="color: #009900"></div>
                                          </div>
                                           <button id="adduser" value="add" type="button" class="btn btn-primary">ADD</button>
                                           <input type="hidden" class="log-user-id" value="{{Auth::getUser()->role_id}}">

                                        </div>
                                      </form>
                </div>
        </div>
    </div>
@endsection




