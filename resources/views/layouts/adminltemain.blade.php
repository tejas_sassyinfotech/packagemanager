<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <link rel="shortcut icon" href="/images/favicon.png" type="image/png">
      <title>{{ env('OSP_LABEL') }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="/adminlte/bootstrap/css/bootstrap.min.css">
  <link href="/css/jquery.datatables.css" rel="stylesheet">
  <link href="/css/style.default.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="/adminlte/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="/adminlte/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->

  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

  {{--<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>--}}
  <!-- jvectormap -->
  <link rel="stylesheet" href="/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="/adminlte/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="/adminlte/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <script src="/js/jquery-1.11.1.min.js"></script>
    <script src="/js/jquery-ui-1.10.3.min.js"></script>
    <script src="/js/jquery.datatables.min.js"></script>
    <div id="fb-root"></div>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="/js/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <script src="/js/modernizr.min.js"></script>
    <script src="/js/jquery-migrate-1.2.1.min.js"></script>

            <script src="/js/toggles.min.js"></script>
            <script src="/js/retina.min.js"></script>
            <script src="/js/jquery.cookies.js"></script>
            <script src="/js/select2.min.js"></script>
            <script src="/js/custom.js"></script>




</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">{{ env('OSP_LABEL') }}</span>
      <!-- logo for regular state and mobile devices -->
      <h1><span>{{ env('OSP_LABEL') }}</span></h1>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
      <input type="hidden" value="{{Auth::user()->role_id}}" id="role_id">

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="hidden-xs">{{Auth::user()->name}}</span><img class="img-circle user-image" src="/images/photos/loggeduser.png" alt="">
                </a>
                <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                    <li><a href="/profile"><i class="glyphicon glyphicon-user"></i> My Profile</a></li>
                    {{--<li><a href="#"><i class="glyphicon glyphicon-cog"></i> Account Settings</a></li>
                    <li><a href="#"><i class="glyphicon glyphicon-question-sign"></i> Help</a></li>--}}
                    <li><a href="/logout"><i class="glyphicon glyphicon-log-out"></i> Log Out</a></li>
                </ul>
            </li>
        </ul>
      </div>
     {{-- <div class="header-right">
                  <ul class="headermenu">
                    <div class="btn-group">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="/images/photos/loggeduser.png" class="user-image" alt="User Image">
                                    <span class="hidden-xs">{{Auth::user()->name}}</span>
                                  </a>
                        --}}{{--<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                          <img src="/images/photos/loggeduser.png" alt="">
                          <span id="usrname"></span>
                          <span class="caret"></span>
                        </button>--}}{{--
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                          <li><a href="/profile"><i class="glyphicon glyphicon-user"></i> My Profile</a></li>
                          <li><a href="#"><i class="glyphicon glyphicon-cog"></i> Account Settings</a></li>
                          <li><a href="#"><i class="glyphicon glyphicon-question-sign"></i> Help</a></li>
                          <li><a href="/logout"><i class="glyphicon glyphicon-log-out"></i> Log Out</a></li>
                        </ul>
                      </div>

                  </ul>
                </div>--}}
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/images/photos/loggeduser.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      {{--<form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>--}}
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li id="dashboard" class="navigator active">
            <a href="/dashboard"><i class="fa fa-home"></i> <span>Dashboard</span></a>
        </li>
        <li class="navigator" id="usermanagement">
            <a href="/usermanagement"><i class="fa fa-users"></i> <span>Users</span></a>
        </li>
        <li id="configurations" class="navigator">
            <a href="/configurations"><i class="fa fa-edit"></i> <span>Policies</span></a>
        </li>
        <li id="applications" class="navigator">
            <a href="/applications"><i class="fa fa-android"></i> <span>Applications</span></a>
        </li>
        <li id="devices" class="navigator">
            <a href="/devices"><i class="fa fa-tablet"></i> <span>Devices</span></a>
        </li>
        <li id="licences" class="navigator">
            <a href="/licences"><i class="fa fa-key"></i> <span>License</span></a>
        </li>
        <?php if(Auth::user()->role_id == 1){ ?>
        <li id="tabmanagement" class="navigator">
            <a href="/adminuserprofile"><i class="fa fa-caret-square-o-down"></i> <span>Tab Management</span></a>
        </li>
        <?php } ?>
        <li class="treeview">
        <a href="#">
                    <i class="fa fa-list-alt"></i> <span>Reports</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li class="active"><a href="/registrations"><i class="fa fa-list-alt"></i>Device Registrations</a></li>
                    <li><a href="/wipes"><i class="fa fa-list-alt"></i>Device Wipe</a></li>
                    <li><a href="/deviceactivation"><i class="fa fa-list-alt"></i>Device Activation</a></li>
                  </ul>
        </li>
        <li class="treeview">
        <a href="#">
                    <i class="fa fa-book"></i> <span>Documentation</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li class="active"><a href="#"><i class="fa fa-book"></i>User Guide</a></li>
                    <li><a href="#"><i class="fa fa-question-circle"></i>FAQ</a></li>
                    <li><a href="#"><i class="fa fa-video-camera"></i>Videos</a></li>
                  </ul>
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->


  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      @yield('content-main')
    </section>
  </div>


</div>

<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
{{--<script src="/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>--}}
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

<script src="/adminlte/plugins/morris/morris.min.js"></script>
{{--<script src="js/morris.min.js"></script>--}}
<!-- Sparkline -->
<script src="/adminlte/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="/adminlte/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="/adminlte/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/adminlte/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/adminlte/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{--<script src="/adminlte/dist/js/pages/dashboard.js"></script>--}}

<!-- AdminLTE for demo purposes -->
</body>
</html>
