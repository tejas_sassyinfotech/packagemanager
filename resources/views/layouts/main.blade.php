<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="shortcut icon" href="/images/favicon.png" type="image/png">
    <title>{{ env('OSP_LABEL') }}</title>
    <link href="/css/style.default.css" rel="stylesheet">
    <script type="text/javascript" src = "/js/jquery-1.11.1.min.js"></script>
    <link href="/css/jquery.datatables.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/js/html5shiv.js"></script>
    <script src="/js/respond.min.js"></script>
    <![endif]-->
        <script src="/js/jquery-1.11.1.min.js"></script>
        <script src="/js/jquery-migrate-1.2.1.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/modernizr.min.js"></script>
        <script src="/js/jquery.sparkline.min.js"></script>
        <script src="/js/toggles.min.js"></script>
        <script src="/js/retina.min.js"></script>
        <script src="/js/jquery.cookies.js"></script>
        <script src="/js/jquery.datatables.min.js"></script>
        <script src="/js/select2.min.js"></script>
        <script src="/js/custom.js"></script>
</head>
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>
      <div class="leftpanel">
        <div class="logopanel">
            <h1><span>{{ env('OSP_LABEL') }}</span></h1>
        </div><!-- logopanel -->
        <div class="leftpanelinner">
            <!-- This is only visible to small devices -->
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media userlogged">
                    <img alt="" src="images/photos/loggeduser.png" class="media-object">
                    <div class="media-body">
                        <h4> </h4>
                    </div>
                </div>
                <h5 class="sidebartitle actitle">Account</h5>
                <ul class="nav nav-pills nav-stacked nav-bracket mb30">
                  <li><a href="#"><i class="fa fa-user"></i> <span>Profile</span></a></li>
                  <li><a href="#"><i class="fa fa-cog"></i> <span>Account Settings</span></a></li>
                  <li><a href="#"><i class="fa fa-question-circle"></i> <span>Help</span></a></li>
                  <li><a href="/logout"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
                </ul>
            </div>
          <input type="hidden" value="{{Auth::user()->role_id}}" id="role_id">
          <h5 class="sidebartitle">Navigation</h5>
          <ul class="nav nav-pills nav-stacked nav-bracket">
            <li class="navigator active" id="dashboard"><a href="/dashboard"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
            <li class="navigator" id="configurations"><a href="/configurations"><i class="fa fa-edit"></i> <span>Configuration</span></a>
            <li class="navigator" id="applications"><a href="/applications"><i class="fa fa-android"></i> <span>Applications</span></a>
            <li class="navigator" id="devices"><a href="/devices"><i class="fa fa-tablet"></i> <span>Devices</span></a></li>
            <li class="navigator" id="registration"><a href="/registrations"><i class="fa fa-users"></i> <span>Registration</span></a></li>
            <li class="navigator" id="licences"><a href="/licences"><i class="fa fa-key"></i> <span>License</span></a></li>
            <li class="navigator"><a href="#"><i class="fa fa-list-alt"></i> <span>Report</span></a></li>
            @if ( Auth::user()->role_id < 4) <li class="navigator" id="usermanagement"><a href="/usermanagement"><i class="fa fa-users"></i> <span>User Management</span></a></li> @endif
                </ul>
            </li>
          </ul>
        </div><!-- leftpanelinner -->
      </div><!-- leftpanel -->
      <div class="mainpanel">
        <div class="headerbar">
          <a class="menutoggle"><i class="fa fa-bars"></i></a>
          <div class="header-right">
            <ul class="headermenu">
                <div class="btn-group">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <img src="/images/photos/loggeduser.png" alt="" />
                    <span id="usrname"></span>
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                    <li><a href="/profile"><i class="glyphicon glyphicon-user"></i> My Profile</a></li>
                    <li><a href="#"><i class="glyphicon glyphicon-cog"></i> Account Settings</a></li>
                    <li><a href="#"><i class="glyphicon glyphicon-question-sign"></i> Help</a></li>
                    <li><a href="/logout"><i class="glyphicon glyphicon-log-out"></i> Log Out</a></li>
                  </ul>
                </div>
              </li>
            </ul>
          </div><!-- header-right -->

        </div><!-- headerbar -->
        @yield('content-main')
       </div>
</body>
</html>
