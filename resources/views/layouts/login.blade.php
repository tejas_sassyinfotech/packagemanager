<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="/images/favicon.png" type="image/png">

    <title>{{ env('OSP_LABEL') }}</title>
    <link href="/css/style.default.css" rel="stylesheet">
</head>
<body class="signin">
        @yield('content')
</body>
</html>
