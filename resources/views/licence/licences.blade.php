@extends('layouts.adminltemain')
@section('content-main')
   <script src="/js/licence.js"></script>
   <link href="/css/progressbar-style.css" rel="stylesheet">
    <div class="pageheader">
        <section class="content-header">
            <h2><i class="fa fa-android"></i>License</h2>
                <ol class="breadcrumb">
                    <li><a href="/dashboard">{{ env('OSP_LABEL') }}</a></li>
                    <li class="active">License</li>
                </ol>
        </section>
      {{--<h2><i class="fa fa-android"></i>  License <span></span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="/dashboard">OSP</a></li>
          <li class="active">License</li>
        </ol>
      </div>--}}
    </div>
    <div class="contentpanel">
    @if ( Auth::user()->role_id < 4)
        <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="panel-btns">
                    <!-- <a href="" class="panel-close">&times;</a>
                    <a href="" class="minimize">&minus;</a> -->
                  </div>
                  <form id="license"  value="license"  method="post" >  
                      {{ csrf_field() }}
                  <div class="form-group">
                      <label class="col-sm-3 control-label">LICENSE INFO</label>
                      <div class="col-sm-5">
                        <input type="text" id="name" placeholder="License Name" class="form-control mb15">
                        <input type="text" id="code" placeholder="License CODE" class="form-control mb15">
                        <input type="text" id="licencedesc" placeholder="Licence Description" class="form-control mb15">
                        <input type="text" id="qty" placeholder="Quantity" class="form-control mb15">
                 <!--       <input type="text" id="eap" placeholder="EAP Method" class="form-control mb15">
                        <input type="text" id="phase2" placeholder="Phase 2 Authentication" class="form-control mb15"> -->
                        <select id="profilelist" class="form-control mb15" required data-placeholder="Choose One">
                           <option value="0">-Select Profile-</option>
                        </select>
                      </div>
                       <button onclick="addlicense()" id="button" value="add" type="button" class="btn btn-primary">ADD</button>
                    </div>
                  </form>
              </div><!-- panel -->
        </div>
        @endif
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class="panel-btns">
                <!-- <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a> -->
                <div class="panel-body">
                  <br />
                   <div id='del-license' style="display:none;color:green"></div></br>
                  <div class="table-responsive">
                    <table class="table" id="licencetable">
                      <thead>
                         <tr>
                         	<th id="group">License Name</th>
                         	<th>License code</th>
                         	<th>Profile</th>
                         	<th>Max Qty</th>
                            <th>Used Qty</th>
                            <th>Start Date</th>
                            @if ( Auth::user()->role_id < 4)
                    		<th>Action</th>
                    		@endif
                         </tr>
                      </thead>
                      <tbody>
                      </tbody>
                   </table>
                  </div><!-- panel-body -->
                </div><!-- panel -->
            </div>
          </div>
        </div>

    </div>
@endsection