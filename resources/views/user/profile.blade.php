@extends('layouts.adminltemain')

@section('content-main')
<div class="pageheader">
    <section class="content-header">
        <h2><i class="fa fa-home"></i>User Profile</h2>
            <ol class="breadcrumb">
                <li><a href="/dashboard">{{ env('OSP_LABEL') }}</a></li>
                <li class="active">My Profile</li>
            </ol>
    </section>

</div>
<section>
<div class="contentpanel">
        <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="panel-btns">
                    <!-- <a href="" class="panel-close">&times;</a>
                    <a href="" class="minimize">&minus;</a> -->
                  </div>
                </div><!-- panel -->
                <div class="panel-body">
                    <div class="">
                        <div class="row">
                            <div class="col-md-6">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="col-md-3"> <label class="control-label">Name :</label></div>
                                                                                  <div class="col-md-7"> <strong>{{ Auth::getUser()->name }}</strong></div>
                                                                              </div>
                                                                          </div>
                                                                          <div class="row">
                                                                              <div class="col-md-10">
                                                                                  <div class="col-md-3"> <label class="control-label">Email :</label></div>
                                                                                  <div class="col-md-7"> <strong>{{ Auth::getUser()->email }}</strong></div>
                                                                              </div>
                                                                          </div>
                                                                         <br/><br/>
                                                                         <div class="col-md-6"><a href="/editprofile"><button class="btn btn-success btn-block">Edit Profile</button></a> </div>
                                                                  </div><!-- col-sm-6 -->

                                                              </div><!-- row -->

                                                              @if(Session::has('alert-success'))
                                                              <div class="alert alert-success"><em> {!! session('alert-success') !!}</em></div>
                                                              @endif
                                                               </div>
                </div>
        </div>
    </div>
</section>

@endsection
