@extends('layouts.adminltemain')

@section('content-main')
    <div class="pageheader">
        <section class="content-header">
            <h2><i class="fa fa-home"></i>Change Password</h2>
                <ol class="breadcrumb">
                    <li><a href="/dashboard">{{ env('OSP_LABEL') }}</a></li>
                    <li><a href="/profile">My Profile</a></li>
                    <li><a href="/editprofile">Edit Profile</a></li>
                    <li class="active">Change Password</li>
                </ol>
        </section>
</div>
<section>

  <div class="contentpanel">
            <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-btns">Change Password
                        <!-- <a href="" class="panel-close">&times;</a>
                        <a href="" class="minimize">&minus;</a> -->
                      </div>
                    </div><!-- panel -->
                    <div class="panel-body">
                         <div class="profilepanel">

                                <div class="row">

                                    <div class="col-md-6">
                                         @if(Session::has('alert-success'))
                                            <div class="alert alert-success"><em> {!! session('alert-success') !!}</em></div>
                                        @endif
                                        <form role="form" method="POST" action="{{ url('/changepassword') }}">
                                             {{ csrf_field() }}
                                            </br>
                                            <div class="password_section">
                                                <div class="mb10">
                                                <label class="control-label">Password</label>
                                                <input id="oldpassword" type="password" class="form-control" name="oldpassword">
                                                 @if(Session::has('oldpassword-error'))
                                                    <span class="help-block">
                                                        <strong>{!! session('oldpassword-error') !!}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="mb10">
                                                <label class="control-label">New Password</label>
                                                <input id="password" type="password" class="form-control" name="password">
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>

                                            <div class="mb10">
                                                <label class="control-label">Confirm Password</label>
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                                @if ($errors->has('password_confirmation'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            </div>

                                            <input type="submit" class="btn btn-success btn-block" value="Save">
                                        </form>
                                    </div><!-- col-sm-6 -->

                                </div><!-- row -->
                            </div><!-- signuppanel -->
                          
                    </div>
            </div>
        </div>







</section>

@endsection
