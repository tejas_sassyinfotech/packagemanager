@extends('layouts.adminltemain')

@section('content-main')
<div class="pageheader">
    <section class="content-header">
        <h2><i class="fa fa-home"></i>Edit Profile</h2>
        <ol class="breadcrumb">
            <li><a href="/dashboard">{{ env('OSP_LABEL') }}</a></li>
            <li><a href="/profile">My Profile</a></li>
            <li class="active">Edit Profile</li>
        </ol>
    </section>
</div>
<section>

  <div class="contentpanel">
          <div class="panel panel-default">
                  <div class="panel-heading">
                    <div class="panel-btns">Edit Profile
                      <!-- <a href="" class="panel-close">&times;</a>
                      <a href="" class="minimize">&minus;</a> -->
                    </div>
                  </div><!-- panel -->
                  <div class="panel-body">
                      <div class="profilepanel">

                              <div class="row">

                                  <div class="col-md-6">
                                       @if(Session::has('alert-success'))
                                          <div class="alert alert-success"><em> {!! session('alert-success') !!}</em></div>
                                      @endif
                                      <form role="form" method="POST" action="{{ url('/update') }}">
                                           {{ csrf_field() }}
                                          </br>
                                          <?php if(Auth::getUser()->name){
                                              $readonly = 'readonly';
                                          }else{
                                              $readonly='';
                                          }?>
                                          <label class="control-label">Name</label>
                                          <div class="mb10">
                                                  <input id="name" type="text" class="form-control" <?php echo $readonly; ?> name="name" value="{{ Auth::getUser()->name }}">
                                              @if ($errors->has('name'))
                                                  <span class="help-block">
                                                      <strong>{{ $errors->first('name') }}</strong>
                                                  </span>
                                              @endif
                                          </div>

                                          <div class="mb10">
                                              <label class="control-label">Email</label>
                                                  <input id="email" type="email" class="form-control" name="email" value="{{ Auth::getUser()->email }}">
                                                   @if ($errors->has('email'))
                                                      <span class="help-block">
                                                          <strong>{{ $errors->first('email') }}</strong>
                                                      </span>
                                                  @endif
                                          </div>
                                          </br>
                                           <div class="mb10">
                                          <input type="submit"class="btn btn-success btn-block passchange" value="Save">
                                           </div>
                                      </form>

                                          <div class="mb10">
                                          <a href="/editpassword"><button class="btn btn-success btn-block">Change Password</button> </a>
                                          </div>

                                  </div><!-- col-sm-6 -->

                              </div><!-- row -->
                          </div>
                  </div>
          </div>
      </div>
    <!-- signuppanel -->
  
</section>

@endsection
