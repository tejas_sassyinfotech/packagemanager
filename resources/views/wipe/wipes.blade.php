@extends('layouts.adminltemain')
@section('content-main')
    <script src="/js/wipe.js"></script>
    <div class="pageheader">
        <section class="content-header">
            <h2><i class="fa fa-users"></i>Device Wipes</h2>
                <ol class="breadcrumb">
                    <li><a href="/dashboard">{{ env('OSP_LABEL') }}</a></li>
                    <li class="active">Device Wipes</li>
                </ol>
        </section>
      {{--<h2><i class="fa fa-users"></i> Registration </h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="/dashboard">OSP</a></li>
          <li class="active">Registration</li>
        </ol>
      </div>--}}
    </div>
    <div class="contentpanel">
          <div class="panel panel-default">
                  <div class="panel-heading">
                    <!-- <div class="panel-btns">
                      <a href="" class="panel-close">&times;</a>
                      <a href="" class="minimize">&minus;</a>
                    </div> -->
                     <div class="form-group">
                            <div class="col-sm-6">
                              <select id="profiles" class="select2" required data-placeholder="Choose One">
                                <option value="0">CHOOSE PROFILE</option>
                              </select>
                            </div>
                       <button id="profilesearch" value="search" type="button" class="btn btn-primary">SEARCH</button>
                       <a class="btn btn-primary" id="export_device_wipe" href="/devicewipeexcel">EXPORT</a>
                      </div><!-- form-group -->
                  </div><!-- panel -->
          </div>
          <div class="panel panel-default">
                  <div class="panel-heading">
                    <!-- <div class="panel-btns">
                      <a href="" class="panel-close">&times;</a>
                      <a href="" class="minimize">&minus;</a>
                      </div> -->
                  <div class="panel-body">
                    <br />
                    <div class="table-responsive">
                      <table class="table" id="wipetable">
                        <thead>
                           <tr>
                           	<th id="group">Device Identifier</th>
                           	  <th>Serial Number</th>
                              <th>Wipe Description</th>
                              <th>Wipe Result</th>
                              <th>Reason</th>
                              <th>Additional Info</th>
                              <th>Wipe Initiated</th>
                              <th>Wipe Date</th>
                           </tr>
                        </thead>
                        <tbody>
                        </tbody>
                     </table>
                  </div><!-- panel-body -->
                </div><!-- panel -->
                </div>
                </div>
                </div>
@endsection