$(document).ready(function() {
    $('#devicetable').dataTable();
    $('#devicelogtable').dataTable({"aaSorting": []});
    $('#deviceapplicationtable').dataTable();
    $('.navigator').removeClass('active');
    $('#devices').addClass('active');
    getDevices();
    getActivatedDeviceList();
    var url = window.location.pathname;
    var serial = url.substring(url.lastIndexOf('/') + 1);
    getEachDevices(serial);
    getDeviceLogs(serial);
    getDeviceApplications(serial);
    getAllProfileList();
    $('#adddevice').click(function(){
        addDevices();
    });

    $('#exportToExcel').click(function(){
        var fromDate = $('.fromDate').val();
        var toDate = $('.toDate').val();
        if(fromDate && toDate){
            $('#exportToExcel').attr('href', '/deviceactivationexcel/'+fromDate+'/'+toDate);
        }else{
            $('#exportToExcel').attr('href', '/deviceactivationexcel');
        }
    });

    $('.fromDate').datepicker({
        format: 'dd-mm-yyyy'
    });
    $('.toDate').datepicker({
        format: 'dd-mm-yyyy'

     });



    $('#searchdevice').click(function(){
        var profile_id = $('#addProfile').val();
        var device_identifier = $('#deviceid').val();
        var device_serial_number = $('#serialno').val();
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            type: 'POST',
            url: '/searchdevicelist',
            data: {_token: CSRF_TOKEN, profile_id: profile_id, device_identifier:device_identifier, device_serial_number:device_serial_number},
            ataType: "json",
            cache: false,
            success: function (response) {
                var trHTML = '';
                var counter = 0;
                var username ='';
                var deviceOspClientVersion;
                var urlLink;
                var t = $('#devicetable').DataTable();
                t.clear().draw();
                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    counter ++;
                    deviceOspClientVersion = '';
                    if (item.device_osp_client_version){
                        deviceOspClientVersion = item.device_osp_client_version;
                    }
                    if($('#role_id').val() < 4) {
                        urlLink = '<a href="/devices/'+item.device_serial_number+'" id="device_'+item.id+'" serial="'+item.device_serial_number+'" class="device_view"><i class="glyphicon glyphicon-eye-open"></i></a><a href="" class="editor_remove" id="Device_settings_' + item.did + '" class="editor_remove" device_id="'+item.did+'" onClick="return confirmDeleteDevice('+item.did+')"><i class="glyphicon glyphicon-trash"></i>/'

                    }else if($('#role_id').val() == 5){
                        urlLink = '<a href="/devices/'+item.device_serial_number+'" id="device_'+item.id+'" serial="'+item.device_serial_number+'" class="device_view"><i class="glyphicon glyphicon-eye-open"></i></a>'
                    }
                    t.row.add( [
                        item.device_serial_number.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                        item.device_identifier.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                        deviceOspClientVersion.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                        item.profile_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                        item.device_profile_counter,
                        item.device_battery_level,
                        item.device_location,
                       // item.device_additional_info,
                        item.updated_at,
                        urlLink
                    ] ).draw( false );
                });
            }
        });
    });

$('#deviceSearch').click(function(){
    if($('.fromDate').val() == '' && $('.toDate').val() != ''){
            $('#filter_error').html('Please enter valid dates');
            $('#filter_error').show();
            return false;
        }else if($('.toDate').val() == '' && $('.fromDate').val() != ''){
            $('#filter_error').html('Please enter valid dates');
            $('#filter_error').show();
            return false;
        }else if($('.fromDate').val() == '' && $('.toDate').val() == ''){
            $('#filter_error').html('Please enter dates');
            $('#filter_error').show();
            return false;
        }else{
            getActivatedDeviceList();
            $('#filter_error').html('');
            $('#filter_error').hide();
        }

 });

    $('#addProfile').change(function(){
        if($(this).val() != 0){
            $('#deviceperror').html('');
            $('#deviceperror').hide();
            $('#devicemgtlink').attr('href','/exportdevice/'+$(this).val());
        }else{
            $('#devicemgtlink').attr('href','/exportdevice');
        }
    });

    $("form#devices").submit(function(e) {
        e.preventDefault();
        $('#deviceperror').html('');
        $('#deviceperror').hide();
        if($('#addProfile').val() == ''){
            $('#deviceperror').html('Please select profile');
            $('#deviceperror').show();
            return false;
        }
        var formData = new FormData($(this)[0]);
        // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/bulkuploaddevices",
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                getDevices($('#userprofileid').val());
                if(response == 'Upload done successfully...') {
                    $('#deviceperror').css('color', 'green');
                } else {
                    $('#deviceperror').css('color', 'red');
                }
                $('#deviceperror').show();
                $('#deviceperror').html(response);
            }
        });
    });
});

function addDevices() {
    $('#deviceperror').hide();
    $('#deviceperror').html('');
    $('#deviceid').removeClass('requirederror');
    $('#serialno').removeClass('requirederror');
    var isValid = true;
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'device_identifier=' + $('#deviceid').val() + '&device_serial_number=' + $('#serialno').val()+ '&profile_id=' + $('#addProfile').val() + '&_token=' + CSRF_TOKEN;
    if($('#addProfile').val() == ''){
        $('#deviceperror').show();
        $('#deviceperror').html('Please select profile');
        isValid = false;
    }
    if($('#deviceid').val() ==''){
        $('#deviceid').addClass('requirederror');
        isValid = false;
    }
    if($('#serialno').val() == ''){
        $('#serialno').addClass('requirederror');
        isValid = false;
    }
    if(isValid) {
        // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/devicecreate",
            data: dataString,
            datatype: 'json',
            cache: false,
            success: function (response) {
                $('#addProfile').removeClass('requirederror');
                if (response['authorisation']) {
                    $('#deviceperror').html(response['authorisation']);
                    $('#deviceperror').show();
                }
                setDevicePushNotifications($('#addProfile').val(), response, 'sync');
                $('#deviceid').val('');
                $('#serialno').val('');
                $('#addProfile').val('');
                getDevices();
            }
        });
    }
}


function getDevices() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/dashboardlist',
        data: {_token: CSRF_TOKEN},
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username ='';
            var deviceOspClientVersion;
            var urlLink;
            var t = $('#devicetable').DataTable();
            t.clear();
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                counter ++;
                deviceOspClientVersion = '';
                if (item.device_osp_client_version){
                    deviceOspClientVersion = item.device_osp_client_version;
                }
                if($('#role_id').val() < 4) {
                    urlLink = '<a href="/devices/'+item.device_serial_number+'" id="device_'+item.id+'" serial="'+item.device_serial_number+'" class="device_view"><i class="glyphicon glyphicon-eye-open"></i></a><a href="" class="editor_remove" id="Device_settings_' + item.did + '" class="editor_remove" device_id="'+item.did+'" onClick="return confirmDeleteDevice('+item.did+')"><i class="glyphicon glyphicon-trash"></i> /'

                }else if($('#role_id').val() == 5){
                    urlLink = '<a href="/devices/'+item.device_serial_number+'" id="device_'+item.id+'" serial="'+item.device_serial_number+'" class="device_view"><i class="glyphicon glyphicon-eye-open"></i></a>'
                }
                t.row.add( [
                    item.device_serial_number.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.device_identifier.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    deviceOspClientVersion.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.profile_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.device_profile_counter,
                    item.device_battery_level,
                    item.device_location,
                    item.updated_at,
                    urlLink
                ] ).draw( false );
            });
        }
    });
}

function confirmDeleteDevice(){

    if ( confirm( "Are you sure you want to delete this User?" ) ) {
        $('#devicetable').on( 'click', 'a.editor_remove', function (e) {
            e.preventDefault();
            var data = $(this).attr('device_id');
            deleteDevice(data);
        });
    } else {
        return false;
    }
}

function deleteDevice(data){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#devicetable').DataTable();
    $.ajax({
        type: 'POST',
        url: '/devicedelete',
        data: {_token: CSRF_TOKEN, deviceId: data},
        dataType: "json",
        cache: false,
        success: function (response) {
            if(response['authorisation']) {
                alert(response['authorisation']);
            } else {
                table
                    .row($('#Device_settings_' + data).parents('tr'))
                    .remove()
                    .draw();
            }
        }
    });
}

function getAllProfileList() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/profilelist',
        data: {_token: CSRF_TOKEN},
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username = '';
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                $('#addProfile').append($('<option>', {
                    value: item.id,
                    text : item.profile_name
                }));
            });

        }
    });
}

function configDelete(){
    if ( confirm( "Are you sure you want to delete this profile?" ) ) {
        $('#tableprofile').on( 'click', 'a.editor_remove', function (e) {
            e.preventDefault();
            var data = $(this).attr('profileId');
            deleteProfile(data);
        });
    }
}



function deleteProfile(data) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#tableprofile').DataTable();
    $.ajax({
        type: 'POST',
        url: '/profiledelete',
        data: {_token: CSRF_TOKEN, profileId: data},
        dataType: "json",
        cache: false,
        success: function (response) {
            table
                .row( $('#profile_'+data).parents('tr') )
                .remove()
                .draw();
            var posts = JSON.parse(JSON.stringify(response));
            if(posts){
                var message = posts+'  Deleted';
                    $('#profile-message').show();
                    $('#profile-message').html(message);
            }
        }
    });
}

function getEachDevices(serialNumber) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/devicelog',
        data: {_token: CSRF_TOKEN, serialNumber: serialNumber},
        dataType: "json",
        cache: false,
        success: function (response) {
            var result;
            for(result in response) {
               var data = response[result];
               $('#serialnumber').html(data.device_serial_number.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"));
               $('#devicename').html(data.device_identifier.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"));
               $('#devicestatus').html(data.provision_status.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"));
               if(data.updated_at) {
                   $('#lastseen').html(data.updated_at);
               } else {
                   $('#lastseen').html(data.created_at);
               }
               $('#deviceactivationdate').html(data.device_activation_date);
               $('#devicebattery').html(data.device_battery_level);
               $('#profilename').html('');
               if(data.user_profile.profile_name) {
                   $('#profilename').html(data.user_profile.profile_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"));
               }
               $('#deviceospversion').html('');
               if(data.device_osp_client_version) {
                   $('#deviceospversion').html(data.device_osp_client_version.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"));
               }
               $('#devicemodelnumber').html('');
               if(data.device_model_number) {
                   $('#devicemodelnumber').html(data.device_model_number.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"));
               }
               $('#devicemacaddress').html('');
               if(data.device_mac_address) {
                   $('#devicemacaddress').html(data.device_mac_address.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"));
               }
               $('#deviceipaddress').html('');
               if(data.device_ip_address) {
                   $('#deviceipaddress').html(data.device_ip_address.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"));
               }
               $('#devicelocation').html('');
               if(data.device_location) {
                  $('#devicelocation').html(data.device_location.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"));
               }
               $('#deviceos').html('');
               if(data.device_operating_system) {
                   $('#deviceos').html(data.device_operating_system.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"));
               }
               $('#devicegateway').html('');
               if(data.device_gateway_ip) {
                   $('#devicegateway').html(data.device_gateway_ip.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"));
               }
               $('#deviceprofilecounter').html('');
               if(data.device_profile_counter) {
                   $('#deviceprofilecounter').html(data.device_profile_counter);
               }
               $('#profileid').val(data.profile_id);
               $('#deviceid').val(data.id);
            }
        }
    });
}

/*function disableDeviceButtons(buttonid) {
    $(this).click(function () {
        $('.btn').attr("disabled", false);
        $("#"+buttonid).attr("disabled", true);
        setTimeout(function() {
            $("#"+buttonid).removeAttr("disabled");      
        }, 15000);
    });
}*/

function getDeviceLogs(serialNumber) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/devicetrace',
        data: {_token: CSRF_TOKEN, serialNumber: serialNumber},
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username ='';
            var t = $('#devicelogtable').DataTable();
            t.clear();
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                counter ++;
                t.row.add( [
                    item.created_at,
                    item.device_activity.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.activity_result.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    '',
                    ''
                    ] ).draw( false );
            });
        }
    });
}

function getDeviceApplications(serialNumber) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/deviceapplications',
        data: {_token: CSRF_TOKEN, serialNumber: serialNumber},
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username ='';
            var t = $('#deviceapplicationtable').DataTable();
            t.clear();
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                counter ++;
                t.row.add( [
                    item.device_application_package_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.device_application_version.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.created_at
                ] ).draw( false );
            });
        }
    });
}

function getActivatedDeviceList() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $('#deviceActivationBody').empty();
    $.ajax({
        type: 'POST',
        url: '/deviceactivationlist',
        data: {_token: CSRF_TOKEN, fromDate: $('.fromDate').val(), toDate: $('.toDate').val()},
        ataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username = '';
            var deviceOspClientVersion;
            var urlLink;
            var device_serial_number = '';
            var device_identifier = '';
            var profile_name = '';
            var device_profile_counter = '';
            var device_battery_level = '';
            var updated_at = '';
            var device_location = '';
            var device_additional_info = '';
            /*$.each(JSON.parse(JSON.stringify(response)), function (i, item) {

             if (item.device_serial_number != null) {
             device_serial_number = item.device_serial_number;
             }
             if (item.device_identifier != null) {
             device_identifier = item.device_identifier;
             }
             if (item.profile_name != null) {
             profile_name = item.profile_name;
             }
             if (item.device_profile_counter != null) {
             device_profile_counter = item.device_profile_counter;
             }
             if (item.device_battery_level != null) {
             device_battery_level = item.device_battery_level;
             }
             if (item.updated_at != null) {
             updated_at = item.updated_at;
             }
             if (item.device_location != null) {
             device_location = item.device_location;
             }
             if (item.device_additional_info != null) {
             device_additional_info = item.device_additional_info;
             }
             counter++;
             deviceOspClientVersion = '';
             if (item.device_osp_client_version) {
             deviceOspClientVersion = item.device_osp_client_version;
             }
             trHTML = '<tr><td>' + device_serial_number + '</td><td>' + device_identifier + '</td><td>' + deviceOspClientVersion + '</td><td>' + profile_name + '</td><td>' + device_profile_counter + '</td><td>' + device_battery_level + '</td><td>'+ device_location+'</td><td>' + device_additional_info + '</td><td>'+ updated_at +'</td><td></td></tr>';
             $('#deviceActivationListtable').append(trHTML);

             });

             $('#exportToExcel').attr('href', '/deviceactivationexcel/' + $('.fromDate').val() + '/' + $('.toDate').val());

             }*/
            var t = $('#deviceActivationListtable').DataTable();
            t.clear();
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                if (item.device_serial_number != null) {
                    device_serial_number = item.device_serial_number;
                }
                if (item.device_identifier != null) {
                    device_identifier = item.device_identifier;
                }
                if (item.profile_name != null) {
                    profile_name = item.profile_name;
                }
                if (item.device_profile_counter != null) {
                    device_profile_counter = item.device_profile_counter;
                }
                if (item.device_battery_level != null) {
                    device_battery_level = item.device_battery_level;
                }
                if (item.updated_at != null) {
                    updated_at = item.updated_at;
                }
                if (item.device_location != null) {
                    device_location = item.device_location;
                }
                if (item.device_additional_info != null) {
                    device_additional_info = item.device_additional_info;
                }
                counter++;
                deviceOspClientVersion = '';
                if (item.device_osp_client_version) {
                    deviceOspClientVersion = item.device_osp_client_version;
                }
                t.row.add([
                    item.device_serial_number.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                    item.device_identifier.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                    deviceOspClientVersion.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                    item.profile_name.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                    item.device_profile_counter,
                    item.device_battery_level,
                    item.device_location,
                    item.device_additional_info,
                    item.updated_at,
                    urlLink
                ]).draw(false);
            });
        }


    });
}

function setDevicePushNotifications(profileId, deviceId, command) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'profile_id='+ profileId + '&device_id='+ deviceId + '&command='+ command + '&_token='+CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/notification',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {

        }
    });
}


