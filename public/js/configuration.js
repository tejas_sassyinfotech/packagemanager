$(document).ready(function() {
    $('#tableprofile').dataTable();
    $('.navigator').removeClass('active');
    $('#configurations').addClass('active');
    getProfiles();
    $('#tableprofile').on( 'click', 'a.editor_copy', function (e) {
        e.preventDefault();
        var data = $(this).attr('profileid');
        configCopy(data);
    });

        $('#tableprofile').on( 'click', 'a.editor_remove', function (e) {
            e.preventDefault();
            if ( confirm( "Are you sure you want to delete this profile?" ) ) {
                var data = $(this).attr('profileId');
                deleteProfile(data);
            }else{
                return false;
            }
        });
});

function getProfiles() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/profilelist',
        data: {_token: CSRF_TOKEN},
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username ='';
            var t = $('#tableprofile').DataTable();
            t.clear();
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                var profile_type = '';
                var urlLink = '';
                if($('#role_id').val() < 4) {
                    urlLink = '<a href="" class="editor_copy" id="profile_'+item.id+'" profileid="'+item.id+'">  /  <i class="fa fa-clone"></i></a> <a href="" class="editor_remove" id="profile_'+item.id+'" profileid="'+item.id+'">  /  <i class="glyphicon glyphicon-trash"></i>  </a> <a href="/userprofile/'+item.profile_id+'" id="profile_'+item.id+'" profileid="'+item.id+'" class="editor_edit"><i class="fa fa-pencil"></i></a>'
                } else if($('#role_id').val() == 5) {
                    urlLink = '<a href="/userprofile/'+item.profile_id+'" id="profile_'+item.id+'" profileid="'+item.id+'" class="editor_view"><i class="fa fa-eye"></i></a>'
                }
                if(item.profile_type == 1){
                    profile_type = 'Android';
                }else if(item.profile_type == 2){
                    profile_type = 'IOS';
                }
                counter ++;
                t.row.add( [
                    item.profile_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.profile_counter,
                    profile_type,
                    item.updated_at,
                    urlLink
                ] ).draw( false );
            });
        }
    });
}

/*function configDelete(){
    if ( confirm( "Are you sure you want to delete this profile?" ) ) {
        $('#tableprofile').on( 'click', 'a.editor_remove', function (e) {
            e.preventDefault();
            var data = $(this).attr('profileid');
            deleteProfile(data);
        });
    } else {
        return false;
    }
}*/

function deleteProfile(data) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#tableprofile').DataTable();
    $.ajax({
        type: 'POST',
        url: '/profiledelete',
        data: {_token: CSRF_TOKEN, profileId: data},
        dataType: "json",
        cache: false,
        success: function (response) {
            if(response['authorisation']) {
                alert(response['authorisation']);
            } else {
                table
                    .row($('#profile_' + data).parents('tr'))
                    .remove()
                    .draw();
            }
        }
    });
}



function configCopy(data){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#tableprofile').DataTable();
    $.ajax({
        type: 'POST',
        url: '/profilecopy',
        data: {_token: CSRF_TOKEN, profileId: data},
        dataType: "json",
        cache: false,
        success: function (response) {
            if(response){
              getProfiles();
            }
        }
    });

}


