$(document).ready(function() {
    $('#err').hide();
    $('#success-message').html('');
    $('#applicationstable').dataTable();
    $('.navigator').removeClass('active');
    $('#applications').addClass('active');
    getApplications();
    $("form#applicationdetails").submit(function(e){
        e.preventDefault();
        $("#myBar").css("width", 0);
        $("#myBar").show();
        $('#err-message').hide();
        $('#err-message').html('');
        var formData = new FormData($(this)[0]);
        $.ajax({
            url: '/applicationcreate',
            type: 'POST',
            data: formData,
            success: function (data) {
                if(data['authorisation']) {
                    $('#err-message').show();
                    $('#err-message').html(data['authorisation']);
                } else {
                    var result;
                    if (data['lastId']) {
                        $('#err-message').hide();
                        upload();
                    } else {
                        for (result in data) {
                            var response = data[result];
                            $('#myBar').hide();
                            $('#err-message').show();
                            $('#err-message').html(response);
                        }
                    }
                    $('#package').val('');
                    $('#vcode').val('');
                    $('#_file').val('');
                }

            },
            cache: false,
            contentType: false,
            processData: false
    });

    return false;
    });
});

function getApplications() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/applicationlist',
        data: {_token: CSRF_TOKEN},
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username ='';
            var t = $('#applicationstable').DataTable();
            t.clear();
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                var urlLink = '';
                if($('#role_id').val() < 4 || $('#role_id').val() != 5) {
                    urlLink = ' <a href="" id="application_'+item.id+'" class="editor_remove" applicationId="'+item.id+'" onClick="return tableoperation()" ><i class="glyphicon glyphicon-trash"></i></a>';
                }
                counter ++;
                t.row.add( [
                    item.application_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.application_version.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.application_package.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.created_at,
                    urlLink
                ] ).draw( false );
            });
        }
    });
}

function tableoperation(){
    if ( confirm( "Are you sure you want to delete this Application?" ) ) {
        $('#applicationstable').on( 'click', 'a.editor_remove', function (e) {
            e.preventDefault();
            var data = $(this).attr('applicationId');
            deleteApplication(data);
        });
    } else {
        return false;
    }
}

function deleteApplication(data) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#applicationstable').DataTable();
    $.ajax({
        type: 'POST',
        url: '/applicationdelete',
        data: {_token: CSRF_TOKEN, applicationId: data},
        dataType: "json",
        cache: false,
        success: function (response) {
            if(response['authorisation']) {
                alert(response['authorisation']);
            } else {
                var posts = JSON.parse(JSON.stringify(response));
                table
                    .row($('#application_' + data).parents('tr'))
                    .remove()
                    .draw();
                if (posts) {
                    var message = posts + '  Deleted';
                    $('#del-message').show();
                    $('#del-message').html(message);
                }
            }
        }
    });
}

function addtotable(packageName, versionName) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var version = "";
    var dataString = 'package='+ packageName + '&version='+ versionName +'&_token='+CSRF_TOKEN;
    // AJAX Code To Submit Form.
    $.ajax({
        type: "POST",
        url: "/applicationcreate",
        data: dataString,
        datatype:'json',
        cache: false,
        success: function(response){
            $('#package').val('');
            $('#vcode').val('');
            getApplications();
        }
    });
    
}

function upload(){   
    var elem = document.getElementById("myBar");   
    var width = 1;
    var id = setInterval(frame, 80);
       function frame() {
         if (width >= 100) {
           clearInterval(id);
         } else {
           width++; 
           elem.style.width = width + '%'; 
         }
           if(width == 100){
               $('#success-message').html('Application uploaded successfully');
               getApplications();
           }
       }

   return;
      
    
}

