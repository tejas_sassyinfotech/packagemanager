$(document).ready(function() {
    var url = $(location).attr('href');
    var checkExt = /\d$/i.test(url);
    if (checkExt) {
        var profileId = url.substring(url.lastIndexOf('/') + 1);
        $('#_create').remove();
        populateprofilevalues(profileId);
    } else {
        getAllApplications();
    }
    getTabSetting();
    if($('#configiosHome #camera_use_allow_status').is(':checked')) {
        $('.face_time_allow_status').show();
        $('#configiosHome #face_time_allow_status').attr('checked', 'checked');
        $('#configiosHome #face_time_allow_status').val(1);
    }else{
        $('.face_time_allow_status').hide();
        $('#configiosHome #face_time_allow_status').removeAttr('checked', 'checked');
        $('#configiosHome #face_time_allow_status').val(0);
    }
    if($('#configiosHome #siri_allow_status').is(':checked')) {
        $('.device_locked_siri_allow_status').show();
        $('#configiosHome #device_locked_siri_allow_status').attr('checked', 'checked');
        $('#configiosHome #device_locked_siri_allow_status').val(1);
    }else{
        $('.device_locked_siri_allow_status').hide();
        $('#configiosHome #device_locked_siri_allow_status').removeAttr('checked', 'checked');
        $('#configiosHome #device_locked_siri_allow_status').val(0);
    }
    if($('#configiosHome #use_safari_allow_status').is(':checked')) {
        $('.use_safari_allow_status_div').show();
        $('#configiosHome #safari_enable_autofill_status').attr('checked', 'checked');
        $('#configiosHome #safari_enable_javascript_status').attr('checked', 'checked');
        $('#configiosHome #safari_block_popup_status').attr('checked', 'checked');
        $('#configiosHome #safari_enable_autofill_status').val(1);
        $('#configiosHome #safari_enable_javascript_status').val(1);
        $('#configiosHome #safari_block_popup_status').val(1);
    }else{
        $('.use_safari_allow_status_div').hide();
        $('#configiosHome #safari_enable_autofill_status').removeAttr('checked', 'checked');
        $('#configiosHome #safari_enable_javascript_status').removeAttr('checked', 'checked');
        $('#configiosHome #safari_block_popup_status').removeAttr('checked', 'checked');
        $('#configiosHome #safari_enable_autofill_status').val(0);
        $('#configiosHome #safari_enable_javascript_status').val(0);
        $('#configiosHome #safari_block_popup_status').val(0);
    }



    $('#configiosHome #camera_use_allow_status').click(function(){
       if($('#configiosHome #camera_use_allow_status').is(':checked')) {
           $('.face_time_allow_status').show();
           $('#configiosHome #face_time_allow_status').attr('checked', 'checked');
           $('#configiosHome #face_time_allow_status').val(1);
       }else{
           $('.face_time_allow_status').hide();
           $('#configiosHome #face_time_allow_status').removeAttr('checked', 'checked');
           $('#configiosHome #face_time_allow_status').val(0);
       }
    });
    $('#configiosHome #siri_allow_status').click(function(){
       if($('#configiosHome #siri_allow_status').is(':checked')) {
           $('.device_locked_siri_allow_status').show();
           $('#configiosHome #device_locked_siri_allow_status').attr('checked', 'checked');
           $('#configiosHome #device_locked_siri_allow_status').val(1);
       }else{
           $('.device_locked_siri_allow_status').hide();
           $('#configiosHome #device_locked_siri_allow_status').removeAttr('checked', 'checked');
           $('#configiosHome #device_locked_siri_allow_status').val(0);
       }
    });
    $('#configiosHome #use_safari_allow_status').click(function(){
       if($('#configiosHome #use_safari_allow_status').is(':checked')) {
           $('.use_safari_allow_status_div').show();
           $('#configiosHome #safari_enable_autofill_status').attr('checked', 'checked');
           $('#configiosHome #safari_enable_javascript_status').attr('checked', 'checked');
           $('#configiosHome #safari_block_popup_status').attr('checked', 'checked');
           $('#configiosHome #safari_enable_autofill_status').val(1);
           $('#configiosHome #safari_enable_javascript_status').val(1);
           $('#configiosHome #safari_block_popup_status').val(1);
       }else{
           $('.use_safari_allow_status_div').hide();
           $('#configiosHome #safari_enable_autofill_status').removeAttr('checked', 'checked');
           $('#configiosHome #safari_enable_javascript_status').removeAttr('checked', 'checked');
           $('#configiosHome #safari_block_popup_status').removeAttr('checked', 'checked');
           $('#configiosHome #safari_enable_autofill_status').val(0);
           $('#configiosHome #safari_enable_javascript_status').val(0);
           $('#configiosHome #safari_block_popup_status').val(0);
       }
    });

    $('#_profileerror').hide();
    $('.navigator').removeClass('active');
    $('#configurations').addClass('active');
    $('#wifitable').dataTable();
    $('#packagedisabletable').dataTable();
    $('#kiosktable').dataTable({"bSort" : false});
    $('#applicationstable').dataTable();
    $('#devicetable').dataTable();
    $('#_create').click(function(){
        if ($('#profilename').val() == '') {
            $('#_profileerror').html('Please enter profile name');
            $('#_profileerror').show();
        } else {
            $('#_profileerror').html('');
            $('#_profileerror').hide();
            addProfile();
        }
    });

    $('.saveWebFilerSettings').click(function(){
        var currentTab = $(this).attr('currentSelectedValue');
        var nextTo = $('.iosWebfilter').attr('next_to');
        UpdateWebfiler(currentTab,nextTo);
    });

    $('.ischecked').click(function(){
        if($(this).val() == 1){
            $(this).val(0);
        }
        else if($(this).val() == 0){
            $(this).val(1);
        }
    });

    $('.ios_webfiler_type').click(function(){
        var className = $(this).val();
        if(className == 1) {
            $('#blacklist_div').show();
            $('#whitelist_div').hide();
        }else if(className == 2) {
            $('#whitelist_div').show();
            $('#blacklist_div').hide();
        }
    });

    $('#credential_submit').click(function(e){
        e.preventDefault();
        var isValid = true;
        if($('#_fileCertificate').val() == ''){
            $('#err-msg').show();
            $('#err-msg').html('No file selected');
            isValid = false;
        }
        var formData = new FormData();
        formData.append('file',$('#_fileCertificate').prop('files')[0]);
        formData.append('profile_id',$('#userprofileid').val());
        formData.append('_token',$('meta[name="csrf-token"]').attr('content'));
        if(isValid) {
            $.ajax({
                url: '/certificatecreate',
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    var result;
                    var res;
                    if(data['lastId']){
                        $('#err').hide();
                        upload();
                        getCertificates($('#userprofileid').val());
                    } else {
                        for(result in data) {
                            var response = data[result];
                            $('#err').show();
                            $('#err').html(response);
                        }
                    }
                    $('#_fileCertificate').val('');
                }
            });
        }
    });

    $('#adminIOSSetting').click(function(){
       // var currentTab = $(this).attr('currenttab');
            updateTabSetting();
    });
    $('#adminAndroidSetting').click(function(){
            updateTabSetting();
    });

    $('#profile_type_admin').change(function(){
        if($(this).val() == 2){
            $('#adminsettings').hide();
            $('#adminIOSsettings').show();
        }else if($(this).val() == 1){
            $('#adminIOSsettings').hide();
            $('#adminsettings').show();
        }else{
            $('#adminIOSsettings').hide();
            $('#adminsettings').show();
        }
    });
    $('.savesettings').click(function(){
        var currentTab = $(this).attr('currenttab');
        var success = $(this).attr('success');
        var nextTo = $('.'+currentTab).attr('next_to');
        updateProfile(currentTab,nextTo,success);
    });
    $('.saveEmailsettings').click(function(e){
        var isValid = true;
        $(".emailRequired").each(function(){
            if (($(this).val())== ""){
                $(this).addClass("requirederror");
                isValid = false;
            }
        });
        if(isValid == true){
            $(this).removeClass("requirederror");
            var currentTab = $(this).attr('currenttab');
            var nextTo = $('.'+currentTab).attr('next_to');
            updateEmailTab(currentTab,nextTo);
        }

    });
    $('.saveactiveSyncsettings').click(function(e){
        e.preventDefault();
        var isValid = true;
        $(".syncRequired").each(function(){
            if (($(this).val())== ""){
                $(this).addClass("requirederror");
                isValid = false;
            }
        });
        if(isValid == true){
            $(this).removeClass("requirederror");
            var currentTab = $(this).attr('currenttab');
            var nextTo = $('.'+currentTab).attr('next_to');
            updateActiveSyncTab(currentTab,nextTo);
        }
    });

    $('.saveIOSsettings').click(function(e){
        var currentTab = $(this).attr('currenttab');
        var nextTo = $('.'+currentTab).attr('next_to');

        if(currentTab == 'iosHome'){
            updateIOShome(currentTab,nextTo);
        }
        else if(currentTab == 'iosActiveSyn') {
            var iosActiveSyn = true;
            $(".iosSyncRequired").each(function() {
                if (($(this).val()) == "") {
                    $(this).addClass("requirederror");
                    iosActiveSyn = false;
                }
            });
            if(iosActiveSyn == true){
                $(this).removeClass("requirederror");
                updateIOSActiveSyn(currentTab,nextTo);
            }
        }
        else if(currentTab == 'iosAdvancedrestriction') {
            updateIOSAdvancedrestriction(currentTab,nextTo);

        }
        else if(currentTab == 'iosApn') {
            var iosApn = true;
            $(".apnRequired").each(function() {
                if (($(this).val()) == "") {
                    $(this).addClass("requirederror");
                    iosApn = false;
                }
            });
            if(iosApn == true) {
                $(this).removeClass("requirederror");
                updateIOSApn(currentTab, nextTo);
            }

        }
        else if(currentTab == 'iosCredentials') {
            updateIOSCredentials(currentTab,nextTo);

        }
        else if(currentTab == 'iosEmailsettings') {
                var iosEmailCheck = true;
                $(".iosemailRequired").each(function() {
                    if (($(this).val()) == "") {
                        $(this).addClass("requirederror");
                        iosEmailCheck = false;
                    }
                });
            if(iosEmailCheck == true){
                $(this).removeClass("requirederror");
                updateIOSEmailsettings(currentTab,nextTo);

            }
        }
        else if(currentTab == 'iosKiosksettings') {
            var iosKiosksettings = true;
            $(".kiosk_required").each(function() {
                if (($(this).val()) == "") {
                    $(this).addClass("requirederror");
                    iosKiosksettings = false;
                }
            });
            if(iosKiosksettings == true){
                $(this).removeClass("requirederror");
                updateIOSKiosksettings(currentTab,nextTo);
            }
        }
        else if(currentTab == 'iosLdap') {
            var iosLdap = true;
            $(".lDap_required").each(function() {
                if (($(this).val()) == "") {
                    $(this).addClass("requirederror");
                    iosLdap = false;
                }
            });
            if(iosLdap == true){
                $(this).removeClass("requirederror");
                updateIOSLdap(currentTab,nextTo);
            }


        }
        else if(currentTab == 'iosPassword') {
            var iosPassword = true;
            $(".password_required").each(function() {
                if (($(this).val()) == "") {
                    $(this).addClass("requirederror");
                    iosPassword = false;
                }
            });
            if(iosPassword == true){
                $(this).removeClass("requirederror");
                updateIOSPassword(currentTab,nextTo);
            }

        }
        else if(currentTab == 'iosVpn') {
            var iosVpn = true;
            $(".vpn_required").each(function() {
                if (($(this).val()) == "") {
                    $(this).addClass("requirederror");
                    iosVpn = false;
                }
            });
            if(iosVpn == true){
                $(this).removeClass("requirederror");
                updateIOSVpn(currentTab,nextTo);

            }
        }
        else if(currentTab == 'iosWifi') {
            var iosWifi = true;
            $(".ios_wifi_required").each(function() {
                if (($(this).val()) == "") {
                    $(this).addClass("requirederror");
                    iosWifi = false;
                }
            });
            if(iosWifi == true) {
                $(this).removeClass("requirederror");
                updateIOSWifi(currentTab, nextTo);
            }
        }
        else if(currentTab == 'iosCardDAV') {
            var iosCardDAV = true;
            $(".dav_required").each(function() {
                if (($(this).val()) == "") {
                    $(this).addClass("requirederror");
                    iosCardDAV = false;
                }
            });
            if(iosCardDAV == true){
                $(this).removeClass("requirederror");
                updateIOSCardDAV(currentTab,nextTo);
            }
        }
        else if(currentTab == 'iosWebclip') {
            var iosWebclip = true;
            $(".webclip_required").each(function() {
                if (($(this).val()) == "") {
                    $(this).addClass("requirederror");
                    iosWebclip = false;
                }
            });
            var ext = $('#webclip_icon').val().split('.').pop().toLowerCase();
            if($.inArray(ext, ['png','jpg','jpeg','JPG','JPEG','PNG']) == -1) {
                $('.invalid_icon').removeClass('hidden');
                iosWebclip = false;
            }

            if(iosWebclip == true) {
                $(this).removeClass("requirederror");
                $('.invalid_icon').addClass('hidden');
                updateIOSWebclip(currentTab,nextTo);
            }
        }
        else{
            $('.iosHome').click();
        }
    });



    $('#adddevice').click(function(){
        addDevices();
    });
    $('#addWifi').click(function(){
        addWifiDetails();
    })
    $('#addKiosk').click(function(){
        addKioskDetails();
    });
    $('#assignapp').click(function() {
        assignProfileApplications($('#addapplications').val());
    });
    $('#disableapp').click(function() {
        addDisabledApplications();
    });
    $('#addKioskSettings').click(function() {
        addProfileKioskSettings();
    });
    // Disabled Application bulk upload
    $("form#bulkupload").submit(function(e) {
        e.preventDefault();
        $('#disableapperror').html('');
        $('#disableapperror').hide();
        var formData = new FormData($(this)[0]);
        // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/bulkuploaddisableapps",
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                getDisabledApplications($('#userprofileid').val());
                if(response == 'Upload done successfully...') {
                    $('#disableapperror').css('color', 'green');
                } else {
                    $('#disableapperror').css('color', 'red');
                }
                $('#disableapperror').show();
                $('#disableapperror').html(response);
            }
        });
    });
    // Devices bulk upload
    $("form#devices").submit(function(e) {
        e.preventDefault();
        $('#deviceperror').html('');
        $('#deviceperror').hide();
        var formData = new FormData($(this)[0]);
        // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/bulkuploaddevices",
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                getDevices($('#userprofileid').val());
                if(response == 'Upload done successfully...') {
                    $('#deviceperror').css('color', 'green');
                } else {
                    $('#deviceperror').css('color', 'red');
                }
                $('#deviceperror').show();
                $('#deviceperror').html(response);
            }
        });
    });
    $("form#config").submit(function(e){
        e.preventDefault();
        $("#myBar-profile").css("width", 0);
        var formData = new FormData($(this)[0]);
        $.ajax({
            url: '/applicationcreate',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                var result;
                var res;
                if(data['lastId']){
                    $('#err').hide();
                    upload();
                    for(res in data) {
                        var appId = data[res];
                    }
                    assignProfileApplications(appId);
                } else {
                    for(result in data) {
                        var response = data[result];
                        $('#err').show();
                        $('#err').html(response);
                    }
                }
                $('#package').val('');
                $('#vcode').val('');
                $('#_file').val('');
            }
        });

        return false;

    });

    $('#_apply').click(function(){
        applyProfile();
    });

    $('#_editProfile').click(function(){
        editProfileName();
    });

    $('#profile_type').change(function(){
        if($('#tabShow').val() == 1){
            if($(this).val() == 2){
                $('#settings').hide();
                $('#IOSsettings').show();
            }else if($(this).val() == 1){
                $('#IOSsettings').hide();
                $('#settings').show();
            }else{
                $('#IOSsettings').hide();
                $('#settings').show();
            }
        }
    });
});

function addProfile() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'profile_name='+ $('#profilename').val() +'&profile_type='+ $('#profile_type').val() +'&_token='+CSRF_TOKEN;
    // AJAX Code To Submit Form.
    $.ajax({
        type: "POST",
        url: "/profilecreate",
        data: dataString,
        datatype:'json',
        cache: false,
        success: function(response){
            $('#_profileerror').hide('');
            $('#_profilesuccess').hide('');
            if(response['authorisation']) {
                $('#_profileerror').html(response['authorisation']);
                $('#_profileerror').show();

            }else if(response['profileExist']) {
                $('#_profileerror').html(response['profileExist']);
                $('#_profileerror').show();

            }else {
                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    $('#userprofileid').val(item.id);
                    $('#profileid').val(item.profile_id);
                });
                $('#tabShow').val(1);
                if($('#profile_type').val() == 1){
                    $('#settings').show();
                }else if($('#profile_type').val() == 2) {
                    $('#IOSsettings').show();
                }else{
                    $('#settings').show();
                }
                $('#settingsapply').show();
                $('#_profilesuccess').html('Profile created successfully');
                $('#_profilesuccess').show();
            }
        }
    });
}

function updateProfile(currentTab,nextTo,sucess) {
    $('#'+sucess).html('');
    $('#'+sucess).hide();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = '';
    var mdm_display_status = 0;
    var status_bar_status = 0;
    var home_key_status = 0;
    var back_key_status = 0;
    var recents_key_status = 0;
    var power_key_status = 0;
    var firmware_upgrade_status = 0;
    var factory_reset_status = 0;
    var s_voice_status = 0;
    var android_settings_status = 0;
    var volume_key_status = 0;
    var multi_window_status = 0;
    var notifications_status = 0;
    var lock_screen_status = 0;
    var bluetooth_status = 0;
    var gps_status = 0;
    var developer_mode_menu_status = 0;
    var backup_reset_menu_status = 0;
    var airplane_mode_menu_status = 0;
    var language_menu_status = 0;
    var multi_window_menu_status = 0;
    var users_settings_menu_status = 0;
    var bluetooth_settings_menu_status = 0;
    var lockscreen_menu_status = 0;
    var wifi_settings_menu_status = 0;
    var power_on_boot_status = 0;
    var disable_all_bloatware_status = 0;
    var unknown_source_status = 0;
    var developer_mode_status = 0;
    var screenshot_status = 0;
    var usb_debugging_status = 0;
    var lte_mode_status = 0;
    var device_timeout = $('#config #device_timeout').val();
    var pull_interval = $('#config #pull_interval').val();
    var brightness_percentage = $('#config #brightness_percentage').val();
    var volume_percentage = $('#config #volume_percentage').val();
    var sleep_percentage = $('#config #sleep_percentage').val();
    var deep_sleep_percentage = $('#config #deep_sleep_percentage').val();
    var orientation = $('#config #orientation').val();
    var proxy = $('#config #proxy').val();
    var port = $('#config #port').val();
    var kiosk_type = $('#config #kiosk_type').val();
    var kiosk_password = $('#config #kiosk_password').val();
    var single_app_kiosk = $('#config #single_app_kiosk').val();
    var browser_kiosk = $('#config #browser_kiosk').val();
    var registration_password = $('#config #registration_password').val();
    var advanced_command = $('#config #advanced_command').val();

    if($('#config #mdm_display_status').is(':checked')) {
        mdm_display_status = 1;
    }
    if($('#config #status_bar_status').is(':checked')) {
        status_bar_status = 1;
    }
    if($('#config #home_key_status').is(':checked')) {
        home_key_status = 1;
    }
    if($('#config #back_key_status').is(':checked')) {
        back_key_status = 1;
    }
    if($('#config #recents_key_status').is(':checked')) {
        recents_key_status = 1;
    }
    if($('#config #power_key_status').is(':checked')) {
        power_key_status = 1;
    }
    if($('#config #firmware_upgrade_status').is(':checked')) {
        firmware_upgrade_status = 1;
    }
    if($('#config #factory_reset_status').is(':checked')) {
        factory_reset_status = 1;
    }
    if($('#config #s_voice_status').is(':checked')) {
        s_voice_status = 1;
    }
    if($('#config #android_settings_status').is(':checked')) {
        android_settings_status = 1;
    }
    if($('#config #volume_key_status').is(':checked')) {
        volume_key_status = 1;
    }
    if($('#config #multi_window_status').is(':checked')) {
        multi_window_status = 1;
    }
    if($('#config #notifications_status').is(':checked')) {
        notifications_status = 1;
    }
    if($('#config #lock_screen_status').is(':checked')) {
        lock_screen_status = 1;
    }
    if($('#config #bluetooth_status').is(':checked')) {
        bluetooth_status = 1;
    }
    if($('#wifi_status').is(':checked')) {
        wifi_status = 1;
    }
    if($('#gps_status').is(':checked')) {
        gps_status = 1;
    }
    if($('#config #developer_mode_menu_status').is(':checked')) {
        developer_mode_menu_status = 1;
    }
    if($('#config #backup_reset_menu_status').is(':checked')) {
        backup_reset_menu_status = 1;
    }
    if($('#config #airplane_mode_menu_status').is(':checked')) {
        airplane_mode_menu_status = 1;
    }
    if($('#config #language_menu_status').is(':checked')) {
        language_menu_status = 1;
    }
    if($('#config #multi_window_menu_status').is(':checked')) {
        multi_window_menu_status = 1;
    }
    if($('#config #users_settings_menu_status').is(':checked')) {
        users_settings_menu_status = 1;
    }
    if($('#config #bluetooth_settings_menu_status').is(':checked')) {
        bluetooth_settings_menu_status = 1;
    }
    if($('#config #lockscreen_menu_status').is(':checked')) {
        lockscreen_menu_status = 1;
    }
    if($('#config #wifi_settings_menu_status').is(':checked')) {
        wifi_settings_menu_status = 1;
    }
    if($('#config #power_on_boot_status').is(':checked')) {
        power_on_boot_status = 1;
    }
    if($('#config #disable_all_bloatware_status').is(':checked')) {
        disable_all_bloatware_status = 1;
    }
    if($('#config #unknown_source_status').is(':checked')) {
        unknown_source_status = 1;
    }
    if($('#config #developer_mode_status').is(':checked')) {
        developer_mode_status = 1;
    }
    if($('#config #screenshot_status').is(':checked')) {
        screenshot_status = 1;
    }
    if($('#config #usb_debugging_status').is(':checked')) {
        usb_debugging_status = 1;
    }
    if($('#config #lte_mode_status').is(':checked')) {
        lte_mode_status = 1;
    }
    dataString = 'profile_id='+$('#userprofileid').val()+'&id='+ $('#profileid').val() +'&mdm_display_status='+mdm_display_status+
    '&status_bar_status='+status_bar_status+'&home_key_status='+home_key_status+'&back_key_status='+back_key_status+'&recents_key_status='+recents_key_status+'&power_key_status='+power_key_status+
    '&firmware_upgrade_status='+firmware_upgrade_status+'&factory_reset_status='+factory_reset_status+'&s_voice_status='+s_voice_status+'&android_settings_status='+android_settings_status+
    '&volume_key_status='+volume_key_status+'&multi_window_status='+multi_window_status+'&notifications_status='+notifications_status+'&lock_screen_status='+lock_screen_status+'&bluetooth_status='+bluetooth_status+'&wifi_status='+wifi_status+
    '&gps_status='+gps_status+'&developer_mode_menu_status='+developer_mode_menu_status+'&backup_reset_menu_status='+backup_reset_menu_status+'&airplane_mode_menu_status='+airplane_mode_menu_status+'&language_menu_status='+language_menu_status+
    '&multi_window_menu_status='+multi_window_menu_status+'&users_settings_menu_status='+users_settings_menu_status+'&bluetooth_settings_menu_status='+bluetooth_settings_menu_status+'&lockscreen_menu_status='+lockscreen_menu_status+
    '&wifi_settings_menu_status='+wifi_settings_menu_status+'&power_on_boot_status='+power_on_boot_status+'&disable_all_bloatware_status='+disable_all_bloatware_status+'&unknown_source_status='+unknown_source_status+'&developer_mode_status='+developer_mode_status+
    '&screenshot_status='+screenshot_status+'&usb_debugging_status='+usb_debugging_status+'&lte_mode_status='+lte_mode_status+'&device_timeout='+device_timeout+'&pull_interval='+pull_interval+'&brightness_percentage='+brightness_percentage+
    '&volume_percentage='+volume_percentage+'&sleep_percentage='+sleep_percentage+'&deep_sleep_percentage='+deep_sleep_percentage+'&orientation='+orientation+'&global_proxy='+proxy+'&global_port='+port+'&kiosk_type='+kiosk_type+
    '&kiosk_password='+kiosk_password+'&single_app_kiosk='+single_app_kiosk+'&browser_kiosk='+browser_kiosk+'&registration_password='+registration_password+'&advanced_command='+advanced_command+'&_token='+CSRF_TOKEN;
    // AJAX Code To Submit Form.
    $.ajax({
        type: "POST",
        url: "/profileupdate",
        data: dataString,
        datatype:'json',
        cache: false,
        success: function(response){
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                $('#userprofileid').val(item.id);
                $('#profileid').val(item.profile_id);
            });
            if(currentTab != nextTo) {
                $('.'+nextTo).click();
            } else {
                $('.home').click();
            }

        }
    });
}

function getAllApplications() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/applicationlist',
        data: {_token: CSRF_TOKEN},
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username = '';
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                $('#addapplications').append($('<option>', {
                    value: item.id,
                    text : item.application_name
                }));
            });

        }
    });
}
function getProfileApplicationsToSelect(profileId) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'profile_id='+profileId+'&_token='+CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/allapplicationlist',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username = '';
            $('#addapplications option[value!="0"]').remove();
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                $('#addapplications').append($('<option>', {
                    value: item.id,
                    text : item.application_name
                }));

            });
        }
    });
}

function populateprofilevalues(profileId) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = '';
    dataString = 'profileId='+profileId+'&_token='+CSRF_TOKEN;
    $.ajax({
        type: "POST",
        url: "/populateprofile",
        data: dataString,
        datatype:'json',
        cache: false,
        success: function(response){
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                $('#userprofileid').val(item.id);
                $('.profileidpersist').val(item.id);
                $('#profileid').val(item.profile_id);
                $('#profilename').val(item.profile_name);
                $('#profile_type').val(item.profile_type);
                if(item.profile_type == 1){
                    $('#disableapplink').attr('href', '/exportdisableaplication/'+item.id);
                    $('#devicemgtlink').attr('href', '/exportdevice/'+item.id);
                    if(item.mdm_display_status == 1) {
                        $('#config #mdm_display_status').attr('checked', 'checked');
                    }
                    if(item.status_bar_status == 1) {
                        $('#config #status_bar_status').attr('checked', 'checked');
                    }
                    if(item.home_key_status == 1) {
                        $('#config #home_key_status').attr('checked', 'checked');
                    }
                    if(item.back_key_status == 1) {
                        $('#config #back_key_status').attr('checked', 'checked');
                    }
                    if(item.recents_key_status == 1) {
                        $('#config #recents_key_status').attr('checked', 'checked');
                    }
                    if(item.power_key_status == 1) {
                        $('#config #power_key_status').attr('checked', 'checked');
                    }
                    if(item.firmware_upgrade_status == 1) {
                        $('#config #firmware_upgrade_status').attr('checked', 'checked');
                    }
                    if(item.factory_reset_status == 1) {
                        $('#config #factory_reset_status').attr('checked', 'checked');
                    }
                    if(item.s_voice_status == 1) {
                        $('#config #s_voice_status').attr('checked', 'checked');
                    }
                    if(item.android_settings_status == 1) {
                        $('#config #android_settings_status').attr('checked', 'checked');
                    }
                    if(item.volume_key_status == 1) {
                        $('#config #volume_key_status').attr('checked', 'checked');
                    }
                    if(item.multi_window_status == 1) {
                        $('#config #multi_window_status').attr('checked', 'checked');
                    }
                    if(item.notifications_status == 1) {
                        $('#config #notifications_status').attr('checked', 'checked');
                    }
                    if(item.lock_screen_status == 1) {
                        $('#config #lock_screen_status').attr('checked', 'checked');
                    }
                    if(item.bluetooth_status == 1) {
                        $('#config #bluetooth_status').attr('checked', 'checked');
                    }
                    if(item.gps_status == 1) {
                        $('#config #gps_status').attr('checked', 'checked');
                    }
                    if(item.developer_mode_menu_status == 1) {
                        $('#config #developer_mode_menu_status').attr('checked', 'checked');
                    }
                    if(item.backup_reset_menu_status == 1) {
                        $('#config #backup_reset_menu_status').attr('checked', 'checked');
                    }
                    if(item.airplane_mode_menu_status == 1) {
                        $('#config #airplane_mode_menu_status').attr('checked', 'checked');
                    }
                    if(item.language_menu_status == 1) {
                        $('#config #language_menu_status').attr('checked', 'checked');
                    }
                    if(item.multi_window_menu_status == 1) {
                        $('#config #multi_window_menu_status').attr('checked', 'checked');
                    }
                    if(item.users_settings_menu_status == 1) {
                        $('#config #users_settings_menu_status').attr('checked', 'checked');
                    }
                    if(item.bluetooth_settings_menu_status == 1) {
                        $('#config #bluetooth_settings_menu_status').attr('checked', 'checked');
                    }
                    if(item.lockscreen_menu_status == 1) {
                        $('#config #lockscreen_menu_status').attr('checked', 'checked');
                    }
                    if(item.wifi_settings_menu_status == 1) {
                        $('#config #wifi_settings_menu_status').attr('checked', 'checked');
                    }
                    if(item.power_on_boot_status == 1) {
                        $('#config #power_on_boot_status').attr('checked', 'checked');
                    }
                    if(item.disable_all_bloatware_status == 1) {
                        $('#config #disable_all_bloatware_status').attr('checked', 'checked');
                    }
                    if(item.unknown_source_status == 1) {
                        $('#config #unknown_source_status').attr('checked', 'checked');
                    }
                    if(item.developer_mode_status == 1) {
                        $('#config #developer_mode_status').attr('checked', 'checked');
                    }
                    if(item.screenshot_status == 1) {
                        $('#config #screenshot_status').attr('checked', 'checked');
                    }
                    if(item.usb_debugging_status == 1) {
                        $('#config #usb_debugging_status').attr('checked', 'checked');
                    }
                    if(item.lte_mode_status == 1) {
                        $('#config #lte_mode_status').attr('checked', 'checked');
                    }
                    $('#config #device_timeout').val(item.device_timeout);
                    $('#config #pull_interval').val(item.pull_interval);
                    $('#config #brightness_percentage').val(item.brightness_percentage);
                    $('#config #volume_percentage').val(item.volume_percentage);
                    $('#config #sleep_percentage').val(item.sleep_percentage);
                    $('#config #deep_sleep_percentage').val(item.deep_sleep_percentage);
                    $('#config #orientation').val(item.orientation);
                    $('#config #proxy').val(item.global_proxy);
                    $('#config #port').val(item.global_port);
                    $('#config #kiosk_type').val(item.kiosk_type);
                    $('#config #kiosk_password').val(item.kiosk_password);
                    $('#config #single_app_kiosk').val(item.single_app_kiosk);
                    $('#config #browser_kiosk').val(item.browser_kiosk);
                    $('#config #registration_password').val(item.registration_password);
                    $('#config #advanced_command').val(item.advanced_command);
                    getDevices(item.id);
                    getWifiDetails(item.id);
                    getKioskDetails(item.id);
                    getProfileApplications(item.id);
                    getDisabledApplications(item.id);
                    getProfileApplicationsToSelect(item.id);
                    getDashboardKioskSettings(item.id);
                    getDashboardKioskSettings(item.id);
                    getProfileEmailDetails(item.id);
                    getProfileActiveSyncDetails(item.id);
                    $('#settings').show();
                } else if(item.profile_type == 2){
                    getIOSsettings(item.id);
                    getProfileActiveSyncDetails(item.id);
                    getProfileEmailDetails(item.id);
                    getProfileldapDetails(item.id);
                    getProfileVpnDetails(item.id);
                    getProfileDavDetails(item.id);
                    getAdvancedRestrictionSetting(item.id);
                    getProfileApnDetails(item.id);
                    getProfileKioskDetails(item.id);
                    getProfileWebclipDetails(item.id);
                    getProfileWifiDetails(item.id);
                    getCertificates(item.id);
                    getProfilePasswordDetails(item.id);
                    getWebfilerUrlList(2,'whiteList',item.id,0);
                    getWebfilerUrlList(1,'blackList',item.id,0);

                    $('#IOSsettings').show();
                }else{
                    $('#settings').show();
                }
                $('#settingsapply').show();
            });
        }
    });
}

function addDevices() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'device_identifier=' + $('#deviceid').val() + '&device_serial_number=' + $('#serialno').val() + '&device_additional_info=' + $('#additionalinfo').val() + '&device_user_name=' + $('#deviceusername').val() + '&device_password=' + $('#devicepassword').val() + '&profile_id=' + $('#userprofileid').val() + '&_token=' + CSRF_TOKEN;
    $('#deviceperror').html('');
    $('#deviceperror').hide();
    if($('#deviceid').val() !='' && $('#serialno').val() != '') {
        // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/devicecreate",
            data: dataString,
            datatype: 'json',
            cache: false,
            success: function (response) {
                if (response['authorisation']) {
                    $('#deviceperror').html(response['authorisation']);
                    $('#deviceperror').show();
                }
                $('#deviceid').val('');
                $('#serialno').val('');
                $('#additionalinfo').val('');
                $('#deviceusername').val('');
                $('#devicepassword').val('');
                getDevices($('#userprofileid').val());
            }
        });
    }
}

function getDevices(profile_id) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'profileId=' + profile_id + '&_token=' + CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/deviceget',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username = '';
            var t = $('#devicetable').DataTable();
            t.clear();
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                var urlLink = '';
                var additionalInfo = '';
                var additionalInfoRaw = '';
                if($('#role_id').val() < 4) {
                    urlLink = ' <a href="" class="editor_remove" id="profile_device_' + item.id + '" deviceId="'+item.did+'" onClick="return deviceDeleteConf()"><i class="glyphicon glyphicon-trash"></i>';
                }
                counter++;
                if(item.device_additional_info) {
                    additionalInfoRaw = item.device_additional_info.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&");
                    if(additionalInfoRaw.length > 20) {
                        additionalInfo = additionalInfoRaw.substr(0, 16);
                        additionalInfo = additionalInfo +'...';
                    } else {
                        additionalInfo = additionalInfoRaw;
                    }

                }
                t.row.add([
                    item.device_identifier.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.device_serial_number.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    additionalInfo,
                    urlLink
                ]).draw(false);
            });
        }
    });
}
function deviceDeleteConf(){
    if ( confirm( "Are you sure you want to delete?" ) ) {
        $('#devicetable').on( 'click', 'a.editor_remove', function (e) {
            e.preventDefault();
            var data = $(this).attr('deviceId');
            deleteDevice(data);
        });
    } else {
        return false;
    }
}

function deleteDevice(data) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#devicetable').DataTable();
    $.ajax({
        type: 'POST',
        url: '/devicedelete',
        data: {_token: CSRF_TOKEN, deviceId: data},
        dataType: "json",
        cache: false,
        success: function (response) {
            if(response['authorisation']) {
                alert(response['authorisation']);
            } else {
                setDevicePushNotifications($('#userprofileid').val(), data, 'devicedeleted');
                table
                    .row($('#profile_device_' + data).parents('tr'))
                    .remove()
                    .draw();
            }
        }
    });
}

function addWifiDetails() {
    $('#_wifierror').html('');
    $('#_wifierror').hide();
    $('#_wifisuccess').html('');
    $('#_wifisuccess').hide();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'ssid=' + $('#ssid').val() + '&wifi_user_name=' + $('#wifiusername').val() + '&wifi_user_password=' + $('#wifipassword').val() + '&eap_method=' + $('#eapmethod').val() + '&phase_2_authentication=' + $('#phase2auth').val() + '&profile_id=' + $('#userprofileid').val() + '&_token=' + CSRF_TOKEN;
    // AJAX Code To Submit Form.
    $.ajax({
        type: "POST",
        url: "/profilewificreate",
        data: dataString,
        datatype: 'json',
        cache: false,
        success: function (response) {
            if(response['authorisation']) {
                $('#_wifierror').html(response['authorisation']);
                $('#_wifierror').show();
            }
            if(response['save_success']) {
                $('#_wifisuccess').html(response['save_success']);
                $('#_wifisuccess').show();
            }

            $('#ssid').val('');
            $('#wifiusername').val('');
            $('#wifipassword').val('');
            $('#eapmethod').val('Open');
            $('#phase2auth').val('NONE');
            getWifiDetails($('#userprofileid').val());
        }
    });
}

function getWifiDetails(profile_id) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'profileId=' + profile_id + '&profile_type=' + $('#profile_type').val() + '&_token=' + CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/profilewifiget',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username = '';
            var t = $('#wifitable').DataTable();
            t.clear();
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                var urlLink = '';
                if($('#role_id').val() < 4) {
                    urlLink = ' <a href="" class="editor_remove" id="wifi_settings_' + item.id + '" wifiId="'+item.id+'" onClick="return wifiDeleteConf()"><i class="glyphicon glyphicon-trash"></i>';
                }
                counter++;
                t.row.add([
                    item.ssid.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.wifi_user_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.eap_method.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.phase_2_authentication.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    urlLink
                ]).draw(false);
            });
        }
    });
}

function wifiDeleteConf(){
    if ( confirm( "Are you sure you want to delete this Wifi settings?" ) ) {
        $('#wifitable').on( 'click', 'a.editor_remove', function (e) {
            e.preventDefault();
            var data = $(this).attr('wifiId');
            deleteWifi(data);
        });
    } else {
        return false;
    }
}

function deleteWifi(data) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#wifitable').DataTable();
    $.ajax({
        type: 'POST',
        url: '/profilewifidelete',
        data: {_token: CSRF_TOKEN, wifiId: data},
        dataType: "json",
        cache: false,
        success: function (response) {
            if(response['authorisation']) {
                alert(response['authorisation']);
            } else {
                table
                    .row($('#wifi_settings_' + data).parents('tr'))
                    .remove()
                    .draw();
            }
        }
    });
}

function addKioskDetails() {

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'kiosk_name=' + $('#kioskdashboard').val() + '&kiosk_browser_type=' + $('#kioskbrowser').val() + '&kiosk_screen_number=' + $('#screennumber').val() + '&kiosk_special_screen_number=' + $('#splscreennumber').val() + '&kiosk_special_screen_column_number=' + $('#numberofcolumns').val() +'&kiosk_browser_priority=' + $('#kioskpriority').val() + '&kiosk_browser_row=' + $('#kioskrow').val() + '&kiosk_browser_column=' + $('#kioskcolumn').val() + '&profile_id=' + $('#userprofileid').val() + '&_token=' + CSRF_TOKEN;
    if($('#kioskdashboard').val()) {
        // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/profilekioskcreate",
            data: dataString,
            datatype: 'json',
            cache: false,
            success: function (response) {
                $('#_kiosksuccess').html('');
                $('#_kiosksuccess').hide();
                if(response['save_success']){
                    $('#_kioskaddsuccess').html(response['save_success']);
                    $('#_kioskaddsuccess').show();
                }
                $('#kioskdashboard').val('');
                $('#kioskbrowser').val(2);
                $('#kioskpriority').val('');
                $('#kioskrow').val('');
                $('#kioskcolumn').val('');
                $('#kioskdashboard').removeClass('requirederror');
                getKioskDetails($('#userprofileid').val());
            }
        });
    }else{
        $('#kioskdashboard').addClass("requirederror");
    }
}

function addProfileKioskSettings() {
    $('#_kioskaddsuccess').html('');
    $('#_kioskaddsuccess').hide();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = '&kiosk_screen_number=' + $('#screennumber').val() + '&kiosk_special_screen_number=' + $('#splscreennumber').val() + '&kiosk_special_screen_column_number=' + $('#numberofcolumns').val() + '&profile_id=' + $('#userprofileid').val() + '&_token=' + CSRF_TOKEN;
    // AJAX Code To Submit Form.
    $.ajax({
        type: "POST",
        url: "/profilekiosksetting",
        data: dataString,
        datatype: 'json',
        cache: false,
        success: function (response) {
            if(response['save_success']){
                $('#_kiosksuccess').html(response['save_success']);
                $('#_kiosksuccess').show();
            }
        }
    });
}

function getKioskDetails(profile_id) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'profileId=' + profile_id +'&profile_type='+ $('#profile_type').val()+ '&_token=' + CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/profilekioskget',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username = '';
            var t = $('#kiosktable').DataTable();
            t.clear();
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                var urlLink = '';
                if($('#role_id').val() < 4) {
                    urlLink = ' <a href="" class="editor_remove" id="kiosk_settings_' + item.id + '" kioskId="'+item.id+'" onClick="return kioskDeleteConf()"><i class="glyphicon glyphicon-trash"></i>';
                }
                counter++;
                var kiosk_browser = 'STANDARD BROWSER';
                if(item.kiosk_browser_type == 1) {
                    kiosk_browser = 'OSP BROWSER';
                } else if(item.kiosk_browser_type == 2) {
                    kiosk_browser = 'APPLICATION';
                }

                t.row.add([
                    item.kiosk_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    kiosk_browser,
                    item.kiosk_browser_priority,
                    item.kiosk_browser_row,
                    item.kiosk_browser_column,
                    urlLink
                ]).draw(false);
            });
        }
    });
}

function kioskDeleteConf(){
    if ( confirm( "Are you sure you want to delete this Kiosk?" ) ) {
        $('#kiosktable').on( 'click', 'a.editor_remove', function (e) {
            e.preventDefault();
            var data = $(this).attr('kioskId');
            deleteKiosk(data);
        });
    } else {
        return false;
    }
}

function deleteKiosk(data) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#kiosktable').DataTable();
    $.ajax({
        type: 'POST',
        url: '/profilekioskdelete',
        data: {_token: CSRF_TOKEN, kioskId: data ,profile_type: $('#profile_type').val()},
        dataType: "json",
        cache: false,
        success: function (response) {
            if(response['authorisation']) {
                alert(response['authorisation']);
            } else {
                table
                    .row($('#kiosk_settings_' + data).parents('tr'))
                    .remove()
                    .draw();
            }
        }
    });
}

function upload(){
    var elem = document.getElementById("myBar-profile");
    var width = 1;
    var id = setInterval(frame, 80);
    function frame() {
        if (width >= 100) {
            clearInterval(id);
        } else {
            width++;
            elem.style.width = width + '%';
        }
    }
    return;

}

function assignProfileApplications(applicationId) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#applicationstable').DataTable();
    var dataString = 'profile_id=' + $('#userprofileid').val() + '&applicationId=' + applicationId + '&_token=' + CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/assignprofileapplication',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            getProfileApplicationsToSelect($('#userprofileid').val())
            getProfileApplications($('#userprofileid').val());
        }
    });
}

function getProfileApplications(profile_id) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#applicationstable').DataTable();
    var dataString = 'profileId=' + profile_id + '&_token=' + CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/profileapplicationlist',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username = '';
            var t = $('#applicationstable').DataTable();
            t.clear();
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                var urlLink = '';
                if($('#role_id').val() < 4) {
                    urlLink = ' <a href="" class="editor_remove" id="profile_application_' + item.id + '" profileApplicationId="'+item.id+'" onClick="return profileApplicationDeleteConf()"><i class="glyphicon glyphicon-trash"></i>';
                }
                counter++;
                t.row.add([
                    item.application.application_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.application.application_package.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.application.application_version.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    urlLink
                ]).draw(false);
            });
        }
    });
}
function profileApplicationDeleteConf(){
    if ( confirm( "Are you sure you want to delete this Application?" ) ) {
        $('#applicationstable').on( 'click', 'a.editor_remove', function (e) {
            e.preventDefault();
            var data = $(this).attr('profileapplicationid');
            deleteprofileApplication(data);
        });
    } else {
        return false;
    }
}

function deleteprofileApplication(data) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#applicationstable').DataTable();
    $.ajax({
        type: 'POST',
        url: '/profileapplicationdelete',
        data: {_token: CSRF_TOKEN, profileApplicationId: data},
        dataType: "json",
        cache: false,
        success: function (response) {
            if (response['authorisation']) {
                alert(response['authorisation']);
            } else {
                var posts = JSON.parse(JSON.stringify(response));
                table
                    .row($('#profile_application_' + data).parents('tr'))
                    .remove()
                    .draw();
                if (posts) {
                    var message = posts + '  Deleted';
                    $('#profileapp-message').show();
                    $('#profileapp-message').html(message);
                }
                getProfileApplicationsToSelect($('#userprofileid').val());
            }
        }
    });
}

function addDisabledApplications() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'disabled_application_name=' + $('#disablepackagename').val() + '&profile_id=' + $('#userprofileid').val() + '&_token=' + CSRF_TOKEN;
    $('#disableapperror').html('');
    $('#disableapperror').hide();
    // AJAX Code To Submit Form.
    $.ajax({
        type: "POST",
        url: "/disableapplicationcreate",
        data: dataString,
        datatype: 'json',
        cache: false,
        success: function (response) {
            if(response['authorisation']) {
                $('#disableapperror').html(response['authorisation']);
                $('#disableapperror').show();
            }
            if(response['save_success']) {
                $('#_disableappsuccess').html(response['save_success']);
                $('#_disableappsuccess').show();
            }
            $('#disablepackagename').val('');
            getDisabledApplications($('#userprofileid').val());
        }
    });
}

function getDisabledApplications(profile_id) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#packagedisabletable').DataTable();
    var dataString = 'profileId=' + profile_id + '&_token=' + CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/disableapplicationget',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username = '';
            var t = $('#packagedisabletable').DataTable();
            t.clear();
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                var urlLink = '';
                if($('#role_id').val() < 4) {
                    urlLink = ' <a href="" class="editor_remove" id="disabled_application_' + item.id + '" disabledApplicationId="'+item.id+'" onClick="return disabledApplicationDeleteConf()"><i class="glyphicon glyphicon-trash"></i>';
                }
                counter++;
                t.row.add([
                    item.disabled_application_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    urlLink
                ]).draw(false);
            });
        }
    });
}

function disabledApplicationDeleteConf(){
    if ( confirm( "Are you sure you want to remove this Application?" ) ) {
        $('#packagedisabletable').on( 'click', 'a.editor_remove', function (e) {
            e.preventDefault();
            var data = $(this).attr('disabledApplicationId');
            deleteDisabledApplication(data);
        });
    } else {
        return false;
    }
}

function deleteDisabledApplication(data) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#packagedisabletable').DataTable();
    $.ajax({
        type: 'POST',
        url: '/disableapplicationdelete',
        data: {_token: CSRF_TOKEN, disabledApplicationId: data},
        dataType: "json",
        cache: false,
        success: function (response) {
            if(response['authorisation']) {
                alert(response['authorisation']);
            } else {
                var posts = JSON.parse(JSON.stringify(response));
                table
                    .row($('#disabled_application_' + data).parents('tr'))
                    .remove()
                    .draw();
            }
        }
    });
}

function getDashboardKioskSettings(profile_id) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'profileId=' + profile_id + '&_token=' + CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/profiledashboardkiosksettingsget',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                $('#screennumber').val(item.kiosk_screen_number);
                $('#splscreennumber').val(item.kiosk_special_screen_number);
                $('#numberofcolumns').val(item.kiosk_special_screen_column_number);
            });
        }
    });
}

function applyProfile() {
    setDevicePushNotifications($('#userprofileid').val(), '', 'all');
}

function editProfileName() {

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/editprofilename',
        data: {'_token': CSRF_TOKEN, 'profile_id': $('#userprofileid').val(), 'profile_name': $('#profilename').val(), 'profile_type': $('#profile_type').val()},
        dataType: "json",
        cache: false,
        success: function (response) {
            $('#_profileerror').hide('');
            $('#_profilesuccess').hide('');
            if(response['authorisation']) {
                $('#_profileerror').html(response['authorisation']);
                $('#_profileerror').show();

            }else if(response['profileExist']) {
                $('#_profileerror').html(response['profileExist']);
                $('#_profileerror').show();

            }else{
                $('#_profilesuccess').html('Profile name updated successfully');
                $('#_profilesuccess').show();
            }

        }
    });
}

function updateEmailTab(currentTab,nextTo) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = '';
    var incoming_mail_ssl_usage = 0;
    var incoming_mail_tls_usage = 0;
    var incoming_mail_certificate_usage = 0;
    var outgoing_mail_ssl_usage = 0;
    var outgoing_mail_tls_usage = 0;
    var outgoing_mail_certificate_usage = 0;
    var account_description = $('#configEmail #account_description').val();
    var user_display_name = $('#configEmail #user_display_name').val();
    var user_email_address = $('#configEmail #user_email_address').val();
    var incoming_mail_account_type = $('#configEmail #incoming_mail_account_type').val();
    var incoming_mail_server = $('#configEmail #incoming_mail_server').val();
    var incoming_mail_server_port = $('#configEmail #incoming_mail_server_port').val();
    var incoming_mail_user_name = $('#configEmail #incoming_mail_user_name').val();
    var incoming_mail_password = $('#configEmail #incoming_mail_password').val();
    var outgoing_mail_account_type = $('#configEmail #outgoing_mail_account_type').val();
    var outgoing_mail_server = $('#configEmail #outgoing_mail_server').val();
    var outgoing_mail_server_port = $('#configEmail #outgoing_mail_server_port').val();
    var outgoing_mail_user_name = $('#configEmail #outgoing_mail_user_name').val();
    var outgoing_mail_password = $('#configEmail #outgoing_mail_password').val();

    if($('#configEmail #incoming_mail_ssl_usage').is(':checked')) {
        incoming_mail_ssl_usage = 1;
    }
    if($('#configEmail #incoming_mail_tls_usage').is(':checked')) {
        incoming_mail_tls_usage = 1;
    }
    if($('#configEmail #incoming_mail_certificate_usage').is(':checked')) {
        incoming_mail_certificate_usage = 1;
    }
    if($('#configEmail #outgoing_mail_ssl_usage').is(':checked')) {
        outgoing_mail_ssl_usage = 1;
    }
    if($('#configEmail #outgoing_mail_tls_usage').is(':checked')) {
        outgoing_mail_tls_usage = 1;
    }
    if($('#configEmail #outgoing_mail_certificate_usage').is(':checked')) {
        outgoing_mail_certificate_usage = 1;
    }
    dataString = 'profile_id='+$('#userprofileid').val()+'&account_description='+ account_description +'&user_display_name='+user_display_name +
    '&user_email_address='+user_email_address+'&incoming_mail_account_type='+incoming_mail_account_type+'&incoming_mail_server='+incoming_mail_server+'&incoming_mail_server_port='+incoming_mail_server_port+'&incoming_mail_user_name='+incoming_mail_user_name+
    '&incoming_mail_password='+incoming_mail_password+'&outgoing_mail_account_type='+outgoing_mail_account_type+'&outgoing_mail_server='+outgoing_mail_server+'&outgoing_mail_server_port='+outgoing_mail_server_port+
    '&outgoing_mail_user_name='+outgoing_mail_user_name+'&outgoing_mail_password='+outgoing_mail_password+'&incoming_mail_ssl_usage='+incoming_mail_ssl_usage+'&incoming_mail_tls_usage='+incoming_mail_tls_usage+'&incoming_mail_certificate_usage='+incoming_mail_certificate_usage+
    '&outgoing_mail_ssl_usage='+outgoing_mail_ssl_usage+'&outgoing_mail_tls_usage='+outgoing_mail_tls_usage+'&outgoing_mail_certificate_usage='+outgoing_mail_certificate_usage+'&_token='+CSRF_TOKEN;
    // AJAX Code To Submit Form.
    $.ajax({
        type: "POST",
        url: "/updateprofileemail",
        data: dataString,
        datatype:'json',
        cache: false,
        success: function(response){
                getProfileEmailDetails($('#userprofileid').val());
                if(currentTab != nextTo){
                    $('.'+nextTo).click();
                }else{
                    $('.home').click();
                }
        }
    });
}


function getProfileEmailDetails(profile_id) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'profileId=' + profile_id + '&profile_type=' + $('#profile_type').val() + '&_token=' + CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/profileemailget',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username = '';
            if ($('#profile_type').val() == 1) {
                var t = $('#androidemailtable').DataTable();
                t.clear();
                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    var urlLink = '';
                    if ($('#role_id').val() < 4) {
                        urlLink = ' <a href="" class="editor_remove" id="profileEmail_settings_' + item.id + '" class="editor_remove" profile_email_id="' + item.id + '" onClick="return confirmDeleteProfileEmail()"><i class="glyphicon glyphicon-trash"></i>';
                    }
                    counter++;
                    t.row.add([
                        item.account_description.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                        item.user_display_name.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                        item.user_email_address.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                        urlLink
                    ]).draw(false);
                        /*$('#configEmail #account_description').val(item.account_description)
                        $('#configEmail #user_display_name').val(item.user_display_name),
                        $('#configEmail #user_email_address').val(item.user_email_address),
                        $('#configEmail #incoming_mail_account_type').val(item.incoming_mail_account_type),
                        $('#configEmail #incoming_mail_server').val(item.incoming_mail_server),
                        $('#configEmail #incoming_mail_server_port').val(item.incoming_mail_server_port),
                        $('#configEmail #incoming_mail_user_name').val(item.incoming_mail_user_name),
                        $('#configEmail #incoming_mail_password').val(item.incoming_mail_password),
                        $('#configEmail #outgoing_mail_account_type').val(item.outgoing_mail_account_type),
                        $('#configEmail #outgoing_mail_server').val(item.outgoing_mail_server),
                        $('#configEmail #outgoing_mail_server_port').val(item.outgoing_mail_server_port),
                        $('#configEmail #outgoing_mail_user_name').val(item.outgoing_mail_user_name),
                        $('#configEmail #outgoing_mail_password').val(item.outgoing_mail_password),
                        $('#configEmail #incoming_mail_ssl_usage').val(item.incoming_mail_ssl_usage),
                        $('#configEmail #incoming_mail_tls_usage').val(item.incoming_mail_tls_usage),
                        $('#configEmail #incoming_mail_certificate_usage').val(item.incoming_mail_certificate_usage),
                        $('#configEmail #outgoing_mail_ssl_usage').val(item.outgoing_mail_ssl_usage),
                        $('#configEmail #outgoing_mail_tls_usage').val(item.outgoing_mail_tls_usage),
                        $('#configEmail #outgoing_mail_certificate_usage').val(item.outgoing_mail_certificate_usage)*/
                });
            } else if ($('#profile_type').val() == 2) {
                var s = $('#iosEmailtable').DataTable();
                s.clear();
                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    var urlLink = '';
                    if ($('#role_id').val() < 4) {
                        urlLink = ' <a href="" class="editor_remove" id="profileIosEmail_settings_' + item.id + '" class="editor_remove" profile_email_id="' + item.id + '" onClick="return confirmDeleteProfileEmail()"><i class="glyphicon glyphicon-trash"></i>';
                    }
                    counter++;
                    s.row.add([
                        item.account_description.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                        item.user_display_name.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                        item.user_email_address.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                        urlLink
                    ]).draw(false);

                    /*if (item.allow_move_status == 1) {
                        $('#configiosEmail #allow_move_status').attr('checked', 'checked');
                    }
                    if (item.incoming_mail_ssl_usage == 1) {
                        $('#configiosEmail #incoming_mail_ssl_usage').attr('checked', 'checked');
                    }
                    if (item.outgoing_mail_password == 1) {
                        $('#configiosEmail #outgoing_mail_password').attr('checked', 'checked');
                    }
                    if (item.recent_address_syncing_allow_status == 1) {
                        $('#configiosEmail #recent_address_syncing_allow_status').attr('checked', 'checked');
                    }
                    if (item.only_in_mail_usage == 1) {
                        $('#configiosEmail #only_in_mail_usage').attr('checked', 'checked');
                    }
                    if (item.outgoing_mail_ssl_usage == 1) {
                        $('#configiosEmail #outgoing_mail_ssl_usage').attr('checked', 'checked');
                    }
                    if (item.outgoing_mail_mime_usage == 1) {
                        $('#configiosEmail #outgoing_mail_mime_usage').attr('checked', 'checked');
                    }
                    $('#configiosEmail #account_description').val(item.account_description);
                    $('#configiosEmail #account_type').val(item.account_type);
                    $('#configiosEmail #user_display_name').val(item.user_display_name);
                    $('#configiosEmail #user_email_address').val(item.user_email_address);
                    $('#configiosEmail #incoming_mail_account_type').val(item.incoming_mail_account_type);
                    $('#configiosEmail #incoming_mail_server').val(item.incoming_mail_server);
                    $('#configiosEmail #incoming_mail_server_port').val(item.incoming_mail_server_port);
                    $('#configiosEmail #incoming_mail_user_name').val(item.incoming_mail_user_name);
                    $('#configiosEmail #incoming_mail_authentication_type').val(item.incoming_mail_authentication_type);
                    $('#configiosEmail #incoming_mail_password').val(item.incoming_mail_password);
                    $('#configiosEmail #outgoing_mail_account_type').val(item.outgoing_mail_account_type);
                    $('#configiosEmail #outgoing_mail_server').val(item.outgoing_mail_server);
                    $('#configiosEmail #outgoing_mail_server_port').val(item.outgoing_mail_server_port);
                    $('#configiosEmail #outgoing_mail_user_name').val(item.outgoing_mail_user_name);
                    $('#configiosEmail #outgoing_mail_authentication_type').val(item.outgoing_mail_authentication_type);*/
                });
            }
        }
    });
}
function confirmDeleteProfileEmail(){
    if ( confirm( "Are you sure you want to remove?" ) ) {
        var profile_type = $('#profile_type').val();
        if(profile_type == 1) {
            $('#androidemailtable').on('click', 'a.editor_remove', function (e) {
                e.preventDefault();
                var data = $(this).attr('profile_email_id');
                deleteProfileEmail(data, profile_type);
            });
        }else if(profile_type == 2){
            $('#iosEmailtable').on( 'click', 'a.editor_remove', function (e) {
                e.preventDefault();
                var data = $(this).attr('profile_email_id');
                deleteProfileEmail(data,profile_type);
            });
        }
    } else {
        return false;
    }
}


function deleteProfileEmail(data,profile_type) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#androidemailtable').DataTable();
    var iosTable = $('#iosEmailtable').DataTable();
    $.ajax({
        type: 'POST',
        url: '/profileemaildelete',
        data: {_token: CSRF_TOKEN, profile_id: data, profile_type: profile_type},
        dataType: "json",
        cache: false,
        success: function (response) {
            if(response['authorisation']) {
                alert(response['authorisation']);
            } else {
                if(profile_type == 1){
                    table
                        .row($('#profileEmail_settings_' + data).parents('tr'))
                        .remove()
                        .draw();
                }else if(profile_type == 2){
                    iosTable
                        .row($('#profileIosEmail_settings_' + data).parents('tr'))
                        .remove()
                        .draw();
                }
            }
        }
    });
}

function updateActiveSyncTab(currentTab,nextTo) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = '';
    var active_sync_ssl_status = 0;
    var active_sync_account_name = $('#config #active_sync_account_name').val();
    var active_sync_host = $('#config #active_sync_host').val();
    var active_sync_domain = $('#config #active_sync_domain').val();
    var active_sync_user_name = $('#config #active_sync_user_name').val();
    var active_sync_user_email = $('#config #active_sync_user_email').val();
    var active_sync_user_password = $('#config #active_sync_user_password').val();
    var active_sync_schedule = $('#config #active_sync_schedule').val();
    if($('#config #active_sync_ssl_status').is(':checked')) {
        active_sync_ssl_status = 1;
    }
    dataString = 'profile_id='+$('#userprofileid').val()+'&active_sync_account_name='+ active_sync_account_name +'&active_sync_host='+active_sync_host +
    '&active_sync_domain='+active_sync_domain+'&active_sync_user_name='+active_sync_user_name+'&active_sync_user_email='+active_sync_user_email+'&active_sync_user_password='+active_sync_user_password+'&active_sync_schedule='+active_sync_schedule+
    '&active_sync_ssl_status='+active_sync_ssl_status+'&_token='+CSRF_TOKEN;
    // AJAX Code To Submit Form.
    $.ajax({
        type: "POST",
        url: "/updateprofileactivesync",
        data: dataString,
        datatype:'json',
        cache: false,
        success: function(response){
            getProfileActiveSyncDetails($('#userprofileid').val());
            if(currentTab != nextTo){
                $('.'+nextTo).click();
            }else{
                $('.home').click();
            }
        }
    });
}

function getProfileActiveSyncDetails(profile_id) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'profileId=' + profile_id  + '&profile_type=' + $('#profile_type').val()+ '&_token=' + CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/profileactivesyncget',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username = '';
            if($('#profile_type').val() == 1) {
                var t = $('#androidActiveSyntable').DataTable();
                t.clear();
                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    var urlLink = '';
                    if($('#role_id').val() < 4) {
                        urlLink = ' <a href="" class="editor_remove" id="profileActive_settings_' + item.id + '" class="editor_remove" profile_active_id="'+item.id+'" onClick="return confirmDeleteProfileSync()"><i class="glyphicon glyphicon-trash"></i>';
                    }
                    counter++;
                    t.row.add([
                        item.active_sync_account_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                        item.active_sync_host.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                        item.active_sync_domain .replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                        item.active_sync_user_email .replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                        urlLink
                    ]).draw(false);
                    /*if (item.active_sync_ssl_status == 1) {
                        $('#config #active_sync_ssl_status').attr('checked', 'checked');
                    }
                    $('#config #active_sync_account_name').val(item.active_sync_account_name);
                    $('#config #active_sync_host').val(item.active_sync_host);
                    $('#config #active_sync_domain').val(item.active_sync_domain);
                    $('#config #active_sync_user_name').val(item.active_sync_user_name);
                    $('#config #active_sync_user_email').val(item.active_sync_user_email);
                    $('#config #active_sync_user_password').val(item.active_sync_user_password);
                    $('#config #active_sync_schedule').val(item.active_sync_schedule);*/
                });
            }else if($('#profile_type').val() == 2){
                var s = $('#iosActiveSyntable').DataTable();
                s.clear();
                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    var urlLink = '';
                    if($('#role_id').val() < 4) {
                        urlLink = ' <a href="" class="editor_remove" id="profileIosActive_settings_' + item.id + '" class="editor_remove" profile_active_id="'+item.id+'" onClick="return confirmDeleteProfileSync()"><i class="glyphicon glyphicon-trash"></i>';
                    }
                    counter++;
                    s.row.add([
                        item.account_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                        item.active_sync_server_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                        item.active_sync_domain.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                        item.active_sync_user_email.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                        urlLink
                    ]).draw(false);
                    /*if (item.move_allow_status == 1) {
                        $('#configiosActiveSyn #move_allow_status').attr('checked', 'checked');
                    }
                    if (item.recent_address_syncing_allow_status == 1) {
                        $('#configiosActiveSyn #recent_address_syncing_allow_status').attr('checked', 'checked');
                    }
                    if (item.use_only_in_mail_status == 1) {
                        $('#configiosActiveSyn #use_only_in_mail_status').attr('checked', 'checked');
                    }
                    if (item.use_ssl_status == 1) {
                        $('#configiosActiveSyn #use_ssl_status').attr('checked', 'checked');
                    }
                    if (item.use_mime_status == 1) {
                        $('#configiosActiveSyn #use_mime_status').attr('checked', 'checked');
                    }
                    if (item.identity_certificate_ios4_compatible_status == 1) {
                        $('#configiosActiveSyn #identity_certificate_ios4_compatible_status').attr('checked', 'checked');
                    }
                    $('#configiosActiveSyn #account_name').val(item.account_name);
                    $('#configiosActiveSyn #active_sync_server_name').val(item.active_sync_server_name);
                    $('#configiosActiveSyn #active_sync_domain').val(item.active_sync_domain);
                    $('#configiosActiveSyn #active_sync_user_name').val(item.active_sync_user_name);
                    $('#configiosActiveSyn #active_sync_user_email').val(item.active_sync_user_email);
                    $('#configiosActiveSyn #active_sync_user_password').val(item.active_sync_user_password);
                    $('#configiosActiveSyn #mail_sync_period').val(item.mail_sync_period);
                    $('#configiosActiveSyn #identity_certificate').val(item.identity_certificate);*/
                });
            }
        }
    });
}

function confirmDeleteProfileSync(){
    if ( confirm( "Are you sure you want to remove?" ) ) {
        var profile_type = $('#profile_type').val();
        if(profile_type == 1){
            $('#androidActiveSyntable').on( 'click', 'a.editor_remove', function (e) {
                e.preventDefault();
                var data = $(this).attr('profile_active_id');
                deleteProfileSync(data,profile_type);
            });
        }else if(profile_type == 2){
            $('#iosActiveSyntable').on( 'click', 'a.editor_remove', function (e) {
                e.preventDefault();
                var data = $(this).attr('profile_active_id');
                deleteProfileSync(data,profile_type);
            });
        }

    } else {
        return false;
    }
}


function deleteProfileSync(data,profile_type) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var activetable = $('#iosActiveSyntable').DataTable();
    var table = $('#androidActiveSyntable').DataTable();
    $.ajax({
        type: 'POST',
        url: '/profilesyncdelete',
        data: {_token: CSRF_TOKEN, profile_id: data ,profile_type: profile_type},
        dataType: "json",
        cache: false,
        success: function (response) {
            if(response['authorisation']) {
                alert(response['authorisation']);
            } else {
                if(profile_type == 1){
                    table
                        .row($('#profileActive_settings_' + data).parents('tr'))
                        .remove()
                        .draw();
                }else if(profile_type == 2){
                    activetable
                        .row($('#profileIosActive_settings_' + data).parents('tr'))
                        .remove()
                        .draw();
                }
            }
        }
    });
}

function updateIOShome(currentTab,nextTo){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = '';
    var app_install_allow_status = 0;
    var air_drop_block_status = 0;
    var camera_use_allow_status = 0;
    var face_time_allow_status = 0;
    var screen_capture_allow_status = 0;
    var roaming_automatic_sync_allow_status = 0;
    var touch_id_status = 0;
    var siri_allow_status = 0;
    var device_locked_siri_allow_status = 0;
    var device_locked_passbook_allow_status = 0;
    var voice_dialing_allow_status = 0;
    var in_app_purchase_allow_status = 0;
    var force_user_credential_on_itune_purchase_allow_status = 0;
    var multiplayer_gaming_allow_status = 0;
    var adding_game_centre_friends_allow_status = 0;
    var trust_enterprise_app_allow_status = 0;
    var modify_trust_enterprise_app_allow_status = 0;
    var enterprise_book_backup_allow_status = 0;
    var managed_apps_to_sync_allow_status = 0;
    var use_youtbe_allow_status = 0;
    var use_itune_store_allow_status = 0;
    var use_safari_allow_status = 0;
    var safari_enable_autofill_status = 0;
    var safari_force_fraud_warning_status = 0;
    var safari_enable_javascript_status = 0;
    var safari_block_popup_status = 0;
    var icloud_backup_allow_status = 0;
    var icloud_document_sync_allow_status = 0;
    var icloud_photo_stream_allow_status = 0;
    var icloud_shared_photo_stream_allow_status = 0;
    var icloud_photo_library_allow_status = 0;
    var lock_screen_notification_allow_status = 0;
    var today_view_in_lock_screen_allow_status = 0;
    var control_center_in_lock_screen_allow_status = 0;
    var over_the_air_PKI_updates_allow_status = 0;
    var ad_tracking_limit_status = 0;
    var diagnostic_data_sent_allow_status = 0;
    var untrusted_TLS_certificate_allow_status = 0;
    var force_encrypt_backup_status = 0;
    var force_apple_watch_wrist_detection_status = 0;
    var rate_music_allow_status = 0;
    var rate_ibookstore_erotica_allow_status = 0;
    var cookie_accept_status = $('#configiosHome #cookie_accept_status').val();
    var rating_region = $('#configiosHome #rating_region').val();
    var movie_allow_status = $('#configiosHome #movie_allow_status').val();
    var tv_shows_allow_status = $('#configiosHome #tv_shows_allow_status').val();
    var apps_allow_status = $('#configiosHome #apps_allow_status').val();

    if($('#configiosHome #app_install_allow_status').is(':checked')) {
        app_install_allow_status = 1;
    }
    if($('#configiosHome #air_drop_block_status').is(':checked')) {
        air_drop_block_status = 1;
    }
    if($('#configiosHome #camera_use_allow_status').is(':checked')) {
        camera_use_allow_status = 1;
        if($('#configiosHome #face_time_allow_status').is(':checked')) {
            face_time_allow_status = 1;
        }
    }

    if($('#configiosHome #screen_capture_allow_status').is(':checked')) {
        screen_capture_allow_status = 1;
    }
    if($('#configiosHome #roaming_automatic_sync_allow_status').is(':checked')) {
        roaming_automatic_sync_allow_status = 1;
    }
    if($('#configiosHome #touch_id_status').is(':checked')) {
        touch_id_status = 1;
    }
    if($('#configiosHome #siri_allow_status').is(':checked')) {
        siri_allow_status = 1;
        if($('#configiosHome #device_locked_siri_allow_status').is(':checked')) {
            device_locked_siri_allow_status = 1;
        }
    }

    if($('#configiosHome #voice_dialing_allow_status').is(':checked')) {
        voice_dialing_allow_status = 1;
    }
    if($('#configiosHome #device_locked_passbook_allow_status').is(':checked')) {
        device_locked_passbook_allow_status = 1;
    }
    if($('#configiosHome #in_app_purchase_allow_status').is(':checked')) {
        in_app_purchase_allow_status = 1;
    }
    if($('#configiosHome #force_user_credential_on_itune_purchase_allow_status').is(':checked')) {
        force_user_credential_on_itune_purchase_allow_status = 1;
    }
    if($('#configiosHome #multiplayer_gaming_allow_status').is(':checked')) {
        multiplayer_gaming_allow_status = 1;
    }
    if($('#configiosHome #adding_game_centre_friends_allow_status').is(':checked')) {
        adding_game_centre_friends_allow_status = 1;
    }
    if($('#configiosHome #trust_enterprise_app_allow_status').is(':checked')) {
        trust_enterprise_app_allow_status = 1;
    }
    if($('#configiosHome #modify_trust_enterprise_app_allow_status').is(':checked')) {
        modify_trust_enterprise_app_allow_status = 1;
    }
    if($('#configiosHome #enterprise_book_backup_allow_status').is(':checked')) {
        enterprise_book_backup_allow_status = 1;
    }
    if($('#configiosHome #managed_apps_to_sync_allow_status').is(':checked')) {
        managed_apps_to_sync_allow_status = 1;
    }
    if($('#configiosHome #use_youtbe_allow_status').is(':checked')) {
        use_youtbe_allow_status = 1;
    }
    if($('#configiosHome #use_itune_store_allow_status').is(':checked')) {
        use_itune_store_allow_status = 1;
    }
    if($('#configiosHome #use_safari_allow_status').is(':checked')) {
        use_safari_allow_status = 1;

        if($('#configiosHome #safari_enable_autofill_status').is(':checked')) {
            safari_enable_autofill_status = 1;
        }
        if($('#configiosHome #safari_force_fraud_warning_status').is(':checked')) {
            safari_force_fraud_warning_status = 1;
        }
        if($('#configiosHome #safari_enable_javascript_status').is(':checked')) {
            safari_enable_javascript_status = 1;
        }
        if($('#configiosHome #safari_block_popup_status').is(':checked')) {
            safari_block_popup_status = 1;
        }
    }

    if($('#configiosHome #icloud_backup_allow_status').is(':checked')) {
        icloud_backup_allow_status = 1;
    }
    if($('#configiosHome #icloud_document_sync_allow_status').is(':checked')) {
        icloud_document_sync_allow_status = 1;
    }
    if($('#configiosHome #icloud_photo_stream_allow_status').is(':checked')) {
        icloud_photo_stream_allow_status = 1;
    }
    if($('#configiosHome #icloud_shared_photo_stream_allow_status').is(':checked')) {
        icloud_shared_photo_stream_allow_status = 1;
    }
    if($('#configiosHome #icloud_photo_library_allow_status').is(':checked')) {
        icloud_photo_library_allow_status = 1;
    }
    if($('#configiosHome #lock_screen_notification_allow_status').is(':checked')) {
        lock_screen_notification_allow_status = 1;
    }
    if($('#configiosHome #today_view_in_lock_screen_allow_status').is(':checked')) {
        today_view_in_lock_screen_allow_status = 1;
    }
    if($('#configiosHome #control_center_in_lock_screen_allow_status').is(':checked')) {
        control_center_in_lock_screen_allow_status = 1;
    }
    if($('#configiosHome #over_the_air_PKI_updates_allow_status').is(':checked')) {
        over_the_air_PKI_updates_allow_status = 1;
    }
    if($('#configiosHome #ad_tracking_limit_status').is(':checked')) {
        ad_tracking_limit_status = 1;
    }
    if($('#configiosHome #diagnostic_data_sent_allow_status').is(':checked')) {
        diagnostic_data_sent_allow_status = 1;
    }
    if($('#configiosHome #untrusted_TLS_certificate_allow_status').is(':checked')) {
        untrusted_TLS_certificate_allow_status = 1;
    }
    if($('#configiosHome #force_encrypt_backup_status').is(':checked')) {
        force_encrypt_backup_status = 1;
    }
    if($('#configiosHome #force_apple_watch_wrist_detection_status').is(':checked')) {
        force_apple_watch_wrist_detection_status = 1;
    }
    if($('#configiosHome #rate_music_allow_status').is(':checked')) {
        rate_music_allow_status = 1;
    }
    if($('#configiosHome #rate_ibookstore_erotica_allow_status').is(':checked')) {
        rate_ibookstore_erotica_allow_status = 1;
    }
    dataString = 'profile_id='+$('#userprofileid').val()+'&id='+ $('#profileid').val() +'&app_install_allow_status='+app_install_allow_status+
    '&air_drop_block_status='+air_drop_block_status+'&camera_use_allow_status='+camera_use_allow_status+'&face_time_allow_status='+face_time_allow_status+'&screen_capture_allow_status='+screen_capture_allow_status+'&roaming_automatic_sync_allow_status='+roaming_automatic_sync_allow_status+
    '&touch_id_status='+touch_id_status+'&siri_allow_status='+siri_allow_status+'&device_locked_siri_allow_status='+device_locked_siri_allow_status+'&voice_dialing_allow_status='+voice_dialing_allow_status+
    '&device_locked_passbook_allow_status='+device_locked_passbook_allow_status+'&in_app_purchase_allow_status='+in_app_purchase_allow_status+'&force_user_credential_on_itune_purchase_allow_status='+force_user_credential_on_itune_purchase_allow_status+'&multiplayer_gaming_allow_status='+multiplayer_gaming_allow_status+'&adding_game_centre_friends_allow_status='+adding_game_centre_friends_allow_status+
    '&trust_enterprise_app_allow_status='+trust_enterprise_app_allow_status+'&modify_trust_enterprise_app_allow_status='+modify_trust_enterprise_app_allow_status+'&enterprise_book_backup_allow_status='+enterprise_book_backup_allow_status+'&managed_apps_to_sync_allow_status='+managed_apps_to_sync_allow_status+'&use_youtbe_allow_status='+use_youtbe_allow_status+
    '&use_itune_store_allow_status='+use_itune_store_allow_status+'&use_safari_allow_status='+use_safari_allow_status+'&safari_enable_autofill_status='+safari_enable_autofill_status+'&safari_force_fraud_warning_status='+safari_force_fraud_warning_status+
    '&safari_enable_javascript_status='+safari_enable_javascript_status+'&safari_block_popup_status='+safari_block_popup_status+'&cookie_accept_status='+cookie_accept_status+'&icloud_backup_allow_status='+icloud_backup_allow_status+'&icloud_document_sync_allow_status='+icloud_document_sync_allow_status+
    '&icloud_photo_stream_allow_status='+icloud_photo_stream_allow_status+'&icloud_shared_photo_stream_allow_status='+icloud_shared_photo_stream_allow_status+'&icloud_photo_library_allow_status='+icloud_photo_library_allow_status+'&lock_screen_notification_allow_status='+lock_screen_notification_allow_status+'&today_view_in_lock_screen_allow_status='+today_view_in_lock_screen_allow_status+'&control_center_in_lock_screen_allow_status='+control_center_in_lock_screen_allow_status+
    '&over_the_air_PKI_updates_allow_status='+over_the_air_PKI_updates_allow_status+'&ad_tracking_limit_status='+ad_tracking_limit_status+'&diagnostic_data_sent_allow_status='+diagnostic_data_sent_allow_status+'&untrusted_TLS_certificate_allow_status='+untrusted_TLS_certificate_allow_status+'&force_encrypt_backup_status='+force_encrypt_backup_status+'&force_apple_watch_wrist_detection_status='+force_apple_watch_wrist_detection_status+'&rate_music_allow_status='+rate_music_allow_status+
    '&rate_ibookstore_erotica_allow_status='+rate_ibookstore_erotica_allow_status+'&rating_region='+rating_region+'&movie_allow_status='+movie_allow_status+'&tv_shows_allow_status='+tv_shows_allow_status+'&apps_allow_status='+apps_allow_status+'&_token='+CSRF_TOKEN;
    // AJAX Code To Submit Form.
    $.ajax({
        type: "POST",
        url: "/iosprofileupdate",
        data: dataString,
        datatype:'json',
        cache: false,
        success: function(response){
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                $('#userprofileid').val(item.id);
                $('#profileid').val(item.profile_id);
            });
            if(currentTab != nextTo) {
                $('.'+nextTo).click();
            } else {
                $('.iosHome').click();
            }
        }
    });
}
function getIOSsettings(profile_id) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'profile_id=' + profile_id + '&_token=' + CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/getiossettings',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
            /*$('#userprofileid').val(item.id);
            $('.profileidpersist').val(item.id);
            $('#profileid').val(item.profile_id);
            $('#profilename').val(item.profile_name);*/

            if (item.app_install_allow_status == 1) {
                $('#configiosHome #app_install_allow_status').attr('checked', 'checked');
            }
            if (item.air_drop_block_status == 1) {
                $('#configiosHome #air_drop_block_status').attr('checked', 'checked');
            }
            if (item.camera_use_allow_status == 1) {
                $('#configiosHome #camera_use_allow_status').attr('checked', 'checked');
                $('.face_time_allow_status').show();
                if (item.face_time_allow_status == 1) {
                    $('#configiosHome #face_time_allow_status').attr('checked', 'checked');
                }
            }
            if (item.screen_capture_allow_status == 1) {
                $('#configiosHome #screen_capture_allow_status').attr('checked', 'checked');
            }
            if (item.roaming_automatic_sync_allow_status == 1) {
                $('#configiosHome #roaming_automatic_sync_allow_status').attr('checked', 'checked');
            }
            if (item.touch_id_status == 1) {
                $('#configiosHome #touch_id_status').attr('checked', 'checked');
            }
            if (item.siri_allow_status == 1) {
                $('#configiosHome #siri_allow_status').attr('checked', 'checked');
                $('.device_locked_siri_allow_status').show();
                if (item.device_locked_siri_allow_status == 1) {
                    $('#configiosHome #device_locked_siri_allow_status').attr('checked', 'checked');
                }
            }
            if (item.voice_dialing_allow_status == 1) {
                $('#configiosHome #voice_dialing_allow_status').attr('checked', 'checked');
            }
            if (item.device_locked_passbook_allow_status == 1) {
                $('#configiosHome #device_locked_passbook_allow_status').attr('checked', 'checked');
            }
            if (item.in_app_purchase_allow_status == 1) {
                $('#configiosHome #in_app_purchase_allow_status').attr('checked', 'checked');
            }
            if (item.force_user_credential_on_itune_purchase_allow_status == 1) {
                $('#configiosHome #force_user_credential_on_itune_purchase_allow_status').attr('checked', 'checked');
            }
            if (item.multiplayer_gaming_allow_status == 1) {
                $('#configiosHome #multiplayer_gaming_allow_status').attr('checked', 'checked');
            }
            if (item.adding_game_centre_friends_allow_status == 1) {
                $('#configiosHome #adding_game_centre_friends_allow_status').attr('checked', 'checked');
            }
            if (item.trust_enterprise_app_allow_status == 1) {
                $('#configiosHome #trust_enterprise_app_allow_status').attr('checked', 'checked');
            }
            if (item.modify_trust_enterprise_app_allow_status == 1) {
                $('#configiosHome #modify_trust_enterprise_app_allow_status').attr('checked', 'checked');
            }
            if (item.enterprise_book_backup_allow_status == 1) {
                $('#configiosHome #enterprise_book_backup_allow_status').attr('checked', 'checked');
            }
            if (item.managed_apps_to_sync_allow_status == 1) {
                $('#configiosHome #managed_apps_to_sync_allow_status').attr('checked', 'checked');
            }
            if (item.use_youtbe_allow_status == 1) {
                $('#configiosHome #use_youtbe_allow_status').attr('checked', 'checked');
            }
            if (item.use_itune_store_allow_status == 1) {
                $('#configiosHome #use_itune_store_allow_status').attr('checked', 'checked');
            }
            if (item.use_safari_allow_status == 1) {
                $('#configiosHome #use_safari_allow_status').attr('checked', 'checked');
                $('.use_safari_allow_status_div').show();
                if (item.safari_enable_autofill_status == 1) {
                    $('#configiosHome #safari_enable_autofill_status').attr('checked', 'checked');
                }
                if (item.safari_force_fraud_warning_status == 1) {
                    $('#configiosHome #safari_force_fraud_warning_status').attr('checked', 'checked');
                }
                if (item.safari_enable_javascript_status == 1) {
                    $('#configiosHome #safari_enable_javascript_status').attr('checked', 'checked');
                }
                if (item.safari_block_popup_status == 1) {
                    $('#configiosHome #safari_block_popup_status').attr('checked', 'checked');
                }
            }

            if (item.icloud_backup_allow_status == 1) {
                $('#configiosHome #icloud_backup_allow_status').attr('checked', 'checked');
            }
            if (item.icloud_document_sync_allow_status == 1) {
                $('#configiosHome #icloud_document_sync_allow_status').attr('checked', 'checked');
            }
            if (item.icloud_photo_stream_allow_status == 1) {
                $('#configiosHome #icloud_photo_stream_allow_status').attr('checked', 'checked');
            }
            if (item.icloud_shared_photo_stream_allow_status == 1) {
                $('#configiosHome #icloud_shared_photo_stream_allow_status').attr('checked', 'checked');
            }
            if (item.icloud_photo_library_allow_status == 1) {
                $('#configiosHome #icloud_photo_library_allow_status').attr('checked', 'checked');
            }
            if (item.lock_screen_notification_allow_status == 1) {
                $('#configiosHome #lock_screen_notification_allow_status').attr('checked', 'checked');
            }
            if (item.today_view_in_lock_screen_allow_status == 1) {
                $('#configiosHome #today_view_in_lock_screen_allow_status').attr('checked', 'checked');
            }
            if (item.control_center_in_lock_screen_allow_status == 1) {
                $('#configiosHome #control_center_in_lock_screen_allow_status').attr('checked', 'checked');
            }
            if (item.over_the_air_PKI_updates_allow_status == 1) {
                $('#configiosHome #over_the_air_PKI_updates_allow_status').attr('checked', 'checked');
            }
            if (item.ad_tracking_limit_status == 1) {
                $('#configiosHome #ad_tracking_limit_statusad_tracking_limit_status').attr('checked', 'checked');
            }
            if (item.diagnostic_data_sent_allow_status == 1) {
                $('#configiosHome #diagnostic_data_sent_allow_status').attr('checked', 'checked');
            }
            if (item.untrusted_TLS_certificate_allow_status == 1) {
                $('#configiosHome #untrusted_TLS_certificate_allow_status').attr('checked', 'checked');
            }
            if (item.force_encrypt_backup_status == 1) {
                $('#configiosHome #force_encrypt_backup_status').attr('checked', 'checked');
            }
            if (item.force_apple_watch_wrist_detection_status == 1) {
                $('#configiosHome #force_apple_watch_wrist_detection_status').attr('checked', 'checked');
            }
            if (item.rate_music_allow_status == 1) {
                $('#configiosHome #rate_music_allow_status').attr('checked', 'checked');
            }
            if (item.rate_ibookstore_erotica_allow_status == 1) {
                $('#configiosHome #rate_ibookstore_erotica_allow_status').attr('checked', 'checked');
            }
            $('#configiosHome #cookie_accept_status').val(item.cookie_accept_status);
            $('#configiosHome #rating_region').val(item.rating_region);
            $('#configiosHome #movie_allow_status').val(item.movie_allow_status);
            $('#configiosHome #tv_shows_allow_status').val(item.tv_shows_allow_status);
            $('#configiosHome #apps_allow_status').val(item.apps_allow_status);
        });
        }
    });
}

function updateIOSActiveSyn(currentTab,nextTo){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = '';
    var move_allow_status = 0;
    var recent_address_syncing_allow_status = 0;
    var use_only_in_mail_status = 0;
    var use_ssl_status = 0;
    var use_mime_status = 0;
    var account_name = $('#configiosActiveSyn #account_name').val();
    var active_sync_server_name = $('#configiosActiveSyn #active_sync_server_name').val();
    var active_sync_domain = $('#configiosActiveSyn #active_sync_domain').val();
    var active_sync_user_name = $('#configiosActiveSyn #active_sync_user_name').val();
    var active_sync_user_email = $('#configiosActiveSyn #active_sync_user_email').val();
    var active_sync_user_password = $('#configiosActiveSyn #active_sync_user_password').val();
    var mail_sync_period = $('#configiosActiveSyn #mail_sync_period').val();
    var identity_certificate = $('#configiosActiveSyn #identity_certificate').val();
    var identity_certificate_ios4_compatible_status = $('#configiosActiveSyn #identity_certificate_ios4_compatible_status').val();


    if($('#configiosActiveSyn #move_allow_status').is(':checked')) {
        move_allow_status = 1;
    }
    if($('#configiosActiveSyn #recent_address_syncing_allow_status').is(':checked')) {
        recent_address_syncing_allow_status = 1;
    }
    if($('#configiosActiveSyn #use_only_in_mail_status').is(':checked')) {
        use_only_in_mail_status = 1;
    }
    if($('#configiosActiveSyn #use_ssl_status').is(':checked')) {
        use_ssl_status = 1;
    }
    if($('#configiosActiveSyn #use_mime_status').is(':checked')) {
        use_mime_status = 1;
    }
    dataString = 'profile_id='+$('#userprofileid').val()+'&id='+ $('#profileid').val() +'&move_allow_status='+move_allow_status+
    '&recent_address_syncing_allow_status='+recent_address_syncing_allow_status+'&use_only_in_mail_status='+use_only_in_mail_status+'&use_ssl_status='+use_ssl_status+'&use_mime_status='+use_mime_status+'&account_name='+account_name+
    '&active_sync_server_name='+active_sync_server_name+'&active_sync_domain='+active_sync_domain+'&active_sync_user_name='+active_sync_user_name+'&active_sync_user_email='+active_sync_user_email+
    '&active_sync_user_password='+active_sync_user_password+'&mail_sync_period='+mail_sync_period+'&identity_certificate='+identity_certificate+'&identity_certificate_ios4_compatible_status='+identity_certificate_ios4_compatible_status+'&_token='+CSRF_TOKEN;
    // AJAX Code To Submit Form.
    $.ajax({
        type: "POST",
        url: "/iosactivesyncupdate",
        data: dataString,
        datatype:'json',
        cache: false,
        success: function(response){
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                $('#userprofileid').val(item.id);
                $('#profileid').val(item.profile_id);
            });
            getProfileActiveSyncDetails($('#userprofileid').val());
            if(currentTab != nextTo) {
                $('.'+nextTo).click();
            } else {
                $('.iosHome').click();
            }
        }
    });
}

function updateIOSAdvancedrestriction(currentTab,nextTo){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = '';
    var airdrop_allow_status =0 ;
    var app_cellular_data_modification_allow_status=0;
    var remove_app_allow_status=0;
    var book_store_allow_status=0;
    var touch_id_add_remove_allow_status=0;
    var imessage_allow_status=0;
    var game_center_allow_status=0;
    var itunes_pairing_allow_status=0;
    var profile_installation_allow_status=0;
    var podcasts_allow_status=0;
    var lookup_definition_allow_status=0;
    var predictive_keyboard_allow_status=0;
    var auto_correction_allow_status=0;
    var spell_check_allow_status=0;
    var apple_music_services_allow_status=0;
    var itunes_radio_allow_status =0;
    var ios_news_allow_status=0;
    var app_install_from_devices_allow_status=0;
    var keyboard_shortcut_allow_status=0;
    var paired_watch_allow_status=0;
    var account_modification_allow_status=0;
    var erase_cintent_and_settings_allow_status=0;
    var user_generated_content_assistant_allow_status=0;
    var find_my_friends_modify_allow_status=0;
    var force_use_of_priority_filter_status=0;
    var spotlight_internet_results_allow_status=0;
    var restrictions_enabling_allow_status=0;
    var passcode_modification_allow_status=0;
    var device_name_modification_allow_status=0;
    var wallpaper_modification_allow_status=0;
    var notification_modification_allow_status=0;
    var automatic_apps_downloading_allow_status=0;
    var autonomous_single_app_mode_apps=$('#configiosRestriction #autonomous_single_app_mode_apps').val();

    if($('#configiosRestriction #airdrop_allow_status').is(':checked')) {
        airdrop_allow_status = 1;
    }
    if($('#configiosRestriction #app_cellular_data_modification_allow_status').is(':checked')) {
        app_cellular_data_modification_allow_status = 1;
    }
    if($('#configiosRestriction #remove_app_allow_status').is(':checked')) {
        remove_app_allow_status = 1;
    }
    if($('#configiosRestriction #book_store_allow_status').is(':checked')) {
        book_store_allow_status = 1;
    }
    if($('#configiosRestriction #touch_id_add_remove_allow_status').is(':checked')) {
        touch_id_add_remove_allow_status = 1;
    }
    if($('#configiosRestriction #imessage_allow_status').is(':checked')) {
        imessage_allow_status = 1;
    }
    if($('#configiosRestriction #game_center_allow_status').is(':checked')) {
        game_center_allow_status = 1;
    }
    if($('#configiosRestriction #itunes_pairing_allow_status').is(':checked')) {
        itunes_pairing_allow_status = 1;
    }
    if($('#configiosRestriction #profile_installation_allow_status').is(':checked')) {
        profile_installation_allow_status = 1;
    }
    if($('#configiosRestriction #podcasts_allow_status').is(':checked')) {
        podcasts_allow_status = 1;
    }
    if($('#configiosRestriction #lookup_definition_allow_status').is(':checked')) {
        lookup_definition_allow_status = 1;
    }
    if($('#configiosRestriction #predictive_keyboard_allow_status').is(':checked')) {
        predictive_keyboard_allow_status = 1;
    }
    if($('#configiosRestriction #auto_correction_allow_status').is(':checked')) {
        auto_correction_allow_status = 1;
    }
    if($('#configiosRestriction #spell_check_allow_status').is(':checked')) {
        spell_check_allow_status = 1;
    }
    if($('#configiosRestriction #apple_music_services_allow_status').is(':checked')) {
        apple_music_services_allow_status = 1;
    }
    if($('#configiosRestriction #itunes_radio_allow_status').is(':checked')) {
        itunes_radio_allow_status = 1;
    }
    if($('#configiosRestriction #ios_news_allow_status').is(':checked')) {
        ios_news_allow_status = 1;
    }
    if($('#configiosRestriction #app_install_from_devices_allow_status').is(':checked')) {
        app_install_from_devices_allow_status = 1;
    }
    if($('#configiosRestriction #keyboard_shortcut_allow_status').is(':checked')) {
        keyboard_shortcut_allow_status = 1;
    }
    if($('#configiosRestriction #paired_watch_allow_status').is(':checked')) {
        paired_watch_allow_status = 1;
    }
    if($('#configiosRestriction #account_modification_allow_status').is(':checked')) {
        account_modification_allow_status = 1;
    }
    if($('#configiosRestriction #erase_cintent_and_settings_allow_status').is(':checked')) {
        erase_cintent_and_settings_allow_status = 1;
    }
    if($('#configiosRestriction #user_generated_content_assistant_allow_status').is(':checked')) {
        user_generated_content_assistant_allow_status = 1;
    }
    if($('#configiosRestriction #find_my_friends_modify_allow_status').is(':checked')) {
        find_my_friends_modify_allow_status = 1;
    }
    if($('#configiosRestriction #force_use_of_priority_filter_status').is(':checked')) {
        force_use_of_priority_filter_status = 1;
    }
    if($('#configiosRestriction #spotlight_internet_results_allow_status').is(':checked')) {
        spotlight_internet_results_allow_status = 1;
    }
    if($('#configiosRestriction #restrictions_enabling_allow_status').is(':checked')) {
        restrictions_enabling_allow_status = 1;
    }
    if($('#configiosRestriction #passcode_modification_allow_status').is(':checked')) {
        passcode_modification_allow_status = 1;
    }
    if($('#configiosRestriction #device_name_modification_allow_status').is(':checked')) {
        device_name_modification_allow_status = 1;
    }
    if($('#configiosRestriction #wallpaper_modification_allow_status').is(':checked')) {
        wallpaper_modification_allow_status = 1;
    }
    if($('#configiosRestriction #notification_modification_allow_status').is(':checked')) {
        notification_modification_allow_status = 1;
    }
    if($('#configiosRestriction #automatic_apps_downloading_allow_status').is(':checked')) {
        automatic_apps_downloading_allow_status = 1;
    }

    dataString = 'profile_id='+$('#userprofileid').val()+'&id='+ $('#profileid').val() +'&airdrop_allow_status='+airdrop_allow_status+
    '&app_cellular_data_modification_allow_status='+app_cellular_data_modification_allow_status+'&remove_app_allow_status='+remove_app_allow_status+'&book_store_allow_status='+book_store_allow_status+'&touch_id_add_remove_allow_status='+touch_id_add_remove_allow_status+'&imessage_allow_status='+imessage_allow_status+
    '&game_center_allow_status='+game_center_allow_status+'&itunes_pairing_allow_status='+itunes_pairing_allow_status+'&profile_installation_allow_status='+profile_installation_allow_status+'&podcasts_allow_status='+podcasts_allow_status+
    '&lookup_definition_allow_status='+lookup_definition_allow_status+'&predictive_keyboard_allow_status='+predictive_keyboard_allow_status+'&auto_correction_allow_status='+auto_correction_allow_status+'&spell_check_allow_status='+spell_check_allow_status+
    '&apple_music_services_allow_status='+apple_music_services_allow_status+'&itunes_radio_allow_status='+itunes_radio_allow_status+'&ios_news_allow_status='+ios_news_allow_status+
    '&app_install_from_devices_allow_status='+app_install_from_devices_allow_status+'&keyboard_shortcut_allow_status='+keyboard_shortcut_allow_status+'&paired_watch_allow_status='+paired_watch_allow_status+'&account_modification_allow_status='+account_modification_allow_status+'&erase_cintent_and_settings_allow_status='+erase_cintent_and_settings_allow_status
    +'&user_generated_content_assistant_allow_status='+user_generated_content_assistant_allow_status+'&find_my_friends_modify_allow_status='+find_my_friends_modify_allow_status+
    '&force_use_of_priority_filter_status='+force_use_of_priority_filter_status+'&spotlight_internet_results_allow_status='+spotlight_internet_results_allow_status+'&restrictions_enabling_allow_status='+restrictions_enabling_allow_status+
    '&passcode_modification_allow_status='+passcode_modification_allow_status+'&device_name_modification_allow_status='+device_name_modification_allow_status+'&wallpaper_modification_allow_status='+wallpaper_modification_allow_status+'&notification_modification_allow_status='+notification_modification_allow_status+
    '&automatic_apps_downloading_allow_status='+automatic_apps_downloading_allow_status+'&autonomous_single_app_mode_apps='+autonomous_single_app_mode_apps+'&_token='+CSRF_TOKEN;
    // AJAX Code To Submit Form.
    $.ajax({
        type: "POST",
        url: "/profileadvancedrestrictionupdate",
        data: dataString,
        datatype:'json',
        cache: false,
        success: function(response){
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                $('#userprofileid').val(item.id);
                $('#profileid').val(item.profile_id);
            });
            if(currentTab != nextTo) {
                $('.'+nextTo).click();
            } else {
                $('.iosHome').click();
            }
        }
    });

}

function getAdvancedRestrictionSetting(profile_id) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'profileId=' + profile_id + '&_token=' + CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/getadvancedrestriction',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                if(item.airdrop_allow_status == 1) {
                    $('#configiosRestriction #airdrop_allow_status').attr('checked', 'checked');
                }
                if(item.app_cellular_data_modification_allow_status == 1) {
                    $('#configiosRestriction #app_cellular_data_modification_allow_status').attr('checked', 'checked');
                }
                if(item.remove_app_allow_status == 1) {
                    $('#configiosRestriction #remove_app_allow_status').attr('checked', 'checked');
                }
                if(item.book_store_allow_status == 1) {
                    $('#configiosRestriction #book_store_allow_status').attr('checked', 'checked');
                }
                if(item.touch_id_add_remove_allow_status == 1) {
                    $('#configiosRestriction #touch_id_add_remove_allow_status').attr('checked', 'checked');
                }
                if(item.imessage_allow_status == 1) {
                    $('#configiosRestriction #imessage_allow_status').attr('checked', 'checked');
                }
                if(item.game_center_allow_status == 1) {
                    $('#configiosRestriction #game_center_allow_status').attr('checked', 'checked');
                }
                if(item.itunes_pairing_allow_status == 1) {
                    $('#configiosRestriction #itunes_pairing_allow_status').attr('checked', 'checked');
                }
                if(item.profile_installation_allow_status == 1) {
                    $('#configiosRestriction #profile_installation_allow_status').attr('checked', 'checked');
                }
                if(item.podcasts_allow_status == 1) {
                    $('#configiosRestriction #podcasts_allow_status').attr('checked', 'checked');
                }
                if(item.lookup_definition_allow_status == 1) {
                    $('#configiosRestriction #lookup_definition_allow_status').attr('checked', 'checked');
                }
                if(item.predictive_keyboard_allow_status == 1) {
                    $('#configiosRestriction #predictive_keyboard_allow_status').attr('checked', 'checked');
                }
                if(item.auto_correction_allow_status == 1) {
                    $('#configiosRestriction #auto_correction_allow_status').attr('checked', 'checked');
                }
                if(item.spell_check_allow_status == 1) {
                    $('#configiosRestriction #spell_check_allow_status').attr('checked', 'checked');
                }
                if(item.apple_music_services_allow_status == 1) {
                    $('#configiosRestriction #apple_music_services_allow_status').attr('checked', 'checked');
                }
                if(item.itunes_radio_allow_status == 1) {
                    $('#configiosRestriction #itunes_radio_allow_status').attr('checked', 'checked');
                }
                if(item.ios_news_allow_status == 1) {
                    $('#configiosRestriction #ios_news_allow_status').attr('checked', 'checked');
                }
                if(item.app_install_from_devices_allow_status == 1) {
                    $('#configiosRestriction #app_install_from_devices_allow_status').attr('checked', 'checked');
                }
                if(item.keyboard_shortcut_allow_status == 1) {
                    $('#configiosRestriction #keyboard_shortcut_allow_status').attr('checked', 'checked');
                }
                if(item.paired_watch_allow_status == 1) {
                    $('#configiosRestriction #paired_watch_allow_status').attr('checked', 'checked');
                }
                if(item.account_modification_allow_status == 1) {
                    $('#configiosRestriction #account_modification_allow_status').attr('checked', 'checked');
                }
                if(item.erase_cintent_and_settings_allow_status == 1) {
                    $('#configiosRestriction #erase_cintent_and_settings_allow_status').attr('checked', 'checked');
                }
                if(item.user_generated_content_assistant_allow_status == 1) {
                    $('#configiosRestriction #user_generated_content_assistant_allow_status').attr('checked', 'checked');
                }
                if(item.find_my_friends_modify_allow_status == 1) {
                    $('#configiosRestriction #find_my_friends_modify_allow_status').attr('checked', 'checked');
                }
                if(item.force_use_of_priority_filter_status == 1) {
                    $('#configiosRestriction #force_use_of_priority_filter_status').attr('checked', 'checked');
                }
                if(item.spotlight_internet_results_allow_status == 1) {
                    $('#configiosRestriction #spotlight_internet_results_allow_status').attr('checked', 'checked');
                }
                if(item.restrictions_enabling_allow_status == 1) {
                    $('#configiosRestriction #restrictions_enabling_allow_status').attr('checked', 'checked');
                }
                if(item.passcode_modification_allow_status == 1) {
                    $('#configiosRestriction #passcode_modification_allow_status').attr('checked', 'checked');
                }
                if(item.device_name_modification_allow_status == 1) {
                    $('#configiosRestriction #device_name_modification_allow_status').attr('checked', 'checked');
                }
                if(item.wallpaper_modification_allow_status == 1) {
                    $('#configiosRestriction #wallpaper_modification_allow_status').attr('checked', 'checked');
                }
                if(item.notification_modification_allow_status == 1) {
                    $('#configiosRestriction #notification_modification_allow_status').attr('checked', 'checked');
                }
                if(item.automatic_apps_downloading_allow_status == 1) {
                    $('#configiosRestriction #automatic_apps_downloading_allow_status').attr('checked', 'checked');
                }
                $('#configiosRestriction #autonomous_single_app_mode_apps').val(item.autonomous_single_app_mode_apps);
            });
        }
    });
}


function updateIOSApn(currentTab,nextTo){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var apn_name = $('#configiosApn #apn_name').val();
    var apn_user_name = $('#configiosApn #apn_user_name').val();
    var apn_password = $('#configiosApn #apn_password').val();
    var apn_proxy_server = $('#configiosApn #apn_proxy_server').val();
    var apn_proxy_server_port = $('#configiosApn #apn_proxy_server_port').val();

    dataString = 'profile_id='+$('#userprofileid').val()+'&id='+ $('#profileid').val() +'&apn_name='+apn_name+
    '&apn_user_name='+apn_user_name+'&apn_password='+apn_password+'&apn_proxy_server='+apn_proxy_server+
    '&apn_proxy_server_port='+apn_proxy_server_port+'&_token='+CSRF_TOKEN;
    // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/iosprofileapnupdate",
            data: dataString,
            datatype:'json',
            cache: false,
            success: function(response){
                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    $('#userprofileid').val(item.id);
                    $('#profileid').val(item.profile_id);
                });
                getProfileApnDetails($('#userprofileid').val());
                if(currentTab != nextTo) {
                    $('.'+nextTo).click();
                } else {
                    $('.iosHome').click();
                }
            }
        });
}

function getProfileApnDetails(profile_id) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'profile_id=' + profile_id  + '&_token=' + CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/profileapnget',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username = '';
            var t = $('#iosApntable').DataTable();
            t.clear();
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                var urlLink = '';
                if($('#role_id').val() < 4) {
                    urlLink = ' <a href="" class="editor_remove" id="profileApn_settings_' + item.id + '" class="editor_remove" profile_apn_id="'+item.id+'" onClick="return confirmDeleteProfileApn()"><i class="glyphicon glyphicon-trash"></i>';
                }
                counter++;
                t.row.add([
                    item.apn_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.apn_user_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.apn_proxy_server .replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    urlLink
                ]).draw(false);
                /*$('#configiosApn #apn_name').val(item.apn_name);
                $('#configiosApn #apn_user_name').val(item.apn_user_name);
                $('#configiosApn #apn_password').val(item.apn_password);
                $('#configiosApn #apn_proxy_server').val(item.apn_proxy_server);
                $('#configiosApn #apn_proxy_server_port').val(item.apn_proxy_server_port);*/
            });
        }
    });
}

function confirmDeleteProfileApn(){
    if ( confirm( "Are you sure you want to remove?" ) ) {
            $('#iosApntable').on('click', 'a.editor_remove', function (e) {
                e.preventDefault();
                var data = $(this).attr('profile_apn_id');
                deleteProfileApn(data);
            });
    } else {
        return false;
    }
}


function deleteProfileApn(data) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#iosApntable').DataTable();
    $.ajax({
        type: 'POST',
        url: '/profileapndelete',
        data: {_token: CSRF_TOKEN, profile_id: data },
        dataType: "json",
        cache: false,
        success: function (response) {
            if(response['authorisation']) {
                alert(response['authorisation']);
            } else {
                    table
                        .row($('#profileApn_settings_' + data).parents('tr'))
                        .remove()
                        .draw();
            }
        }
    });
}

function updateIOSCredentials(currentTab,nextTo){if(currentTab != nextTo) {
    $('.'+nextTo).click();
    } else {
    $('.iosHome').click();
    }

}

function updateIOSEmailsettings(currentTab,nextTo){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = '';
    var allow_move_status = 0;
    var incoming_mail_ssl_usage = 0;
    var outgoing_mail_password = 0;
    var recent_address_syncing_allow_status = 0;
    var only_in_mail_usage = 0;
    var outgoing_mail_ssl_usage = 0;
    var outgoing_mail_mime_usage = 0;
    var account_description = $('#configiosEmail #account_description').val();
    var account_type = $('#configiosEmail #account_type').val();
    var user_display_name = $('#configiosEmail #user_display_name').val();
    var user_email_address = $('#configiosEmail #user_email_address').val();
    var incoming_mail_account_type = $('#configiosEmail #incoming_mail_account_type').val();
    var incoming_mail_server = $('#configiosEmail #incoming_mail_server').val();
    var incoming_mail_server_port = $('#configiosEmail #incoming_mail_server_port').val();
    var incoming_mail_user_name = $('#configiosEmail #incoming_mail_user_name').val();
    var incoming_mail_authentication_type = $('#configiosEmail #incoming_mail_authentication_type').val();
    var incoming_mail_password = $('#configiosEmail #incoming_mail_password').val();
    var outgoing_mail_account_type = $('#configiosEmail #outgoing_mail_account_type').val();
    var outgoing_mail_server = $('#configiosEmail #outgoing_mail_server').val();
    var outgoing_mail_server_port = $('#configiosEmail #outgoing_mail_server_port').val();
    var outgoing_mail_user_name = $('#configiosEmail #outgoing_mail_user_name').val();
    var outgoing_mail_authentication_type = $('#configiosEmail #outgoing_mail_authentication_type').val();


    if($('#configiosEmail #allow_move_status').is(':checked')) {
        allow_move_status = 1;
    }
    if($('#configiosEmail #incoming_mail_ssl_usage').is(':checked')) {
        incoming_mail_ssl_usage = 1;
    }
    if($('#configiosEmail #outgoing_mail_password').is(':checked')) {
        outgoing_mail_password = 1;
    }
    if($('#configiosEmail #recent_address_syncing_allow_status').is(':checked')) {
        recent_address_syncing_allow_status = 1;
    }
    if($('#configiosEmail #only_in_mail_usage').is(':checked')) {
        only_in_mail_usage = 1;
    }
    if($('#configiosEmail #outgoing_mail_ssl_usage').is(':checked')) {
        outgoing_mail_ssl_usage = 1;
    }
    if($('#configiosEmail #outgoing_mail_mime_usage').is(':checked')) {
        outgoing_mail_mime_usage = 1;
    }
    dataString = 'profile_id='+$('#userprofileid').val()+'&id='+ $('#profileid').val() +'&allow_move_status='+allow_move_status+
    '&incoming_mail_ssl_usage='+incoming_mail_ssl_usage+'&outgoing_mail_password='+outgoing_mail_password+'&recent_address_syncing_allow_status='+recent_address_syncing_allow_status+'&only_in_mail_usage='+only_in_mail_usage+'&outgoing_mail_ssl_usage='+outgoing_mail_ssl_usage+
    '&outgoing_mail_mime_usage='+outgoing_mail_mime_usage+'&account_description='+account_description+'&account_type='+account_type+'&user_display_name='+user_display_name+
    '&user_email_address='+user_email_address+'&incoming_mail_account_type='+incoming_mail_account_type+'&incoming_mail_server='+incoming_mail_server+'&incoming_mail_server_port='+incoming_mail_server_port+
    '&incoming_mail_user_name='+incoming_mail_user_name+'&incoming_mail_authentication_type='+incoming_mail_authentication_type+'&incoming_mail_password='+incoming_mail_password+
    '&outgoing_mail_account_type='+outgoing_mail_account_type+'&outgoing_mail_server='+outgoing_mail_server+'&outgoing_mail_server_port='+outgoing_mail_server_port+'&outgoing_mail_user_name='+outgoing_mail_user_name+'&outgoing_mail_authentication_type='+outgoing_mail_authentication_type+'&_token='+CSRF_TOKEN;
    // AJAX Code To Submit Form.
    $.ajax({
        type: "POST",
        url: "/iosprofileemailupdate",
        data: dataString,
        datatype:'json',
        cache: false,
        success: function(response){
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                $('#userprofileid').val(item.id);
                $('#profileid').val(item.profile_id);
            });
            getProfileEmailDetails($('#userprofileid').val());
            if(currentTab != nextTo) {
                $('.'+nextTo).click();
            } else {
                $('.iosHome').click();
            }
        }
    });
}

function updateIOSKiosksettings(currentTab,nextTo){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = '';
    var kiosk_app = $('#configiosKiosk #kiosk_app').val();
    var kiosk_app_name = $('#configiosKiosk #kiosk_app_name').val();
    var kiosk_app_identifier = $('#configiosKiosk #kiosk_app_identifier').val();
    var kiosk_disable_touch_status = 0;
    var kiosk_disable_device_rotation_status = 0;
    var kiosk_disable_volume_button_status = 0;
    var kiosk_disable_ringer_switch_status = 0;
    var kiosk_disable_sleep_wake_button_status = 0;
    var kiosk_disable_auto_lock_status = 0;
    var kiosk_enable_voice_over_status = 0;
    var kiosk_enable_zoom_status = 0;
    var kiosk_enable_invert_colors_status = 0;
    var kiosk_enable_assistive_touch_status = 0;
    var kiosk_enable_speak_selection_status = 0;
    var kiosk_voice_over_status = 0;
    var kiosk_zoom_status = 0;
    var kiosk_invert_colors_status = 0;
    var kiosk_assistive_touch_status = 0;

    if($('#configiosKiosk #kiosk_disable_touch_status').is(':checked')) {
        kiosk_disable_touch_status = 1;
    }
    if($('#configiosKiosk #kiosk_disable_device_rotation_status').is(':checked')) {
        kiosk_disable_device_rotation_status = 1;
    }
    if($('#configiosKiosk #kiosk_disable_volume_button_status').is(':checked')) {
        kiosk_disable_volume_button_status = 1;
    }
    if($('#configiosKiosk #kiosk_disable_ringer_switch_status').is(':checked')) {
        kiosk_disable_ringer_switch_status = 1;
    }
    if($('#configiosKiosk #kiosk_disable_sleep_wake_button_status').is(':checked')) {
        kiosk_disable_sleep_wake_button_status = 1;
    }
    if($('#configiosKiosk #kiosk_disable_auto_lock_status').is(':checked')) {
        kiosk_disable_auto_lock_status = 1;
    }
    if($('#configiosKiosk #kiosk_enable_voice_over_status').is(':checked')) {
        kiosk_enable_voice_over_status = 1;
    }
    if($('#configiosKiosk #kiosk_enable_zoom_status').is(':checked')) {
        kiosk_enable_zoom_status = 1;
    }
    if($('#configiosKiosk #kiosk_enable_invert_colors_status').is(':checked')) {
        kiosk_enable_invert_colors_status = 1;
    }
    if($('#configiosKiosk #kiosk_enable_assistive_touch_status').is(':checked')) {
        kiosk_enable_assistive_touch_status = 1;
    }
    if($('#configiosKiosk #kiosk_enable_speak_selection_status').is(':checked')) {
        kiosk_enable_speak_selection_status = 1;
    }
    if($('#configiosKiosk #kiosk_voice_over_status').is(':checked')) {
        kiosk_voice_over_status = 1;
    }
    if($('#configiosKiosk #kiosk_zoom_status').is(':checked')) {
        kiosk_zoom_status = 1;
    }
    if($('#configiosKiosk #kiosk_invert_colors_status').is(':checked')) {
        kiosk_invert_colors_status = 1;
    }
    if($('#configiosKiosk #kiosk_assistive_touch_status').is(':checked')) {
        kiosk_assistive_touch_status = 1;
    }

    dataString = 'profile_id='+$('#userprofileid').val()+'&id='+ $('#profileid').val() +'&kiosk_app='+kiosk_app+
    '&kiosk_app_name='+kiosk_app_name+'&kiosk_app_identifier='+kiosk_app_identifier+'&kiosk_disable_touch_status='+kiosk_disable_touch_status+
    '&kiosk_disable_device_rotation_status='+kiosk_disable_device_rotation_status+'&kiosk_disable_volume_button_status='+kiosk_disable_volume_button_status+'&kiosk_disable_ringer_switch_status='+kiosk_disable_ringer_switch_status+'&kiosk_disable_sleep_wake_button_status='+kiosk_disable_sleep_wake_button_status+
    '&kiosk_disable_auto_lock_status='+kiosk_disable_auto_lock_status+'&kiosk_enable_voice_over_status='+kiosk_enable_voice_over_status+
    '&kiosk_enable_zoom_status='+kiosk_enable_zoom_status+'&kiosk_enable_invert_colors_status='+kiosk_enable_invert_colors_status+'&kiosk_enable_assistive_touch_status='+kiosk_enable_assistive_touch_status+
    '&kiosk_enable_speak_selection_status='+kiosk_enable_speak_selection_status+'&kiosk_voice_over_status='+kiosk_voice_over_status+
    '&kiosk_zoom_status='+kiosk_zoom_status+'&kiosk_assistive_touch_status='+kiosk_assistive_touch_status+'&kiosk_invert_colors_status='+kiosk_invert_colors_status+'&_token='+CSRF_TOKEN;
        // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/iosprofilekioskupdate",
            data: dataString,
            datatype:'json',
            cache: false,
            success: function(response){
                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    $('#userprofileid').val(item.id);
                    $('#profileid').val(item.profile_id);
                });
                getProfileKioskDetails($('#userprofileid').val());
                if(currentTab != nextTo) {
                    $('.'+nextTo).click();
                } else {
                    $('.iosHome').click();
                }
            }
        });
    }

    function getProfileKioskDetails(profile_id) {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var dataString = 'profileId=' + profile_id  + '&profile_type='+ $('#profile_type').val() + '&_token=' + CSRF_TOKEN;
        $.ajax({
            type: 'POST',
            url: '/profilekioskget',
            data: dataString,
            dataType: "json",
            cache: false,
            success: function (response) {
                var trHTML = '';
                var counter = 0;
                var username = '';
                var t = $('#iosKiosktable').DataTable();
                t.clear();
                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    var urlLink = '';
                    if($('#role_id').val() < 4) {
                        urlLink = ' <a href="" class="editor_remove" id="profileKiosk_settings_' + item.id + '" class="editor_remove" profile_kiosk_id="'+item.id+'" onClick="return confirmDeleteProfileKiosk()"><i class="glyphicon glyphicon-trash"></i>';
                    }
                    counter++;
                    t.row.add([
                        item.kiosk_app_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                        item.kiosk_app_identifier.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                        urlLink
                    ]).draw(false);
                    $/*('#configiosKiosk #kiosk_app').val(item.kiosk_app);
                    $('#configiosKiosk #kiosk_app_name').val(item.kiosk_app_name);
                    $('#configiosKiosk #kiosk_app_identifier').val(item.kiosk_app_identifier);
                    if(item.kiosk_disable_touch_status == 1) {
                        $('#configiosKiosk #kiosk_disable_touch_status').attr('checked', 'checked');
                    }
                    if(item.kiosk_disable_device_rotation_status == 1) {
                        $('#configiosKiosk #kiosk_disable_device_rotation_status').attr('checked', 'checked');
                    }
                    if(item.kiosk_disable_volume_button_status == 1) {
                        $('#configiosKiosk #kiosk_disable_volume_button_status').attr('checked', 'checked');
                    }
                    if(item.kiosk_disable_ringer_switch_status == 1) {
                        $('#configiosKiosk #kiosk_disable_ringer_switch_status').attr('checked', 'checked');
                    }
                    if(item.kiosk_disable_sleep_wake_button_status == 1) {
                        $('#configiosKiosk #kiosk_disable_sleep_wake_button_status').attr('checked', 'checked');
                    }
                    if(item.kiosk_disable_auto_lock_status == 1) {
                        $('#configiosKiosk #kiosk_disable_auto_lock_status').attr('checked', 'checked');
                    }
                    if(item.kiosk_enable_voice_over_status == 1) {
                        $('#configiosKiosk #kiosk_enable_voice_over_status').attr('checked', 'checked');
                    }
                    if(item.kiosk_enable_zoom_status == 1) {
                        $('#configiosKiosk #kiosk_enable_zoom_status').attr('checked', 'checked');
                    }
                    if(item.kiosk_enable_invert_colors_status == 1) {
                        $('#configiosKiosk #kiosk_enable_invert_colors_status').attr('checked', 'checked');
                    }
                    if(item.kiosk_enable_assistive_touch_status == 1) {
                        $('#configiosKiosk #kiosk_enable_assistive_touch_status').attr('checked', 'checked');
                    }
                    if(item.kiosk_enable_speak_selection_status == 1) {
                        $('#configiosKiosk #kiosk_enable_speak_selection_status').attr('checked', 'checked');
                    }
                    if(item.kiosk_voice_over_status == 1) {
                        $('#configiosKiosk #kiosk_voice_over_status').attr('checked', 'checked');
                    }
                    if(item.kiosk_zoom_status == 1) {
                        $('#configiosKiosk #kiosk_zoom_status').attr('checked', 'checked');
                    }
                    if(item.kiosk_invert_colors_status == 1) {
                        $('#configiosKiosk #kiosk_invert_colors_status').attr('checked', 'checked');
                    }
                    if(item.kiosk_assistive_touch_status == 1) {
                        $('#configiosKiosk #kiosk_assistive_touch_status').attr('checked', 'checked');
                    }*/
                });
            }
        });
    }

    function confirmDeleteProfileKiosk(){
        if ( confirm( "Are you sure you want to remove?" ) ) {
            $('#iosKiosktable').on('click', 'a.editor_remove', function (e) {
                e.preventDefault();
                var data = $(this).attr('profile_kiosk_id');
                var profile_type = $('#profile_type').val();
                deleteProfileKiosk(data, profile_type);
            });
        } else {
            return false;
        }
    }


    function deleteProfileKiosk(data, profile_type) {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var table = $('#iosKiosktable').DataTable();
        $.ajax({
            type: 'POST',
            url: '/profilekioskdelete',
            data: {_token: CSRF_TOKEN, kioskId: data, profile_type: profile_type},
            dataType: "json",
            cache: false,
            success: function (response) {
                if(response['authorisation']) {
                    alert(response['authorisation']);
                } else {
                    table
                        .row($('#profileKiosk_settings_' + data).parents('tr'))
                        .remove()
                        .draw();
                }
            }
        });
    }


function updateIOSLdap(currentTab,nextTo){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = '';
    var ldap_ssl_usage_status = 0;
    var ldap_account_description = $('#configiosLdap #ldap_account_description').val();
    var ldap_account_user_name = $('#configiosLdap #ldap_account_user_name').val();
    var ldap_account_password = $('#configiosLdap #ldap_account_password').val();
    var ldap_account_host_name = $('#configiosLdap #ldap_account_host_name').val();
    if($('#configiosLdap #ldap_ssl_usage_status').is(':checked')) {
        ldap_ssl_usage_status = 1;
    }
    dataString = 'profile_id='+$('#userprofileid').val()+'&id='+ $('#profileid').val() +'&ldap_ssl_usage_status='+ldap_ssl_usage_status+
    '&ldap_account_description='+ldap_account_description+'&ldap_account_user_name='+ldap_account_user_name+'&ldap_account_password='+ldap_account_password+'&ldap_account_host_name='+ldap_account_host_name+'&_token='+CSRF_TOKEN;
    // AJAX Code To Submit Form.
    $.ajax({
        type: "POST",
        url: "/iosprofileldapupdate",
        data: dataString,
        datatype:'json',
        cache: false,
        success: function(response){
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                $('#userprofileid').val(item.id);
                $('#profileid').val(item.profile_id);
            });
            getProfileldapDetails($('#userprofileid').val());
            if(currentTab != nextTo) {
                $('.'+nextTo).click();
            } else {
                $('.iosHome').click();
            }
        }
    });
}

function getProfileldapDetails(profile_id) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'profileId=' + profile_id + '&_token=' + CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/profileldapget',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username = '';
            var t = $('#iosLdaptable').DataTable();
            t.clear();
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                var urlLink = '';
                if ($('#role_id').val() < 4) {
                    urlLink = ' <a href="" class="editor_remove" id="profileLdap_settings_' + item.id + '" class="editor_remove" profile_ldap_id="' + item.id + '" onClick="return confirmDeleteProfileLdap()"><i class="glyphicon glyphicon-trash"></i>';
                }
                counter++;
                t.row.add([
                    item.ldap_account_description.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                    item.ldap_account_user_name.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                    item.ldap_account_host_name.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                    urlLink
                ]).draw(false);
                    /*if (item.ldap_ssl_usage_status == 1) {
                        $('#configiosLdap #ldap_ssl_usage_status').attr('checked', 'checked');
                    }
                    $('#configiosLdap #ldap_account_description').val(item.ldap_account_description);
                    $('#configiosLdap #ldap_account_user_name').val(item.ldap_account_user_name);
                    $('#configiosLdap #ldap_account_password').val(item.ldap_account_password);
                    $('#configiosLdap #ldap_account_host_name').val(item.ldap_account_host_name);*/
            });
        }
    });
}

function confirmDeleteProfileLdap(){
    if ( confirm( "Are you sure you want to delete?" ) ) {
        $('#iosLdaptable').on( 'click', 'a.editor_remove', function (e) {
            e.preventDefault();
            var data = $(this).attr('profile_ldap_id');
            deleteLdap(data);
        });
    } else {
        return false;
    }
}

function deleteLdap(data){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#iosLdaptable').DataTable();
    $.ajax({
        type: 'POST',
        url: '/profileldapdelete',
        data: {_token: CSRF_TOKEN, profile_id: data},
        dataType: "json",
        cache: false,
        success: function (response) {
            if(response['authorisation']) {
                alert(response['authorisation']);
            } else {
                    table
                        .row($('#profileLdap_settings_' + data).parents('tr'))
                        .remove()
                        .draw();
            }
        }
    });
}

function updateIOSPassword(currentTab,nextTo){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var dataString = '';
        var allow_simple_ios_password_status = 0;
        var ios_password_alpha_numeric_value_require_status = 0;
        var ios_password_minmum_length = $('#configiosPassword #ios_password_minmum_length').val();
        var ios_password_minmum_complex_character_count = $('#configiosPassword #ios_password_minmum_complex_character_count').val();
        var ios_password_maximum_age = $('#configiosPassword #ios_password_maximum_age').val();
        var ios_device_auto_lock_status = $('#configiosPassword #ios_device_auto_lock_status').val();
        var ios_password_history_count = $('#configiosPassword #ios_password_history_count').val();
        var ios_device_lock_grace_period = $('#configiosPassword #ios_device_lock_grace_period').val();
        var ios_password_failed_attempts_count = $('#configiosPassword #ios_password_failed_attempts_count').val();
        if($('#configiosPassword #allow_simple_ios_password_status').is(':checked')) {
            allow_simple_ios_password_status = 1;
        }
        if($('#configiosPassword #ios_password_alpha_numeric_value_require_status').is(':checked')) {
            ios_password_alpha_numeric_value_require_status = 1;
        }
        dataString = 'profile_id='+$('#userprofileid').val()+'&id='+ $('#profileid').val() +'&allow_simple_ios_password_status='+allow_simple_ios_password_status+
        '&ios_password_alpha_numeric_value_require_status='+ios_password_alpha_numeric_value_require_status+'&ios_password_minmum_length='+ios_password_minmum_length+
        '&ios_password_minmum_complex_character_count='+ios_password_minmum_complex_character_count+'&ios_password_maximum_age='+ios_password_maximum_age+
        '&ios_device_auto_lock_status='+ios_device_auto_lock_status+'&ios_password_history_count='+ios_password_history_count+'&ios_device_lock_grace_period='+ios_device_lock_grace_period+
        '&ios_password_failed_attempts_count='+ios_password_failed_attempts_count+'&_token='+CSRF_TOKEN;
        // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/iosprofilepasswordupdate",
            data: dataString,
            datatype:'json',
            cache: false,
            success: function(response){
                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    $('#userprofileid').val(item.id);
                    $('#profileid').val(item.profile_id);
                });
                getProfilePasswordDetails($('#userprofileid').val());
                if(currentTab != nextTo) {
                    $('.'+nextTo).click();
                } else {
                    $('.iosHome').click();
                }
            }
        });
    }

    function getProfilePasswordDetails(profile_id) {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var dataString = 'profile_id=' + profile_id + '&_token=' + CSRF_TOKEN;
        $.ajax({
            type: 'POST',
            url: '/profilepasswordget',
            data: dataString,
            dataType: "json",
            cache: false,
            success: function (response) {
                var trHTML = '';
                var counter = 0;
                var username = '';
                var t = $('#iosPasswordtable').DataTable();
                t.clear();
                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    var urlLink = '';
                    if ($('#role_id').val() < 4) {
                        urlLink = ' <a href="" class="editor_remove" id="profilePass_settings_' + item.id + '" class="editor_remove" profile_pass_id="' + item.id + '" onClick="return confirmDeleteProfilePassword()"><i class="glyphicon glyphicon-trash"></i>';
                    }
                    counter++;
                    t.row.add([
                        item.ios_password_minmum_length,
                        item.ios_device_auto_lock_status.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                        item.ios_device_lock_grace_period.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                        urlLink
                    ]).draw(false);
/*
                    if (item.allow_simple_ios_password_status == 1) {
                        $('#configiosPassword #allow_simple_ios_password_status').attr('checked', 'checked');
                    }
                    if (item.ios_password_alpha_numeric_value_require_status == 1) {
                        $('#configiosPassword #ios_password_alpha_numeric_value_require_status').attr('checked', 'checked');
                    }
                    $('#configiosPassword #ios_password_minmum_length').val(item.ios_password_minmum_length);
                    $('#configiosPassword #ios_password_minmum_complex_character_count').val(item.ios_password_minmum_complex_character_count);
                    $('#configiosPassword #ios_password_maximum_age').val(item.ios_password_maximum_age);
                    $('#configiosPassword #ios_device_auto_lock_status').val(item.ios_device_auto_lock_status);
                    $('#configiosPassword #ios_password_history_count').val(item.ios_password_history_count);
                    $('#configiosPassword #ios_device_lock_grace_period').val(item.ios_device_lock_grace_period);
                    $('#configiosPassword #ios_password_failed_attempts_count').val(item.ios_password_failed_attempts_count);*/
                });
            }
        });
    }

    function confirmDeleteProfilePassword(){
        if ( confirm( "Are you sure you want to delete?" ) ) {
            $('#iosPasswordtable').on( 'click', 'a.editor_remove', function (e) {
                e.preventDefault();
                var data = $(this).attr('profile_pass_id');
                deletePassword(data);
            });
        } else {
            return false;
        }
    }

    function deletePassword(data){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var table = $('#iosPasswordtable').DataTable();
        $.ajax({
            type: 'POST',
            url: '/profilepassworddelete',
            data: {_token: CSRF_TOKEN, item_id: data},
            dataType: "json",
            cache: false,
            success: function (response) {
                if(response['authorisation']) {
                    alert(response['authorisation']);
                } else {
                    table
                        .row($('#profilePass_settings_' + data).parents('tr'))
                        .remove()
                        .draw();
                }
            }
        });
    }


function updateIOSVpn(currentTab,nextTo){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = '';
    var vpn_sent_all_traffic_status = 0;
    var vpn_connection_name = $('#configiosVpn #vpn_connection_name').val();
    var vpn_connection_type = $('#configiosVpn #vpn_connection_type').val();
    var vpn_server = $('#configiosVpn #vpn_server').val();
    var vpn_account  = $('#configiosVpn #vpn_account').val();
    var vpn_user_authentication_type  = $('#configiosVpn #vpn_user_authentication_type').val();
    var vpn_shared_secret  = $('#configiosVpn #vpn_shared_secret').val();
    var vpn_proxy  = $('#configiosVpn #vpn_proxy').val();
    if($('#configiosVpn #vpn_sent_all_traffic_status').is(':checked')) {
        vpn_sent_all_traffic_status = 1;
    }
    dataString = 'profile_id='+$('#userprofileid').val()+'&id='+ $('#profileid').val() +'&vpn_sent_all_traffic_status='+vpn_sent_all_traffic_status+
    '&vpn_connection_name='+vpn_connection_name+'&vpn_connection_type='+vpn_connection_type+'&vpn_server='+vpn_server+'&vpn_account='+vpn_account+
    '&vpn_user_authentication_type='+vpn_user_authentication_type+'&vpn_shared_secret='+vpn_shared_secret+'&vpn_proxy='+vpn_proxy+'&_token='+CSRF_TOKEN;
    // AJAX Code To Submit Form.
    $.ajax({
        type: "POST",
        url: "/iosprofilevpnupdate",
        data: dataString,
        datatype:'json',
        cache: false,
        success: function(response){
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                $('#userprofileid').val(item.id);
                $('#profileid').val(item.profile_id);
            });
            getProfileVpnDetails($('#userprofileid').val());
            if(currentTab != nextTo) {
                $('.'+nextTo).click();
            } else {
                $('.iosHome').click();
            }
        }
    });
}

function getProfileVpnDetails(profile_id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'profileId=' + profile_id + '&_token=' + CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/profilevpnget',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username = '';
            var t = $('#iosVpntable').DataTable();
            t.clear();
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                var urlLink = '';
                if ($('#role_id').val() < 4) {
                    urlLink = ' <a href="" class="editor_remove" id="profileVpn_settings_' + item.id + '" class="editor_remove" profile_vpn_id="' + item.id + '" onClick="return confirmDeleteProfileVpn()"><i class="glyphicon glyphicon-trash"></i>';
                }
                counter++;
                t.row.add([
                    item.vpn_connection_name.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                    item.vpn_connection_type.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                    item.vpn_server.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                    urlLink
                ]).draw(false);
                /*if (item.vpn_sent_all_traffic_status == 1) {
                    $('#configiosVpn #vpn_sent_all_traffic_status').attr('checked', 'checked');
                }
                $('#configiosVpn #vpn_connection_name').val(item.vpn_connection_name);
                $('#configiosVpn #vpn_connection_type').val(item.vpn_connection_type);
                $('#configiosVpn #vpn_server').val(item.vpn_server);
                $('#configiosVpn #vpn_account').val(item.vpn_account);
                $('#configiosVpn #vpn_user_authentication_type').val(item.vpn_user_authentication_type);
                $('#configiosVpn #vpn_shared_secret').val(item.vpn_shared_secret);
                $('#configiosVpn #vpn_proxy').val(item.vpn_proxy);*/
            });
        }
    });
}

function confirmDeleteProfileVpn(){
    if ( confirm( "Are you sure you want to delete?" ) ) {
        $('#iosVpntable').on( 'click', 'a.editor_remove', function (e) {
            e.preventDefault();
            var data = $(this).attr('profile_vpn_id');
            deleteVpn(data);
        });
    } else {
        return false;
    }
}

function deleteVpn(data){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#iosVpntable').DataTable();
    $.ajax({
        type: 'POST',
        url: '/profilevpndelete',
        data: {_token: CSRF_TOKEN, profile_id: data},
        dataType: "json",
        cache: false,
        success: function (response) {
            if(response['authorisation']) {
                alert(response['authorisation']);
            } else {
                table
                    .row($('#profileVpn_settings_' + data).parents('tr'))
                    .remove()
                    .draw();
            }
        }
    });
}

function updateIOSWifi(currentTab,nextTo){

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var dataString = '';
        var wifi_auto_join_status = 0;
        var wifi_service_set_identifier = $('#configiosWifi #wifi_service_set_identifier').val();
        var wifi_hidden_network = $('#configiosWifi #wifi_hidden_network').val();
        var wifi_security_type = $('#configiosWifi #wifi_security_type').val();
        var wifi_security_password = $('#configiosWifi #wifi_security_password').val();
        var wifi_proxy = $('#configiosWifi #wifi_proxy').val();
        if($('#configiosWifi #wifi_auto_join_status').is(':checked')) {
            wifi_auto_join_status = 1;
        }
        dataString = 'profile_id='+$('#userprofileid').val()+'&id='+ $('#profileid').val() +'&wifi_service_set_identifier='+wifi_service_set_identifier+
        '&wifi_auto_join_status='+wifi_auto_join_status+'&wifi_hidden_network='+wifi_hidden_network+'&wifi_security_type='+wifi_security_type+'&wifi_security_password='+wifi_security_password+
        '&wifi_proxy='+wifi_proxy+'&_token='+CSRF_TOKEN;
        // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/iosprofilewifiupdate",
            data: dataString,
            datatype:'json',
            cache: false,
            success: function(response){
                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    $('#userprofileid').val(item.id);
                    $('#profileid').val(item.profile_id);
                });
                getProfileWifiDetails($('#userprofileid').val());
                if(currentTab != nextTo) {
                    $('.'+nextTo).click();
                } else {
                    $('.iosHome').click();
                }
            }
        });
    }

    function getProfileWifiDetails(profile_id) {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var dataString = 'profileId=' + profile_id + '&profile_type=' + $('#profile_type').val() + '&_token=' + CSRF_TOKEN;
        $.ajax({
            type: 'POST',
            url: '/profilewifiget',
            data: dataString,
            dataType: "json",
            cache: false,
            success: function (response) {
                var trHTML = '';
                var counter = 0;
                var username = '';
                var t = $('#iosWifitable').DataTable();
                t.clear();
                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    var urlLink = '';
                    if ($('#role_id').val() < 4) {
                        urlLink = ' <a href="" class="editor_remove" id="profileWifi_settings_' + item.id + '" class="editor_remove" profile_wifi_id="' + item.id + '" onClick="return confirmDeleteProfileWifi()"><i class="glyphicon glyphicon-trash"></i>';
                    }
                    counter++;
                    t.row.add([
                        item.wifi_service_set_identifier.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                        item.wifi_hidden_network.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                        item.wifi_security_type.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                        urlLink
                    ]).draw(false);
                    /*if (item.wifi_auto_join_status == 1) {
                        $('#configiosWifi #wifi_auto_join_status').attr('checked', 'checked');
                    }
                    $('#configiosWifi #wifi_service_set_identifier').val(item.wifi_service_set_identifier);
                    $('#configiosWifi #wifi_hidden_network').val(item.wifi_hidden_network);
                    $('#configiosWifi #wifi_security_type').val(item.wifi_security_type);
                    $('#configiosWifi #wifi_security_password').val(item.wifi_security_password);
                    $('#configiosWifi #wifi_proxy').val(item.wifi_proxy);*/
                });
            }
        });
    }

    function confirmDeleteProfileWifi(){
        if ( confirm( "Are you sure you want to delete?" ) ) {
            $('#iosWifitable').on( 'click', 'a.editor_remove', function (e) {
                e.preventDefault();
                var data = $(this).attr('profile_wifi_id');
                deleteIosWifi(data);
            });
        } else {
            return false;
        }
    }

    function deleteIosWifi(data){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var table = $('#iosWifitable').DataTable();
        $.ajax({
            type: 'POST',
            url: '/profilewifidelete',
            data: {_token: CSRF_TOKEN,profile_type: $('#profile_type').val(), wifiId: data},
            dataType: "json",
            cache: false,
            success: function (response) {
                if(response['authorisation']) {
                    alert(response['authorisation']);
                } else {
                    table
                        .row($('#profileWifi_settings_' + data).parents('tr'))
                        .remove()
                        .draw();
                }
            }
        });
    }


function updateIOSCardDAV(currentTab,nextTo){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var dataString = '';
        var carddav_ssl_usage_status = 0;
        var carddav_account_description = $('#configiosDav #carddav_account_description').val();
        var carddav_account_host_name = $('#configiosDav #carddav_account_host_name').val();
        var carddav_port = $('#configiosDav #carddav_port').val();
        var carddav_princpal_url = $('#configiosDav #carddav_princpal_url').val();
        var carddav_account_user_name = $('#configiosDav #carddav_account_user_name').val();
        var carddav_account_password = $('#configiosDav #carddav_account_password').val();
        var vpn_proxy = $('#configiosDav #vpn_proxy').val();
    
    
    
        if($('#configiosDav #carddav_ssl_usage_status').is(':checked')) {
            carddav_ssl_usage_status = 1;
        }
        dataString = 'profile_id='+$('#userprofileid').val()+'&id='+ $('#profileid').val() +'&carddav_ssl_usage_status='+carddav_ssl_usage_status+
        '&carddav_account_description='+carddav_account_description+'&carddav_account_host_name='+carddav_account_host_name+'&carddav_port='+carddav_port+'&carddav_princpal_url='+carddav_princpal_url+
        '&carddav_account_user_name='+carddav_account_user_name+'&carddav_account_password='+carddav_account_password+'&vpn_proxy='+vpn_proxy+'&_token='+CSRF_TOKEN;
        // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/iosprofilecarddavupdate",
            data: dataString,
            datatype:'json',
            cache: false,
            success: function(response){
                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    $('#userprofileid').val(item.id);
                    $('#profileid').val(item.profile_id);
                });
                getProfileDavDetails($('#userprofileid').val());
                if(currentTab != nextTo) {
                    $('.'+nextTo).click();
                } else {
                    $('.iosHome').click();
                }
            }
        });
    }

    function getProfileDavDetails(profile_id) {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var dataString = 'profileId=' + profile_id + '&_token=' + CSRF_TOKEN;
        $.ajax({
            type: 'POST',
            url: '/profilecarddavget',
            data: dataString,
            dataType: "json",
            cache: false,
            success: function (response) {
                var trHTML = '';
                var counter = 0;
                var username = '';
                var t = $('#iosCardDavtable').DataTable();
                t.clear();
                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    var urlLink = '';
                    if ($('#role_id').val() < 4) {
                        urlLink = ' <a href="" class="editor_remove" id="profileDav_settings_' + item.id + '" class="editor_remove" profile_carddav_id="' + item.id + '" onClick="return confirmDeleteProfileDav()"><i class="glyphicon glyphicon-trash"></i>';
                    }
                    counter++;
                    t.row.add([
                        item.carddav_account_description.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                        item.carddav_account_host_name.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                        item.carddav_account_user_name.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                        urlLink
                    ]).draw(false);
                    /*if (item.ldap_ssl_usage_status == 1) {
                        $('#configiosDav #ldap_ssl_usage_status').attr('checked', 'checked');
                    }
                    $('#configiosDav #carddav_account_description').val(item.carddav_account_description);
                    $('#configiosDav #carddav_account_host_name').val(item.carddav_account_host_name);
                    $('#configiosDav #carddav_port').val(item.carddav_port);
                    $('#configiosDav #carddav_princpal_url').val(item.carddav_princpal_url);
                    $('#configiosDav #carddav_account_user_name').val(item.carddav_account_user_name);
                    $('#configiosDav #carddav_account_password').val(item.carddav_account_password);
                    $('#configiosDav #vpn_proxy').val(item.vpn_proxy);*/
                });
            }
        });
    }

    function confirmDeleteProfileDav(){
        if ( confirm( "Are you sure you want to delete?" ) ) {
            $('#iosCardDavtable').on( 'click', 'a.editor_remove', function (e) {
                e.preventDefault();
                var data = $(this).attr('profile_carddav_id');
                deleteDav(data);
            });
        } else {
            return false;
        }
    }

    function deleteDav(data){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var table = $('#iosCardDavtable').DataTable();
        $.ajax({
            type: 'POST',
            url: '/profilecarddavdelete',
            data: {_token: CSRF_TOKEN, profile_id: data},
            dataType: "json",
            cache: false,
            success: function (response) {
                if(response['authorisation']) {
                    alert(response['authorisation']);
                } else {
                    table
                        .row($('#profileDav_settings_' + data).parents('tr'))
                        .remove()
                        .draw();
                }
            }
        });
    }


function updateIOSWebclip(currentTab,nextTo){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var dataString = '';
        var webclip_removable_status = 0;
        var webclip_precomposed_icon_status = 0;
        var webclip_fullscreen_status = 0;
        var webclip_label = $('#configiosWebclip #webclip_label').val();
        var webclip_url = $('#configiosWebclip #webclip_url').val();
        var webclip_icon = $('#configiosWebclip #webclip_icon').val();

        if($('#configiosWebclip #webclip_removable_status').is(':checked')) {
            webclip_removable_status = 1;
        }
        if($('#configiosWebclip #webclip_precomposed_icon_status').is(':checked')) {
            webclip_precomposed_icon_status = 1;
        }
        if($('#configiosWebclip #webclip_fullscreen_status').is(':checked')) {
            webclip_fullscreen_status = 1;
        }

        var formData = new FormData();
        formData.append('file',$('#webclip_icon').prop('files')[0]);
        formData.append('profile_id', $('#userprofileid').val());
        formData.append('id', $('#profileid').val());
        formData.append('webclip_removable_status', webclip_removable_status);
        formData.append('webclip_precomposed_icon_status', webclip_precomposed_icon_status);
        formData.append('webclip_fullscreen_status', webclip_fullscreen_status);
        formData.append('webclip_label', webclip_label);
        formData.append('webclip_url', webclip_url);
        formData.append('_token',CSRF_TOKEN);
        // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/iosprofilewebclipupdate",
            data: formData,
            datatype:'json',
            cache: false,
            contentType: false,
            processData: false,
            success: function(response){
                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    $('#userprofileid').val(item.id);
                    $('#profileid').val(item.profile_id);
                });
                getProfileWebclipDetails($('#userprofileid').val());
                if(currentTab != nextTo) {
                    $('.'+nextTo).click();
                } else {
                    $('.iosHome').click();
                }
            }
        });
    }

    function getProfileWebclipDetails(profile_id) {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var dataString = 'profile_id=' + profile_id + '&_token=' + CSRF_TOKEN;
        $.ajax({
            type: 'POST',
            url: '/profilewebclipget',
            data: dataString,
            dataType: "json",
            cache: false,
            success: function (response) {
                var trHTML = '';
                var counter = 0;
                var username = '';
                var t = $('#iosWebcliptable').DataTable();
                t.clear();
                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    var message = "<img src="+item.webclip_icon+">";
                    var messageList = "<img src="+item.webclip_icon+" width='30' height='30'>";
                    var urlLink = '';
                    if ($('#role_id').val() < 4) {
                        urlLink = ' <a href="" class="editor_remove" id="profileWebclip_settings_' + item.id + '" class="editor_remove" profile_webclip_id="' + item.id + '" onClick="return confirmDeleteProfileWebclip()"><i class="glyphicon glyphicon-trash"></i>';
                    }
                    counter++;
                    t.row.add([
                        item.webclip_label.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                        item.webclip_url.replace(/&/g, "%amp%amp;").replace(/%amp%/g, "&"),
                        messageList,
                        urlLink
                    ]).draw(false);
                    /*if (item.webclip_removable_status == 1) {
                        $('#configiosWebclip #webclip_removable_status').attr('checked', 'checked');
                    }
                    if (item.webclip_precomposed_icon_status == 1) {
                        $('#configiosWebclip #webclip_precomposed_icon_status').attr('checked', 'checked');
                    }
                    if (item.webclip_fullscreen_status == 1) {
                        $('#configiosWebclip #webclip_fullscreen_status').attr('checked', 'checked');
                    }
                    $('#configiosWebclip #webclip_icon_show').html(message);
                    $('#configiosWebclip #webclip_label').val(item.webclip_label);
                    $('#configiosWebclip #webclip_url').val(item.webclip_url);
                    $('#configiosWebclip #webclip_icon').val(item.webclip_icon);*/
                });
            }
        });
    }

    function confirmDeleteProfileWebclip(){
        if ( confirm( "Are you sure you want to delete?" ) ) {
            $('#iosWebcliptable').on( 'click', 'a.editor_remove', function (e) {
                e.preventDefault();
                var data = $(this).attr('profile_webclip_id');
                deleteWebclip(data);
            });
        } else {
            return false;
        }
    }

    function deleteWebclip(data){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var table = $('#iosWebcliptable').DataTable();
        $.ajax({
            type: 'POST',
            url: '/profilewebclipdelete',
            data: {_token: CSRF_TOKEN, webclipId: data},
            dataType: "json",
            cache: false,
            success: function (response) {
                if(response['authorisation']) {
                    alert(response['authorisation']);
                } else {
                    table
                        .row($('#profileWebclip_settings_' + data).parents('tr'))
                        .remove()
                        .draw();
                }
            }
        });
    }

    function updateTabSetting(){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var dataString = '';
        if($('#profile_type_admin').val() == 2){
            var iosHome = 0;
            var iosActiveSyn = 0;
            var iosAdvancedrestriction = 0;
            var iosApn = 0;
            var iosCredentials = 0;
            var iosEmailsettings = 0;
            var iosKiosksettings = 0;
            var iosLdap = 0;
            var iosPassword = 0;
            var iosVpn = 0;
            var iosWebfilter = 0;
            var iosWifi = 0;
            var iosCardDAV = 0;
            var iosWebclip = 0;

            if($('#configios #iosHome').is(':checked')) {
                iosHome = 1;
            }
            if($('#configios #iosActiveSyn').is(':checked')) {
                iosActiveSyn = 1;
            }
            if($('#configios #iosAdvancedrestriction').is(':checked')) {
                iosAdvancedrestriction = 1;
            }
            if($('#configios #iosApn').is(':checked')) {
                iosApn = 1;
            }
            if($('#configios #iosCredentials').is(':checked')) {
                iosCredentials = 1;
            }
            if($('#configios #iosEmailsettings').is(':checked')) {
                iosEmailsettings = 1;
            }
            if($('#configios #iosKiosksettings').is(':checked')) {
                iosKiosksettings = 1;
            }
            if($('#configios #iosLdap').is(':checked')) {
                iosLdap = 1;
            }
            if($('#configios #iosPassword').is(':checked')) {
                iosPassword = 1;
            }
            if($('#configios #iosVpn').is(':checked')) {
                iosVpn = 1;
            }
            if($('#configios #iosWebfilter').is(':checked')) {
                iosWebfilter = 1;
            }
            if($('#configios #iosWifi').is(':checked')) {
                iosWifi = 1;
            }
            if($('#configios #iosCardDAV').is(':checked')) {
                iosCardDAV = 1;
            }
            if($('#configios #iosWebclip').is(':checked')) {
                iosWebclip = 1;
            }
            dataString = 'profile_type='+$('#profile_type_admin').val()+'&tab[iosHome]='+iosHome+
            '&tab[iosActiveSyn]='+iosActiveSyn+'&tab[iosAdvancedrestriction]='+iosAdvancedrestriction+'&tab[iosApn]='+iosApn+'&tab[iosCredentials]='+iosCredentials+
            '&tab[iosEmailsettings]='+iosEmailsettings+'&tab[iosKiosksettings]='+iosKiosksettings+'&tab[iosLdap]='+iosLdap+
            '&tab[iosPassword]='+iosPassword+'&tab[iosVpn]='+iosVpn+'&tab[iosWebfilter]='+iosWebfilter+'&tab[iosWifi]='+iosWifi+'&tab[iosCardDAV]='+iosCardDAV+'&tab[iosWebclip]='+iosWebclip+'&_token='+CSRF_TOKEN;
        }else if($('#profile_type_admin').val() == 1){

            var home = 0;
            var wifi = 0;
            var packagedisabler = 0;
            var kiosksettings = 0;
            var applicationsettings = 0;
            var emailsettings = 0;
            var activeSyn = 0;
            var commands = 0;

            if($('#configandroid #home').is(':checked')) {
                home = 1;
            }
            if($('#configandroid #wifi').is(':checked')) {
                wifi = 1;
            }
            if($('#configandroid #packagedisabler').is(':checked')) {
                packagedisabler = 1;
            }
            if($('#configandroid #kiosksettings').is(':checked')) {
                kiosksettings = 1;
            }
            if($('#configandroid #applicationsettings').is(':checked')) {
                applicationsettings = 1;
            }
            if($('#configandroid #emailsettings').is(':checked')) {
                emailsettings = 1;
            }
            if($('#configandroid #activeSyn').is(':checked')) {
                activeSyn = 1;
            }
            if($('#configandroid #commands').is(':checked')) {
                commands = 1;
            }
            dataString = 'profile_type='+$('#profile_type_admin').val()+'&tab[home]='+home+
            '&tab[wifi]='+wifi+'&tab[packagedisabler]='+packagedisabler+'&tab[kiosksettings]='+kiosksettings+'&tab[applicationsettings]='+applicationsettings+
            '&tab[emailsettings]='+emailsettings+'&tab[activeSyn]='+activeSyn+'&tab[commands]='+commands+'&_token='+CSRF_TOKEN;
        }
        // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/updateadminprofiletab",
            data: dataString,
            datatype:'json',
            success: function(response){
                window.location.replace("/dashboard");
            }
        });
    }

    function getTabSetting(){
        $.ajax({
            type: "POST",
            url: "/getabsetting",
            data: {_token: $('meta[name="csrf-token"]').attr('content')},
            datatype:'json',
            success: function(response){

                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    if(item.tab_name == 'iosHome' && item.tab_status == 0){
                        $('#configios #iosHome').removeAttr('checked');
                        $('#configios #iosHome').val(0);
                    }
                    if(item.tab_name == 'iosActiveSyn' && item.tab_status  == 0){
                        $('#configios #iosActiveSyn').removeAttr('checked');
                        $('#configios #iosActiveSyn').val(0);
                    }
                    if(item.tab_name == 'iosAdvancedrestriction' && item.tab_status  == 0){
                        $('#configios #iosAdvancedrestriction').removeAttr('checked');
                        $('#configios #iosAdvancedrestriction').val(0);
                    }
                    if(item.tab_name == 'iosApn' && item.tab_status  == 0){
                        $('#configios #iosApn').removeAttr('checked');
                        $('#configios #iosApn').val(0);
                    }
                    if(item.tab_name == 'iosCredentials' && item.tab_status  == 0){
                        $('#configios #iosCredentials').removeAttr('checked');
                        $('#configios #iosCredentials').val(0);
                    }
                    if(item.tab_name == 'iosEmailsettings' && item.tab_status  == 0){
                        $('#configios #iosEmailsettings').removeAttr('checked');
                        $('#configios #iosEmailsettings').val(0);
                    }
                    if(item.tab_name == 'iosKiosksettings' && item.tab_status  == 0){
                        $('#configios #iosKiosksettings').removeAttr('checked');
                        $('#configios #iosKiosksettings').val(0);
                    }
                    if(item.tab_name == 'iosLdap' && item.tab_status  == 0){
                        $('#configios #iosLdap').removeAttr('checked');
                        $('#configios #iosLdap').val(0);
                    }
                    if(item.tab_name == 'iosPassword' && item.tab_status  == 0){
                        $('#configios #iosPassword').removeAttr('checked');
                        $('#configios #iosPassword').val(0);
                    }
                    if(item.tab_name == 'iosVpn' && item.tab_status  == 0){
                        $('#configios #iosVpn').removeAttr('checked');
                        $('#configios #iosVpn').val(0);
                    }
                    if(item.tab_name == 'iosWebfilter' && item.tab_status  == 0){
                        $('#configios #iosWebfilter').removeAttr('checked');
                        $('#configios #iosWebfilter').val(0);
                    }
                    if(item.tab_name == 'iosWifi' && item.tab_status  == 0){
                        $('#configios #iosWifi').removeAttr('checked');
                        $('#configios #iosWifi').val(0);
                    }
                    if(item.tab_name == 'iosCardDAV' && item.tab_status  == 0){
                        $('#configios #iosCardDAV').removeAttr('checked');
                        $('#configios #iosCardDAV').val(0);
                    }
                    if(item.tab_name == 'iosWebclip' && item.tab_status  == 0){
                        $('#configios #iosWebclip').removeAttr('checked');
                        $('#configios #iosWebclip').val(0);
                    }
                    if(item.tab_name == 'home' && item.tab_status  == 0){
                        $('#configandroid #home').removeAttr('checked');
                        $('#configandroid #home').val(0);
                    }
                    if(item.tab_name == 'wifi' && item.tab_status  == 0){
                        $('#configandroid #wifi').removeAttr('checked');
                        $('#configandroid #wifi').val(0);
                    }
                    if(item.tab_name == 'packagedisabler' && item.tab_status  == 0){
                        $('#configandroid #packagedisabler').removeAttr('checked');
                        $('#configandroid #packagedisabler').val(0);
                    }
                    if(item.tab_name == 'kiosksettings' && item.tab_status  == 0){
                        $('#configandroid #kiosksettings').removeAttr('checked');
                        $('#configandroid #kiosksettings').val(0);
                    }
                    if(item.tab_name == 'applicationsettings' && item.tab_status  == 0){
                        $('#configandroid #applicationsettings').removeAttr('checked');
                        $('#configandroid #applicationsettings').val(0);
                    }
                    if(item.tab_name == 'emailsettings' && item.tab_status  == 0){
                        $('#configandroid #emailsettings').removeAttr('checked');
                        $('#configandroid #emailsettings').val(0);
                    }
                    if(item.tab_name == 'activeSyn' && item.tab_status  == 0){
                        $('#configandroid #activeSyn').removeAttr('checked');
                        $('#configandroid #activeSyn').val(0);
                    }
                    if(item.tab_name == 'commands' && item.tab_status  == 0){
                        $('#configandroid #commands').removeAttr('checked');
                        $('#configandroid #commands').val(0);
                    }
                });
            }
        });
    }

    function getCertificates(profileId){
        $.ajax({
            type: "POST",
            url: "/getcertificatesetting",
            data: {profileId: profileId, _token: $('meta[name="csrf-token"]').attr('content')},
            datatype:'json',
            success: function(response) {
                var trHTML = '';
                var counter = 0;
                var username = '';
                var t = $('#ioscertificatetable').DataTable();
                t.clear();
                $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                    var urlLink = '';
                    if($('#role_id').val() < 4) {
                        urlLink = ' <a href="" class="editor_remove" id="profile_certificate_' + item.id + '" certificateId="'+item.id+'" onClick="return deleteCertificate()"><i class="glyphicon glyphicon-trash"></i>';
                    }
                    counter++;
                    t.row.add([
                        item.certificate_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                        urlLink
                    ]).draw(false);
                });
            }
        });
    }

    function deleteCertificate() {
            if ( confirm( "Are you sure you want to delete?" ) ) {
                $('#ioscertificatetable').on( 'click', 'a.editor_remove', function (e) {
                    e.preventDefault();
                    var data = $(this).attr('certificateId');
                    deleteIosCertificate(data);
                });
            } else {
                return false;
            }
    }

    function deleteIosCertificate(data){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var table = $('#ioscertificatetable').DataTable();
        $.ajax({
            type: 'POST',
            url: '/profilecertificatedelete',
            data: {_token: CSRF_TOKEN, certificateId: data},
            dataType: "json",
            cache: false,
            success: function (response) {
                if(response['authorisation']) {
                    alert(response['authorisation']);
                } else {
                    table
                        .row($('#profile_certificate_' + data).parents('tr'))
                        .remove()
                        .draw();
                }
            }
        });
    }

    function UpdateWebfiler(currentTab,nextTo){
        var dataString = '';
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var url = '';
        var isValid = true;
        var url_type = 0;
        var restrict_inappropriate_content  = 0;
        if(currentTab == 'blackList'){
            url = $('#blacklist_url_name').val();
            if($('#blacklist_restricted_inappropriate_content').is(':checked')) {
                restrict_inappropriate_content = 1;
            }
            var UrlArray = url.split(",");
            $.each(JSON.parse(JSON.stringify(UrlArray)), function (i, item) {
                if(validateUrl(item) == false || url == ''){
                    $('#_urlblerror').show();
                    $('#_urlwlerror').hide();
                    isValid = false;
                }
            });
            url_type = 1;

        }else if(currentTab == 'whiteList'){
            url = $('#whitelist_url_name').val();
            var UrlArray = url.split(",");
            $.each(JSON.parse(JSON.stringify(UrlArray)), function (i, item) {
                if(validateUrl(item) == false || url == ''){
                    $('#_urlwlerror').show();
                    $('#_urlblerror').hide();
                    isValid = false;
                }
            });
            url_type = 2;
        }
        dataString = 'profile_id='+$('#userprofileid').val()+'&restrict_inappropriate_content='+ restrict_inappropriate_content +'&url='+url+
        '&currentTab='+currentTab+'&url_type='+url_type+'&_token='+CSRF_TOKEN;
        if(isValid){
            $.ajax({
                type: 'POST',
                url: '/updateioswebfiler',
                data: dataString,
                dataType: "json",
                cache: false,
                success: function (response) {
                    getWebfilerUrlList(url_type,currentTab,$('#userprofileid').val());
                    if(currentTab != nextTo) {
                        $('.'+nextTo).click();
                    } else {
                        $('.iosHome').click();
                    }
                    $('#_urlwlerror').hide();
                    $('#_urlblerror').hide();
                }
            });
        }
    }

    function getWebfilerUrlList(url_type,currentTab,profile_id,initialLoad){
        var dataString = '';
        dataString = 'profile_id='+profile_id+'&url_type='+url_type+'&_token='+$('meta[name="csrf-token"]').attr('content');
        $.ajax({
            type: "POST",
            url: "/getwebfilerurllist",
            data: dataString,
            datatype:'json',
            success: function(response) {
                var trHTML = '';
                var counter = 0;
                var t ='';
                var url = '';
                var comma = '';
                if(currentTab == 'blackList'){
                    t = $('#iosWebfilerBlacklistUrlTable').DataTable();
                    t.clear();
                    $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                        if(counter >= 1){
                            comma = ',';
                        }
                        url = url+comma+item.webfiler_url ;
                        var urlLink = '';
                        if($('#role_id').val() < 4) {
                            urlLink = ' <a href="" class="editor_remove" id="profile_webfiler_bl_' + item.id + '" blacklist_id="'+item.id+'" currentTab="'+currentTab+'" onClick="return deleteBLUrlConfirm()"><i class="glyphicon glyphicon-trash"></i>';
                        }
                        counter++;
                        t.row.add([
                            item.webfiler_url .replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                            urlLink
                        ]).draw(false);
                        if (item.webfiler_url_restrict_content_status == 1) {
                            $('#blacklist_restricted_inappropriate_content').attr('checked', 'checked');
                        }
                    });
                    $('#blacklist_url_name').html(url);
                }else if(currentTab == 'whiteList'){
                    t = $('#iosWebfilerWhitelistUrlTable').DataTable();
                    t.clear();
                    $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                        if(counter >= 1){
                            comma = ',';
                        }
                        url = url+comma+item.webfiler_url;
                        var urlLink = '';
                        if($('#role_id').val() < 4) {
                            urlLink = ' <a href="" class="editor_remove" id="profile_webfiler_wl_' + item.id + '" whitelist_id="'+item.id+'" currentTab="'+currentTab+'" onClick="return deleteWLUrlConfirm()"><i class="glyphicon glyphicon-trash"></i>';
                        }
                        counter++;
                        t.row.add([
                            item.webfiler_url.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                            urlLink
                        ]).draw(false);
                    });
                    $('#whitelist_url_name').html(url);
                }
                if(initialLoad == 0){
                    $('#ios_webfiler_type_bl').attr('checked',true);
                    $('#blacklist_div').show();
                }
            }
        });
    }

    function deleteBLUrlConfirm(){
        if ( confirm( "Are you sure you want to delete?" ) ) {
            $('#iosWebfilerBlacklistUrlTable').on( 'click', 'a.editor_remove', function (e) {
                e.preventDefault();
                var data = $(this).attr('blacklist_id');
                var currentTab = $(this).attr('currentTab');
                var tableName = 'iosWebfilerBlacklistUrlTable';
                var rowName = 'profile_webfiler_bl_';
                deleteUrl(currentTab,data,tableName,rowName);
            });
        } else {
            return false;
        }
    }

    function deleteWLUrlConfirm(){
        if ( confirm( "Are you sure you want to delete?" ) ) {
            $('#iosWebfilerWhitelistUrlTable').on( 'click', 'a.editor_remove', function (e) {
                e.preventDefault();
                var data = $(this).attr('whitelist_id');
                var currentTab = $(this).attr('currentTab');
                var tableName = 'iosWebfilerWhitelistUrlTable';
                var rowName = 'profile_webfiler_wl_';
                deleteUrl(currentTab,data,tableName,rowName);
            });
        } else {
            return false;
        }
    }


function deleteUrl(currentTab,data,tableName,rowName) {
        var dataString = '';
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var table = $('#'+tableName).DataTable();
        dataString = 'currentTab='+currentTab+'&_token='+CSRF_TOKEN+'&itemId='+data;
        $.ajax({
            type: 'POST',
            url: '/webfilerurldelete',
            data: dataString,
            dataType: "json",
            cache: false,
            success: function (response) {
                if(response['authorisation']) {
                    alert(response['authorisation']);
                } else {
                    table
                        .row($('#'+rowName + data).parents('tr'))
                        .remove()
                        .draw();
                }
            }
        });
    }

    function validateUrl(textval)   // return true or false.
    {
        var urlregex = new RegExp(
            "^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
        return urlregex.test(textval);
    }













