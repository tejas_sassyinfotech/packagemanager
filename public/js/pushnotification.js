var buttonDisable = 0;
$(document).ready(function() {
    $('.pushnotification').click(function(){
        if (buttonDisable == 0) {
            if ($(this).attr('id') == 'refresh') {
                disableDeviceButtons($(this).attr('id'));
                setDevicePushNotifications($('#profileid').val(), $('#deviceid').val(), $(this).attr('command'));
            } else if ($(this).attr('id') == 'reboot') {
                if($('#addProfile').val()) {
                    if (confirm("Are you sure you want to do the reboot activity?")) {
                        setDevicePushNotifications($('#addProfile').val(), '', $(this).attr('command'));
                    }
                } else {
                    alert('Please select one profile to reboot');
                }
            }else {
                if (confirm("Are you sure you want to do the device activity?")) {
                    disableDeviceButtons($(this).attr('id'));
                    setDevicePushNotifications($('#profileid').val(), $('#deviceid').val(), $(this).attr('command'));
                }
            }
        }
    });
});

function setDevicePushNotifications(profileId, deviceId, command) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'profile_id='+ profileId + '&device_id='+ deviceId + '&command='+ command + '&_token='+CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/notification',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            if(command == 'refresh') {
                window.location.reload();
            } else if(command == 'reboot') {
                alert('Reboot done successfully');
            }
        }
    });
}

function disableDeviceButtons(buttonid) {
    $(this).click(function () {
        $('.btn').attr("disabled", false);
        $("#"+buttonid).attr("disabled", true);
        buttonDisable = 1;
        setTimeout(function() {
            $("#"+buttonid).removeAttr("disabled");
            buttonDisable = 0;
        }, 15000);
    });
}