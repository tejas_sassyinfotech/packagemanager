$(document).ready(function() {
    $('#err').hide();
    $('#licencetable').dataTable();
    $('.navigator').removeClass('active');
    $('#licences').addClass('active');
    getlicenses();
    getProfiles();
});
function addlicense(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'licence_name=' + $('#name').val() + '&license_code=' + $('#code').val() + '&licence_description=' + $('#licencedesc').val() + '&quantity=' + $('#qty').val()+ '&profile_id='+ $('#profilelist').val()+'&_token=' + CSRF_TOKEN;
    $.ajax({
        type: "POST",
        url: "/licensecreate",
        data: dataString,
        datatype: 'json',
        cache: false,
        success: function (response) {
            $('#name').val('');
            $('#code').val('');
            $('#licencedesc').val('');
            $('#profilelist').val(0);
            $('#qty').val('');
            getlicenses();
        }
    });
}
function getlicenses() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/licenselist',
        data: {_token: CSRF_TOKEN},
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username ='';
            var t = $('#licencetable').DataTable();
            t.clear();
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                var urlLink = '';
                if($('#role_id').val() < 4) {
                    urlLink = ' <a href="" id="licence_'+item.id+'" class="editor_remove" licenseId="'+item.id+'" onClick="licenseoperation()" ><i class="glyphicon glyphicon-trash"></i></a>';
                }
                counter ++;
                t.row.add( [
                    item.licence_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.licence_code.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.user_profile.profile_name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.maximum_quantity,
                    item.used_quantity,
                    item.created_at,
                    urlLink
                ] ).draw( false );
            });
        }
    });
}
function licenseoperation(){
    if ( confirm( "Are you sure you want to delete this License?" ) ) {
        $('#licencetable').on( 'click', 'a.editor_remove', function (e) {
            e.preventDefault();
            var data = $(this).attr('licenseId');
            deleteLicense(data);
        });
    }
}
function deleteLicense(data) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#licencetable').DataTable();
    $.ajax({
        type: 'POST',
        url: '/licencedelete',
        data: {_token: CSRF_TOKEN, licenseId: data},
        dataType: "json",
        cache: false,
        success: function (response) {
            var posts = JSON.parse(JSON.stringify(response));
            table
                .row( $('#licence_'+data).parents('tr') )
                .remove()
                .draw();
            if(posts){
            var message = posts+'  Deleted';
                $('#del-license').show();
                $('#del-license').html(message);
            }
        }
    });
}

function getProfiles() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/profilelist',
        data: {_token: CSRF_TOKEN},
        dataType: "json",
        cache: false,
        success: function (response) {
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                $('#profilelist').append($('<option>', {
                    value: item.id,
                    text : item.profile_name
                }));
            });
        }
    });
}

