$(document).ready(function() {
    $('#err').hide();
    $('#licencetable').dataTable();
    $('.navigator').removeClass('active');
    $('#usermanagement').addClass('active');
    getUsers();
    getRoles();
    if($('.log-user-id').val()) {
        getTestUsers();
    }
    $('#rolelist').change(function(){
        if($('#rolelist').val() == 3) {
            $('#adminlist').show();
            getAdminUsers(2);
        } else if($('#rolelist').val() == 4) {
            $('#adminlist').show();
            $('#testuserlist').show();
            getAdminUsers(2);
        } else {
            $('#adminlist').hide();
            $('#testuserlist').hide();
            $('.test-list').hide();
            $('.admin-list').hide();

        }
    });
    $('#adminlist').change(function(){
        getTestUsers();
    });
    $('#adduser').click(function(){
        addUser();
    });
    $('#updateuser').click(function(){
        updateUser();
    });
});
function getUsers() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/userlist',
        data: {_token: CSRF_TOKEN},
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username ='';
            var urlLink = '';
            var t = $('#tableuser').DataTable();
            t.clear();
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                if($('#role_id').val() < 3) {
                    urlLink = ' <a href="/useredit/'+item.id+'"><i class="glyphicon glyphicon-pencil"></i></a><a href="" class="editor_remove" id="User_settings_' + item.id + '" class="editor_remove" user_id="'+item.id+'" onClick="return confirmDeleteUser()"><i class="glyphicon glyphicon-trash"></i>/'
                }else if($('#role_id').val() == 5 || $('#role_id').val() == 3 || $('#role_id').val() == 4){
                    urlLink = ' <a href="/usermanagement/'+item.id+'" id="usermanagement'+item.id+'" userName="'+item.name+'" class="user_view"><i class="glyphicon glyphicon-eye-open"></i></a>'
                }
                counter ++;
                t.row.add( [
                    item.name.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.email.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.role.name,
                    urlLink
                ] ).draw( false );
            });
        }
    });
}

function confirmDeleteUser(){
    if ( confirm( "Are you sure you want to delete this User?" ) ) {
        $('#tableuser').on( 'click', 'a.editor_remove', function (e) {
            e.preventDefault();
            var data = $(this).attr('user_id');
            deleteUser(data);
        });
    } else {
        return false;
    }
}

function deleteUser(data){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var table = $('#tableuser').DataTable();
    $.ajax({
        type: 'POST',
        url: '/userdelete',
        data: {_token: CSRF_TOKEN, userId: data},
        dataType: "json",
        cache: false,
        success: function (response) {
            if(response['authorisation']) {
                alert(response['authorisation']);
            } else {
                table
                    .row($('#User_settings_' + data).parents('tr'))
                    .remove()
                    .draw();
            }
        }
    });
}


function getRoles() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/rolelist',
        data: {_token: CSRF_TOKEN},
        dataType: "json",
        cache: false,
        success: function (response) {
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                $('#rolelist').append($('<option>', {
                    value: item.id,
                    text : item.name
                }));
            });
        }
    });
}
function getAdminUsers(type) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $('#adminlist').html('');
    $('#adminlist').append($('<option>', {
        value: 0,
        text : '-Select Administrator-'
    }));
    $.ajax({
        type: 'POST',
        url: '/userlistbyrole',
        data: {_token: CSRF_TOKEN, roleId: type},
        dataType: "json",
        cache: false,
        success: function (response) {
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                $('#adminlist').append($('<option>', {
                    value: item.id,
                    text : item.name
                }));
            });
            if(type == 1 || type == 2) {
                $('#adminlist').show();
                $('#testuserlist').hide();
                $('.admin-list').hide();
                $('.testuser-list').hide();
            } else {
                $('#adminlist').show();
                $('#testuserlist').show();
                $('.admin-list').hide();
                $('.testuser-list').hide();
            }

            if($('#rolelist').val() == 3) {
                $('#adminlist').show();
            }
            if($('#rolelist').val() == 4) {
                $('#adminlist').show();
                $('#testuserlist').show();
            }
        }
    });
}
function getTestUsers() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $('#testuserlist').html('');
    $('#testuserlist').append($('<option>', {
        value: 0,
        text : '-Select Test User-'
    }));
    $.ajax({
        type: 'POST',
        url: '/testuserlist',
        data: {_token: CSRF_TOKEN, adminId: $('#adminlist').val()},
        dataType: "json",
        cache: false,
        success: function (response) {
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                $('#testuserlist').append($('<option>', {
                    value: item.id,
                    text : item.name
                }));
            });
        }
    });
}
function addUser() {
    $('.username_error').html('');
    $('.useremail_error').html('');
    $('.adminlist_error').html('');
    $('.testuserlist_error').html('');
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'role_id=' + $('#rolelist').val() + '&admin_id=' + $('#adminlist').val() + '&testuser_id=' + $('#testuserlist').val() + '&name=' + $('#username').val()+ '&email='+ $('#useremail').val()+'&_token=' + CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/userroleadd',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            if(response.name) {
                $('.username_error').html(response.name);
            }
            if(response.email) {
                $('.useremail_error').html(response.email);
            }
            if(response['admin_null']) {
                $('.adminlist_error').html(response['admin_null']);
            }
            if(response['testuser_null']) {
                $('.testuserlist_error').html(response['testuser_null']);
            }
            if(response['success']) {
                $('.success-message').html(response['success']);
                window.location = '/usermanagement';
            }

        }
    });
}
function updateUser() {
    $('.username_error').html('');
    $('.useremail_error').html('');
    $('.adminlist_error').html('');
    $('.testuserlist_error').html('');
    $('.success-message').html('');
    var admin_id = $('#adminlist').val();
    var test_id = $('#testuserlist').val();
    if(admin_id == 0) {
        admin_id =  $('.admin-list').val();
    }
    if(test_id == 0) {
        test_id = $('.test-list').val();
    }
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'user_id=' + $('#userid').val() +'&role_id=' + $('#rolelist').val() + '&admin_id=' + admin_id + '&testuser_id=' + test_id + '&name=' + $('#username').val()+ '&email='+ $('#useremail').val()+'&user_password=' + $('#userpassword').val()+'&_token=' + CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/userroleupdate',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            if(response.name) {
                $('.username_error').html(response.name);
            }
            if(response.email) {
                $('.useremail_error').html(response.email);
            }
            if(response['admin_null']) {
                $('.adminlist_error').html(response['admin_null']);
            }
            if(response['testuser_null']) {
                $('.testuserlist_error').html(response['testuser_null']);
            }
            if(response['success']) {
                $('.success-message').html(response['success']);
                window.location = '/usermanagement';
            }

        }
    });
}
function valueselect(role_id)
{
    if(role_id == 3) {
        $('.admin-list').remove();
        $('.test-list').remove();
    } else if (role_id == 4) {
        $('.admin-list').remove();
        $('.test-list').remove();
    }

}