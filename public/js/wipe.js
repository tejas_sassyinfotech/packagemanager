$(document).ready(function() {
    $('#wipetable').dataTable();
    $('.navigator').removeClass('active');
    $('#registration').addClass('active');
    getAllProfiles();
    $('#profilesearch').click(function(){
        var profileId = $('#profiles').val();
        if(profileId){
            $('#export_device_wipe').attr('href', '/devicewipeexcel/'+profileId);
        }else{
            $('#export_device_wipe').attr('href', '/devicewipeexcel');
        }
        getDeviceWipes(profileId);
    });
});

function getAllProfiles() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/allprofilelist',
        data: {_token: CSRF_TOKEN},
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username = '';
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                $('#profiles').append($('<option>', {
                    value: item.id,
                    text : item.profile_name
                }));

            });
        }
    });
}

function getDeviceWipes() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataString = 'profileId='+$('#profiles').val()+'&_token=' + CSRF_TOKEN;
    $.ajax({
        type: 'POST',
        url: '/devicewipelist',
        data: dataString,
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var t = $('#wipetable').DataTable();
            t.clear().draw();
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                var regStatus = '';
                if(item.registration_status == 1) {
                    regStatus = 'Active';
                } else {
                    regStatus = 'End';
                }
                counter ++;
                t.row.add( [
                    item.device_identifier.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.device_serial_number.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.wipe_description.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.wipe_result.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.wipe_reason.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.wipe_additional_info.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.wipe_initiated.replace(/&/g,"%amp%amp;").replace(/%amp%/g,"&"),
                    item.wipe_date,
                ] ).draw();
            });
        }
    });
}