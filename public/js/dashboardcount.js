$(document).ready(function() {
    $('.navigator').removeClass('active');
    $('#dashboard').addClass('active');
    getDevices();
    getDeviceCount();
    getDeviceStatusCount('unprovisioned');
    getDeviceStatusCount('Provisioned');
    getChart();
    getModelOSChart();
    getAllProfileList();
    getDownDevices();
    $('.fromDate').datepicker({
        format: 'dd-mm-yyyy'
    });
    $('.toDate').datepicker({
            format: 'dd-mm-yyyy'
    });

    $('#FilterDevice').click(function(){
        if($('.fromDate').val() == '' && $('.toDate').val() != ''){
            $('#filter_error').html('Please enter valid dates');
            $('#filter_error').show();
            return false;
        }else if($('.toDate').val() == '' && $('.fromDate').val() != ''){
            $('#filter_error').html('Please enter valid dates');
            $('#filter_error').show();
            return false;
        }else if($('.fromDate').val() == '' && $('.toDate').val() == '' && $('#addProfile').val() == ''){
            $('#filter_error').html('Please enter any filter');
            $('#filter_error').show();
            return false;
        }else{
            getDeviceFilter();
            getDownDevices();
            $('#filter_error').html('');
            $('#filter_error').hide();
        }

    });
    jQuery(function($) {
        // Asynchronously Load the map API
        var script = document.createElement('script');
        script.src = "//maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyDCfqQtLcP_YBxHw4Q-wWo8j_zGV2QOrHw&callback=initialize";
        document.body.appendChild(script);
    });
});

function getChart(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    $.ajax({
        type: 'POST',
        url: '/dashboardChart',
        data: {_token: CSRF_TOKEN},
        dataType: "json",
        cache: false,

        success: function (response) {
            var morrisLineData = [];
            $.each(response.month, function(key, val){
               morrisLineData.push({y:key,item1:val});
             });
            Morris.Area({
                resize: true,
                element: 'line-chart',
                data: morrisLineData,
                xkey: 'y',
                ykeys: ['item1'],
                labels: ['Count'],
                parseTime: false
            });
        }
    });
}

function getModelOSChart(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/dashboardOsModelChart',
        data: {_token: CSRF_TOKEN},
        dataType: "json",
        cache: false,

        success: function (response) {
            var morrisModelData = [];
            var morrisOSData = [];
            $.each(response.model, function(key, val){
                morrisModelData.push({label:key,value:val});
            });
            Morris.Donut({
                element: 'device-model-chart',
                resize: true,
                colors: ["#3c8dbc", "#f56954", "#00a65a"],
                data: morrisModelData,
                hideHover: 'auto'
            });

            $.each(response.os, function(key, val){
                morrisOSData.push({label:key,value:val});
            });
            Morris.Donut({
                element: 'device-os-chart',
                resize: true,
                colors: ["#3c8dbc", "#f56954", "#00a65a"],
                data: morrisOSData,
                hideHover: 'auto'
            });
        }
    });
}

function getDevices() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/dashboardlist',
        data: {_token: CSRF_TOKEN},
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var buttonHTML = '';
            var counter = 0;
            var username ='';
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                if(item.provision_status == 'Provisioned'){
                    var className = 'class="label label-success"';
                }else{
                    var className = 'class="label label-danger"';
                }
                counter ++;
                trHTML = '<tr><td>'+counter+'</td><td>'+item.device_identifier+'</td><td>'+item.device_serial_number+'</td><td><span '+className+'>'+item.provision_status+'</span></td><td></td></tr>';

                if(counter <= 10){
                    $('#tableconfig').append(trHTML);
                }
                if(counter > 10){
                    $('#viewMore').removeClass('hidden');
                }

            });

        }
    });
}

function getDeviceCount() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/dashboardlistcount',
        data: {_token: CSRF_TOKEN},
        dataType: "json",
        cache: false,
        success: function (response) {
          $('#totalstat').html(response);
        }
    });
}

function getDeviceStatusCount(provisionStatus) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/devicestatuscount',
        data: {_token: CSRF_TOKEN, provisionStatus: provisionStatus},
        dataType: "json",
        cache: false,
        success: function (response) {

            if (provisionStatus == 'unprovisioned') {
                $('#offlinestat').html(response);
            } else {
                $('#onlinestat').html(response);
            }
        }
    });
}

function initialize() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/dashboardmappoints',
        data: {_token: CSRF_TOKEN},
        dataType: "json",
        cache: false,
        success: function (response) {
            var map;
            var bounds = new google.maps.LatLngBounds();
            var mapOptions = {
                mapTypeId: 'roadmap'
            };

            // Display a map on the page
            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
            map.setTilt(45);
            var markers = response;
            // Loop through our array of markers & place each one on the map
            for (i = 0; i < markers.length; i++) {
                //alert(JSON.stringify(markers[i], null, 4));
                var array = markers[i]['device_location'].split(',');
                var m1, m2;
                $.each(array, function (i) {
                    m1 = array[0];
                    m2 = array[1];
                });
                if (m1 != '' && m2 != '') {
                    var position = new google.maps.LatLng(m1, m2);
                    bounds.extend(position);
                    marker = new google.maps.Marker({
                        position: position,
                        map: map,
                        title: markers[i]['device_identifier']
                    });


                    // Automatically center the map fitting all markers on the screen
                    map.fitBounds(bounds);
                }
            }

            // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
            var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
                this.setZoom(14);
                google.maps.event.removeListener(boundsListener);
            });
        }
    });
}

function getAllProfileList() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/profilelist',
        data: {_token: CSRF_TOKEN},
        dataType: "json",
        cache: false,
        success: function (response) {
            var trHTML = '';
            var counter = 0;
            var username = '';
            $.each(JSON.parse(JSON.stringify(response)), function (i, item) {
                $('#addProfile').append($('<option>', {
                    value: item.id,
                    text : item.profile_name
                }));
            });

        }
    });
}

function getDeviceFilter() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/dashboardfilterlist',
        data: {_token: CSRF_TOKEN, profileId: $('#addProfile').val(), fromDate: $('.fromDate').val(), toDate: $('.toDate').val()},
        dataType: "json",
        cache: false,
        success: function (response) {
            $('#tableFilterBody').empty();
            var trHTML = '';
            var buttonHTML = '';
            var counter = 0;
            var username ='';
            $.each(JSON.parse(JSON.stringify(response.deviceTable)), function (i, item) {
                if(item.provision_status == 'Provisioned'){
                    var className = 'class="label label-success"';
                }else{
                    var className = 'class="label label-danger"';
                }
                counter ++;
                trHTML = '<tr><td>'+counter+'</td><td>'+item.device_identifier+'</td><td>'+item.device_serial_number+'</td><td><span '+className+'>'+item.provision_status+'</span></td><td></td></tr>';
                if(counter <= 10){
                    $('#tableconfig').append(trHTML);
                }
                if(counter > 10){
                    $('#viewMore').removeClass('hidden');
                }
            });
            $('#offlinestat').html(response.Unprovisioned);
            $('#onlinestat').html(response.Provisioned);
            $('#totalstat').html(response.dashboardCount);

           //Display os and model chart
            var morrisModelData = [];
            var morrisOSData = [];
            $('#device-model-chart').html('');
            $('#device-os-chart').html('');
            if(response.os != '') {
                $.each(response.model, function (key, val) {
                    morrisModelData.push({label: key, value: val});
                });
                Morris.Donut({
                    element: 'device-model-chart',
                    resize: true,
                    colors: ["#3c8dbc", "#f56954", "#00a65a"],
                    data: morrisModelData,
                    hideHover: 'auto'
                });
            }else{
                $('#device-model-chart').html('<p class="blue">N/A</p><p class="blue-info">No information available</p><p class="bottom-info">This module returned no result for the period</p>');
            }
            if(response.os != ''){
                $.each(response.os, function(key, val){
                    morrisOSData.push({label:key,value:val});
                });
                Morris.Donut({
                    element: 'device-os-chart',
                    resize: true,
                    colors: ["#3c8dbc", "#f56954", "#00a65a"],
                    data: morrisOSData,
                    hideHover: 'auto'
                });
            }else{
                $('#device-os-chart').html('<p class="blue">N/A</p><p class="blue-info">No information available</p><p class="bottom-info">This module returned no result for the period</p>');
            }

        }
    });

    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    $.ajax({
        type: 'POST',
        url: '/dashboardfilterchart',
        data: {_token: CSRF_TOKEN, profileId: $('#addProfile').val(), fromDate: $('.fromDate').val(), toDate: $('.toDate').val()},
        dataType: "json",
        cache: false,

        success: function (response) {
            $('#line-chart').html('');
            var morrisLineData = [];
            if (response.month != '') {
                $.each(response.month, function (key, val) {
                    morrisLineData.push({y: key, item1: val});
                });
                Morris.Area({
                    resize: true,
                    element: 'line-chart',
                    data: morrisLineData,
                    xkey: 'y',
                    ykeys: ['item1'],
                    labels: ['Count'],
                    parseTime: false
                });
            }else{
                $('#line-chart').html('<p class="blue">N/A</p><p class="blue-info">No information available</p><p class="bottom-info">This module returned no result for the period</p>');
            }
        }

    });


    $.ajax({
        type: 'POST',
        url: '/dashboardfiltermappoints',
        data: {_token: CSRF_TOKEN, profileId: $('#addProfile').val(), fromDate: $('.fromDate').val(), toDate: $('.toDate').val()},
        dataType: "json",
        cache: false,
        success: function (response) {
            $('#map_canvas').html('');
            var map;
            var bounds = new google.maps.LatLngBounds();
            var mapOptions = {
                mapTypeId: 'roadmap'
            };

            // Display a map on the page
            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
            map.setTilt(45);
            var markers = response;
            // Loop through our array of markers & place each one on the map
            for (i = 0; i < markers.length; i++) {
                //alert(JSON.stringify(markers[i], null, 4));
                var array = markers[i]['device_location'].split(',');
                var m1, m2;
                $.each(array, function (i) {
                    m1 = array[0];
                    m2 = array[1];
                });
                if (m1 != '' && m2 != '') {
                    var position = new google.maps.LatLng(m1, m2);
                    bounds.extend(position);
                    marker = new google.maps.Marker({
                        position: position,
                        map: map,
                        title: markers[i]['device_identifier']
                    });


                    // Automatically center the map fitting all markers on the screen
                    map.fitBounds(bounds);
                }
            }

            // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
            var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
                this.setZoom(14);
                google.maps.event.removeListener(boundsListener);
            });
        }
    });
}

function getDownDevices() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: '/downdevices',
        data: {_token: CSRF_TOKEN, profileId: $('#addProfile').val(), fromDate: $('.fromDate').val(), toDate: $('.toDate').val()},
        dataType: "json",
        cache: false,

        success: function (response) {
            var morrisStatusData = [];
            $('#device-status-chart').html('');
            if(response.up != 0 || response.down != 0) {
                Morris.Donut({
                    element: 'device-status-chart',
                    resize: true,
                    colors: ["#36bf60", "#ae061b"],
                    data: [
                            {label: "Up", value: response.up},
                            {label: "Down", value: response.down}
                    ],
                    hideHover: 'auto'
                });
            }else{
                $('#device-status-chart').html('<p class="blue">N/A</p><p class="blue-info">No information available</p><p class="bottom-info">This module returned no result for the period</p>');
            }
        }

    });
}